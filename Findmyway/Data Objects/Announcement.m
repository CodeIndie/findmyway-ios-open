//
//  Announcement.m
//  Findmyway
//
//  Created by Bilo on 4/13/15.
//

#import "Announcement.h"

@implementation Announcement

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self)
    {
        self.announcementType = [dictionary objectForKey:@"announcementType"];
        self.operatorName = [dictionary objectForKey:@"operator"];
        self.descriptionText = [dictionary objectForKey:@"description"];
        self.startDate = [dictionary objectForKey:@"announcementType"];
        self.endDate = [dictionary objectForKey:@"announcementType"];
        NSDictionary *pointDictionary = [dictionary objectForKey:@"point"];

        if(![pointDictionary isEqual:(id)[NSNull null]])
        {
            self.point = [[Location alloc] initWithDictionary:pointDictionary];
        }
        
        NSArray *modesArray = [dictionary objectForKey:@"modes"];
        self.modes = [[NSArray alloc] initWithArray:modesArray];
    }
    return self;
}

@end
