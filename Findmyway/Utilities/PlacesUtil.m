//
//  PlacesUtil.m
//  Findmyway
//
//  Created by Bilo on 3/18/15.
//

#import "PlacesUtil.h"

@implementation PlacesUtil

+(void)storeLocation:(SearchItem*)searchItem
{
    NSMutableArray *array = (NSMutableArray*)[[DefaultsUtil instance] GetArrayForKey:@"Favourites&Recents"];
    for(SearchItem *item in array)
    {
        if([item isEqualTo:searchItem])//[[item.location gpsCoordinates] isEqualToString:[searchItem.location gpsCoordinates]])
        {
            NSLog(@"Item already stored");
            return;
        }
    }
    
    NSLog(@"Storing location: %@", searchItem.location.name);
    if(array.count == 0)
    {
        array = [[NSMutableArray alloc] init];
    }
    [array addObject:searchItem];
    [[DefaultsUtil instance] SetArray:array ForKey:@"Favourites&Recents"];
}

+(NSArray*)getLocations
{
    NSArray *array = [[DefaultsUtil instance] GetArrayForKey:@"Favourites&Recents"];
    [[array reverseObjectEnumerator] allObjects];
    return array;
}

+(void)setLocations:(NSArray*)locations
{
    [[DefaultsUtil instance] SetArray:locations ForKey:@"Favourites&Recents"];
}
@end
