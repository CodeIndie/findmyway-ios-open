//
//  FmwJourneyScreen.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/09.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIView.h"
#import "StagePage.h"
#import "AppViewController.h"
#import "XUIShape.h"
#import "XUIEffect.h"

@interface FmwJourneyScreen : XUIView<UIScrollViewDelegate>
{
    float pageWidth;
    float pageHeight;
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    NSMutableArray *stagePages;
    UIButton *mapButton;
    UIButton *toggleSizeButton;
    XUIShape *positionIndicator;
    XUIEffect *pulseEffect;
}

-(void)minimiseJourney;
-(void)showMap;

@end
