//
//  SearchItem.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/08.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "SearchItem.h"

@implementation SearchItem

-(id)initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if(self)
    {
        NSDictionary *pointDictionary = [dictionary objectForKey:@"point"];
        self.name = [dictionary objectForKey:@"name"];
        self.address = [dictionary objectForKey:@"address"];
        self.location = [[Location alloc] initWithDictionary:pointDictionary];
        self.location.address = [dictionary objectForKey:@"address"];
        self.location.name = [dictionary objectForKey:@"name"];
        
        if(self.location.address == nil)
        {
            self.location.address = [dictionary objectForKey:@"description"];
        }
        
        self.descriptionText = [dictionary objectForKey:@"description"];
        //self.distance = [[dictionary objectForKey:@"distance"] intValue];
        self.resourceId = [dictionary objectForKey:@"resourceId"];
        
        NSDictionary *categoryArray = [dictionary objectForKey:@"categories"];
        self.categories = [[NSMutableArray alloc] init];
        for(NSString *category in categoryArray)
        {
            [self.categories addObject:category];
        }
    }
    return self;
}

-(id)initWithSearchItem:(SearchItem *)searchItem
{
    self = [super init];
    if(self)
    {
        self.descriptionText = searchItem.descriptionText;
        self.distance = searchItem.distance;
        self.resourceId = searchItem.resourceId;
        self.name = searchItem.name;
        self.address = searchItem.address;
        self.location = [[Location alloc] initWithLocation:searchItem.location];
        self.location.name = self.name;
        self.location.address = self.address;
        if(self.address == nil)
        {
            self.address = self.descriptionText;
            self.location.address = self.descriptionText;
        }
        self.categories = [[NSMutableArray alloc] init];
        for(NSString *category in searchItem.categories)
        {
            [self.categories addObject:category];
        }
    }
    return self;
}

-(id)initWithLocation:(Location*)location
{
    self = [super init];
    if(self)
    {
        self.name = location.name;
        self.distance = -1;
        self.resourceId = nil;
        self.address = location.address;
        self.location = [[Location alloc] initWithLocation:location];
        self.location.name = location.name;
        self.location.address = location.address;
        if(self.address == nil)
        {
            self.address = self.descriptionText;
            self.location.address = self.descriptionText;
        }
        self.categories = nil;
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if(self)
    {
        self.name = [aDecoder decodeObjectForKey:@"name"];
        self.address = [aDecoder decodeObjectForKey:@"address"];
        self.location = [[Location alloc] initWithLocation:[aDecoder decodeObjectForKey:@"location"]];
        self.location.name = self.name;
        self.location.address = self.address;
        self.descriptionText = [aDecoder decodeObjectForKey:@"descriptionText"];
        if(self.address == nil)
        {
            self.address = self.descriptionText;
            self.location.address = self.descriptionText;
        }
        self.resourceId = [aDecoder decodeObjectForKey:@"resourceId"];
        self.isFavourite = [aDecoder decodeBoolForKey:@"isFavourite"];
        self.distance = [aDecoder decodeIntForKey:@"distance"];
        self.categories = [aDecoder decodeObjectForKey:@"categories"];
    }
    return self;
}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.location forKey:@"location"];
    [aCoder encodeObject:self.name forKey:@"name"];
    [aCoder encodeObject:self.address forKey:@"address"];
    [aCoder encodeObject:self.descriptionText forKey:@"descriptionText"];
    [aCoder encodeObject:self.resourceId forKey:@"resourceId"];
    [aCoder encodeBool:self.isFavourite forKey:@"isFavourite"];
    [aCoder encodeInt:self.distance forKey:@"distance"];
    [aCoder encodeObject:self.location forKey:@"location"];
    [aCoder encodeObject:self.categories forKey:@"categories"];
}

-(BOOL)isEqualTo:(SearchItem*)searchItem
{
    return [self.location.gpsCoordinates isEqualToString:searchItem.location.gpsCoordinates];
}
@end
