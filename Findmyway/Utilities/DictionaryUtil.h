//
//  DictionaryUtil.h
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//

#import <Foundation/Foundation.h>

@interface DictionaryUtil : NSObject

+(void)printDictionary:(NSDictionary *)dictionary;
+(void)printMutableDictionary:(NSMutableDictionary *)dictionary;
+(NSDictionary *)getChildDictionary:(NSDictionary *)dictionary withKey:(NSString *)key;
+(NSMutableDictionary *)getChildMutableDictionary:(NSDictionary *)dictionary withKey:(NSString *)key;

+(void)printDictionaryKeys:(NSDictionary *)dictionary;
+(void)printMutableDictionaryKeys:(NSMutableDictionary *)dictionary;
+(void)printDictionaryKeyValuePairs:(NSDictionary *)dictionary;
+(void)printMutableDictionaryKeyValuePairs:(NSMutableDictionary *)dictionary;

@end
