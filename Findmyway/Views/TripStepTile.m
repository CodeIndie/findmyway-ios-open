//
//  TripStepTile.m
//  Findmyway
//
//  Created by Bilo on 3/5/15.
//

#import "TripStepTile.h"

@implementation TripStepTile

-(id)initWithFrame:(CGRect)frame stepType:(TripStepType)stepType step:(MultimodalPoint*)step stage:(MultimodalStage*)stage trip:(MultimodalTrip*)operatorName instructions:(NSString*)instructions directionIndex:(int)directionIndex
{
    self = [super initWithFrame:frame];
    if(self)
    {
        appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        layout = appDelegate.layout;
        
        float largeRadius = (layout.buttonSize - 2 * layout.padding)/2;
        float mediumRadius = largeRadius * 0.9;
        float smallRadius = layout.padding/2;
        
        switch(stepType)
        {
            //
            // WALKING
            //
            case WalkingStart:
            {
                [self drawLine:step.colour size:layout.padding offset:layout.buttonSize - layout.padding];
                [self drawCircle:step.colour radius:largeRadius + 4 fill:NO];
                [self assignModeIcon:stage.modeType radius:largeRadius - 2 offset:2 color:stage.colour];
                [self assignMainText:step.name fontSizeOffset:0 color:appDelegate.colour.fmwTextColor];
                [mainText setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
                [self assignTime:step.time fontSizeOffset:-1];
                [self assignToggleButton];
            }
            break;
                
            case WalkingMiddle:
            {
                [self drawLine:step.colour size:layout.buttonSize offset:0];
                [self assignMainText:instructions fontSizeOffset:-2 color:appDelegate.colour.fmwTextColorLight];
                [mainText setNumberOfLines:0];
                [self assignDistance:((RouteStep*)[stage.directions.steps objectAtIndex:directionIndex]).distance fontSizeOffset:-1];
                [self assignToggleButton];
            }
            break;
                
            case WalkingEnd:
            {
                [self drawCircle:step.colour radius:largeRadius fill:NO];
                [self drawLine:step.colour size:layout.padding offset:0];
                [self assignMainText:step.name fontSizeOffset:0 color:appDelegate.colour.fmwTextColor];
                [mainText setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
                [self assignTime:step.time fontSizeOffset:-1];
                [self assignToggleButton];
            }
            break;
                
            case WalkingExpansionButton:
            {
                [self drawLine:step.colour size:layout.buttonSize offset:0];
                [self drawCircle:step.colour radius:mediumRadius fill:NO];
                [self assignMainText:stage.hasDirections ? NSLocalizedString(@"Show walking directions", nil) : NSLocalizedString(@"Get walking directions", nil) fontSizeOffset:-2 color:appDelegate.colour.fmwTextColor];
                [self assignStopCount:@"..." radius:mediumRadius color:step.colour];
                [self assignToggleButton];
            }
            break;
            //
            // PUBLIC TRANSPORT
            //
            case TransportStart:
            {
                [self drawLine:step.colour size:layout.padding offset:layout.buttonSize - layout.padding];
                [self drawCircle:step.colour radius:largeRadius + 4 fill:NO];
                [self assignModeIcon:stage.modeType radius:largeRadius - 2 offset:2 color:stage.colour];
                [self assignMainText:step.name fontSizeOffset:0 color:appDelegate.colour.fmwTextColor];
                [mainText setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
                if(stage.vehicleNumber.length > 0)
                {
                    [self assignSubText:[NSString stringWithFormat:@"%@ %@, %@ R%@", stage.modeName, stage.vehicleNumber, NSLocalizedString(@"Approx. cost: ", nil), stage.cost] fontSizeOffset:-2 color:appDelegate.colour.fmwTextColorLight];
                }
                else
                {
                    [self assignSubText:[NSString stringWithFormat:@"%@, %@ R%@", stage.modeName, NSLocalizedString(@"Approx. cost: ", nil), stage.cost] fontSizeOffset:-2 color:appDelegate.colour.fmwTextColorLight];
                }
                [self assignTime:step.time fontSizeOffset:-1];
                [self assignToggleButton];
            }
            break;
                
            case TransportMiddle:
            {
                [self drawLine:step.colour size:layout.buttonSize offset:0];
                [self drawCircle:step.colour radius:smallRadius fill:NO];
                [self assignMainText:step.name fontSizeOffset:-2 color:appDelegate.colour.fmwTextColor];
                [self assignTime:step.time fontSizeOffset:-1];
                [self assignToggleButton];
            }
            break;
                
            case TransportEnd:
            {
                [self drawLine:step.colour size:layout.padding offset:0];
                [self drawCircle:step.colour radius:largeRadius fill:NO];
                [self assignMainText:step.name fontSizeOffset:0 color:appDelegate.colour.fmwTextColor];
                [mainText setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
                [self assignTime:step.time fontSizeOffset:0];
                [self assignToggleButton];
            }
            break;
                
            case TransportExpansionButton:
            {
                [self drawLine:step.colour size:layout.buttonSize offset:0];
                [self drawCircle:step.colour radius:mediumRadius fill:NO];
                [self assignMainText:NSLocalizedString(@"Show intermittent stops", nil) fontSizeOffset:-2 color:appDelegate.colour.fmwTextColor];
                //[self assignTime:step.time fontSizeOffset:-1];
                [self assignStopCount:[NSString stringWithFormat:@"%d", stage.points.count - 2] radius:mediumRadius color:stage.colour];
                [self assignToggleButton];
            }
            break;
        }
    }
    return self;
}

-(void)assignToggleButton
{
    self.button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, layout.buttonSize)];
    [self setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.button];
}

-(void)assignModeIcon:(NSString*)mode radius:(float)radius offset:(float)offset color:(UIColor*)color
{
    CGPoint origin = self.frame.origin;
    
//    icon = [[UIImageView alloc] initWithFrame:CGRectMake(
//                                                         origin.x + layout.margin + layout.padding + offset + 1,
//                                                         layout.padding * 2 + offset + layout.buttonSize/4,
//                                                         radius * 2,
//                                                         radius * 2)];
    icon = [[UIImageView alloc] initWithFrame:CGRectMake(
                                                         origin.x + layout.padding + offset + 1,
                                                         layout.padding + offset,
                                                         radius * 2,
                                                         radius * 2)];
    [icon setImage:[appDelegate getImageForMode:mode]];
    [icon setTintColor:color];
    [self addSubview:icon];
}

-(void)drawCircle:(UIColor*)color radius:(float)radius fill:(BOOL)fill
{
    CGPoint origin = self.frame.origin;
//    CGPoint circleOrigin = CGPointMake(origin.x + appDelegate.layout.buttonSize*1.5 + 0.5 + appDelegate.layout.padding - radius,
//                                       (appDelegate.layout.buttonSize/2) - radius);
    CGPoint circleOrigin = CGPointMake(origin.x + 0.5 + appDelegate.layout.padding * 2 - radius,
                                       (appDelegate.layout.buttonSize/2) - radius);
    
    circle = [XUIShape circle:circleOrigin size:radius*2 borderWidth:appDelegate.layout.delimiterSize * 2 fillColor:(fill ? color : [UIColor whiteColor]) strokeColor:color];
    [self addSubview:circle];
}

-(void)drawLine:(UIColor*)color size:(float)size offset:(float)offset
{
//    line = [[UIView alloc] initWithFrame:CGRectMake(appDelegate.layout.padding + appDelegate.layout.buttonSize * 1.5, offset, appDelegate.layout.delimiterSize * 2, size)];
    line = [[UIView alloc] initWithFrame:CGRectMake(appDelegate.layout.padding * 2, offset, appDelegate.layout.delimiterSize * 2, size)];
    [line setBackgroundColor:color];
    [self addSubview:line];
}

-(void)assignMainText:(NSString*)text fontSizeOffset:(float)fontSizeOffset color:(UIColor*)color
{
//    mainText = [[UILabel alloc] initWithFrame:CGRectMake(appDelegate.layout.margin + appDelegate.layout.buttonSize * 2,
//                                                         0,
//                                                         appDelegate.layout.screenWidth - appDelegate.layout.buttonSize * 3,
//                                                         appDelegate.layout.buttonSize)];
    mainText = [[UILabel alloc] initWithFrame:CGRectMake(appDelegate.layout.buttonSize * 1.2,
                                                         0,
                                                         appDelegate.layout.screenWidth - appDelegate.layout.buttonSize * 3,
                                                         appDelegate.layout.buttonSize)];
    [mainText setText:text];
    [mainText setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] + fontSizeOffset]];
    [mainText setTextColor:color];
    [self addSubview:mainText];
}

-(void)assignSubText:(NSString*)subTextString fontSizeOffset:(float)fontSizeOffset color:(UIColor*)color
{
//    subText = [[UILabel alloc] initWithFrame:CGRectMake(appDelegate.layout.margin + appDelegate.layout.buttonSize * 2, layout.buttonSize - layout.padding * 1.5, layout.screenWidth - layout.buttonSize * 2, layout.padding * 3)];
    subText = [[UILabel alloc] initWithFrame:CGRectMake(appDelegate.layout.buttonSize * 1.2, layout.buttonSize - layout.padding * 1.5, layout.screenWidth - layout.buttonSize * 2, layout.padding * 3)];
    [subText setText:subTextString];
    [subText setTextColor:color];
    [subText setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] + fontSizeOffset]];
    [subText setNumberOfLines:0];
    [self addSubview:subText];
}

-(void)assignTime:(NSString*)timeText fontSizeOffset:(float)fontSizeOffset
{
//    time = [[UILabel alloc] initWithFrame:CGRectMake(-layout.padding, 0, appDelegate.layout.buttonSize * 1.5, appDelegate.layout.buttonSize)];
    time = [[UILabel alloc] initWithFrame:CGRectMake(layout.screenWidth - 2*layout.buttonSize, 0, appDelegate.layout.buttonSize * 1.5, appDelegate.layout.buttonSize)];
    [time setText:[DateTimeUtil parseDateTime:timeText].timeString];
    [time setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] + fontSizeOffset]];
    [time setTextAlignment:NSTextAlignmentRight];
    [self addSubview:time];
}

-(void)assignDistance:(NSString*)distanceText fontSizeOffset:(float)fontSizeOffset
{
    time = [[UILabel alloc] initWithFrame:CGRectMake(appDelegate.layout.screenWidth - appDelegate.layout.buttonSize * 2, 0, appDelegate.layout.buttonSize * 1.5, appDelegate.layout.buttonSize)];
    [time setText:[DistanceUtil distanceString:[distanceText intValue] metricSystem:YES]];
    [time setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] + fontSizeOffset]];
    [time setTextAlignment:NSTextAlignmentRight];
    [self addSubview:time];
}

-(void)assignStopCount:(NSString*)text radius:(float)radius color:(UIColor*)color
{
    CGPoint origin = self.frame.origin;
    //    CGPoint circleOrigin = CGPointMake(origin.x + appDelegate.layout.buttonSize*1.5 + 0.5 + appDelegate.layout.padding - radius,
    //                                       (appDelegate.layout.buttonSize/2) - radius);
    CGPoint circleOrigin = CGPointMake(origin.x + 0.5 + appDelegate.layout.padding * 2 - radius,
                                       (appDelegate.layout.buttonSize/2) - radius);
    
    stopCount = [[UILabel alloc] initWithFrame:CGRectMake(circleOrigin.x, circleOrigin.y, radius * 2, radius * 2)];
    [stopCount setText:text];
    [stopCount setTextColor:color];
    [stopCount setTextAlignment:NSTextAlignmentCenter];
    [stopCount setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 2]];
    [self addSubview:stopCount];
}
@end
