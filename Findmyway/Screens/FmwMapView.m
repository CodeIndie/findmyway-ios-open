//
//  FmwMapView.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/12.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "FmwMapView.h"

@implementation FmwMapView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(device.screenWidth * 1.5, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth/2, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    [super initView:name inController:parentViewController];
    [self initMap];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    [self setBackgroundColor:colour.fmwBackgroundColor];
    float toolbarYPos = self.bounds.size.height - layout.buttonSize;
    
    // TOP BAR
    topbarBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.buttonSize)];
    [topbarBackground setBackgroundColor:colour.fmwHeader];
    
    topbarLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, layout.delimiterSize)];
    [topbarLine setBackgroundColor:colour.fmwTextColor];
    
    float topButtonWidth = layout.buttonSize * 1.5;
    topLeftRect = CGRectMake(layout.padding, 0, topButtonWidth, layout.buttonSize);
    topRightRect = CGRectMake(layout.screenWidth - topButtonWidth - layout.padding, 0, layout.buttonSize * 2, layout.buttonSize);
    
    topbarLabel = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding + topButtonWidth, 0, layout.paddedWidth - 2 * topButtonWidth, layout.buttonSize)];
    [topbarLabel setTextColor:appDelegate.colour.fmwTextColor];
    [topbarLabel setTextAlignment:NSTextAlignmentCenter];
    [topbarLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
    

    backToSearchButton = [XUIButton backButtonWithTitle:NSLocalizedString(@"Search", nil) color:colour.fmwActiveColor layout:layout];
    [backToSearchButton addTarget:self action:@selector(backToSearch) forControlEvents:UIControlEventTouchUpInside];
    
    backToTripsButton = [XUIButton backButtonWithTitle:NSLocalizedString(@"Trips", nil) color:colour.fmwActiveColor layout:layout];
    [backToTripsButton addTarget:self action:@selector(backToTrips) forControlEvents:UIControlEventTouchUpInside];
    
    backToOverviewButton = [XUIButton backButtonWithTitle:NSLocalizedString(@"Back", nil) color:colour.fmwActiveColor layout:layout];
    [backToOverviewButton addTarget:self action:@selector(backToOverview) forControlEvents:UIControlEventTouchUpInside];
    
    backToHomeButton = [XUIButton backButtonWithTitle:NSLocalizedString(@"Back", nil) color:colour.fmwActiveColor layout:layout];
    [backToHomeButton addTarget:self action:@selector(backToHomeScreen) forControlEvents:UIControlEventTouchUpInside];
    
    cancelButton = [XUIButton initWithFrame:topLeftRect
                                      title:[NSString stringWithFormat:@" %@",NSLocalizedString(@"Cancel", nil)]
                                   fontSize:[UIFont systemFontSize] + 1
                                       bold:YES
                                 titleColor:colour.fmwActiveColor
                            backgroundColor:colour.clear
                        horizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [cancelButton addTarget:self action:@selector(backToSearch) forControlEvents:UIControlEventTouchUpInside];
    
    submitButton = [XUIButton initWithFrame:topRightRect
                                      title:NSLocalizedString(@"Submit", nil)
                                   fontSize:[UIFont systemFontSize] + 1
                                       bold:YES
                                 titleColor:colour.fmwActiveColor
                            backgroundColor:colour.clear
                        horizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [submitButton addTarget:self action:@selector(submitSelection) forControlEvents:UIControlEventTouchUpInside];
    
    // BOTTOM BAR
    toolbarBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, toolbarYPos, layout.screenWidth, layout.buttonSize)];
    [toolbarBackground setBackgroundColor:colour.fmwHeader];
    toolbarLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, toolbarYPos, layout.screenWidth, layout.delimiterSize)];
    [toolbarLine setBackgroundColor:colour.fmwTextColor];
    
    // BOTTOM RECTS
    bottomLeftRect = CGRectMake(0, toolbarYPos, layout.buttonSize, layout.buttonSize);
    bottomRightRect = CGRectMake(layout.screenWidth - layout.buttonSize, toolbarYPos, layout.buttonSize, layout.buttonSize);
    bottomMiddleRect = CGRectMake(layout.buttonSize, toolbarYPos, layout.screenWidth - 2 * layout.buttonSize, layout.buttonSize);
    
    // BOTTOM ITEMS
    showLocationButton = [[UIButton alloc] initWithFrame:bottomLeftRect];
    [showLocationButton setImage:[ImageUtil getImage:@"iconGPS.png"] forState:UIControlStateNormal];
    [showLocationButton.imageView setTintColor:colour.fmwActiveColor];
    [showLocationButton addTarget:self action:@selector(showUserLocation) forControlEvents:UIControlEventTouchUpInside];
    
    bottomBarLabel = [XUILabel initWithFrame:CGRectMake(layout.buttonSize, toolbarYPos, layout.screenWidth - 2 * layout.buttonSize, layout.buttonSize)
                                        text:NSLocalizedString(@"Long press to drop pin", nil)
                                    fontSize:[UIFont systemFontSize] + 1
                                        bold:NO
                                   textColor:colour.fmwTextColorLight
                             backgroundColor:colour.clear
                               textAlignment:NSTextAlignmentCenter];
    
    beginJourneyButton = [[UIButton alloc] initWithFrame:bottomMiddleRect];
    [beginJourneyButton setTitle:NSLocalizedString(@"Begin Journey", nil) forState:UIControlStateNormal];
    [beginJourneyButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    
    infoButton = [UIButton buttonWithType:UIButtonTypeInfoDark];
    infoButton.frame = bottomRightRect;
    [infoButton addTarget:self action:@selector(showMapTypePanel) forControlEvents:UIControlEventTouchUpInside];
}

-(void)addUIToView
{
    [self addSubview:self.mapView];
    
    // top bar
    [self addSubview:topbarBackground];
    [self addSubview:topbarLine];
    [self addSubview:topbarLabel];
    
    // bottom bar
    [self addSubview:toolbarBackground];
    [self addSubview:toolbarLine];
    [self addSubview:bottomBarLabel];
    
    // buttons
    [self addSubview:showLocationButton];
    [self addSubview:infoButton];
}

-(void)animateIn
{
    if(isAnimating)
    {
        return;
    }
    isAnimating = YES;
    
    [super animateIn];
    [self addNotificationListeners];
    [self presentMapFirstTime];
    
    [parentView.view addSubview:self];
    self.alpha = 0;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.alpha = 1;
     }
     completion:^(BOOL finished)
     {
         isAnimating = NO;
     }];
}

-(void)animateOut
{
    if(isAnimating)
    {
        return;
    }
    isAnimating = YES;
    
    [super animateOut];
    [self removeNotificationListeners];
    
    self.alpha = 1;
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
     animations:^
     {
         self.alpha = 0;
     }
     completion:^(BOOL finished){
         locationSelected = NO;
         isAnimating = NO;
         [self removeFromSuperview];
     }];
}

-(void)addNotificationListeners
{
    [super addNotificationListeners];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(drawRoute) name:[[Notifications instance] MapReloadRoute] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setAddress) name:[[Notifications instance] GETAddressSuccess] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addressFailedLoading) name:[[Notifications instance] GETAddressFail] object:nil];
}

-(void)removeNotificationListeners
{
    [super removeNotificationListeners];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[[Notifications instance] GETAddressSuccess] object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[[Notifications instance] GETAddressFail] object:nil];
}

// MAP FUNCTIONS

-(void)initMap
{
    self.mapView = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height - layout.buttonSize)];
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    self.mapView.showsBuildings = NO;
    self.mapView.showsPointsOfInterest = YES;
    self.mapView.pitchEnabled = YES;
    
    StartEndPinIdentifier = @"StartEndPinIdentifier";
    [self addLongPressGestureRecognizer];
    showingMapFirstTime = YES;
}

-(void)presentMapFirstTime
{
    if(showingMapFirstTime)
    {
        showingMapFirstTime = NO;
        MKMapCamera *mapCamera;
        if([appDelegate.locationUtil isLocationAvailable])
        {
            CLLocationCoordinate2D ground = appDelegate.locationUtil.userLocation.coordinate;
            
            mapCamera = [MKMapCamera cameraLookingAtCenterCoordinate:ground
                                                   fromEyeCoordinate:ground
                                                         eyeAltitude:5000];
        }
        else
        {
            //CLLocationCoordinate2DMake(appDelegate.locationUtil.userLocation.latitude, appDelegate.locationUtil.userLocation.longitude);
            //CLLocationCoordinate2D eye = CLLocationCoordinate2DMake(appDelegate.locationUtil.capeTown.latitude + 0.001, appDelegate.locationUtil.capeTown.longitude + 0.001);
            //CLLocationCoordinate2D ground = appDelegate.locationUtil.capeTown.coordinate;
            CLLocationCoordinate2D ground = appDelegate.locationUtil.southAfrica.coordinate;
            
            mapCamera = [MKMapCamera cameraLookingAtCenterCoordinate:ground
                                                   fromEyeCoordinate:ground
                                                         eyeAltitude:5000000];
        }
        self.mapView.camera = mapCamera;
    }
}

-(void)setState:(MapState)state
{
    [self XLog:[NSString stringWithFormat:@"%u", state]];
    switch(state)
    {
        case PointPreviewFromHome:
        {
            [self clearTopBar];
            [self addSubview:backToHomeButton];
            [self addSubview:submitButton];
            [self addSubview:topbarLabel];
            [bottomBarLabel setText:@"Long press to drop pin"];
        }break;
            
        case PointPreview:
        {
            [self clearTopBar];
            [self addSubview:backToSearchButton];
            [self addSubview:submitButton];
            [self addSubview:topbarLabel];
            [bottomBarLabel setText:@"Long press to drop pin"];
        }break;
            
        case PointSelection:
        {
            [self clearTopBar];
            [self addSubview:backToSearchButton];
            [self addSubview:topbarLabel];
            [bottomBarLabel setText:@"Long press to drop pin"];
            [topbarLabel setText:@"Find on Map"];
        }break;
            
        case JourneyMode:
        {
            [self clearTopBar];
            [bottomBarLabel setText:@""];
            [self addSubview:backToTripsButton];
            [self drawRoute];
            [self showWholeRoute];
        }break;
            
        case JourneyPreview:
        {
            [self clearTopBar];
            [self addSubview:backToTripsButton];
            //[self addSubview:beginJourneyButton];
            [self drawRoute];
            [self showWholeRoute];
        }break;
            
        case JourneyOverView:
        {
            [self clearTopBar];
            [self addSubview:backToOverviewButton];
            //[self addSubview:beginJourneyButton];
            [self drawRoute];
            [self showWholeRoute];
        }break;
            
        default:
        {
            
        }break;
    }
}

-(void)clearTopBar
{
    [bottomBarLabel setText:@""];
    [submitButton removeFromSuperview];
    
    // LEFT BUTTONS
    [cancelButton removeFromSuperview];
    [backToSearchButton removeFromSuperview];
    [backToTripsButton removeFromSuperview];
    [backToHomeButton removeFromSuperview];
}

-(void)showUserLocation
{
    NSLog(@"Showing User Location");
    if([appDelegate.locationUtil isLocationEnabled])
    {
        [self.mapView setCenterCoordinate:appDelegate.locationUtil.userLocation.coordinate animated:YES];
    }
    else
    {
        [self.mapView setCenterCoordinate:appDelegate.locationUtil.capeTown.coordinate animated:YES];
    }
}

-(void)setSearchItemForPreview:(SearchItem*)item
{
    if(appDelegate.tripQuery.isEditingStart)
    {
        [self.mapView removeAnnotation:selectedStartLocation];
        selectedStartLocation = [[MKPointAnnotation alloc] init];
        selectedStartLocation.coordinate = item.location.coordinate;
        [self.mapView addAnnotation:selectedStartLocation];
        [self.mapView setCenterCoordinate:item.location.coordinate animated:YES];
        [topbarLabel setText:item.name];
    }
    else
    {
        [self.mapView removeAnnotation:selectedEndLocation];
        selectedEndLocation = [[MKPointAnnotation alloc] init];
        selectedEndLocation.coordinate = item.location.coordinate;
        [self.mapView addAnnotation:selectedEndLocation];
        [self.mapView setCenterCoordinate:item.location.coordinate animated:YES];
        [topbarLabel setText:item.name];
    }
    
    appDelegate.apiResponse.location = item.location;
}

// Long Press to Drop Pin
-(void)addLongPressGestureRecognizer
{
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    lpgr.numberOfTouchesRequired = 1;
    lpgr.minimumPressDuration = 0.6; //in seconds
    [self.mapView addGestureRecognizer:lpgr];
}

-(void)handleLongPress:(UIGestureRecognizer*)gestureRecognizer
{
    if(gestureRecognizer.state == UIGestureRecognizerStateBegan && appDelegate.mapConfig.state == PointSelection)
    {
        AudioServicesPlaySystemSound(1352); // use 1352 as argument
        CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
        CLLocationCoordinate2D touchMapCoordinate =
        [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
        
        if(appDelegate.tripQuery.isEditingStart)
        {
            [self.mapView removeAnnotation:selectedStartLocation];
            selectedStartLocation = [[MKPointAnnotation alloc] init];
            selectedStartLocation.coordinate = touchMapCoordinate;
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
            [self.mapView addAnnotation:selectedStartLocation];
            NSLog(@"\nSelected Map Location\nlat:%f\nlon:%f", selectedStartLocation.coordinate.latitude, selectedStartLocation.coordinate.longitude);
        }
        else
        {
            [self.mapView removeAnnotation:selectedEndLocation];
            selectedEndLocation = [[MKPointAnnotation alloc] init];
            selectedEndLocation.coordinate = touchMapCoordinate;
            AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
            [self.mapView addAnnotation:selectedEndLocation];
            NSLog(@"\nSelected Map Location\nlat:%f\nlon:%f",
                  selectedEndLocation.coordinate.latitude,
                  selectedEndLocation.coordinate.longitude);
        }
        [self pointSelected];
        [self reverseGeocodeSelection];
    }
}

-(void)pointSelected
{
    if(!locationSelected)
    {
        locationSelected = YES;
        [backToSearchButton removeFromSuperview];
        [self addSubview:cancelButton];
        [self addSubview:submitButton];
    }
}

-(MKAnnotationView*)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if([annotation isEqual:selectedStartLocation] || [annotation isEqual:selectedEndLocation])
    {
        MKPinAnnotationView *customPinView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
        customPinView.animatesDrop = YES;
        customPinView.canShowCallout = YES;
        customPinView.pinColor = [annotation isEqual:selectedStartLocation] ? MKPinAnnotationColorGreen : MKPinAnnotationColorRed;
        return customPinView;
    }
    return nil;
}

// ROUTE PINS & STOP ANNOTATIONS

-(void)showStartAndEndPins
{
    [self.mapView removeAnnotation:selectedStartLocation];
    selectedStartLocation = [[MKPointAnnotation alloc] init];
    selectedStartLocation.coordinate = ((Location*)appDelegate.tripQuery.selectedTrip.mapRoute.coordinates.firstObject).coordinate;
    [self.mapView addAnnotation:selectedStartLocation];
    
    [self.mapView removeAnnotation:selectedEndLocation];
    selectedEndLocation = [[MKPointAnnotation alloc] init];
    selectedEndLocation.coordinate = ((Location*)appDelegate.tripQuery.selectedTrip.mapRoute.coordinates.lastObject).coordinate;
    [self.mapView addAnnotation:selectedEndLocation];
}

-(void)showStopPins
{
    for(MultimodalStage *stage in appDelegate.tripQuery.selectedTrip.stages)
    {
        for(MultimodalPoint *point in stage.points)
        {
            MKPointAnnotation *stopPoint = [[MKPointAnnotation alloc] init];
            stopPoint.coordinate = point.location.coordinate;
            [self.mapView addAnnotation:stopPoint];
        }
    }
}

-(MKPinAnnotationView*) returnPointView: (CLLocationCoordinate2D) location andTitle: (NSString*) title andColor: (int) color{
    /*Method that acts as a point-generating machine. Takes the parameters of the location, the title, and the color of the
     pin, and it returns a view that holds the pin with those specified details*/
    
    MKPointAnnotation *resultPin = [[MKPointAnnotation alloc] init];
    MKPinAnnotationView *result = [[MKPinAnnotationView alloc] initWithAnnotation:resultPin reuseIdentifier:Nil];
    [resultPin setCoordinate:location];
    resultPin.title = title;
    result.pinColor = color;
    return result;
}

// REVERSE GEOCODE

-(void)reverseGeocodeSelection
{
    [topbarLabel setText:@"... getting address ..."];
    //[self startLoader:CGPointMake(layout.screenWidth/2 - layout.buttonSize/2, layout.buttonSize/2) inView:self];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    
    if(appDelegate.tripQuery.isEditingStart)
    {
        [parameters setValue:[NSString stringWithFormat:@"%f", selectedStartLocation.coordinate.latitude] forKey:@"latitude"];
        [parameters setValue:[NSString stringWithFormat:@"%f", selectedStartLocation.coordinate.longitude] forKey:@"longitude"];
    }
    else
    {
        [parameters setValue:[NSString stringWithFormat:@"%f", selectedEndLocation.coordinate.latitude] forKey:@"latitude"];
        [parameters setValue:[NSString stringWithFormat:@"%f", selectedEndLocation.coordinate.longitude] forKey:@"longitude"];
    }
    
    // API CALL
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:
                     [appDelegate.apiUtil urlForRequest:ReverseGeocode callType:GET parameters:parameters]];
    [submitButton removeFromSuperview];
    
    [connection executeRequestOnSuccess:^(NSHTTPURLResponse *response, NSString *bodyString){
         
         NSLog(@"API - ReverseGeocode: %ld", (long)response.statusCode);
         [appDelegate.apiResponse addAddress:[JSONParser convertJSONToDictionary:bodyString]];
        if(![appDelegate.apiResponse.location.address isEqualToString:@""] &&
           appDelegate.apiResponse.location.address != nil)
        {
            [self addSubview:submitButton];
            [self setAddress];
        }
     }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
    {
        NSLog(@"API CONNECTION FAILURE - ReverseGeocode: %@", error);
        [self addressFailedLoading];
     }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite){
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
     }];
}

-(void)setAddress
{
    //[self stopLoader];
    [appDelegate.tripQuery setPoint:appDelegate.apiResponse.location];
    [topbarLabel setText:appDelegate.apiResponse.location.name];
}

-(void)addressFailedLoading
{
    [self stopLoader];
}

-(void)submitSelection
{
    [appDelegate.tripQuery setPoint:appDelegate.apiResponse.location];
    if(appDelegate.searchConfig.state == HomeScreen)
    {
        [self backToHomeScreen];
    }
    else if(appDelegate.searchConfig.state == TripsScreen)
    {
        appDelegate.tripQuery.refreshingRouteOptions = YES;
        SearchItem *item = [[SearchItem alloc] initWithLocation:appDelegate.apiResponse.location];
        [self updateFavouritesAndRecents:item];
        [self backToTrips];
    }
}

-(void)updateFavouritesAndRecents:(SearchItem*)selectedItem
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSMutableArray *recentPlaces = [[NSMutableArray alloc] init];
    NSArray *recents = [PlacesUtil getLocations];
    for(SearchItem *item in recents)
    {
        [recentPlaces addObject:item];
    }
    
    //[[recentPlaces reverseObjectEnumerator] allObjects];
    [array addObject:selectedItem];
    for(SearchItem *item in recentPlaces)
    {
        if(![item isEqualTo:selectedItem])
        {
            [array addObject:item ];
        }
    }
    //[[array reverseObjectEnumerator] allObjects];
    
    [PlacesUtil setLocations:(NSArray*)array];
}

-(void)backToSearch
{
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).searchScreen];
}

-(void)backToTrips
{
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwRouteOptions];
}

-(void)backToOverview
{
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).journeyOverviewScreen];
}

-(void)backToHomeScreen
{
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwScreen];
}

// DRAWING

-(void)drawRoute
{
    if(appDelegate.tripQuery.selectedTrip.hasMapRoute)
    {
        [self drawPublicRoute:appDelegate.tripQuery.selectedTrip.mapRoute];
    }
    [self showStartAndEndPins];
    [self showStopPins];
}

-(void)showWholeRoute
{
    MKMapRect zoomRect = MKMapRectNull;
    MKMapPoint topLeftPoint = MKMapPointForCoordinate(appDelegate.tripQuery.selectedTrip.mapRoute.boundingBoxTopLeft.coordinate);
    MKMapPoint bottomRightPoint = MKMapPointForCoordinate(appDelegate.tripQuery.selectedTrip.mapRoute.boundingBoxBottomRight.coordinate);
    
    MKMapRect MapTopLeftRect = MKMapRectMake(topLeftPoint.x, topLeftPoint.y, 2, 2);
    MKMapRect MapBottomRightRect = MKMapRectMake(bottomRightPoint.x, bottomRightPoint.y, 2, 2);
    
    zoomRect = MKMapRectUnion(zoomRect, MapTopLeftRect);
    zoomRect = MKMapRectUnion(zoomRect, MapBottomRightRect);
    
    [self.mapView setVisibleMapRect:zoomRect animated:YES];
}

-(void)drawPublicRoute:(PublicMapRoute*)selectedRoute
{
    //[self.mapView removeAnnotation:selectedStartLocation];
    if(selectedRoute != nil)
    {
        [self.mapView removeOverlays:polylines];
        polylines = [[NSMutableArray alloc] init];
        
        [self addRoute:selectedRoute.coordinates color:[UIColor whiteColor] lineWidth:8];
        for(PublicMapRouteSegment *routeSegment in selectedRoute.segments)
        {
            [self addRoute:routeSegment.coordinates color:routeSegment.color lineWidth:6];
        }
    }
}

-(void)addRoute:(NSMutableArray*)coordinates color:(UIColor*)color lineWidth:(float)lineWidth
{
    int numberOfPoints = (int)coordinates.count;
    CLLocationCoordinate2D *points = malloc(numberOfPoints * sizeof(CLLocationCoordinate2D));
    
    for(int i = 0 ; i <  numberOfPoints; i++)
    {
        CLLocationDegrees lat = ((Location*)[coordinates objectAtIndex:i]).latitude;
        CLLocationDegrees lon = ((Location*)[coordinates objectAtIndex:i]).longitude;
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, lon);
        
        points[i] = coordinate;
    }
    
    @try
    {
        MulticolorMapLine *routeLine = [MulticolorMapLine polylineWithCoordinates:points count:[coordinates count] - 1];
        routeLine.color = color;
        routeLine.width = lineWidth;
        [polylines addObject:routeLine];
        
        [self.mapView addOverlay:routeLine];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Invalid Argument Exception ...");
    }
    @finally
    {
        //NSLog(@"Continuing program execution...");
    }
}

-(MKOverlayRenderer*)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay
{
    if(true)//[overlay isKindOfClass:[MKPolyline class]])
    {
        MKPolyline *routeLine = overlay;
        MKPolylineRenderer *routeRenderer = [[MKPolylineRenderer alloc] initWithPolyline:routeLine];
        routeRenderer.strokeColor = colour.fmwActiveColor;
        
        for(int i = 0 ; i < [polylines count] ; i++)
        {
            MulticolorMapLine *mapLine = (MulticolorMapLine*)[polylines objectAtIndex:i];
            
            if([mapLine isEqual:overlay])
            {
                routeRenderer.strokeColor = mapLine.color;
                routeRenderer.lineWidth = mapLine.width;
            }
        }
        return routeRenderer;
    }
    else
    {
        NSLog(@"Could not create polyline renderer in \"TctMapViewController.m\" ...");
        return nil;
    }
}

// ACTIONS

-(void)beginJourney
{
    
}

-(void)showMapTypePanel
{
    if(mapTypeView == nil)
    {
        mapTypeView = [[MapTypeView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [mapTypeView initView:@"Map Type View" inController:parentView];
        
        mapTypeView.standardMapButton.tag = StandardMapView;
        [mapTypeView.standardMapButton addTarget:self action:@selector(setMapViewType:) forControlEvents:UIControlEventTouchUpInside];
        
        mapTypeView.satelliteMapButton.tag = SatelliteMapView;
        [mapTypeView.satelliteMapButton addTarget:self action:@selector(setMapViewType:) forControlEvents:UIControlEventTouchUpInside];

        mapTypeView.hybridMapButton.tag = HybridMapView;
        [mapTypeView.hybridMapButton addTarget:self action:@selector(setMapViewType:) forControlEvents:UIControlEventTouchUpInside];
        
        [mapTypeView.backgroundButton addTarget:self action:@selector(hideMapTypePanel) forControlEvents:UIControlEventTouchUpInside];
    }
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController showView:mapTypeView];
}

-(void)hideMapTypePanel
{
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController hideView:mapTypeView];
}

-(IBAction)setMapViewType:(id)sender
{
    NSInteger tag = ((UIButton*)sender).tag;
    switch(tag)
    {
        case StandardMapView:
        {
            self.mapView.mapType = MKMapTypeStandard;
        }break;
        
        case SatelliteMapView:
        {
            self.mapView.mapType = MKMapTypeSatellite;
        }break;
        
        case HybridMapView:
        {
            self.mapView.mapType = MKMapTypeHybrid;
        }break;
    }
    [self hideMapTypePanel];
}
@end
