//
//  SearchScreen.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/02.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIView.h"
#import "SearchListItem.h"
#import "AppViewController.h"
#import "JCDHTTPConnection.h"
#import "PlacesUtil.h"



@interface SearchScreen : XUIView<UITextFieldDelegate>
{
    UIScrollView *scrollView;
    NSMutableArray *recentPlaces;
    NSMutableArray *searchItems;

    UIView *headerBackground;
    UITextField *searchField;
    UIImageView *searchIcon;
    UIButton *backButton;
    UIButton *cancelButton;
    
    UIButton *mapButton;
    UIButton *gpsButton;
    UIImageView *gpsIcon;
    UIImageView *mapIcon;
    UIImageView *buttonBackground;
    UIImageView *verticalLine;
    UIImageView *horizontalLine;
    
    UIImageView *favouritesBackground;
    UIButton *viewFavouritePlaces;
    NSTimer *searchTimer;
    
    BOOL canceledSearch;
    float itemHeight;
}

-(void)useCurrentLocation;
-(void)useLocationOnMap;

-(void)searchFieldListener;
-(void)cancelSearch;
-(void)updateSearchItems;

-(IBAction)highlightItem:(id)sender;
-(IBAction)selectItem:(id)sender;
-(IBAction)showOnMap:(id)sender;
-(void)backbuttonPressed;
@end
