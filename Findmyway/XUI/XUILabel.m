//
//  XUILabel.m
//  Findmyway
//
//  Created by Bilo on 4/15/15.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUILabel.h"

@implementation XUILabel

+(XUILabel*)initWithFrame:(CGRect)frame text:(NSString *)text textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor textAlignment:(NSTextAlignment)textAlignment
{
    XUILabel *label = [[XUILabel alloc] initWithFrame:frame];
    [label setText:text];
    [label setTextColor:textColor];
    [label setTextAlignment:textAlignment];
    [label setBackgroundColor:backgroundColor];
    return label;
}

+(XUILabel*)initWithFrame:(CGRect)frame text:(NSString*)text fontSize:(float)fontSize bold:(BOOL)bold textColor:(UIColor*)textColor backgroundColor:(UIColor*)backgroundColor textAlignment:(NSTextAlignment)textAlignment
{
    XUILabel *label = [[XUILabel alloc] initWithFrame:frame];
    [label setText:text];
    [label setTextColor:textColor];
    [label setTextAlignment:textAlignment];
    [label setBackgroundColor:backgroundColor];
    [label setFont:(bold ? [UIFont boldSystemFontOfSize:fontSize] : [UIFont systemFontOfSize:fontSize])];
    return label;
}
@end
