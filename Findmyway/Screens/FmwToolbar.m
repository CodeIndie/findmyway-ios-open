//
//  FmwToolbar.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "FmwToolbar.h"

@implementation FmwToolbar

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    //float middleWidth = device.screenWidth - 2 * (layout.buttonSize + layout.delimiterSize);

    
    [self setBackgroundColor:colour.fmwHeader];
    UIImageView *blueLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.delimiterSize)];
    [blueLine setBackgroundColor:colour.fmwActiveColor];
    [self addSubview:blueLine];
    
    tripsButton = [[XUIToolbarButton alloc] initWithIndex:0 imageName:@"iconFMW" title:NSLocalizedString(@"Trips", nil)];
    [tripsButton.button addTarget:self action:@selector(showFindMyWay) forControlEvents:UIControlEventTouchUpInside];
    
    myTripsButton = [[XUIToolbarButton alloc] initWithIndex:1 imageName:@"iconHeart" title:NSLocalizedString(@"My Trips", nil)];
    [myTripsButton.button addTarget:self action:@selector(showMyTrips) forControlEvents:UIControlEventTouchUpInside];
    
    searchButton = [[XUIToolbarButton alloc] initWithIndex:2 imageName:@"iconPlaces" title:NSLocalizedString(@"Places", nil)];
    [searchButton.button addTarget:self action:@selector(showPlaces) forControlEvents:UIControlEventTouchUpInside];
    
    alertsButton = [[XUIToolbarButton alloc] initWithIndex:3 imageName:@"iconAlerts" title:NSLocalizedString(@"Alerts", nil)];
    [alertsButton.button addTarget:self action:@selector(showAlerts) forControlEvents:UIControlEventTouchUpInside];
    
    moreButton = [[XUIToolbarButton alloc] initWithIndex:4 imageName:@"iconMenu" title:NSLocalizedString(@"More", nil)];
    [moreButton.button addTarget:self action:@selector(showMainMenu) forControlEvents:UIControlEventTouchUpInside];
    
    [tripsButton setActive:YES];
}

-(void)animateIn
{
    
}

-(void)animateOut
{
    
}

-(void)addUIToView
{
//    [self addSubview:tripsButton];
//    [self addSubview:myTripsButton];
//    [self addSubview:searchButton];
//    [self addSubview:alertsButton];
//    [self addSubview:moreButton];
    
    [self addSubview:tripsButton.label];
    [self addSubview:tripsButton.button];
    
    [self addSubview:myTripsButton.label];
    [self addSubview:myTripsButton.button];
    
    [self addSubview:searchButton.label];
    [self addSubview:searchButton.button];
    
    [self addSubview:alertsButton.label];
    [self addSubview:alertsButton.button];
    
    [self addSubview:moreButton.label];
    [self addSubview:moreButton.button];
}

-(void)deactivateAllToolbarButtons
{
    [tripsButton setActive:NO];
    [myTripsButton setActive:NO];
    [searchButton setActive:NO];
    [alertsButton setActive:NO];
    [moreButton setActive:NO];
}
-(void)showFindMyWay
{
    [self XLog:@"FMW Button Pressed"];
    [self deactivateAllToolbarButtons];
    [tripsButton setActive:YES];
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwScreen];
}

-(void)showMainMenu
{
    [self XLog:@"Main Menu Button Pressed"];
    [self deactivateAllToolbarButtons];
    [moreButton setActive:YES];
    [((AppViewController*)parentView) showView:((AppViewController*)parentView).mainMenu];
}

-(void)showAlerts
{
    [self XLog:@"Alerts Button Pressed"];
    [self deactivateAllToolbarButtons];
    [alertsButton setActive:YES];
}

-(void)showMyTrips
{
    [self XLog:@"MyTrips Button Pressed"];
    [self deactivateAllToolbarButtons];
    [myTripsButton setActive:YES];
}

-(void)showPlaces
{
    [self XLog:@"Places Button Pressed"];
    [self deactivateAllToolbarButtons];
    [searchButton setActive:YES];
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).searchScreen];
}
@end
