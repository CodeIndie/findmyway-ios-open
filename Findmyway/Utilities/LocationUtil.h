//
//  LocationUtility.h
//  tct
//
//  Created by Bilo on 5/28/14.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Location.h"
#import "AppDelegate.h"
#import "JCDHTTPConnection.h"

@class AppDelegate;

@protocol LocationUtilityDelegate
@required

-(void)locationUpdate:(CLLocation*)location;

@end

@interface LocationUtil : NSObject<CLLocationManagerDelegate>
{
    CLLocationManager *locationManager;
}

@property BOOL logLocationUpdates;
@property BOOL hasAddressForCoords;
@property Location *southAfrica;
@property Location *userLocation;
@property Location *capeTown;
@property Location *tshwane;
@property (nonatomic, weak) id delegate;

-(id)init;
+(id)instance;
+(id)sharedInstance;

-(void)getAddressFromAPI;
-(BOOL)isLocationEnabled;
-(void)setLocation:(CLLocationCoordinate2D)location;
-(Location*)getLocation;
-(void)startUpdatingUserLocation;
-(void)stopUpdatingUserLocation;
-(BOOL)isLocationAvailable;
@end
