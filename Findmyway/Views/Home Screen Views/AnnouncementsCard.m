//
//  AnnouncementsView.m
//  Findmyway
//
//  Created by Bilo on 4/20/15.
//

#import "AnnouncementsCard.h"

@implementation AnnouncementsCard

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    [self updateAnnouncements];
    [self setBackgroundColor:colour.clear];
//    CALayer *layer = [self layer];
//    [layer setBorderColor:colour.fmwBorderColor.CGColor];
//    [layer setBackgroundColor:colour.fmwActiveColorLight.CGColor];
//    [layer setBorderWidth:layout.delimiterSize];
    
    title = [XUILabel initWithFrame:CGRectMake(layout.padding, 0, layout.paddedWidth, layout.buttonSize)
                               text:NSLocalizedString(@"Announcements", nil)
                          textColor:colour.fmwTextColor
                    backgroundColor:colour.clear
                      textAlignment:NSTextAlignmentCenter];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, [self width], [self height] - layout.buttonSize * 2)];
    
    showAllButton = [XUIButton initWithFrame:CGRectMake(0, [self height] - layout.buttonSize, [self width], layout.buttonSize)
                                       title:NSLocalizedString(@"SHOW ALL", nil)
                                  titleColor:colour.fmwActiveColor
                             backgroundColor:colour.clear
                         horizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [showAllButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [showAllButton addTarget:self action:@selector(showAllAnnouncements) forControlEvents:UIControlEventTouchUpInside];
    CALayer *buttonLayer = [showAllButton layer];
    [buttonLayer setBorderColor:colour.fmwBorderColor.CGColor];
    [buttonLayer setBorderWidth:layout.delimiterSize];
}

-(void)addUIToView
{
    [self addSubview:title];
    [self addSubview:scrollView];
    [self addSubview:showAllButton];
}

-(void)updateAnnouncements
{
    NSLog(@"Updating Announcements");
    AppViewController *viewController = (AppViewController*)parentView;
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    //[parameters setValue:[viewController.fmwScreen.modesPicker excludedModes] forKey:@"excludedModes"];
    //[parameters setValue:[viewController.fmwScreen.modesPicker excludedOperators] forKey:@"excludedOperators"];
    
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:Announcements callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
    ^(NSHTTPURLResponse *response, NSString *bodyString)
    {
        NSLog(@"API - Announcements: %ld", (long)response.statusCode);
        [self handleApiError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode];
        [appDelegate.apiResponse addAnnouncements:[JSONParser convertJSONToDictionary:bodyString]];
        [self addAnnouncements];
        [viewController.fmwScreen updateUI];
    }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
    {
        NSLog(@"API CONNECTION FAILURE - Announcements: %@", error);
        [self stopLoader];
        [self handleError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode error:error];
    }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
    {
         // This is not going to be called in this example but it's included for completeness.
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
    }];
}

-(void)addAnnouncements
{
    [self clearScrollView];
    
    float yPos = 0;
    int i = 0;
    for(Announcement *announcement in appDelegate.apiResponse.announcements)
    {
        if(i>=1) break;
        
        AnnouncementListItem *item = [[AnnouncementListItem alloc] initWithAnnouncement:announcement frame:CGRectMake(0, layout.buttonSize + yPos, layout.screenWidth, layout.buttonSize)];
        item.shareButton.tag = i;
        [item.shareButton addTarget:self action:@selector(shareAnnouncement:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:item];
        NSLog(@"Announcement %d: %@",i, announcement.descriptionText);
        yPos += [item height] + layout.margin;
        i++;
    }
    
    NSLog(@"yPos: %f", yPos);
    NSLog(@"Origin: %f", self.frame.origin.y);
    //self.bounds = CGRectMake(0, self.frame.origin.y, [self width], layout.buttonSize * 2 + yPos);
    CGRect newFrame = CGRectMake(0, layout.buttonSize, [self width], yPos);
    scrollView.frame = newFrame;
    //[self setBackgroundColor:colour.fmwGradientEnd];
    //[scrollView setBackgroundColor:colour.fmwGradientStart];
    scrollView.contentSize = CGSizeMake([self width], yPos);
    //showAllButton.frame = CGRectMake(0, [self height] - layout.buttonSize, self.frame.size.width, layout.buttonSize);
    showAllButton.frame = CGRectMake(0, yPos + layout.buttonSize, self.frame.size.width, layout.buttonSize);
}

-(void)clearScrollView
{
    for(UIView *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
}

-(void)animateIn
{
    [super animateIn];
    //[parentView.view addSubview:self];
//    [UIView animateWithDuration:animationInterval
//                          delay:delay
//                        options:UIViewAnimationOptionCurveEaseInOut
//                     animations:^{
//                         self.alpha = 1;
//                     }completion:^(BOOL finished){
//                         
//                     }];
}

-(void)animateOut
{
    [super animateOut];
//    [UIView animateWithDuration:animationInterval
//                          delay:delay
//                        options:UIViewAnimationOptionCurveEaseInOut
//                     animations:^{
//                         self.alpha = 0;
//                     }completion:^(BOOL finished){
//                         [self removeFromSuperview];
//                     }];
}

-(void)showAllAnnouncements
{
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController showView:viewController.announcementScreen];
}

-(IBAction)shareAnnouncement:(id)sender
{
    NSInteger tag = ((UIButton*)sender).tag;
    NSString *announcementString = ((Announcement*)[appDelegate.apiResponse.announcements objectAtIndexedSubscript:tag]).descriptionText;
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[announcementString] applicationActivities:nil];
    [parentView presentViewController:activityViewController animated:YES completion:nil];
}
@end
