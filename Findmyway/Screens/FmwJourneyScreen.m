//
//  FmwJourneyScreen.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/09.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "FmwJourneyScreen.h"

@implementation FmwJourneyScreen

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(device.screenWidth * 1.5, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth/2, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)animateIn
{
    [super animateIn];
    [self addNotificationListeners];
    
    [parentView.view addSubview:self];
    [parentView.navigationItem setTitle:self.viewName];
    self.center = centerOffScreen;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
                    {
                    self.center = centerOnScreen;
                    }
                    completion:^(BOOL finished)
                    {
                    }];
}

-(void)animateOut
{
    [super animateOut];
    [self removeNotificationListeners];
    
    self.center = centerOnScreen;
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOffScreen;
     }
                     completion:^(BOOL finished){
                         for(UIView *view in scrollView.subviews)
                         {
                             [view removeFromSuperview];
                         }
                         [self removeFromSuperview];
                     }];
}

-(void)addNotificationListeners
{
    
}

-(void)removeNotificationListeners
{
    
}

-(void)initViewElements
{
    pageWidth = layout.screenWidth;
    pageHeight = layout.screenHeight - layout.topOfContent;
    
    [self initScrollViewPaging];
    [self initStages];
    //[self initPositionIndicator];
    [self initToolbar];
}

-(void)initToolbar
{
    float y = layout.screenHeight - layout.buttonSize - layout.margin;
    mapButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding, y, layout.buttonSize, layout.buttonSize)];
    [mapButton setImage:[ImageUtil getImage:@"iconMap.png"] forState:UIControlStateNormal];
    [mapButton.imageView setTintColor:colour.white];
    [mapButton addTarget:self action:@selector(showMap) forControlEvents:UIControlEventTouchUpInside];
    
    toggleSizeButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth - layout.padding - layout.buttonSize, y, layout.buttonSize, layout.buttonSize)];
    [toggleSizeButton setImage:[ImageUtil getImage:@"iconMinimise1.png"] forState:UIControlStateNormal];
    float p = layout.padding;
    [toggleSizeButton setImageEdgeInsets:UIEdgeInsetsMake(p, p, 0, 0)];
    [toggleSizeButton.imageView setTintColor:colour.white];
    [toggleSizeButton addTarget:self action:@selector(minimiseJourney) forControlEvents:UIControlEventTouchUpInside];
}

-(void)minimiseJourney
{
    NSLog(@"Minimising Journey");
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwRouteOptions];
}

-(void)showMap
{
    NSLog(@"Showing Map");
    [((AppViewController*)parentView).fmwMapView setState:JourneyMode];
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwMapView];
}

-(void)initScrollViewPaging
{
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, pageWidth, pageHeight)];
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    scrollView.showsHorizontalScrollIndicator = NO;
    
    NSUInteger pageCount = appDelegate.tripQuery.selectedTrip.stages.count;
    scrollView.contentSize = CGSizeMake(pageCount * pageWidth, pageHeight);
    
    pageControl = [[UIPageControl alloc] init];//WithFrame:CGRectMake(0, layout.screenHeight - layout.topOfContent, layout.screenWidth, layout.margin)];
    pageControl.center = CGPointMake(layout.screenWidth/2, layout.screenHeight - layout.topOfContent - layout.buttonSize/4);
    [pageControl setNumberOfPages:pageCount];
}

-(void)initStages
{
    stagePages = [[NSMutableArray alloc] init];
    MultimodalTrip *trip = appDelegate.tripQuery.selectedTrip;
    
    int index = 0;
    for(MultimodalStage *stage in trip.stages)
    {
        StagePage *page = [[StagePage alloc] initWithFrame:CGRectMake(index * pageWidth, 0, pageWidth, pageHeight)];
        [page setIndex:index stageCount:trip.stages.count stage:stage appDelegate:appDelegate];

        [stagePages addObject:page];
        [scrollView addSubview:page];
        index++;
    }
}

-(void)initPositionIndicator
{
    positionIndicator = [[XUIShape alloc] initWithFrame:CGRectMake(layout.margin + layout.padding, layout.buttonSize * 2 + layout.margin, layout.buttonSize - layout.margin, layout.buttonSize - layout.margin)];
    float size = positionIndicator.frame.size.height;
    NSMutableArray *pointsArray = [[NSMutableArray alloc] init];
    [pointsArray addObject:[NSValue valueWithCGPoint:CGPointMake(0, 0)]];
    [pointsArray addObject:[NSValue valueWithCGPoint:CGPointMake(size/2, size)]];
    [pointsArray addObject:[NSValue valueWithCGPoint:CGPointMake(size, 0)]];
    [pointsArray addObject:[NSValue valueWithCGPoint:CGPointMake(size/2, size/4)]];
    
    [positionIndicator polygonWithPoints:pointsArray strokeWidth:layout.delimiterSize strokeColor:colour.white backgroundColor:colour.clear];
    
    UIImageView *circleImage = [[UIImageView alloc] initWithFrame:CGRectMake(layout.margin + layout.padding, layout.buttonSize * 2 + layout.margin, layout.buttonSize - layout.margin, layout.buttonSize - layout.margin)];
    [circleImage setBackgroundColor:colour.fmwActiveColor];
    CALayer *circleLayer = [circleImage layer];
    [circleLayer setBorderWidth:(layout.buttonSize - layout.margin)/2];
    
    pulseEffect = [[XUIEffect alloc] initPulseWithDuration:0.8 pause:0.8 mainGraphic:circleImage effectGraphic:circleImage];
    pulseEffect.frame = self.frame;
}

-(void)addUIToView
{
    [self addSubview:scrollView];
    [scrollView addSubview:positionIndicator];
    [scrollView addSubview:pulseEffect];
    [self addSubview:pageControl];
    
    [self addSubview:mapButton];
    [self addSubview:toggleSizeButton];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageSizeWidth = scrollView.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageSizeWidth / 2) / pageSizeWidth) + 1;
    pageControl.currentPage = page;
}
@end
