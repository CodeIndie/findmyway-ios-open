//
//  XUIShape.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/12.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface XUIShape : UIView
{
}

@property UIColor *strokeColor;
@property UIColor *fillColor;
@property float strokeWidth;
@property NSMutableArray *points;

+(UIView*)square:(CGPoint)origin size:(float)size borderWidth:(float)borderWidth fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor;

+(UIView*)circle:(CGPoint)origin size:(float)size borderWidth:(float)borderWidth fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor;

-(void)polygonWithPoints:(NSMutableArray *)points strokeWidth:(float)strokeSize strokeColor:(UIColor *)strokeColor backgroundColor:(UIColor*)backgroundColor;

@end
