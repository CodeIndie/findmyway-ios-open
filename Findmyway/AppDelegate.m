//
//  AppDelegate.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self configureApp];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

-(void)configureApp
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile: [[NSBundle mainBundle] pathForResource:@"FmwAppConfig" ofType:@"plist"]];
    
    [self setEarthHourDate];
    
    self.device = [[DeviceConfig alloc] init:self];
    self.colour = [[ColourConfig alloc] init:dictionary];
    self.layout = [[LayoutConfig alloc] init:dictionary];
    self.tripQuery = [[TripQuery alloc] init];
    
    self.mapConfig = [[MapConfig alloc] init];
    self.searchConfig = [[SearchConfig alloc] init];
    
    self.apiUtil = [[ApiUtil alloc] init:dictionary];
    self.apiResponse = [[ApiResponse alloc] init];
    [self initLocationManager];
}

-(void)getNewToken
{
    if(!gettingNewToken)
    {
        gettingNewToken = YES;
        JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:[self.apiUtil urlForRequest:Users callType:POST parameters:nil]];
        [connection
        executeRequestOnSuccess:^(NSHTTPURLResponse *response, NSString *bodyString)
        {
            NSLog(@"API - Token: %ld", (long)response.statusCode);
            [self.apiResponse addToken:[JSONParser convertJSONToDictionary:bodyString]];
            gettingNewToken = NO;
        }
        failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
        {
            NSLog(@"API CONNECTION FAILURE - Token: %@", error);
            gettingNewToken = NO;
            //[self showAlertView:@"Connection Error" message:@"There was a connection error.\n\nPlease retry loading data." button:@"Retry"];
        }
        didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
        {
            NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
        }];
    }
}

-(void)openWebsite:(NSString*)urlString
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

-(void)initLocationManager
{
    self.locationUtil = [LocationUtil sharedInstance];
    [self.locationUtil startUpdatingUserLocation];
}

-(UIImage*)getImageForMode:(NSString*)mode
{
    UIImage *modeImage;
    if([[mode uppercaseString] isEqualToString:[@"Rail" uppercaseString]])
    {
        modeImage = [ImageUtil getImage:@"iconModeRail.png"];
    }
    else if([[mode uppercaseString] isEqualToString:[@"Bus" uppercaseString]])
    {
        modeImage = [ImageUtil getImage:@"iconModeBus.png"];
    }
    else if([[mode uppercaseString] isEqualToString:[@"Taxi" uppercaseString]])
    {
        modeImage = [ImageUtil getImage:@"iconModeTaxi.png"];
    }
    else if([[mode uppercaseString] isEqualToString:[@"Boat" uppercaseString]])
    {
        modeImage = [ImageUtil getImage:@"iconModeBoat.png"];
    }
    else if([[mode uppercaseString] isEqualToString:[@"Pedestrian" uppercaseString]])
    {
        modeImage = [ImageUtil getImage:@"iconModeWalking.png"];
    }
    return modeImage;
}

-(void)setEarthHourDate
{
    NSDateComponents *startComps = [[NSDateComponents alloc] init];
    [startComps setDay:27];
    [startComps setMonth:3];
    [startComps setYear:2015];
    [startComps setHour:5];
    [startComps setMinute:00];
    [startComps setSecond:00];
    self.earthHourStartDate = [[NSCalendar currentCalendar] dateFromComponents:startComps];
    
    NSDateComponents *endComps = [[NSDateComponents alloc] init];
    [endComps setDay:27];
    [endComps setMonth:3];
    [endComps setYear:2015];
    [endComps setHour:23];
    [endComps setMinute:59];
    [endComps setSecond:59];
    self.earthHourEndDate = [[NSCalendar currentCalendar] dateFromComponents:endComps];
}

-(BOOL)isCompetitionActive
{
    return [DateTimeUtil secondsUntilDate:self.earthHourStartDate] < 0 &&
           [DateTimeUtil secondsUntilDate:self.earthHourEndDate] > 0;
}

#pragma mark - NSURLConnection Networking

-(NSCachedURLResponse *)connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse
{
    return nil;
}

-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_asyncResponseData appendData:data];
}

-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    _asyncResponse = (NSHTTPURLResponse*)response;
    _asyncResponseData = [[NSMutableData alloc] init];
    _statusCode = (int)[_asyncResponse statusCode];
    //NSLog(@"ASYNC Connection didReceiveResponse: %ld", (long)[_asynchronousResponse statusCode]);
}

-(void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    NSLog(@"ASYNC Connection didFinishLoading");
    
    NSDictionary *responseDictionary;
    NSArray *responseArray;
    NSError *error;
    
    if(![JSONParser responseIsArray:_asyncResponseData])
    {
        responseDictionary = [JSONParser convertResponseToDictionary:_asyncResponseData withError:&error];
    }
    else
    {
        responseArray = [JSONParser convertResponseToArray:_asyncResponseData withError:&error];
    }
    
    NSLog(@"ASYNC Connection didFinishLoading: %d", _statusCode);
    [self processApiResponse:(int)_statusCode responseRoot:[[responseDictionary allKeys] objectAtIndex:0] responseDictionary:responseDictionary];
}

-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSLog(@"\n\nConnection Finished With Error: \n%@", error);
    [self handleiOSNetworkError:error];
}

#pragma mark - REST Request Types

-(void)GETRequest:(NSString*)urlString isAsynchronous:(BOOL)isAsynchronous cacheResponse:(BOOL)cachingEnabled
{
    if(isAsynchronous)
    {
        [self GETAsync:urlString cacheResponse:cachingEnabled];
    }
    else
    {
        [self GETSync:urlString cacheResponse:cachingEnabled];
    }
}

-(void)GETAsync:(NSString*)urlString cacheResponse:(BOOL)cachingEnabled
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    
    [request setValue:self.device.userAgent forHTTPHeaderField:@"User-Agent"];
    [request setValue:self.apiUtil.appKey.encodedString forHTTPHeaderField:@"AppKey"];
    
    if(self.apiUtil.token != nil && self.apiUtil.token != (id)[NSNull null])
    {
        [request setValue:self.apiUtil.token.encodedString forHTTPHeaderField:@"token"];
    }
    //    if([self.locationUtility isLocationAvailable])
    //    {
    //        NSLog(@"Sending Location: (%@,%@)", self.locationUtility.userLocation.latitudeString, self.locationUtility.userLocation.longitudeString);
    //        [request setValue:self.locationUtility.userLocation.latitudeString forHTTPHeaderField:@"Latitude"];
    //        [request setValue:self.locationUtility.userLocation.longitudeString forHTTPHeaderField:@"Longitude"];
    //    }
    NSLog(@"%@",[request valueForHTTPHeaderField:@"User-Agent"]);
    
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    NSLog(@"ASYNC Connection: %@", [connection.currentRequest URL]);
}

-(void)GETSync:(NSString*)urlString cacheResponse:(BOOL)cachingEnabled
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];
    //[request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    [request setValue:self.device.userAgent forHTTPHeaderField:@"User-Agent"];
    
    if(self.apiUtil.token != nil && self.apiUtil.token != (id)[NSNull null])
    {
        [request setValue:self.apiUtil.token.encodedString forHTTPHeaderField:@"token"];
    }
    
    NSLog(@"%@",[request valueForHTTPHeaderField:@"User-Agent"]);
    
    
    NSURLResponse *response = nil;
    NSError *error = nil;
    NSData *responseData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
    
    if(error == nil)
    {
        NSDictionary *responseDictionary = [JSONParser convertResponseToDictionary:responseData withError:&error];
        [self processApiResponse:(int)[httpResponse statusCode] responseRoot:[[responseDictionary allKeys] objectAtIndex:0] responseDictionary:responseDictionary];
    }
    else
    {
        NSLog(@"\nThere was an error parsing the obtaining the data.\nError: %@", error);
    }
}

-(void)printResponseSummary:(NSDictionary*)dictionary statusCode:(int)statusCode
{
    NSLog(@"HTTP  Status:%d", statusCode);
    NSString *responseRoot = [[dictionary allKeys] objectAtIndex:0];
    NSLog(@"Response Type: %@", responseRoot);
    if([responseRoot isEqualToString:@"ErrorCode"] || [responseRoot isEqualToString:@"message"])
    {
        NSLog(@"Response  Msg: %@", [dictionary objectForKey:@"Message"]);
    }
}

-(void)processApiResponse:(int)statusCode responseRoot:(NSString *)responseRoot responseDictionary:(NSDictionary *)responseDictionary
{
    // HTTP status code meaning obtained from: http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
    [self printResponseSummary:responseDictionary statusCode:statusCode];
    //[DictionaryUtil printDictionary:responseDictionary];
    
    if([responseRoot isEqualToString:@"message"] || [responseRoot isEqualToString:@"errorCode"])
    {
        self.apiError = [[ApiError alloc] initWithDictionary:responseDictionary];
    }
    
    if(statusCode >= 100 && statusCode < 200)
    {
        // Informational
        /*
         100 Continue
         101 Switching Protocols
         */
    }
    else if (statusCode >= 200 && statusCode < 300)
    {
        [self processJsonResponseBody:responseRoot responseDictionary:responseDictionary];
        // Successful
        /*
         200 OK
         201 Created
         202 Accepted
         203 Non-Authoritative Information
         204 No Content
         205 Reset Content
         206 Partial Content
         */
    }
    else if(statusCode >= 300 && statusCode < 400)
    {
        // Redirection
        /*
         300 Multiple Choices
         301 Moved Permanently
         302 Found
         303 See Other
         304 Not Modified
         305 Use Proxy
         306 (Unused)
         307 Temporary Redirect
         */
    }
    else if(statusCode >= 400 && statusCode < 500)
    {
        //[self handleBadRequestError:statusCode message:self.apiMessage];
        [self handleApiError:responseDictionary];
        // Client Error
        /*
         400 - Bad request
         401 Unauthorized
         402 Payment Required
         403 Forbidden
         404 Not found
         405 Method not allowed
         406 Not Acceptable
         407 Proxy Authentication Required
         408 Request Timeout
         409 Conflict
         410 Gone
         411 Length Required
         412 Precondition Failed
         413 Request Entity Too Large
         414 Request URI Too long
         415 Unsupported Media Type
         416 Request Range Not Staisfiable
         417 Expectation Failed
         */
    }
    else if(statusCode >= 500)
    {
        [self handleServerError:statusCode message:self.apiError.message];
        // Server Error
        /*
         500 Internal Server Error
         501 Not Implemented
         502 Bad Gateway
         503 Service Unavailable
         504 Gateway Timeout
         505 HTTP Version Not Supported
         */
    }
}

-(void)processJsonResponseBody:(NSString *)responseRoot responseDictionary:(NSDictionary *)responseDictionary
{
    switch(self.apiUtil.lastCall)
    {
            // -------------------
            // Application Startup
            
        case ReverseGeocode:
        {
            [self.apiResponse addAddress:responseDictionary];
            [[NSNotificationCenter defaultCenter] postNotificationName:[[Notifications instance] GETAddressSuccess] object:nil];
        }break;
            
        case SearchItems:
        {
            [self.apiResponse addSearchItems:responseDictionary];
            [[NSNotificationCenter defaultCenter] postNotificationName:[[Notifications instance] GETSearchItemsSuccess] object:nil];
        }break;
            
        case Trips:
        {
            //[self.apiResponse addTrips:responseDictionary];
            //[[NSNotificationCenter defaultCenter] postNotificationName:[[Notifications instance] GETTripsSuccess] object:nil];
        }break;
            
        case Paths:
        {
            [self.apiResponse addPaths:responseDictionary];
            [[NSNotificationCenter defaultCenter] postNotificationName:[[Notifications instance] GETPathsSuccess] object:nil];
        }break;
            
        default:
        {
            NSLog(@"API Response not supported");
        }
            break;
    }
}

-(void)handleiOSNetworkError:(NSError *)error
{
    // For all error codes see: https://developer.apple.com/library/mac/documentation/Cocoa/Reference/Foundation/Miscellaneous/Foundation_Constants/index.html#//apple_ref/doc/uid/TP40003793-CH3g-SW40
    
    switch(error.code)
    {
        case -1001:// TimeOut
        {
            //[[NSNotificationCenter defaultCenter] postNotificationName:[[NotificationStrings instance] errorRequestTimeOut] object:nil];
            [self handleBadRequestError:(int)error.code message:@"iOS: Error Timeout"];
        }
            break;
            
        case -1003:// CannotFindHost
        {
            // A server with the specified hostname could not be found.
            //[[NSNotificationCenter defaultCenter] postNotificationName:[[NotificationStrings instance] errorServerConnection] object:nil];
            [self handleBadRequestError:(int)error.code message:@"iOS: Could not connect to server."];
        }
            break;
            
        case -1004:// CannotConnectToHost
        {
            //[[NSNotificationCenter defaultCenter] postNotificationName:[[NotificationStrings instance] errorServerConnection ] object:nil];
            [self handleBadRequestError:(int)error.code message:@"iOS: Could not connect to server."];
        }
            break;
            
        case -1005:// NetworkConnectionLost
        {
            //[[NSNotificationCenter defaultCenter] postNotificationName:[[NotificationStrings instance] errorServerConnection ] object:nil];
            [self handleBadRequestError:(int)error.code message:@"iOS: Could not connect to server."];
        }
            break;
            
        case -1006:// DNSLookupFailed
        {
            
        }
            break;
            
        case -1008:// ResourceUnavailable
        {
            
        }
            break;
            
        case -1009:// NotConnectedToInternet
        {
            //[[NSNotificationCenter defaultCenter] postNotificationName:[[NotificationStrings instance] errorNoInternet ] object:nil];
            [self handleBadRequestError:(int)error.code message:@"iOS: No Internet"];
        }
            break;
            
        default:
        {
            //[[NSNotificationCenter defaultCenter] postNotificationName:[[NotificationStrings instance] errorServerConnection] object:nil];
            [self handleBadRequestError:(int)error.code message:@"iOS: Could not connect to server."];
        }
            break;
    }
}

-(void)handleApiError:(NSDictionary *)apiErrorJson
{
    self.apiError = [[ApiError alloc] initWithDictionary:apiErrorJson];
    NSLog(@"ERROR OCCURED: %d - %@", self.apiError.errorCode, self.apiError.message);
}

-(void)handleBadRequestError:(int)statusCode message:(NSString *)message
{

}

-(void)handleServerError:(int)statusCode message:(NSString*)message;
{
    //TODO: handle server error
    //[self handleBadRequestError:statusCode message:message];
    //[[NSNotificationCenter defaultCenter] postNotificationName:[[NotificationStrings instance] errorServerConnection] object:nil];
}

#pragma mark - Core Data stack

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "com.codeindie.FindMyWay" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSManagedObjectModel *)managedObjectModel {
    // The managed object model for the application. It is a fatal error for the application not to be able to find and load its model.
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FindMyWay" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    // The persistent store coordinator for the application. This implementation creates and return a coordinator, having added the store for the application to it.
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    // Create the coordinator and store
    
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"FindMyWay.sqlite"];
    NSError *error = nil;
    NSString *failureReason = @"There was an error creating or loading the application's saved data.";
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        // Report any error we got.
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        dict[NSLocalizedDescriptionKey] = @"Failed to initialize the application's saved data";
        dict[NSLocalizedFailureReasonErrorKey] = failureReason;
        dict[NSUnderlyingErrorKey] = error;
        error = [NSError errorWithDomain:@"YOUR_ERROR_DOMAIN" code:9999 userInfo:dict];
        // Replace this with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext {
    // Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.)
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (!coordinator) {
        return nil;
    }
    _managedObjectContext = [[NSManagedObjectContext alloc] init];
    [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    return _managedObjectContext;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        NSError *error = nil;
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
            // Replace this implementation with code to handle the error appropriately.
            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

@end
