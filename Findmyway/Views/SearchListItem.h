//
//  SearchItem.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/08.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchItem.h"
#import "AppDelegate.h"
#import "ImageUtil.h"
#import "DistanceUtil.h"

@interface SearchListItem : UIView
{
    LayoutConfig *layout;
    ColourConfig *colour;
}
@property SearchItem *searchItem;
@property UIImageView *icon;
@property UIImageView *horizontalLine;
@property UILabel *label;
@property UILabel *distanceLabel;
@property UIButton *button;
@property UIButton *mapButton;
@property UIButton *favouritesButton;

-(void)initView:(AppDelegate*)appDelegate searchItem:(SearchItem*)searchItem height:(float)height;
-(void)setItemTag:(int)index;
@end
