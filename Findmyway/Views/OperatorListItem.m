//
//  OperatorListItem.m
//  Findmyway
//
//  Created by Bilo on 2/18/15.
//

#import "OperatorListItem.h"

@implementation OperatorListItem

-(id)initWithFrame:(CGRect)frame operator:(Operator*)operatorItem appDelegate:(AppDelegate*)appDel
{
    self = [super initWithFrame:frame];
    if(self)
    {
        appDelegate = appDel;
        self.name = operatorItem.displayName;
        self.apiName = operatorItem.name;
        
        self.uiSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(self.frame.size.width - appDelegate.layout.buttonSize * 1.5,
                                                                   0,
                                                                   appDelegate.layout.buttonSize * 1.5,
                                                                   appDelegate.layout.buttonSize)];
        [self.uiSwitch addTarget:self action:@selector(toggleOperator) forControlEvents:(UIControlEventValueChanged|UIControlEventTouchDragInside)];
        self.uiSwitch.onTintColor = appDelegate.colour.fmwActiveColor;
        [self addSubview:self.uiSwitch];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(appDelegate.layout.padding, 0, frame.size.width - 2 * appDelegate.layout.buttonSize, appDelegate.layout.buttonSize)];
        [self.label setText:self.name];
        [self.label setTextColor:appDelegate.colour.fmwActiveColor];
        [self addSubview:self.label];
        
        if(operatorItem.isPublic)
        {
            [self.uiSwitch setOn:YES];
        }
        else
        {
            [self.uiSwitch setOn:NO];
        }
        [self toggleOperator];
    }
    return self;
}

-(void)toggleOperator
{
    if(self.uiSwitch.isOn)
    {
        [self.label setTextColor:appDelegate.colour.fmwActiveColor];
    }
    else
    {
        [self.label setTextColor:appDelegate.colour.fmwTextColorLight];
    }
}

@end
