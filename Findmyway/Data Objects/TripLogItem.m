//
//  TripLogItem.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/11.
//

#import "TripLogItem.h"

@implementation TripLogItem

-(id)initWithStart:(NSString*)startLocation end:(NSString*)endLocation points:(int)points dateTime:(NSString*)dateTime
{
    self = [super init];
    if(self)
    {
        self.startLocation = startLocation;
        self.endLocation = endLocation;
        self.points = points;
        self.dateTime = dateTime;
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)coder
{
    [coder encodeObject:self.startLocation forKey:@"startLocation"];
    [coder encodeObject:self.endLocation forKey:@"endLocation"];
    [coder encodeObject:@(self.points) forKey:@"points"];
    [coder encodeObject:self.dateTime forKey:@"dateTime"];
}

- (id)initWithCoder:(NSCoder *)coder
{
    self = [super init];
    if (self)
    {
        self.startLocation = [coder decodeObjectForKey:@"startLocation"];
        self.endLocation = [coder decodeObjectForKey:@"endLocation"];
        self.points = (int)[[coder decodeObjectForKey:@"points"] integerValue];
        self.dateTime = [coder decodeObjectForKey:@"dateTime"];
    }
    return self;
}

-(BOOL)isEqualTo:(TripLogItem *)item
{
    if([self.startLocation isEqualToString:item.startLocation] &&
       [self.endLocation isEqualToString:item.endLocation] &&
       [self.dateTime isEqualToString:item.dateTime])
    {
        return YES;
    }
    return NO;
}

@end
