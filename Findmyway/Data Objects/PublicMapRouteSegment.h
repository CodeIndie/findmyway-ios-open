//
//  PublicMapRouteSegment.h
//  tct
//
//  Created by Bilo on 7/21/14.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Location.h"

@interface PublicMapRouteSegment : NSObject

@property NSString *hexColor;
@property UIColor *color;
@property NSMutableArray *coordinates;

-(id)initWithDictionary:(NSDictionary*)dictionary;
-(id)initWithMapRouteSegment:(PublicMapRouteSegment*)segment;

@end
