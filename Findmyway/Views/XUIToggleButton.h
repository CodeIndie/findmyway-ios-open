//
//  XUIToggleButton.h
//  Findmyway
//
//  Created by Bilo on 12/4/14.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ImageUtil.h"

@interface XUIToggleButton : NSObject
{
    UIColor *activeColor;
    UIColor *inactiveColor;
}

@property NSString *name;
@property BOOL isActive;
@property UIButton *button;
@property UIImage *activeImage;
@property UIImage *inactiveImage;


-(id)initWithName:(NSString*)name activeImage:(NSString*)activeImageName inactiveImage:(NSString*)inactiveImageName xPosition:(float)xPosition yPosition:(float)yPosition size:(float)size target:(id)target;
-(id)initWithName:(NSString *)name activeImage:(NSString *)activeImageName inactiveImage:(NSString *)inactiveImageName activeColor:(UIColor*)activeCol inactiveColor:(UIColor*)inactiveCol xPosition:(float)xPosition yPosition:(float)yPosition size:(float)size target:(id)target;

-(void)toggle;
-(void)setActive:(BOOL)active;
@end
