//
//  JourneyHub.h
//  Findmyway
//
//  Created by Bilo on 2/24/15.
//

#import <Foundation/Foundation.h>

@protocol JourneyHub <NSObject>

-(void)gainInterestInJourney:(NSString*)journeyGUID;
-(void)loseInterestInJourney:(NSString*)journeyGUID;
-(void)startJourney:(NSString*)journeyGUID;
-(void)endJourney;

//-(void)ping
-(void)message:(NSString*)message;
@end
