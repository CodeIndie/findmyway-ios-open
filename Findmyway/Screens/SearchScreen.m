//
//  SearchScreen.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.

#import "SearchScreen.h"
#import "NSURLConnection+Blocks.h"

@implementation SearchScreen

-(void)addNotificationListeners
{
    [super addNotificationListeners];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateSearchItems) name:[[Notifications instance] GETSearchItemsSuccess] object:nil];
}

-(void)removeNotificationListeners
{
    [super removeNotificationListeners];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[[Notifications instance] GETSearchItemsSuccess] object:nil];
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(device.screenWidth * 1.5, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth/2, frame.origin.y + frame.size.height/2);
        recentPlaces = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
    itemHeight = layout.buttonSize + layout.padding;
}

-(void)initViewElements
{
    [self setBackgroundColor:colour.fmwBackgroundColor];
    
    headerBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, layout.buttonSize)];
    [headerBackground setBackgroundColor:colour.fmwHeader];
    
    backButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding/2, 0, layout.buttonSize, layout.buttonSize)];
    [backButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [backButton setImage:[ImageUtil getImage:@"iconBack.png"] forState:UIControlStateNormal];
    [backButton.imageView setTintColor:colour.fmwActiveColor];
    float p = layout.padding/2;
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(p, p, p, p)];
    [backButton addTarget:self action:@selector(backbuttonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    searchField = [[UITextField alloc] initWithFrame:CGRectMake(layout.buttonSize + layout.padding, layout.padding / 2, layout.screenWidth - 2 * layout.buttonSize + 2 * layout.padding, layout.buttonSize - layout.padding)];
    searchField.autocorrectionType = UITextAutocorrectionTypeNo;
    [searchField setBackgroundColor:colour.fmwBackgroundColor];
    searchField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    searchField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0,0, layout.buttonSize,layout.padding)];
    searchField.leftViewMode = UITextFieldViewModeAlways;
    [searchField setReturnKeyType:UIReturnKeyDone];
    searchField.delegate = self;
    [searchField setTextColor:colour.fmwTextColor];
    [searchField setPlaceholder:NSLocalizedString(@"Search or enter an address", nil)];
    [searchField setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [self searchFieldCustomPlaceholder:NSLocalizedString(@"Search or enter an address", nil) color:colour.fmwTextColorLight];
    CALayer *searchLayer = [searchField layer];
    [searchLayer setBorderColor:[colour.fmwTextColor CGColor]];
    [searchLayer setCornerRadius:layout.padding];
    [searchLayer setBorderWidth:layout.delimiterSize];
    
    searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(layout.padding, layout.padding/2, layout.buttonSize - 2 * layout.padding, layout.buttonSize - 2 * layout.padding)];
    [searchIcon setImage:[ImageUtil getImage:@"iconSearch.png"]];
    [searchIcon setTintColor:colour.fmwActiveColor];
    [searchField addSubview:searchIcon];
    
    horizontalLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, layout.delimiterSize)];
    [horizontalLine setBackgroundColor:colour.fmwBorderColor];
    
    float clearButtonSize = layout.buttonSize * 0.6;
    cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(searchField.frame.origin.x + searchField.frame.size.width - clearButtonSize - layout.padding/2, layout.padding*0.6 + 2*layout.delimiterSize, clearButtonSize, clearButtonSize)];
    [cancelButton addTarget:self action:@selector(cancelSearch) forControlEvents:UIControlEventTouchUpInside];
    [cancelButton setImage:[ImageUtil getImage:@"iconCross.png"] forState:UIControlStateNormal];
    [cancelButton.imageView setTintColor:colour.white];
    [cancelButton setBackgroundColor:colour.fmwActiveColor];
    float padding = layout.padding*0.7;
    [cancelButton setImageEdgeInsets:UIEdgeInsetsMake(padding, padding, padding, padding)];
    [cancelButton addTarget:self action:@selector(cancelSearch) forControlEvents:UIControlEventTouchUpInside];
    CALayer *layer = [cancelButton layer];
    [layer setMasksToBounds:YES];
    [layer setCornerRadius:clearButtonSize/2];
    
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize + layout.delimiterSize, layout.screenWidth, layout.screenHeight - layout.buttonSize * 2 + layout.margin)];
    
    [self initDefaultContent];
}

-(void)searchFieldCustomPlaceholder:(NSString*)placeholderText color:(UIColor*)color
{
    if ([searchField respondsToSelector:@selector(setAttributedPlaceholder:)])
    {
        searchField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholderText attributes:
          @{
              NSForegroundColorAttributeName: color
          }];
    }
    else
    {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
}

-(void)initDefaultContent
{
    UIEdgeInsets imageEdgeInsets = UIEdgeInsetsMake(layout.padding,
                                                    (layout.paddedWidth - layout.buttonSize - layout.padding*6)/2,
                                                    layout.buttonSize + layout.padding,
                                                    (layout.paddedWidth - layout.buttonSize - layout.padding*6)/2);
    
    //buttonBackground = [[UIImageView alloc] initWithFrame:CGRectMake(layout.padding, 0, layout.paddedWidth, layout.buttonSize * 2)];
    //[buttonBackground setBackgroundColor:colour.fmwBackgroundColor];
    
    mapIcon = [[UIImageView alloc] initWithFrame:CGRectMake(layout.padding + layout.paddedWidth * 0.25 - layout.buttonSize/2 + layout.padding/2, layout.padding * 1.5, layout.buttonSize - layout.padding, layout.buttonSize - layout.padding)];
    [mapIcon setImage:[ImageUtil getImage:@"iconMap.png"]];
    [mapIcon setTintColor:colour.fmwActiveColor];
    mapButton = [[UIButton alloc] initWithFrame:CGRectMake(0, -layout.delimiterSize, layout.screenWidth/2 + layout.delimiterSize, layout.buttonSize * 2)];
    [mapButton setBackgroundColor:colour.clear];
    [mapButton setImageEdgeInsets:imageEdgeInsets];
    [mapButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [mapButton setTitle:NSLocalizedString(@"Find on map", nil) forState:UIControlStateNormal];
    [mapButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [mapButton setTitleEdgeInsets:UIEdgeInsetsMake(layout.buttonSize + layout.padding, layout.padding, layout.padding, layout.padding)];
    [mapButton addTarget:self action:@selector(useLocationOnMap) forControlEvents:UIControlEventTouchUpInside];
    CALayer *mapLayer = [mapButton layer];
    [mapLayer setBorderWidth:layout.delimiterSize];
    [mapLayer setBorderColor:colour.fmwBorderColor.CGColor];
    
    gpsIcon = [[UIImageView alloc] initWithFrame:CGRectMake(layout.padding + layout.paddedWidth * 0.75 - layout.buttonSize/2, layout.padding, layout.buttonSize, layout.buttonSize)];
    [gpsIcon setImage:[ImageUtil getImage:@"iconGPS.png"]];
    [gpsIcon setTintColor:colour.fmwActiveColor];
    gpsButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth/2, -layout.delimiterSize, layout.screenWidth/2, layout.buttonSize * 2)];
    [gpsButton setBackgroundColor:colour.clear];
    [gpsButton setImageEdgeInsets:imageEdgeInsets];
    [gpsButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [gpsButton setTitle:NSLocalizedString(@"Current Location", nil) forState:UIControlStateNormal];
    [gpsButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [gpsButton setTitleEdgeInsets:UIEdgeInsetsMake(layout.buttonSize + layout.padding, layout.padding, layout.padding, layout.padding)];
    [gpsButton addTarget:self action:@selector(useCurrentLocation) forControlEvents:UIControlEventTouchUpInside];
    CALayer *gpsLayer = [gpsButton layer];
    [gpsLayer setBorderWidth:layout.delimiterSize];
    [gpsLayer setBorderColor:colour.fmwBorderColor.CGColor];
    
    verticalLine = [[UIImageView alloc] initWithFrame:CGRectMake(layout.screenWidth/2, layout.padding, layout.delimiterSize, (layout.buttonSize - layout.padding) * 2)];
    [verticalLine setBackgroundColor:colour.fmwActiveColor];
    
    [self initSearchFieldListener];
    //[self initRecentPlaces];
    
    scrollView.contentSize = CGSizeMake(layout.screenWidth, layout.buttonSize * 2 + layout.padding + layout.buttonSize * recentPlaces.count);
}

-(void)addDefaultContent
{
    [scrollView addSubview:buttonBackground];
    
    [scrollView addSubview:mapIcon];
    [scrollView addSubview:mapButton];
    
    [scrollView addSubview:gpsIcon];
    [scrollView addSubview:gpsButton];
}

-(void)initRecentPlaces
{
    NSArray *recents = [PlacesUtil getLocations];// reverseObjectEnumerator] allObjects];
    if(recentPlaces.count > 0)
    {
        recentPlaces = nil;
        recentPlaces = [[NSMutableArray alloc] init];
    }
    
    for(SearchItem *item in recents)
    {
        [recentPlaces addObject:item];
    }
    
    [self addRecentsUI];
}

-(void)addRecentsUI
{
    if(searchField.text.length == 0)
    {
        for(UIView *view in scrollView.subviews)
        {
            [view removeFromSuperview];
        }
        
        [self addDefaultContent];
        
        int i = 0;
        for(SearchItem *item in recentPlaces)
        {
            SearchListItem *listItem = [[SearchListItem alloc] initWithFrame:CGRectMake(0, i * itemHeight + layout.buttonSize*2 + layout.delimiterSize, layout.screenWidth, itemHeight)];
            [listItem initView:appDelegate searchItem:item height:itemHeight];
            [listItem setItemTag:i];
            [listItem.button addTarget:self action:@selector(selectRecentItem:) forControlEvents:UIControlEventTouchUpInside];
            [listItem.mapButton addTarget:self action:@selector(showRecentOnMap:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:listItem];
            i++;
        }
        scrollView.contentSize = CGSizeMake(layout.screenWidth, layout.buttonSize * (4 +  recentPlaces.count) + layout.padding);
    }
}

-(void)selectRecentItem:(id)sender
{
    UIButton *button = (UIButton*)sender;
    [button setBackgroundColor:colour.clear];
    
    NSInteger tag = ((UIButton*)sender).tag;
    SearchItem *selectedItem = (SearchItem*)[recentPlaces objectAtIndex:tag];
    [self updateFavouritesAndRecents:selectedItem];
    
    [self XLog:[NSString stringWithFormat:@"Selected Item [%ld]: %@", (long)tag, selectedItem.location.address]];
    [appDelegate.tripQuery setPoint:selectedItem.location];
    [PlacesUtil storeLocation:selectedItem];
    if(appDelegate.searchConfig.state == TripsScreen)
    {
        appDelegate.tripQuery.refreshingRouteOptions = YES;
    }
    [self backbuttonPressed];
    //[((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwScreen];
}

-(void)addUIToView
{
    [self addSubview:headerBackground];
    [self addSubview:searchField];
    [self addSubview:backButton];
    [self addSubview:horizontalLine];
    
    [self addDefaultContent];
    [self addSubview:scrollView];
}

-(void)animateIn
{
    [super animateIn];
    [self addNotificationListeners];
    [self contextualiseNavigationBar];
    
    [parentView.view addSubview:self];
    self.center = centerOffScreen;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOnScreen;
     }
                     completion:^(BOOL finished)
     {
         [searchField becomeFirstResponder];
         [self initRecentPlaces];
     }];
}

-(void)animateOut
{
    [super animateOut];
    [self removeNotificationListeners];
    [self resetNavigationBar];
    
    self.center = centerOnScreen;
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOffScreen;
     }
                     completion:^(BOOL finished){
                         [searchField resignFirstResponder];
                         [self removeFromSuperview];
                     }];
}

-(void)contextualiseNavigationBar
{
//    parentView.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemOrganize target:self action:nil];
}

-(void)resetNavigationBar
{
    parentView.navigationItem.rightBarButtonItem = nil;
}

// LOGIC

-(void)useCurrentLocation
{
    [self XLog:@"Using Current Location"];
    if(![appDelegate.locationUtil isLocationAvailable])
    {
        [self showAlertView:NSLocalizedString(@"Location Services", nil)
                    message:NSLocalizedString(@"To use your location, enable location services for Findmyway in the device settings.\n\nOtherwise search for a location.", nil)
                     button:NSLocalizedString(@"Ok", nil)];
        return;
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.locationUtil.userLocation.latitudeString] forKey:@"latitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.locationUtil.userLocation.longitudeString] forKey:@"longitude"];
    
    [gpsIcon removeFromSuperview];
    [self startLoader:CGPointMake(layout.screenWidth / 4 - layout.delimiterSize, layout.buttonSize - layout.padding/2) inView:gpsButton];
    
    
     NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:ReverseGeocode callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
     {
        NSLog(@"API - ReverseGeocode: %ld", (long)response.statusCode);
        [self handleApiError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode];
        [self stopLoader];
        [mapButton addSubview:mapIcon];
         
        [appDelegate.apiResponse addAddress:[JSONParser convertJSONToDictionary:bodyString]];
        [appDelegate.tripQuery setPoint:appDelegate.apiResponse.location];
         if(appDelegate.searchConfig.state == TripsScreen)
         {
             appDelegate.tripQuery.refreshingRouteOptions = YES;
         }
        [self backbuttonPressed];
         
     }
     failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
     {
         NSLog(@"API CONNECTION FAILURE - ReverseGeocode: %@", error);
         [self stopLoader];
         [mapButton addSubview:mapIcon];
     }
     didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
     {
         // This is not going to be called in this example but it's included for completeness.
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
     }];
}

-(void)useLocationOnMap
{
    [self XLog:@"Using Map Location"];
    [((AppViewController*)parentView).fmwMapView setState:PointSelection];
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwMapView];
}

// SEARCH

-(void)initSearchFieldListener
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(searchFieldListener)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:searchField];
    
    //[self addTextFieldChangeListener];
}

-(void)searchFieldListener
{
    [self softsearchTextFieldChanged];
}

-(void)softsearchTextFieldChanged
{
    if(searchField.text.length > 0)
    {
        [self startSoftSearchTimer];
        [self addSubview:cancelButton];
    }
    else
    {
        [self cancelSearch];
    }
}

-(void)startSoftSearchTimer
{
    if(searchTimer != (id)[NSNull null])
    {
        [searchTimer invalidate];
        searchTimer = nil;
    }
    searchTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(initSoftSearch) userInfo:nil repeats:NO];
}

-(void)initSoftSearch
{
    if(searchField.text.length > 0)
    {
        [searchIcon removeFromSuperview];
        [self startLoader:CGPointMake(layout.buttonSize * 0.4, layout.buttonSize/2 - layout.padding/2) inView:searchField];
        NSLog(@"Soft Searching: %@", searchField.text);
        
        NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
        [parameters setValue:[URLString encodeString:searchField.text] forKey:@"searchText"];
        
        if(appDelegate.locationUtil.isLocationEnabled)
        {
            [parameters setValue:appDelegate.locationUtil.userLocation.latitudeString forKey:@"latitude"];
            [parameters setValue:appDelegate.locationUtil.userLocation.longitudeString forKey:@"longitude"];
        }
        canceledSearch = NO;
        NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:SearchItems callType:GET parameters:parameters];
        JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
        [connection executeRequestOnSuccess:
            ^(NSHTTPURLResponse *response, NSString *bodyString)
            {
                if(!canceledSearch)
                {
                    NSLog(@"API - SearchItems: %ld", (long)response.statusCode);
                    [self handleApiError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode];

                    [appDelegate.apiResponse addSearchItems:[JSONParser convertJSONToDictionary:bodyString]];
                    [self updateSearchItems];
                }
            }
            failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
            {
                if(!canceledSearch)
                {
                     NSLog(@"API CONNECTION FAILURE - SearchItems: %@", error);
                    [self stopLoader];
                    [searchField addSubview:searchIcon];
                    [self handleError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode error:error];
                }
            }
            didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
            {
                // This is not going to be called in this example but it's included for completeness.
                NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
            }];
    }
}

-(void)updateSearchItems
{
    [self stopLoader];
    [searchField addSubview:searchIcon];
    for(UIScrollView *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    searchItems = [[NSMutableArray alloc] init];
    int index = 0;

    for(SearchItem *item in appDelegate.apiResponse.searchItems)
    {
        SearchListItem *listItem = [[SearchListItem alloc] initWithFrame:CGRectMake(0, index*(itemHeight), layout.paddedWidth, itemHeight)];
        
        [listItem initView:appDelegate searchItem:item height:itemHeight];
        [listItem setItemTag:index];
        [listItem.button addTarget:self action:@selector(selectItem:) forControlEvents:UIControlEventTouchUpInside];
        [listItem.mapButton addTarget:self action:@selector(showItemOnMap:) forControlEvents:UIControlEventTouchUpInside];
        
        [searchItems addObject:listItem];
        [scrollView addSubview:listItem];
        
        index++;
    }
    
    scrollView.contentSize = CGSizeMake(layout.screenWidth, searchItems.count * itemHeight);
}

-(void)removeSearchItems
{
    [self stopLoader];
    for(UIView *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self addDefaultContent];
}

-(void)cancelSearch
{
    [searchField addSubview:searchIcon];
    [searchField setText:@""];
    [cancelButton removeFromSuperview];
    canceledSearch = YES;
    [self stopLoader];
    
    for(UIView *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    //[self addDefaultContent];
    [self initRecentPlaces];
    [scrollView setContentOffset:CGPointMake(0, 0)];
}

-(void)backbuttonPressed
{
    if(appDelegate.searchConfig.state == HomeScreen)
    {
        [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwScreen];
    }
    
    if(appDelegate.searchConfig.state == TripsScreen)
    {
        [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwRouteOptions];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [searchField resignFirstResponder];
    return YES;
}

// SEARCH ITEM

-(void)showRecentOnMap:(id)sender
{
    NSInteger tag = ((UIButton*)sender).tag;
    SearchItem *searchItem = (SearchItem*)[recentPlaces objectAtIndex:tag];
    AppViewController *viewController = ((AppViewController*)parentView);
    [viewController.fmwMapView setState:PointPreview];
    [viewController.fmwMapView setSearchItemForPreview:searchItem];
    [viewController swapInView:viewController.fmwMapView];
    [self XLog:[NSString stringWithFormat:@"Showing on Map [%ld]: %@", (long)tag, searchItem.location.address]];
}

-(void)showItemOnMap:(id)sender
{
    NSInteger tag = ((UIButton*)sender).tag;
    SearchListItem *selectedItem = (SearchListItem*)[searchItems objectAtIndex:tag];
    AppViewController *viewController = ((AppViewController*)parentView);
    [viewController.fmwMapView setState:PointPreview];
    [viewController.fmwMapView setSearchItemForPreview:selectedItem.searchItem];
    [viewController swapInView:viewController.fmwMapView];
    [self XLog:[NSString stringWithFormat:@"Showing on Map [%ld]: %@", (long)tag, selectedItem.searchItem.location.address]];
}

-(IBAction)highlightItem:(id)sender
{
    UIButton *button = (UIButton*)sender;
    [button setBackgroundColor:colour.fmwActiveColorLight];
}

-(IBAction)selectItem:(id)sender
{
    UIButton *button = (UIButton*)sender;
    [button setBackgroundColor:colour.clear];
    
    NSInteger tag = ((UIButton*)sender).tag;
    SearchListItem *selectedItem = (SearchListItem*)[searchItems objectAtIndex:tag];
    [self updateFavouritesAndRecents:selectedItem.searchItem];
    [self XLog:[NSString stringWithFormat:@"Selected Item [%ld]: %@", (long)tag, selectedItem.searchItem.location.address]];
    [appDelegate.tripQuery setPoint:selectedItem.searchItem.location];
    [PlacesUtil storeLocation:selectedItem.searchItem];
    if(appDelegate.searchConfig.state == TripsScreen)
    {
        appDelegate.tripQuery.refreshingRouteOptions = YES;
    }
    [self backbuttonPressed];
    //[((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwScreen];
}

-(void)updateFavouritesAndRecents:(SearchItem*)selectedItem
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    //[[recentPlaces reverseObjectEnumerator] allObjects];
    [array addObject:selectedItem];
    for(SearchItem *item in recentPlaces)
    {
        if(![item isEqualTo:selectedItem])
        {
            [array addObject:item ];
        }
    }
    //[[array reverseObjectEnumerator] allObjects];
    
    [PlacesUtil setLocations:(NSArray*)array];
}

@end
