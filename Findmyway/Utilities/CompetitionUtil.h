//
//  CompetitionUtil.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/12.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface CompetitionUtil : NSObject

-(id)init;
+(id)instance;

+(BOOL)isEarthHourTime;
+(BOOL)isBeforeEarthHour;
+(BOOL)isAfterEarthHour;
+(BOOL)isCompeting;
+(BOOL)isCompetitionOperator:(NSString*)operatorName;
+(float)operatorWeighting:(NSString*)operatorName;
+(void)setCompetitionMember1:(NSString*)member1 member2:(NSString*)member2;
+(BOOL)logJourney:(NSString*)startLocation end:(NSString*)endLocation dateTime:(NSString*)dateTime points:(int)points;

+(void)setExitedCompetition:(BOOL)exitCompetition;
+(BOOL)hasExitedCompetition;
@end
