//
//  ApiError.m
//  tct
//
//  Created by Bilo on 10/10/14.
//

#import "ApiError.h"

@implementation ApiError

-(id)initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if(self)
    {
        self.errorCode = (int)[[dictionary objectForKey:@"errorCode"] floatValue];
        self.message = [dictionary objectForKey:@"message"];
        self.isAuthenticating = [[dictionary objectForKey:@"authRequired"] boolValue];
    }
    return self;
}
@end
