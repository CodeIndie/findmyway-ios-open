//
//  XUIView.h
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ApiError.h"
#import "HZActivityIndicatorView.h"
#import "ImageUtil.h"
#import "XUIShape.h"
#import "XUIViewDelegate.h"
#import "XUINotification.h"
#import "XUIButton.h"
#import "XUILabel.h"
#import "XUILoader.h"

@class XUIViewController;

@interface XUIView : UIView
{
    AppDelegate *appDelegate;
    LayoutConfig *layout;
    ColourConfig *colour;
    DeviceConfig *device;
    ApiError *apiError;
    
    UIViewController *parentView;
    CGPoint centerOnScreen;
    CGPoint centerOffScreen;
    float animationInterval;

    float transitionDuration;
    float delay;
    BOOL initialised;
    BOOL isAnimating;
    
    UIActivityIndicatorView *uiLoader;
    HZActivityIndicatorView *activityIndicator;
    XUINotification *notification;
    XUILoader *xuiLoader;
    
    UIAlertView *alertViewPopup;
    UIScrollView *scrollableView;
}

@property NSString *viewName;
@property BOOL isActive;

@property CGPoint oldCenter;
@property CGPoint newCenter;

@property float oldAlpha;
@property float newAlpha;

+(CAGradientLayer*)gradientInView:(UIView*)view gradientStart:(UIColor*)start gradientEnd:(UIColor*)end;

-(float)width;
-(float)height;

-(id)initWithFrame:(CGRect)frame;

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;

-(void)addNotificationListeners;
-(void)removeNotificationListeners;

-(void)animateIn;
-(void)animateOut;
-(void)updateUI;

-(float)getDelay:(int)layerIndex;
-(void)XLog:(NSString*)logString;

-(void)startLoader:(CGPoint)point inView:(UIView*)iew;
-(void)stopLoader;

-(void)showAlertView:(NSString*)title message:(NSString*)message buttons:(NSArray*)buttons;

-(void)showAlertView:(NSString *)title message:(NSString *)message button:(NSString *)button;

-(void)showAlertView:(NSString *)title message:(NSString *)message button1:(NSString*)button1 button2:(NSString*)button2;
-(void)hideAlertView;

-(void)showNotification:(NSString*)message inView:(UIView*)view;
-(void)showNotification:(NSString*)title message:(NSString*)message buttons:(NSArray*)buttons;
-(void)hideNotification;

-(void)handleApiError:(NSDictionary*)errorDictionary statusCode:(int)statusCode;

-(void)handleError:(NSDictionary*)errorDictionary statusCode:(int)statusCode error:(NSError*)error;

@end
