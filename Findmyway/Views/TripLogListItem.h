//
//  TripLogListItem.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/11.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TripLogItem.h"
#import "XUIShape.h"

@interface TripLogListItem : UIView
{
    AppDelegate *appDelegate;
    UILabel *points;
    UIView *circle;
    UILabel *locationName;
    UILabel *savedDateTime;
}

-(id)initWithFrame:(CGRect)frame item:(TripLogItem*)item;

@end
