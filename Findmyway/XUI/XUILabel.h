//
//  XUILabel.h
//  Findmyway
//
//  Created by Bilo on 4/15/15.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XUILabel : UILabel

+(XUILabel*)initWithFrame:(CGRect)frame text:(NSString*)text textColor:(UIColor*)textColor backgroundColor:(UIColor*)backgroundColor textAlignment:(NSTextAlignment)textAlignment;
+(XUILabel*)initWithFrame:(CGRect)frame text:(NSString*)text fontSize:(float)fontSize bold:(BOOL)bold textColor:(UIColor*)textColor backgroundColor:(UIColor*)backgroundColor textAlignment:(NSTextAlignment)textAlignment;
@end
