//
//  LayoutConfig.h
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"

@interface LayoutConfig : NSObject

@property float topOfContent;
@property float buttonSize;
@property float margin;
@property float padding;
@property float delimiterSize;

@property float systemFontSize;
@property float paddedWidth;
@property float marginWidth;

@property float screenWidth;
@property float screenHeight;
@property float aspectRatio;

-(id)init:(NSDictionary*)configDictionary;
@end
