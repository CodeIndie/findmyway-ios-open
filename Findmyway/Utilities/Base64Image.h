//
//  Base64Image.h
//  FindMyWayFinal
//
//  Created by Kyle Bradley on 7/22/13.
//  Copyright (c) 2013 FindMyWay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
//Used for converting image strings into images


@interface Base64Image : NSObject
+ (UIColor *) colorWithHexString: (NSString *) stringToConvert;
+ (NSData *)base64DataFromString: (NSString *)string;
@end
