//
//  XUIToolbarButton.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "XUIToolbarButton.h"

@implementation XUIToolbarButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithIndex:(int)index imageName:(NSString *)imageName title:(NSString *)title
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    layout = appDelegate.layout;
    colour = appDelegate.colour;
    float padding = appDelegate.layout.padding;
    float fontSize = [UIFont systemFontSize] - 2;
    float toolbarItemWidth = layout.screenWidth / 5;
    float toolbarButtonOffset = (toolbarItemWidth - layout.buttonSize)/2;
    
    self = [super init];//WithFrame:CGRectMake(toolbarItemWidth * index, 0, toolbarItemWidth, layout.buttonSize)];
    if(self)
    {
        self.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png", imageName]];
        self.activeImage = [UIImage imageNamed:[NSString stringWithFormat:@"%@%@", imageName, @"Blue.png"]];
        
        self.button = [[UIButton alloc] initWithFrame:CGRectMake(toolbarButtonOffset + toolbarItemWidth * index, 0, layout.buttonSize, layout.buttonSize)];
        [self.button setImage:self.image forState:UIControlStateNormal];
        [self.button setImageEdgeInsets:UIEdgeInsetsMake(padding * 0.5, padding, padding * 1.5, padding)];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(toolbarItemWidth * index, layout.buttonSize * 2/3, layout.buttonSize * 1.5, layout.buttonSize/3)];
        [self.label setTextAlignment:NSTextAlignmentCenter];
        [self.label setText:title];
        [self.label setFont:[UIFont systemFontOfSize:fontSize]];
        [self.label setTextColor:colour.fmwTextColorLight];
        
        [self addSubviewsToView];
    }
    return self;
}

-(void)addSubviewsToView
{
    [self addSubview:self.label];
    [self addSubview:self.button];
}

-(void)setActive:(BOOL)isActive
{
    self.isActive = isActive;
    if(!self.isActive)
    {
        [self.button setImage:self.image forState:UIControlStateNormal];
        [self.label setTextColor:colour.fmwTextColorLight];
    }
    else
    {
        [self.button setImage:self.activeImage forState:UIControlStateNormal];
        [self.label setTextColor:colour.fmwTextColorLight];
    }
    
}

@end
