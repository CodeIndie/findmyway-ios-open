//
//  DefaultsUtil.h
//  tct
//
//  Created by Bilo on 5/27/14.
//

#import <Foundation/Foundation.h>

@interface DefaultsUtil : NSObject
{
    NSUserDefaults *defaults;
}

-(id)init;
+(id)instance;
-(id)GetObjectForKey:(NSString*)key;
-(void)SetObject:(NSString*)value ForKey:(NSString*)key;
-(id)GetDataForKey:(NSString*)key;
-(void)SetData:(NSData*)data ForKey:(NSString*)key;
-(void)SetString:(NSString*)string ForKey:(NSString*)key;
-(NSString*)GetStringForKey:(NSString*)key;
-(void)ResetToRegistrationDomain;
-(void)RemoveObjectForKey:(NSString*)key;

-(NSArray*)GetArrayForKey:(NSString*)key;
-(void)SetArray:(NSArray*)array ForKey:(NSString*)key;
@end
