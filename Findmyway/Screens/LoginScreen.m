//
//  LoginScreen.m
//  Findmyway
//
//  Created by Bilo on 2/9/15.
//

#import "LoginScreen.h"

@implementation LoginScreen

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(device.screenWidth * 1.5, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth/2, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    y = layout.margin;
    cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding, y, layout.buttonSize * 2, layout.buttonSize)];
    [cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    [cancelButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    cancelButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    
    signInLabel = [[UILabel alloc] initWithFrame:CGRectMake(layout.buttonSize * 2, y, layout.screenWidth - 4 * layout.buttonSize, layout.buttonSize)];
    [signInLabel setText:NSLocalizedString(@"Sign-In", nil)];
    [signInLabel setTextColor:colour.fmwTextColor];
    [signInLabel setTextAlignment:NSTextAlignmentCenter];
    
    y += layout.buttonSize;
    topLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.delimiterSize)];
    [topLine setBackgroundColor:colour.fmwTextColor];
    
    
    y += layout.padding;
    scrollableView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.screenHeight - y)];
    
    y = 0;
    explanationLabel = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding, y, layout.paddedWidth, 4 * layout.buttonSize)];
    [explanationLabel setTextColor:colour.fmwTextColorLight];
    [explanationLabel setText:NSLocalizedString(@"Sign-In Description", nil)];
    [explanationLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 1]];
    [explanationLabel setTextAlignment:NSTextAlignmentJustified];
    [explanationLabel setNumberOfLines:0];
    
    y += layout.buttonSize * 4 + layout.margin;
    facebookLogin = [[LoginButton alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 1.5) name:@"facebook" image:@"iconFacebook.png" colour:colour.facebook layout:layout];

    y+= layout.buttonSize + layout.margin;
    twitterLogin = [[LoginButton alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 1.5) name:@"twitter" image:@"iconTwitter.png" colour:colour.twitter layout:layout];
    
    y+= layout.buttonSize + layout.margin;
    googleLogin = [[LoginButton alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 1.5) name:@"google" image:@"iconGoogle.png" colour:colour.google layout:layout];
    
    y+= layout.buttonSize + layout.margin;
    emailLogin = [[LoginButton alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 1.5) name:@"email" image:@"iconEmail.png" colour:colour.fmwTextColor layout:layout];
    
    y+= layout.buttonSize + layout.margin;
}

-(void)addUIToView
{
    [self addSubview:cancelButton];
    [self addSubview:signInLabel];
    [self addSubview:topLine];
    
//    [self addSubview:explanationLabel];
    
//    [self addSubview:facebookLogin];
//    [self addSubview:twitterLogin];
//    [self addSubview:googleLogin];
//    [self addSubview:emailLogin];
    
    [scrollableView addSubview:explanationLabel];
    
    [scrollableView addSubview:facebookLogin];
    [scrollableView addSubview:twitterLogin];
    [scrollableView addSubview:googleLogin];
    [scrollableView addSubview:emailLogin];
    
    [scrollableView setContentSize:CGSizeMake(layout.screenWidth, y + layout.buttonSize + layout.margin)];
    [self addSubview:scrollableView];
}

-(void)animateIn
{
    [super animateIn];
    [self addNotificationListeners];
    
    [parentView.view addSubview:self];
    self.center = centerOffScreen;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOnScreen;
     }
     completion:^(BOOL finished)
     {
     }];
}

-(void)animateOut
{
    [super animateOut];
    [self removeNotificationListeners];
    
    self.center = centerOnScreen;
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOffScreen;
     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                     }];
}

@end
