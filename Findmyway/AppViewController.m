//
//  AppViewController.m
//  Findmyway
//
//  Created by Bilo on 11/24/14.
//

#import "AppViewController.h"

@interface AppViewController ()

@end

@implementation AppViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialise];
    [self initViews];
    [self initWithView:self.startupScreen];
    //[self initWithView:self.loginScreen];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialise
{
    [self.navigationController.navigationBar setBarTintColor:appDelegate.colour.fmwHeader];
    [self.navigationController.navigationBar setAlpha:1];
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : colour.fmwActiveColor}];
    
    appDelegate.layout.topOfContent = self.navigationController.navigationBar.bounds.size.height + self.navigationController.navigationBar.bounds.origin.y + appDelegate.layout.margin - layout.delimiterSize * 2;
    
    backgroundImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, appDelegate.layout.screenWidth, appDelegate.layout.screenHeight)];
    //[backgroundImage setImage:[UIImage imageNamed:@"Background"]];
    [self.view addSubview:backgroundImage];
    [self initBackButton];
    NSString *token = [[DefaultsUtil instance] GetStringForKey:@"token"];
    NSLog(@"token: %@", token);
}

-(void)initBackButton
{
    UIImage *backButtonImage = [ImageUtil getImage:@"iconBack.png"];
    [self.view setBackgroundColor:colour.fmwHeader];
    float backButtonSize = layout.buttonSize;//self.navigationController.navigationBar.bounds.size.height;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:backButtonImage];
    imageView.frame = CGRectMake(0,0,backButtonSize, backButtonSize);
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    backButton.frame = CGRectMake(0,0,backButtonSize, backButtonSize);
    [backButton setImage:backButtonImage forState:UIControlStateNormal];
    [backButton.imageView setTintColor:colour.fmwActiveColor];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(layout.padding/2, layout.padding/2, layout.padding/2, layout.padding/2)];
    [backButton addTarget:self action:@selector(customBackAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
    customBarItem.title = @"";
    [[self navigationItem] setLeftBarButtonItem:customBarItem];
}

-(void)customBackAction
{
    [self XLog:@"Custom Back Action"];
    [self popView];
    //[self.navigationController popViewControllerAnimated:YES];
}

-(void)initViews
{
    // STARTUP SCREEN
    self.startupScreen = [[StartupScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
    [self.startupScreen initView:@"Startup" inController:self];
    
    // LOGIN SCREEN
    self.loginScreen = [[LoginScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
    [self.loginScreen initView:@"Login Screen" inController:self];
    
    // FMW SCREEN
    self.fmwScreen = [[FmwScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
    [self.fmwScreen initView:@"FindMyWay" inController:self];
    
    // SEARCH SCREEN
    self.searchScreen = [[SearchScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
    [self.searchScreen initView:@"Search" inController:self];
    
    // MODE PICKER
    self.fmwModePicker = [[FmwModePicker alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.buttonSize)];
    [self.fmwModePicker initView:@"" inController:self];
    
    // MAIN MENU
    self.mainMenu = [[MainMenu alloc] initWithFrame:CGRectMake(0, layout.topOfContent + layout.buttonSize, layout.screenWidth, layout.screenHeight - layout.topOfContent - layout.buttonSize)];
    [self.mainMenu initView:@"Main Menu" inController:self];
    
    // OPERATORS LIST
    self.operatorList = [[OperatorList alloc] initWithFrame:CGRectMake(0, layout.topOfContent + layout.buttonSize, layout.screenWidth, layout.screenHeight - layout.topOfContent - layout.buttonSize)];
    [self.operatorList initView:@"Operator List" inController:self];
    
    // FMW ROUTE OPTIONS
    self.fmwRouteOptions = [[FmwRouteOptions alloc] initWithFrame:CGRectMake(0, layout.topOfContent, device.screenWidth, device.screenHeight - layout.topOfContent)];
    [self.fmwRouteOptions initView:@"Route Options" inController:self];
    
    // FMW JOURNEY SCREEN
    self.fmwJourneyScreen = [[FmwJourneyScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, device.screenWidth, device.screenHeight - layout.topOfContent)];
    [self.fmwJourneyScreen initView:@"Journey Mode" inController:self];
    
    // FMW MAP VIEW
    self.fmwMapView = [[FmwMapView alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
    [self.fmwMapView initView:@"Map" inController:self];

    // JOURNEY OVERVIEW
    self.journeyOverviewScreen = [[JourneyOverviewScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
    [self.journeyOverviewScreen initView:@"Journey Overview" inController:self];
    
    // TIME PICKER
    self.timePicker = [[TimePicker alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
    [self.timePicker initView:@"" inController:self];
    
    // WWF EARTH HOUR COMPETITION
    self.competitionTripsView = [[CompetitionTripsScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, device.screenWidth, device.screenHeight - layout.topOfContent)];
    [self.competitionTripsView initView:@"Competition Trips" inController:self];
    
    // TRANSIT MAP SCREEN
    self.transitMapScreen = [[TransitMapScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
    [self.transitMapScreen initView:@"Transit Map Screen" inController:self];
    
    // ANNOUCNEMENT SCREEN
    self.announcementScreen = [[AnnouncementScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
    [self.announcementScreen initView:@"Announcements Screen" inController:self];
}

-(void)initView:(XUIView*)view
{
    if(view != nil)
    {
        return;
    }
    
    if([view isEqual:self.fmwScreen])//[view isKindOfClass:(id)[FmwScreen class]])//view == self.fmwScreen)//
    {
        // FMW SCREEN
        self.fmwScreen = [[FmwScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
        [self.fmwScreen initView:@"FindMyWay" inController:self];
    }
    else if([view isEqual:self.startupScreen])//[view isKindOfClass:(id)[StartupScreen class]])//view == self.startupScreen)//
    {
        // STARTUP SCREEN
        self.startupScreen = [[StartupScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
        [self.startupScreen initView:@"Startup" inController:self];
    }
    else if([view isKindOfClass:(id)[LoginScreen class]])
    {
        // LOGIN SCREEN
        self.loginScreen = [[LoginScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
        [self.loginScreen initView:@"Login Screen" inController:self];
    }
    else if([view isKindOfClass:(id)[SearchScreen class]])
    {
        // SEARCH SCREEN
        self.searchScreen = [[SearchScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
        [self.searchScreen initView:@"Search" inController:self];
    }
    else if([view isKindOfClass:(id)[FmwModePicker class]])
    {
        // MODE PICKER
        self.fmwModePicker = [[FmwModePicker alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.buttonSize)];
        [self.fmwModePicker initView:@"" inController:self];
    }
    else if([view isKindOfClass:[MainMenu class]])
    {
        // MAIN MENU
        self.mainMenu = [[MainMenu alloc] initWithFrame:CGRectMake(0, layout.topOfContent + layout.buttonSize, layout.screenWidth, layout.screenHeight - layout.topOfContent - layout.buttonSize)];
        [self.mainMenu initView:@"Main Menu" inController:self];
    }
    else if([view isKindOfClass:[OperatorList class]])
    {
        // OPERATORS LIST
        self.operatorList = [[OperatorList alloc] initWithFrame:CGRectMake(0, layout.topOfContent + layout.buttonSize, layout.screenWidth, layout.screenHeight - layout.topOfContent - layout.buttonSize)];
        [self.operatorList initView:@"Operator List" inController:self];
    }
    else if([view isKindOfClass:[FmwRouteOptions class]])
    {
        // FMW ROUTE OPTIONS
        self.fmwRouteOptions = [[FmwRouteOptions alloc] initWithFrame:CGRectMake(0, layout.topOfContent, device.screenWidth, device.screenHeight - layout.topOfContent)];
        [self.fmwRouteOptions initView:@"" inController:self];
    }
    else if([view isKindOfClass:[FmwJourneyScreen class]])
    {
        // FMW JOURNEY SCREEN
        self.fmwJourneyScreen = [[FmwJourneyScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, device.screenWidth, device.screenHeight - layout.topOfContent)];
        [self.fmwJourneyScreen initView:@"Journey Mode" inController:self];
    }
    else if([view isKindOfClass:[FmwMapView class]])
    {
        // FMW MAP VIEW
        self.fmwMapView = [[FmwMapView alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
        [self.fmwMapView initView:@"Map" inController:self];
    }
    else if([view isKindOfClass:[JourneyOverviewScreen class]])
    {
        // JOURNEY OVERVIEW
        self.journeyOverviewScreen = [[JourneyOverviewScreen alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
        [self.journeyOverviewScreen initView:@"Journey Overview" inController:self];
    }
    else if([view isKindOfClass:[TimePicker class]])
    {
        // TIME PICKER
        self.timePicker = [[TimePicker alloc] initWithFrame:CGRectMake(0, layout.topOfContent, layout.screenWidth, layout.screenHeight - layout.topOfContent)];
        [self.timePicker initView:@"" inController:self];
    }
}

-(void)initWithView:(XUIView *)xuiView
{
    //[self initView:xuiView];
    [super initWithView:xuiView];
}

-(void)swapInView:(XUIView *)xuiView
{
    //[self initView:xuiView];
    [super swapInView:xuiView];
}
@end
