//
//  OperatorList.h
//  Findmyway
//
//  Created by Bilo on 2/13/15.
//

#import "XUIView.h"
#import "AppViewController.h"
#import "Operator.h"
#import "OperatorListItem.h"

@interface OperatorList : XUIView
{
    UIImageView *background;
    //UIButton *blackBackButton;
    UIScrollView *scrollView;
    NSMutableArray *operators;
    UILabel *privateOperatorLabel;
    UILabel *publicOperatorLabel;
}

@property NSMutableArray *operatorListItems;

-(void)addOperators;
-(void)checkModesAgainsOperators;
-(NSMutableArray*)excludedOperators;

@end
