//
//  SearchItem.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/08.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchItem.h"
#import "AppDelegate.h"
#import "ImageUtil.h"

@interface SearchListItem : UIView

@property SearchItem *searchItem;
@property UIImageView *icon;
@property UIImageView *horizontalLine;
@property UIButton *button;
@property UIButton *mapButton;

-(void)initView:(AppDelegate*)appDelegate searchItem:(SearchItem*)searchItem;
-(void)setItemTag:(int)index;
@end
