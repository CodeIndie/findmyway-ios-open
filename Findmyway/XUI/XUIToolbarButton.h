//
//  XUIToolbarButton.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface XUIToolbarButton : UIView
{
    LayoutConfig *layout;
    ColourConfig *colour;
}
@property UIButton *button;
@property UILabel *label;
@property UIImage *image;
@property UIImage *activeImage;
@property BOOL isActive;

-(id)initWithIndex:(int)index imageName:(NSString*)imageName title:(NSString*)title;

-(void)setActive:(BOOL)isActive;
@end
