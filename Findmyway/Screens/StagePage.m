//
//  StagePage.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/09.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "StagePage.h"

@implementation StagePage

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        
    }
    return self;
}

-(void)setIndex:(NSUInteger)stageIndex stageCount:(NSUInteger)stageCount stage:(MultimodalStage*)multimodalStage appDelegate:(AppDelegate*)appDelegate
{
    LayoutConfig *layout = appDelegate.layout;
    
    index = stageIndex;
    count = stageCount;
    stage = multimodalStage;
    
    modeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(layout.margin, layout.margin, 2 * layout.buttonSize, 2 * layout.buttonSize)];
    [modeIcon setImage:[self imageForMode:stage.modeType]];
    
    operatorName = [[UILabel alloc] initWithFrame:CGRectMake(layout.margin + 2 * layout.buttonSize, layout.margin, 3 * layout.buttonSize, layout.buttonSize)];
    [operatorName setText:stage.modeName];
    [operatorName setTextColor:[UIColor whiteColor]];
    [self setBackgroundColor:stage.colour];
    
    
    [self drawGraphic:layout];
    
    
    UIColor *titleColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:0.5];
    
    float y = layout.buttonSize * 3 + layout.margin;
    stageStart = [[TitledLabel alloc] initWithDelegate:appDelegate frame:CGRectMake(layout.margin, y, layout.marginWidth/2, layout.buttonSize) title:stage.startLocation.name contentLabel:[DateTimeUtil parseDateTime:stage.startLocation.time].timeString alignment:NSTextAlignmentLeft color:[UIColor whiteColor] titleColor:titleColor];
    
    stageEnd = [[TitledLabel alloc] initWithDelegate:appDelegate frame:CGRectMake(layout.screenWidth/2, y, layout.marginWidth/2, layout.buttonSize) title:stage.endLocation.name contentLabel:[DateTimeUtil parseDateTime:stage.endLocation.time].timeString alignment:NSTextAlignmentRight color:[UIColor whiteColor] titleColor:titleColor];

    y += layout.buttonSize * 1.5;
    nextStop = [[TitledLabel alloc] initWithDelegate:appDelegate frame:CGRectMake(layout.margin, y, layout.marginWidth/2, layout.buttonSize) title:@"Next Stop" contentLabel:stage.startLocation.name alignment:NSTextAlignmentLeft color:[UIColor whiteColor] titleColor:titleColor];
    
    y += layout.buttonSize * 1.5;
    eta = [[TitledLabel alloc] initWithDelegate:appDelegate frame:CGRectMake(layout.margin, y, layout.marginWidth/2, layout.buttonSize) title:@"ETA to destination" contentLabel:[DateTimeUtil parseDateTime:stage.startLocation.time].timeString alignment:NSTextAlignmentLeft color:[UIColor whiteColor] titleColor:titleColor];
    
    y += layout.buttonSize * 1.5;
    stopsTillDestination = [[TitledLabel alloc] initWithDelegate:appDelegate frame:CGRectMake(layout.margin, y, layout.marginWidth/2, layout.buttonSize) title:@"Stops till destination" contentLabel:[NSString stringWithFormat:@"%lu",(unsigned long)stage.points.count] alignment:NSTextAlignmentLeft color:[UIColor whiteColor] titleColor:titleColor];
    
    [self addUIToView];
}

-(void)addUIToView
{
    [self addSubview:operatorName];
    [self addSubview:modeIcon];
    [self addSubview:stageStart];
    [self addSubview:stageEnd];
    [self addSubview:nextStop];
    [self addSubview:eta];
    [self addSubview:stopsTillDestination];
}

-(void)drawGraphic:(LayoutConfig*)layout
{
    float x = index == 0 ? layout.margin : 0;
    float width = index == count - 1 ? layout.screenWidth - layout.margin : layout.screenWidth;
    
    UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(x, layout.buttonSize * 3, width, layout.delimiterSize)];
    [line setBackgroundColor:[UIColor whiteColor]];
    [self addSubview:line];
    
    float largeSize = layout.margin;
    float smallSize = layout.padding;
    float gapSize = ((layout.screenWidth - 4 * layout.margin - layout.padding)/(stage.points.count-2));
    
    // FIRST STOP
    CGPoint firstOrigin = CGPointMake(layout.margin, layout.buttonSize * 3 - layout.margin/2);
    if(index == 0)
    {
        // FIRST STOP IN TRIP
        NSMutableArray *points = [[NSMutableArray alloc] init];
        [points addObject:[NSValue valueWithCGPoint:CGPointMake(0, 0)]];
        [points addObject:[NSValue valueWithCGPoint:CGPointMake(0, largeSize)]];
        [points addObject:[NSValue valueWithCGPoint:CGPointMake(largeSize * (2.0/3.0), largeSize/2)]];
        XUIShape *firstStop = [[XUIShape alloc] initWithFrame:CGRectMake(layout.margin, layout.buttonSize * 3 - layout.margin + largeSize/2, largeSize * (2.0/3.0) + 0.5, largeSize)];
        [firstStop polygonWithPoints:points strokeWidth:layout.delimiterSize strokeColor:[UIColor whiteColor] backgroundColor:stage.colour];
        [self addSubview:firstStop];
    }
    else
    {
        // FIRST STOP IN STAGE
        UIView *firstStop = [XUIShape circle:firstOrigin size:largeSize borderWidth:layout.delimiterSize fillColor:stage.colour strokeColor:[UIColor whiteColor]];
        [self addSubview:firstStop];
    }
    
    // LAST STOP
    CGPoint lastOrigin = CGPointMake(layout.screenWidth - 2*layout.margin, layout.buttonSize * 3 - layout.margin/2);
    UIView *lastStop = (index == count - 1) ? [XUIShape square:lastOrigin size:largeSize borderWidth:layout.delimiterSize fillColor:stage.colour strokeColor:[UIColor whiteColor]] : [XUIShape circle:lastOrigin size:largeSize borderWidth:layout.delimiterSize fillColor:stage.colour strokeColor:[UIColor whiteColor]];
    [self addSubview:lastStop];
    
    // INTERMEDIATE STOPS
    for(int i = 1 ; i < stage.points.count - 1 ; i++)
    {
        CGPoint origin = CGPointMake(layout.margin * 2 - layout.padding + i * gapSize, layout.buttonSize * 3 - layout.padding/2);
        UIView *circle = [XUIShape circle:origin size:smallSize borderWidth:layout.delimiterSize fillColor:stage.colour strokeColor:[UIColor whiteColor]];
        [self addSubview:circle];
    }
}

-(UIImage*)imageForMode:(NSString*)mode
{
    UIImage *modeImage;
    if([[mode uppercaseString] isEqualToString:[@"Rail" uppercaseString]])
    {
        modeImage = [UIImage imageNamed:@"iconModeRailLarge.png"];
    }
    else if([[mode uppercaseString] isEqualToString:[@"Bus" uppercaseString]])
    {
        modeImage = [UIImage imageNamed:@"iconModeBusLarge.png"];
    }
    else if([[mode uppercaseString] isEqualToString:[@"Taxi" uppercaseString]])
    {
        modeImage = [UIImage imageNamed:@"iconModeTaxi.png"];
    }
    else if([[mode uppercaseString] isEqualToString:[@"Boat" uppercaseString]])
    {
        modeImage = [UIImage imageNamed:@"iconModeBoat.png"];
    }
    else if([[mode uppercaseString] isEqualToString:[@"Pedestrian" uppercaseString]])
    {
        modeImage = [UIImage imageNamed:@"iconModeWalkingLarge.png"];
    }
    return modeImage;
}
@end