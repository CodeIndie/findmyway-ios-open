//
//  FavouritesView.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/10.
//

#import "FavouritesView.h"

@implementation FavouritesView

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    float y = layout.buttonSize;
    title = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding, 0, layout.paddedWidth, layout.buttonSize)];
    [title setText:NSLocalizedString(@"Favourites and Recent", nil)];
    CALayer *titleLayer = [title layer];
    [titleLayer setBorderWidth:layout.delimiterSize];
    [titleLayer setBorderColor:colour.fmwBorderColor.CGColor];
    
    for(int i = 0; i < locations.count && i < 3; i++)
    {
        // TODO: CREATE FAVOURITES & RECENTS HERE
        y+= layout.buttonSize;
        
    }
    
    y += layout.buttonSize;
    showAll = [[UIButton alloc] initWithFrame:CGRectMake(0, self.frame.size.height - layout.buttonSize, self.frame.size.width, layout.buttonSize)];
    [showAll setTitle:NSLocalizedString(@"All", nil) forState:UIControlStateNormal];
    [showAll setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    CALayer *allLayer = [showAll layer];
    [allLayer setBorderWidth:layout.delimiterSize];
    [allLayer setBorderColor:colour.fmwBorderColor.CGColor];
}

-(void)addUIToView
{
    
}

-(void)animateIn
{
    [super animateIn];
}

-(void)animateOut
{
    [super animateOut];
}

@end
