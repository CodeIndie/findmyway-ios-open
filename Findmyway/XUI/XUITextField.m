//
//  XUITextField.m
//  Findmyway
//
//  Created by Bilo on 3/3/15.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUITextField.h"

@implementation XUITextField

-(id)initWithFrame:(CGRect)frame placeHolder:(NSString*)placeHolder text:(NSString*)text appDelegate:(AppDelegate*)appDelegate
{
    self = [super initWithFrame:frame];
    if(self)
    {
        CALayer *layer = self.layer;
        [layer setBorderColor:appDelegate.colour.fmwTextColor.CGColor];
        [layer setCornerRadius:appDelegate.layout.padding];
        
    }
    return self;
}

@end
