//
//  MapRoute.h
//  tct
//
//  Created by Bilo on 4/16/14.
//

#import <Foundation/Foundation.h>
#import "Location.h"
#import "RouteStep.h"

@interface MapRoute : NSObject

@property NSString *totalDistance;
@property NSString *estimatedTotalTime;

@property Location *boundingBoxTopLeft;
@property Location *boundingBoxBottomRight;

@property NSMutableArray *steps;
@property NSMutableArray *coordinates;

-(id)initWithDictionary:(NSDictionary*)dictionary;
-(id)initWithMapRoute:(MapRoute*)mapRoute;
@end
