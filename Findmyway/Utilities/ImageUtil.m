//
//  ImageUtil.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/14.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "ImageUtil.h"

@implementation ImageUtil

+(UIImage*)getImage:(NSString*)imageName
{
    return [[UIImage imageNamed:imageName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

@end
