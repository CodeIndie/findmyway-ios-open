//
//  DistanceUtil.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/13.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "DistanceUtil.h"

@implementation DistanceUtil

+(int)metersBetweenStartLat:(float)startLat startLon:(float)startLon endLat:(float)endLat endLon:(float)endLon
{
    CLLocation *locationA = [[CLLocation alloc] initWithLatitude:startLat longitude:startLon];
    CLLocation *locationB = [[CLLocation alloc] initWithLatitude:endLat longitude:endLon];
    
    CLLocationDistance distance = [locationA distanceFromLocation:locationB];
    return (int)distance;
}

+(int)metersBetweenStartLatStr:(NSString *)startLat startLonStr:(NSString *)startLon endLatStr:(NSString *)endLat endLonStr:(NSString *)endLon
{
    CLLocation *locationA = [[CLLocation alloc] initWithLatitude:[startLat floatValue] longitude:[startLon floatValue]];
    CLLocation *locationB = [[CLLocation alloc] initWithLatitude:[endLat floatValue] longitude:[endLon floatValue]];
    
    CLLocationDistance distance = [locationA distanceFromLocation:locationB];
    if(distance < 0) distance *= -1;
    
    return (int)distance;
}

+(NSString*)distanceString:(int)meters metricSystem:(BOOL)isUsingMetricSystem
{
    NSMutableString *distanceString = [[NSMutableString alloc] initWithString:@""];
    
    if(meters <= 0)
    {
        return  distanceString;
    }
    
    if(isUsingMetricSystem)
    {
        if(meters > 1000)
        {
            [distanceString appendString:[NSString stringWithFormat:@"%d.%dkm", (meters / 1000), (meters / 100) % 10]];
        }
        else
        {
            [distanceString appendString:[NSString stringWithFormat:@"%dm", meters ] ];
        }
    }
    else
    {
        float feet = meters* 3.28084;
        float yards = meters * 1.09361;
        float miles = meters / 1609.34;
        
        if(miles > 1)
        {
            [distanceString appendString:[NSString stringWithFormat:@"%d.%dmi", (int) miles , (int)( (int)(miles * 10) / 10)]];
        }
        else if(yards > 1)
        {
            [distanceString appendString:[NSString stringWithFormat:@"%dyd", (int)yards]];//, ((int)feet) % ((int)yards)]];
        }
        else
        {
            [distanceString appendString:[NSString stringWithFormat:@"%dft", (int)feet]];
        }
    }
    
    return distanceString;
}

+(float)distanceBetween:(Location*)locationA locationB:(Location*)locationB
{
    float deltaLat = (locationA.latitude - locationB.latitude);
    float deltaLon = (locationA.longitude - locationB.longitude);
    
    float conversionConstant = 110599;
    float deltaCoords = sqrt(deltaLon * deltaLon + deltaLat * deltaLat);
    
    return conversionConstant * deltaCoords;
}

+(int)metersBetween:(Location*)locationA locationB:(Location*)locationB
{
    return [DistanceUtil metersBetweenStartLat:locationA.latitude startLon:locationA.longitude endLat:locationB.latitude endLon:locationB.longitude];
}

+(NSString*)metersStringBetween:(Location*)locationA locationB:(Location*)locationB
{
    return [DistanceUtil distanceString:[DistanceUtil metersBetween:locationA locationB:locationB] metricSystem:YES];
}

@end
