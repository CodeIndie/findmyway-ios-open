//
//  Notifications.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/07.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Notifications : NSObject

-(id)init;
+(id)instance;

@property NSString *GETAddressSuccess;
@property NSString *GETAddressFail;

@property NSString *GETTripsSuccess;
@property NSString *GETTripsFail;

@property NSString *GETPathsSuccess;
@property NSString *GETPathsFail;

@property NSString *GETSearchItemsSuccess;
@property NSString *GETSearchItemsFail;

@property NSString *GETOperatorsSuccess;
@property NSString *GETOperatorsFail;

@property NSString *MapReloadRoute;

@end
