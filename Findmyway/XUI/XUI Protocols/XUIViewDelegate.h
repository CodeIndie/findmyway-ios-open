//
//  XUIViewDelegate.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/10.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "XUIViewController.h"

@protocol XUIViewDelegate <NSObject>


-(void)initViewElements;
-(void)addUIToView;
-(void)animateIn;
-(void)animateOut;

@end
