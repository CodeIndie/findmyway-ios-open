//
//  ApiResponse.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/07.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "ApiResponse.h"

@implementation ApiResponse

-(id)init
{
    self = [super init];
    if(self != nil)
    {
        appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        [self initDataObjects];
    }
    return self;
}

-(void)initDataObjects
{
    self.searchItems = [[NSMutableArray alloc] init];
    self.trips = [[NSMutableArray alloc] init];
}

-(void)addTrip:(NSDictionary*)dictionary toIndex:(int)index
{
    for(int i = 0 ; i < self.trips.count ; i++)
    {
        if(i == index)
        {
            PublicMapRoute *route = [[PublicMapRoute alloc] initWithDictionary:dictionary];
            [((MultimodalTrip*)[self.trips objectAtIndex:i]) addMapRoute:route];
        }
    }
}

-(void)addPaths:(NSDictionary*)dictionary
{
    NSArray *pathsArray = [dictionary objectForKey:@"results"];
    
    if([self.trips count] > 0)
    {
        @try
        {
            self.trips = nil;
            self.trips = [[NSMutableArray alloc] init];
        }
        @catch (NSException *exception)
        {
            NSLog(@"Exception: %@", exception);
        }
    }
    
    NSLog(@"Trips count: %d (should be '0')", self.trips.count);
    
    for(NSDictionary *d in pathsArray)
    {
        MultimodalTrip *routeOption = [[MultimodalTrip alloc] initWithDictionary:d];
        [self.trips addObject:routeOption];
    }
    NSLog(@"Added Route Options: %lu", (unsigned long)[self.trips count]);
}

-(void)addAddress:(NSDictionary*)dictionary
{
    NSArray *addressArray = [dictionary objectForKey:@"locations"];
    
    NSDictionary *addressDict = (NSDictionary*)[addressArray objectAtIndex:0];
    NSDictionary *pointDictionary = [addressDict objectForKey:@"point"];
    self.location = [[Location alloc]
                 initWithLatitudeString:[pointDictionary objectForKey:@"latitude"]
                 longitudeString:[pointDictionary objectForKey:@"longitude"]];
    self.location.address = [addressDict objectForKey:@"address"];
    self.location.name = [addressDict objectForKey:@"name"];
}

-(void)addToken:(NSDictionary*)dictionary
{
    self.fmwToken = [dictionary objectForKey:@"token"];
    [[DefaultsUtil instance] SetString:self.fmwToken ForKey:@"token"];
    appDelegate.apiUtil.token = [[URLString alloc] initWithString:((NSString*)[[DefaultsUtil instance] GetObjectForKey:@"token"])];
    appDelegate.apiUtil.fmwToken = [[[URLString alloc] initWithString:((NSString*)[[DefaultsUtil instance] GetObjectForKey:@"token"])] encodedString];
}

-(void)addOperators:(NSDictionary *)dictionary
{
    self.operators = [[NSMutableArray alloc] init];
    NSArray *operatorsArray = [dictionary objectForKey:@"operators"];
    
    for(NSDictionary *operator in operatorsArray)
    {
        [self.operators addObject:[[Operator alloc] initWithDictionary:operator]];
    }
    NSLog(@"Operators: %lu", (unsigned long)self.operators.count);
    [[NSNotificationCenter defaultCenter] postNotificationName:[[Notifications instance] GETOperatorsSuccess] object:nil];
}

-(void)addSearchItems:(NSDictionary *)dictionary
{
    NSArray *addressArray = [dictionary objectForKey:@"searchItems"];
    self.searchItems = [[NSMutableArray alloc] init];
    for(NSDictionary *locationDictionary in addressArray)
    {
        SearchItem *item = [[SearchItem alloc] initWithDictionary:locationDictionary];
        [self.searchItems addObject:item];
    }
}

-(void)addAnnouncements:(NSDictionary *)dictionary
{
    NSArray *announcementsArray = [dictionary objectForKey:@"announcements"];
    self.announcements = [[NSMutableArray alloc] init];
    for(NSDictionary *announcementDictionary in announcementsArray)
    {
        Announcement *announcement = [[Announcement alloc] initWithDictionary:announcementDictionary];
        [self.announcements addObject:announcement];
    }
    
    NSLog(@"Announcements: %d", self.announcements.count);
}

@end
