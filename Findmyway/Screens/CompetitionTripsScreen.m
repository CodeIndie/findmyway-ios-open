//
//  CompetitionTripsScreen.m
//  Findmyway
//
//  Created by Bilo on 3/16/15.
//

#import "CompetitionTripsScreen.h"

@implementation CompetitionTripsScreen

-(void)animateIn
{
    [super animateIn];
    [parentView.view addSubview:self];
    self.alpha = 0;
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.alpha = 1;
     }
                     completion:^(BOOL finished)
     {
         [self refreshTrips];
         [self setTeamName];
     }];
}

-(void)animateOut
{
    [super animateOut];
    [super animateIn];
    self.alpha = 1;
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.alpha = 0;
     }
                     completion:^(BOOL finished)
     {
         
     }];
}

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    // TOP & BOTTOM BAR
    topbarBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.buttonSize)];
    [topbarBackground setBackgroundColor:colour.fmwBackgroundColor];
    
    topbarLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, layout.delimiterSize)];
    [topbarLine setBackgroundColor:colour.fmwTextColor];
    
    // BOTTOM BAR
    bottomBarBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.screenHeight - layout.buttonSize - layout.topOfContent, layout.screenWidth, layout.buttonSize)];
    [bottomBarBackground setBackgroundColor:colour.fmwBackgroundColor];
    
    bottomBarLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.screenHeight - layout.buttonSize - layout.topOfContent, layout.screenWidth, layout.delimiterSize)];
    [bottomBarLine setBackgroundColor:colour.fmwTextColor];
    
    backButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding, 0, layout.buttonSize * 1.5, layout.buttonSize)];
    [backButton setTitle:[NSString stringWithFormat:@"  %@",NSLocalizedString(@"Back", nil)] forState:UIControlStateNormal];
    [backButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    backArrow = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding *0, layout.padding/2, layout.buttonSize - layout.padding, layout.buttonSize - layout.padding)];
    [backArrow setImage:[ImageUtil getImage:@"iconLeftThick.png"] forState:UIControlStateNormal];
    [backArrow.imageView setTintColor:colour.fmwActiveColor];
    
    totalPoints = [[UILabel alloc] initWithFrame:CGRectMake(layout.buttonSize * 2, 0, layout.screenWidth - layout.buttonSize *3, layout.buttonSize)];
    [totalPoints setTextColor:colour.fmwTextColor];
    [totalPoints setTextAlignment:NSTextAlignmentCenter];
    
    teamButton = [[UIButton alloc] initWithFrame:CGRectMake(0, layout.screenHeight - layout.buttonSize - layout.topOfContent, layout.screenWidth, layout.buttonSize)];
    [teamButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [teamButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    
    [self setTeamName];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize + layout.delimiterSize, layout.screenWidth, layout.screenHeight - 2 * (layout.buttonSize + layout.padding) + layout.delimiterSize)];
}

-(void)setTeamName
{
    NSString *teamMember1 = [[DefaultsUtil instance] GetObjectForKey:@"TeamMember1"];
    NSString *teamMember2 = [[DefaultsUtil instance] GetObjectForKey:@"TeamMember2"];
    
    [teamButton setTitle:[NSString stringWithFormat:@"%@: %@ %@",
                          NSLocalizedString(@"Team", nil),
                          teamMember1,
                          teamMember2 == nil ? @"" : [NSString stringWithFormat:@"& %@", teamMember2]]
                forState:UIControlStateNormal];
    [teamButton addTarget:self action:@selector(showTeamAlertView) forControlEvents:UIControlEventTouchUpInside];
}

-(void)showTeamAlertView
{
    NSLog(@"Showing Team Alert View");
    teamAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Enter Challenge", nil) message:NSLocalizedString(@"Enter your names below to enter the Earth Hour City Challenge in Tshwane", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Submit", nil), nil];
    [teamAlertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    
    [teamAlertView textFieldAtIndex:1].secureTextEntry = NO;
    [teamAlertView textFieldAtIndex:0].placeholder = NSLocalizedString(@"Team Member 1", nil);
    [teamAlertView textFieldAtIndex:1].placeholder = NSLocalizedString(@"Team Member 2", nil);
    
    NSString *teamMember1 = [[DefaultsUtil instance] GetStringForKey:@"TeamMember1"];
    NSString *teamMember2 = [[DefaultsUtil instance] GetStringForKey:@"TeamMember2"];
    
    if(teamMember1 != nil && ![teamMember1 isEqual:(id)[NSNull null]] && [teamMember1 length] > 0)
    {
        [[teamAlertView textFieldAtIndex:0] setText:teamMember1];
    }
    if(teamMember2 != nil && ![teamMember2 isEqual:(id)[NSNull null]] && [teamMember2 length] > 0)
    {
        [[teamAlertView textFieldAtIndex:1] setText:teamMember2];
    }
    
    [teamAlertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView isEqual:teamAlertView])
    {
        if(buttonIndex == 0)
        {
            [teamAlertView dismissWithClickedButtonIndex:0 animated:YES];
        }
        else
        {
            [self submitDetails];
        }
    }
}

-(void)submitDetails
{
    [CompetitionUtil setCompetitionMember1:[teamAlertView textFieldAtIndex:0].text member2:[teamAlertView textFieldAtIndex:1].text];
    [teamButton setTitle:[NSString stringWithFormat:@"%@: %@ & %@",
                         NSLocalizedString(@"Team", nil),
                         [[DefaultsUtil instance] GetObjectForKey:@"TeamMember1"],
                         [[DefaultsUtil instance] GetObjectForKey:@"TeamMember2"]] forState:UIControlStateNormal];
    [teamButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
}

-(void)addUIToView
{
    [self addSubview:topbarBackground];
    [self addSubview:topbarLine];
    [self addSubview:totalPoints];
    
    [self addSubview:bottomBarBackground];
    [self addSubview:bottomBarLine];
    [self addSubview:teamButton];
    
    [self addSubview:backArrow];
    [self addSubview:backButton];
    //[self addSubview:mapButton];
    [self addSubview:scrollView];
}

-(void)back
{
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController swapInView:viewController.fmwScreen];
}

-(void)refreshTrips
{
    for(TripLogListItem *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    loggedTrips = nil;
    
    
    NSArray *loggedTripItems = (NSArray*)[[DefaultsUtil instance] GetArrayForKey:@"LoggedTrips"];
    if(loggedTripItems.count > 0)
    {
        [self XLog:[NSString stringWithFormat:@"Logged Trips: %d", loggedTripItems.count]];
        loggedTrips = [[NSMutableArray alloc] init];
        
        int totalScore = 0;
        for(int i = 0; i < loggedTripItems.count; i ++)
        {
            TripLogItem *item = (TripLogItem*)[loggedTripItems objectAtIndex:i];
            totalScore += item.points;
            TripLogListItem *listItem = [[TripLogListItem alloc] initWithFrame:CGRectMake(0, i * (layout.padding + layout.buttonSize * 1.2), self.frame.size.width, layout.buttonSize * 1.2) item:item];
            [loggedTrips addObject:listItem];
            [scrollView addSubview:listItem];
        }
        [totalPoints setText:[NSString stringWithFormat:@"Total Points: %d", totalScore]];
    }
    
    scrollView.contentSize = CGSizeMake(layout.screenWidth, (loggedTripItems.count + 2) * layout.buttonSize * 1.2);
}
@end
