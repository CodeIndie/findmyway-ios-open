//
//  ImageUtil.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/14.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ImageUtil : NSObject

+(UIImage*)getImage:(NSString*)imageName;

@end
