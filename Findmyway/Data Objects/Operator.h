//
//  Operator.h
//  Findmyway
//
//  Created by Bilo on 2/18/15.
//

#import <Foundation/Foundation.h>

@interface Operator : NSObject

@property NSString *name;
@property NSString *displayName;
@property NSMutableArray *modes;
@property NSString *category;
@property NSString *twitterHandle;
@property NSString *websiteAddress;
@property NSString *routeMapUrl;
@property NSString *contactEmail;
@property NSString *contactNumber;
@property BOOL isPublic;

-(id)initWithDictionary:(NSDictionary*)dictionary;
-(id)initWithOperator:(Operator*)operator;

@end
