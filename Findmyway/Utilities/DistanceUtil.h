//
//  DistanceUtil.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/13.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Location.h"

@interface DistanceUtil : NSObject

+(int)metersBetweenStartLat:(float)startLat startLon:(float)startLon endLat:(float)endLat endLon:(float)endLon;

+(int)metersBetweenStartLatStr:(NSString*)startLat startLonStr:(NSString*)startLon endLatStr:(NSString*)endLat endLonStr:(NSString*)endLon;

+(NSString*)distanceString:(int)meters metricSystem:(BOOL)isUsingMetricSystem;

+(int)metersBetween:(Location*)locationA locationB:(Location*)locationB;
+(NSString*)metersStringBetween:(Location*)locationA locationB:(Location*)locationB;
+(float)distanceBetween:(Location*)locationA locationB:(Location*)locationB;
@end
