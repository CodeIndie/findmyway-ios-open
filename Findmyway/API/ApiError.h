//
//  ApiError.h
//  tct
//
//  Created by Bilo on 10/10/14.
//

#import <Foundation/Foundation.h>

@interface ApiError : NSObject

@property int errorCode;
@property NSString *message;
@property BOOL isAuthenticating;

-(id)initWithDictionary:(NSDictionary*)dictionary;

@end
