//
//  PublicMapRoute.m
//  tct
//
//  Created by Bilo on 7/21/14.
//

#import "PublicMapRoute.h"

@implementation PublicMapRoute

-(id)initWithDictionary:(NSDictionary*)dictionary
{
    self = [super init];
    if(self)
    {
        self.boundingBoxTopLeft = [[Location alloc] initWithDictionary:[dictionary objectForKey:@"boundingBoxTopLeft"]];
        self.boundingBoxBottomRight = [[Location alloc] initWithDictionary:[dictionary objectForKey:@"boundingBoxBottomRight"]];
        
        self.segments = [[NSMutableArray alloc] init];
        NSArray *routesArray = [dictionary objectForKey:@"routes"];
        
        for(int i = 0 ; i < [routesArray count] ; i++)
        {
            PublicMapRouteSegment *segment = [[PublicMapRouteSegment alloc] initWithDictionary:[routesArray objectAtIndex:i]];
            [self.segments addObject:segment];
        }
    }
    [self consolidateCoordinates];
    return self;
}

-(id)initWithMapRoute:(PublicMapRoute *)mapRoute
{
    self = [super init];
    if(self)
    {
        self.boundingBoxTopLeft = mapRoute.boundingBoxTopLeft;
        self.boundingBoxBottomRight = mapRoute.boundingBoxBottomRight;
        
        self.segments = [[NSMutableArray alloc] init];
        
        for(int i = 0 ; i < [mapRoute.segments count] ; i++)
        {
            PublicMapRouteSegment *segment = [[PublicMapRouteSegment alloc] initWithMapRouteSegment:(PublicMapRouteSegment*)[mapRoute.segments objectAtIndex:i]];
            [self.segments addObject:segment];
        }
    }
    [self consolidateCoordinates];
    return self;
}

-(void)consolidateCoordinates
{
    self.coordinates = [[NSMutableArray alloc] init];
    
    for(PublicMapRouteSegment *route in self.segments)
    {
        for(Location *coordinate in route.coordinates)
        {
            [self.coordinates addObject:[[Location alloc] initWithLocation:coordinate]];
        }
    }
    
    NSLog(@"Stored coordinates: %lu", (unsigned long)[self.coordinates count]);
}
@end
