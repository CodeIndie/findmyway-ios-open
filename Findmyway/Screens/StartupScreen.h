//
//  StartupScreen.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/02/11.
//

#import "XUIView.h"
#import "AppViewController.h"
#import "SignalR.h"

@interface StartupScreen : XUIView<UIAlertViewDelegate>
{
    BOOL appViewControllerInstantiated;
    UIImageView *logoBackground;
    UIImageView *animatedImage;
    UIImageView *fmwLogo;
    UILabel *loadingLabel;
    UIButton *poweredByButton;
    float logoAngle;
    float increment;
    NSTimer *animationTimer;
    NSTimer *pauseTimer;
    int loadingIndex;
    BOOL loadingData;
    BOOL increaseLoadingIndex;
}


@end
