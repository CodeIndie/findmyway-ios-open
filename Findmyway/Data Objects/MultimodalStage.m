//
//  MultimodalStage.m
//  tct
//
//  Created by Bilo on 9/10/14.
//

#import "MultimodalStage.h"

@implementation MultimodalStage

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self)
    {
        self.name = [dictionary objectForKey:@"name"];
        self.modeName = [dictionary objectForKey:@"operator"];
        self.modeType = [dictionary objectForKey:@"mode"];
        self.colourHex = [dictionary objectForKey:@"colour"];
        self.colour = [self colourWithHexString:self.colourHex];
        self.vehicleNumber = [dictionary objectForKey:@"vehicleNumber"];
        self.duration = [dictionary objectForKey:@"duration"];
        self.cost = [dictionary objectForKey:@"cost"];
        self.routeDescription = [dictionary objectForKey:@"description"];
        self.points = [[NSMutableArray alloc] init];
        NSMutableArray *points = [dictionary objectForKey:@"stageLocations"];
        for(int i = 0 ; i < [points count] ; i++)
        {
            MultimodalPoint *point = (MultimodalPoint*)[[MultimodalPoint alloc] initWithDictionary:[points objectAtIndex:i]];
            point.colour = self.colour;
            [self.points addObject:point];
        }  
        self.announcements = [[NSMutableArray alloc] init];
        
        self.startLocation = (MultimodalPoint*)[self.points objectAtIndex:0];
        self.endLocation = (MultimodalPoint*)[self.points objectAtIndex:[self.points count] - 1];
    }
    return self;
}

-(UIColor *)colourWithHexString:(NSString *)hexString
{
    unsigned int hexInt = [self intFromHexString:hexString];
    
    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexInt & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexInt & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexInt & 0xFF))/255
                    alpha:1];
    
    return color;
}

-(unsigned int)intFromHexString:(NSString *)hexString
{
    unsigned int hexInt = 0;
    
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}

-(UIColor*)colourWithHexStringOld:(NSString *)hexString
{
    NSString *cString = [[hexString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) return [UIColor grayColor];
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"]) cString = [cString substringFromIndex:2];
    
    if ([cString length] != 6) return  [UIColor grayColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    NSString *rString = [cString substringWithRange:range];
    
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:1.0f];
}

-(void)addDirections:(MapRoute *)mapRoute
{
    self.directions = [[MapRoute alloc] initWithMapRoute:mapRoute];
    
    if([self.directions.steps count] > 0)
    {
        NSLog(@"Directions added");
        self.hasDirections = YES;
        self.isShowingDirections = YES;
    }
    else
    {
        NSLog(@"Failed to add directions");
    }
}

@end
