//
//  DeviceConfig.m
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//

#import "DeviceConfig.h"
#import "AppDelegate.h"
#import <sys/utsname.h>

@implementation DeviceConfig

-(id)initWithDebugOutput:(BOOL*)printDebugInfo delegate:(AppDelegate *)appDelegate
enableProximityMonitoring:(BOOL*)proximityMonitoringEnabled
 enableBatteryMonitoring:(BOOL*)batteryMonitoringEnabled
{
    if(self == nil)
    {
        self = [super init];
    }
    return [self init:appDelegate];
}

-(id)initWithDelegate:(AppDelegate *)appDelegate
{
    if(self == nil)
    {
        self = [super init];
    }
    
    // Device Screen Properties
    _screenScale = [[UIScreen mainScreen] scale];
    _screenWidth = [[UIScreen mainScreen] bounds].size.width;
    _screenHeight = [[UIScreen mainScreen] bounds].size.height;
    _screenWidthPixels = _screenWidth * _screenScale;
    _screenHeightPixels = _screenHeight * _screenScale;
    _screenRect = [[UIScreen mainScreen] bounds];
    
    device = [UIDevice currentDevice];
    device.proximityMonitoringEnabled = NO;
    device.batteryMonitoringEnabled = YES;
    deviceModelName = [self getDeviceModel:device.model];
    deviceType = device.localizedModel;
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    
    _appDisplayName = [infoDict objectForKey:@"CFBundleDisplayName"];
    _appVersionNumber = [infoDict objectForKey:@"CFBundleVersion"];
    _majorVersion = infoDict[@"CFBundleShortVersionString"];
    self.deviceModel = deviceModelName;
    appDelegate.storyboardName = @"Main";
//    if(false)//[device.model isEqualToString:@"iPad"])
//    {
//        appDelegate.storyboardName = @"Main_iPad";
//    }
//    else
//    {
//        appDelegate.storyboardName = @"Main_iPhone";
//    }
    
    self.userAgent = [NSString stringWithFormat:@"Apple %@,  %@ %@", deviceModelName, device.systemName, device.systemVersion ];
    NSLog(@"User-Agent: %@", self.userAgent);
    
    NSLog(@"Storyboard name: %@", appDelegate.storyboardName);
    return self;
}

-(id)init:(AppDelegate *)appDelegate
{
    if(self == nil)
    {
        self = [super init];
    }
    
    self = [self initWithDelegate:appDelegate];
    
    NSLog(@"Screen Properties:\n---------------\n"
          @"Dimensions: %f x %f\n"
          @"Pixels: %f x %f\n"
          @"AspectRatio: %f\n",
          _screenWidth,
          _screenHeight,
          _screenWidthPixels,
          _screenHeightPixels,
          _screenWidth / _screenHeight);
    
    NSLog(@"\n\niOS Device Properties:\n---------------\n"
          @"Name:           %@\n"
          @"System Name:    %@\n"
          @"System Version: %@\n"
          
          @"Model:          %@\n"
          @"Model Raw:      %@\n"
          
          @"LocalizedModel: %@\n"
          @"Description:    %@\n"
          //@"UI Idiom:       %ld\n"
          @"Id for Vendor:  %@\n"
          @"\n------------------\n"
          @"Other properties:\n--------\n"
          @"Battery Level:    %d\n"
          //@"Battery State:    %ld\n"
          //@"Orientation:      %ld\n"
          @"Proximity State: %d\n",
          
          device.name,
          device.systemName,
          device.systemVersion,
          
          deviceModelName,
          device.model,
          
          deviceType,
          device.description,
          //device.userInterfaceIdiom,
          device.identifierForVendor,
          (int)(device.batteryLevel * 100),
          //device.batteryState,
          //device.orientation,
          device.proximityState);
    
    NSLog(@"\nApp Version Properties:\n"
          @"App Name: %@\n"
          @"Version No. (Format X.Y.Z):  %@\n"
          @"Version No. (Format X.Y):    %@\n",
          
          _appDisplayName,
          _appVersionNumber,
          _majorVersion);
    
    return self;
}

-(NSString *)getDeviceModel:(NSString*) deviceModel
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    NSLog(@"Localized: %@", [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding]);
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    
    return platform;
}

@end
