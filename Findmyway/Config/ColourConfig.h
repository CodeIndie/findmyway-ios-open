//
//  ColourConfig.h
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Base64Image.h"
#import "DictionaryUtil.h"
#import "DateTimeUtil.h"

@interface ColourConfig : NSObject

typedef enum
{
    Findmyway = 0,
    Areyeng = 1
}FmwAppTheme;

// TIME SENSITIVE THEMES
@property NSDate *areyengStartDate;
@property NSDate *areyengEndDate;

// THEME VARS
@property UIColor *fmwBorderColor;
@property UIColor *fmwTextColor;
@property UIColor *fmwTextColorLight;
@property UIColor *fmwActiveColor;
@property UIColor *fmwActiveColorLight;
@property UIColor *fmwBackgroundColor;
@property UIColor *gradientStart;
@property UIColor *gradientEnd;
@property UIColor *fmwLightBorder;
@property UIColor *fmwDarkBorder;

// FMW COLORS
@property UIColor *fmwGradientStart;
@property UIColor *fmwGradientEnd;
@property UIColor *fmwDark;
@property UIColor *fmwLight;
@property UIColor *fmwHeader;
@property UIColor *fmwRed;
@property UIColor *fmwBlue1;
@property UIColor *fmwHeaderLight;
@property UIColor *fmwHeaderDark;

// SOCIAL MEDIA
@property UIColor *facebook;
@property UIColor *twitter;
@property UIColor *google;

// AREYENTG COLORS
@property UIColor *areyengGradientStart;
@property UIColor *areyengGradientEnd;
@property UIColor *areyengPurple;
@property UIColor *areyengGreen;

// COLORS
@property UIColor *lightGray;
@property UIColor *white;
@property UIColor *black;
@property UIColor *clear;
@property UIColor *transparent;

// IOS 8 COLOURS
@property UIColor *iosCyan;
@property UIColor *iosYellow;
@property UIColor *iosOrange;
@property UIColor *iosPink;
@property UIColor *iosBlue;
@property UIColor *iosGreen;
@property UIColor *iosRed;
@property UIColor *iosGrey;


-(id)init:(NSDictionary*)dictionary;
-(void)setDarkTheme:(BOOL)isDarkTheme;
@end
