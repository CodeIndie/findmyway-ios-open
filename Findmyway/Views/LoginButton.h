//
//  LoginButton.h
//  Findmyway
//
//  Created by Bilo on 2/9/15.
//

#import <UIKit/UIKit.h>
#import "LayoutConfig.h"
#import "ImageUtil.h"

@interface LoginButton : UIView

@property UIImageView *bottomLine;
@property UIImageView *logo;
@property UILabel *buttonName;
@property UIButton *button;

-(id)initWithFrame:(CGRect)frame name:(NSString*)name image:(NSString*)image colour:(UIColor*)color layout:(LayoutConfig*)layout;
@end
