//
//  XUIShape.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/12.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIShape.h"

@implementation XUIShape

+(UIView*)square:(CGPoint)origin size:(float)size borderWidth:(float)borderWidth fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(origin.x, origin.y, size, size)];
    
    [view setBackgroundColor:fillColor];
    CALayer *layer = [view layer];
    [layer setMasksToBounds:YES];
    [layer setBorderWidth:borderWidth];
    [layer setBorderColor:[strokeColor CGColor]];
    return view;
}

+(UIView*)circle:(CGPoint)origin size:(float)size borderWidth:(float)borderWidth fillColor:(UIColor*)fillColor strokeColor:(UIColor*)strokeColor
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(origin.x, origin.y, size, size)];
    
    [view setBackgroundColor:fillColor];
    CALayer *layer = [view layer];
    [layer setMasksToBounds:YES];
    [layer setCornerRadius:size/2];
    [layer setBorderWidth:borderWidth];
    [layer setBorderColor:[strokeColor CGColor]];
    return view;
}

-(void)polygonWithPoints:(NSMutableArray *)points strokeWidth:(float)strokeSize strokeColor:(UIColor *)strokeColor backgroundColor:(UIColor*)backgroundColor
{
    self.strokeColor = strokeColor;
    self.fillColor = backgroundColor;
    self.strokeWidth = strokeSize;
    [self setBackgroundColor:self.fillColor];
    
    self.points = points;
}

-(void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetLineWidth(context, self.strokeWidth);
    CGContextSetStrokeColorWithColor(context, self.strokeColor.CGColor);
    CGContextSetFillColorWithColor(context, self.fillColor.CGColor);
    
    for(int i = 0; i < self.points.count; i++)
    {
        NSValue *pointObject = (NSValue*)[self.points objectAtIndex:i];
        CGPoint point = pointObject.CGPointValue;
        
        // FIRST POINT
        if(i == 0)
        {
            CGContextMoveToPoint(context, point.x, point.y);
        }
        // LAST POINT
        else if(i == self.points.count - 1)
        {
            CGContextAddLineToPoint(context, point.x, point.y);
            
            // FOR CLOSED SHAPE ADD FIRST POINT AT END
            pointObject = (NSValue*)[self.points objectAtIndex:0];
            point = pointObject.CGPointValue;
            CGContextAddLineToPoint(context, point.x, point.y);
        }
        // INTERMEDIATE POINTS
        else
        {
            CGContextAddLineToPoint(context, point.x, point.y);
        }
    }
    
    CGContextStrokePath(context);
}

@end
