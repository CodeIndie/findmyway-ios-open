//
//  DictionaryUtil.m
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//

#import "DictionaryUtil.h"

@implementation DictionaryUtil

+(void)printDictionary:(NSDictionary *)dictionary
{
    NSLog(@"Printing dictionary...");
    for(NSString *key in [dictionary allKeys])
    {
        NSLog(@"%@ : %@", key, [dictionary objectForKey:key]);
    }
}

+(void)printMutableDictionary:(NSMutableDictionary *)dictionary
{
    
}

+(NSDictionary *)getChildDictionary:(NSDictionary *)dictionary withKey:(NSString *)key
{
    NSDictionary *childDictionary = [dictionary objectForKey:key];
    return childDictionary;
}

+(NSMutableDictionary *)getChildMutableDictionary:(NSDictionary *)dictionary withKey:(NSString *)key
{
    NSMutableDictionary *childDictionary = [dictionary objectForKey:key];
    return childDictionary;
}

+(void)printDictionaryKeys:(NSDictionary *)dictionary
{
    NSLog(@"Printing Dictionary Keys:\n\n%@", [dictionary allKeys]);
}

+(void)printMutableDictionaryKeys:(NSMutableDictionary *)dictionary
{
    NSLog(@"Printing Dictionary Keys:\n\n%@", [dictionary allKeys]);
    /*
     
     */
}

+(void)printDictionaryKeyValuePairs:(NSDictionary *)dictionary
{
    for(NSString *key in [dictionary allKeys])
    {
        NSLog(@"%@:%@", key, [dictionary objectForKey:key]);
    }
}

+(void)printMutableDictionaryKeyValuePairs:(NSMutableDictionary *)dictionary
{
    for(NSString *key in [dictionary allKeys])
    {
        NSLog(@"%@:%@", key, [dictionary objectForKey:key]);
    }
}

@end
