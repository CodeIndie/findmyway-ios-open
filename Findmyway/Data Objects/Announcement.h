//
//  Announcement.h
//  Findmyway
//
//  Created by Bilo on 4/13/15.
//

#import <Foundation/Foundation.h>
#import "Location.h"

@interface Announcement : NSObject

@property NSString *announcementType;
@property NSString *operatorName;
@property NSArray *modes;
@property NSString *descriptionText;
@property NSString *startDate;
@property NSString *endDate;
@property Location *point;

-(id)initWithDictionary:(NSDictionary*)dictionary;

@end
