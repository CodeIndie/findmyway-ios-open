//
//  MainMenuButton.h
//  Findmyway
//
//  Created by Bilo on 12/4/14.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ImageUtil.h"

@interface MainMenuButton : UIView
{
    
}

@property UIImageView *icon;
@property UILabel *label;
@property UIButton *button;

-(void)setDelegate:(AppDelegate*)appDelegate title:(NSString *)title iconName:(NSString *)iconName index:(int)index;

@end
