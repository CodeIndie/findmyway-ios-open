//
//  XUINotification.h
//  Findmyway
//
//  Created by Bilo on 4/9/15.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface XUINotification : NSObject
{
    AppDelegate *appDelegate;
    LayoutConfig *layout;
    ColourConfig *colour;
    
    float displayDuration;
    float animDuration;
    UIView *parentView;
    UIView *background;
    UILabel *messageLabel;
    CGPoint messageCenter;
}
-(id)initWithMessage:(NSString*)message inView:(UIView*)view;

@end
