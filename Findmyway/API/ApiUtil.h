//
//  ApiUtil.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/07.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "URLString.h"
#import "AppDelegate.h"

@class AppDelegate;

typedef enum
{
    About,
    Announcements,
    Operators,
    Paths,
    ReverseGeocode,
    SearchItems,
    Trips,
    Directions,
    
    AboutFmw,
    Users,
    Messages,
    AuthPins

}ApiRequest;

typedef enum
{
    UberRequestTypes
}UberRequest;

typedef enum
{
    GET,
    POST,
    PUT,
    UPDATE,
    DELETE,
    PATCH
    
}RestCallType;

@interface ApiUtil : NSObject
{
    AppDelegate* appDelegate;
}
@property URLString *appKey;
@property URLString *token;

@property NSString *fmwToken;

@property URLString *apiUrl;
@property URLString *apiFmwUrl;
@property URLString *apiUberUrl;

@property ApiRequest lastCall;

-(id)init:(NSDictionary*)dictionary;
+(id)instance;

-(NSMutableURLRequest*)urlForRequest:(ApiRequest)requestType callType:(RestCallType)callType parameters:(NSDictionary*)parameters;
-(NSMutableString*)uberRequest:(UberRequest)requestType parameters:(NSDictionary*)parameters;
-(NSMutableString*)apiRequest:(ApiRequest)requestType parameters:(NSDictionary*)parameters;
@end
