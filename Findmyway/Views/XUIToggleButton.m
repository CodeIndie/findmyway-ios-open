//
//  XUIToggleButton.m
//  Findmyway
//
//  Created by Bilo on 12/4/14.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIToggleButton.h"

@implementation XUIToggleButton

-(id)initWithName:(NSString *)name activeImage:(NSString *)activeImageName inactiveImage:(NSString *)inactiveImageName xPosition:(float)xPosition yPosition:(float)yPosition size:(float)size target:(id)target
{
    self = [super init];
    if(self)
    {
        self.name = name;
        self.activeImage = [ImageUtil getImage:activeImageName];
        self.inactiveImage = [ImageUtil getImage:inactiveImageName];
        self.button = [[UIButton alloc] initWithFrame:CGRectMake(xPosition, yPosition, size, size)];
        [self.button setImageEdgeInsets:UIEdgeInsetsMake(size/6, size/6, size/6, size/6)];
        [self.button addTarget:self action:@selector(toggle) forControlEvents:UIControlEventTouchUpInside];
        self.isActive = NO;
        
        activeColor = [UIColor whiteColor];
        inactiveColor = [UIColor whiteColor];
        [self toggle];
    }
    return self;
}

-(id)initWithName:(NSString *)name activeImage:(NSString *)activeImageName inactiveImage:(NSString *)inactiveImageName activeColor:(UIColor*)activeCol inactiveColor:(UIColor*)inactiveCol xPosition:(float)xPosition yPosition:(float)yPosition size:(float)size target:(id)target
{
    self = [super init];
    if(self)
    {
        self.name = name;
        self.activeImage = [ImageUtil getImage:activeImageName];
        self.inactiveImage = [ImageUtil getImage:inactiveImageName];
        
        activeColor = activeCol;
        inactiveColor = inactiveCol;
        
        self.button = [[UIButton alloc] initWithFrame:CGRectMake(xPosition, yPosition, size, size)];
        [self.button setImageEdgeInsets:UIEdgeInsetsMake(size/6, size/6, size/6, size/6)];
        [self.button addTarget:self action:@selector(toggle) forControlEvents:UIControlEventTouchUpInside];
        self.isActive = NO;
        [self toggle];
    }
    return self;
}

-(void)toggle
{
    self.isActive = !self.isActive;
    [self setActive:self.isActive];
}

-(void)setActive:(BOOL)active
{
    self.isActive = active;
    if(self.isActive)
    {
        [self.button setImage:self.activeImage forState:UIControlStateNormal];
        [self.button.imageView setTintColor:activeColor];
    }
    else
    {
        [self.button setImage:self.inactiveImage forState:UIControlStateNormal];
        [self.button.imageView setTintColor:inactiveColor];
    }
}
@end
