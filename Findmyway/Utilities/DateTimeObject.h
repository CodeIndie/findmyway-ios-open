//
//  DateTimeObject.h
//  tct
//
//  Created by Bilo on 4/22/14.
//

#import <Foundation/Foundation.h>

@interface DateTimeObject : NSObject

@property NSString *date;
@property NSString *dateDay;
@property NSString *dateMonth;
@property NSString *dateMonthNumber;
@property NSString *dateYear;

@property NSString *time;
@property NSString *hours;
@property NSString *minutes;
@property NSString *days;

@property NSString *dateString;
@property NSString *timeString;
@property NSString *dateTimeString;

-(id)initWithDateTime:(NSString*)dateTime;
-(NSString*)substring:(NSString*)sampleString upTo:(NSString*)end;
-(NSString*)substring:(NSString*)sampleString from:(NSString*)start length:(NSUInteger)length;
-(NSString*)formattedString;
-(NSString*)toString;
-(void)print;

@end
