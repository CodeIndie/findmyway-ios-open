//
//  FmwRouteOptions.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/06.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "FmwRouteOptions.h"

@implementation FmwRouteOptions

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(device.screenWidth * 0.5, frame.origin.y - device.screenHeight);
        centerOnScreen = CGPointMake(device.screenWidth * 0.5, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    float margin = layout.padding;
    float labelSize = layout.buttonSize * 1.2;
    float buttonSize = layout.buttonSize * 0.8;
    minRefreshScrollOffset = 80;
    
    y = 0;//layout.topOfContent;
    cancelButton = [XUIButton initWithFrame:CGRectMake(layout.padding, y, layout.buttonSize * 2, layout.buttonSize)
                                      title:NSLocalizedString(@"Cancel", nil)
                                   fontSize:[UIFont systemFontSize] + 1
                                       bold:YES
                                 titleColor:colour.fmwActiveColor
                            backgroundColor:colour.clear
                        horizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    
    [cancelButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    resultsLabel = [XUILabel initWithFrame:CGRectMake(layout.screenWidth/2 - layout.buttonSize *1.5, y, layout.buttonSize*3, layout.buttonSize)
                                      text:NSLocalizedString(@"Results", nil)
                                  fontSize:[UIFont systemFontSize] + 1
                                      bold:YES
                                 textColor:colour.fmwTextColor
                           backgroundColor:colour.clear
                             textAlignment:NSTextAlignmentCenter];
    
    y += layout.buttonSize;
    locationLabel = [XUILabel initWithFrame:CGRectMake(0, 0, labelSize, buttonSize)
                                       text:NSLocalizedString(@"Start:", nil)
                                   fontSize:[UIFont systemFontSize]
                                       bold:NO
                                  textColor:colour.fmwTextColorLight
                            backgroundColor:colour.clear
                              textAlignment:NSTextAlignmentRight];
    //    locationButton = [[UIButton alloc] initWithFrame:CGRectMake(margin + layout.buttonSize, y, layout.marginWidth - layout.buttonSize/2, buttonSize)];
    locationButton = [[UIButton alloc] initWithFrame:CGRectMake(margin + layout.buttonSize, y, layout.marginWidth - layout.buttonSize/2, buttonSize)];
    [locationButton setTitle:appDelegate.tripQuery.startLocation.name forState:UIControlStateNormal];
    [locationButton setTitleEdgeInsets:UIEdgeInsetsMake(0, labelSize + layout.padding, 0, 0)];
    [locationButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
    [locationButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    locationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [locationButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [locationButton addTarget:self action:@selector(searchLocation) forControlEvents:UIControlEventTouchUpInside];
    CALayer *location = [locationButton layer];
    [location setMasksToBounds:YES];
    [location setBorderWidth:layout.delimiterSize];
    [location setBorderColor:[colour.fmwTextColorLight CGColor]];
    [location setCornerRadius:layout.padding];
    [locationButton setBackgroundColor:colour.white];
    [locationButton addSubview:locationLabel];
    
    y += buttonSize + layout.padding;
    destinationLabel = [XUILabel initWithFrame:CGRectMake(0, 0, labelSize, buttonSize) text:NSLocalizedString(@"End:", nil)
                                      fontSize:[UIFont systemFontSize]
                                          bold:NO
                                     textColor:colour.fmwTextColorLight
                               backgroundColor:colour.clear
                                 textAlignment:NSTextAlignmentRight];
    //    destinationButton = [[UIButton alloc] initWithFrame:CGRectMake(margin + layout.buttonSize, y, layout.marginWidth - layout.buttonSize/2, buttonSize)];
    destinationButton = [[UIButton alloc] initWithFrame:CGRectMake(margin + layout.buttonSize, y, layout.marginWidth - layout.buttonSize/2, buttonSize)];
    [destinationButton setTitle:appDelegate.tripQuery.endLocation.name forState:UIControlStateNormal];
    [destinationButton setTitleEdgeInsets:UIEdgeInsetsMake(0, labelSize + layout.padding, 0, 0)];
    [destinationButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [destinationButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
    [destinationButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    destinationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [destinationButton setBackgroundColor:colour.white];
    [destinationButton addTarget:self action:@selector(searchDestination) forControlEvents:UIControlEventTouchUpInside];
    CALayer *destination = [destinationButton layer];
    [destination setMasksToBounds:YES];
    [destination setBorderWidth:layout.delimiterSize];
    [destination setBorderColor:[colour.fmwTextColorLight CGColor]];
    [destination setCornerRadius:layout.padding];
    [destinationButton addSubview:destinationLabel];
    
    switchButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding, y - buttonSize * 0.5 - layout.padding / 2, layout.buttonSize, layout.buttonSize)];
    [switchButton setImage:[ImageUtil getImage:@"iconSwitch.png"] forState:UIControlStateNormal];
    [switchButton.imageView setTintColor:colour.fmwActiveColor];
    [switchButton addTarget:self action:@selector(swapRouteEndpoints) forControlEvents:UIControlEventTouchUpInside];
    
    y += buttonSize + layout.padding;
    timeButton = [[UIButton alloc] initWithFrame:CGRectMake(margin + layout.buttonSize, y, layout.marginWidth - layout.buttonSize/2, buttonSize)];
    timeButton = [XUIButton initWithFrame:CGRectMake(margin + layout.buttonSize, y, layout.marginWidth - layout.buttonSize/2, buttonSize)
                                    title:NSLocalizedString(@"Arrive", nil)
                                 fontSize:[UIFont systemFontSize]
                                     bold:YES
                               titleColor:colour.fmwActiveColor
                          backgroundColor:colour.white
                      horizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [timeButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    [timeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, labelSize + layout.padding, 0, 0)];
    [timeButton addTarget:self action:@selector(showTimePicker) forControlEvents:UIControlEventTouchUpInside];
    CALayer *time = [timeButton layer];
    [time setMasksToBounds:YES];
    [time setBorderWidth:layout.delimiterSize];
    [time setBorderColor:[colour.fmwTextColorLight CGColor]];
    [time setCornerRadius:layout.padding];
    [time setBackgroundColor:colour.white.CGColor];
    [timeButton setBackgroundColor:[UIColor whiteColor]];
    timeLabel = [XUILabel initWithFrame:CGRectMake(0, 0, labelSize, buttonSize)
                                   text:NSLocalizedString(@"Time:", nil)
                               fontSize:[UIFont systemFontSize]
                                   bold:NO
                              textColor:colour.fmwTextColorLight
                        backgroundColor:colour.clear
                          textAlignment:NSTextAlignmentRight];
    [timeButton addSubview:timeLabel];
    
    y += buttonSize + layout.padding;
    headerBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, y)];
    [headerBackground setBackgroundColor:colour.fmwHeader];
    
    horizontalLine = [[UIView alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.delimiterSize)];
    [horizontalLine setBackgroundColor:colour.fmwTextColorLight];
    
    y += layout.delimiterSize;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.screenHeight - y - 2* layout.padding)];
    scrollView.clipsToBounds = YES;
    scrollView.delegate = self;
    scrollView.contentSize = CGSizeMake(layout.screenWidth, layout.screenHeight);
    
    feedbackLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, y + layout.buttonSize, layout.screenWidth, layout.buttonSize * 2)];
    [feedbackLabel setText:@""];
    [feedbackLabel setTextColor:colour.fmwTextColorLight];
    [feedbackLabel setNumberOfLines:0];
    [feedbackLabel setTextAlignment:NSTextAlignmentCenter];
    
    float arrowSize = layout.buttonSize * 1.5;
    feedbackArrow = [[UIImageView alloc] initWithFrame:CGRectMake(layout.screenWidth/2 - arrowSize/2, y + layout.buttonSize * 2+ layout.margin * 2, arrowSize, arrowSize)];
    [feedbackArrow setImage:[ImageUtil getImage:@"iconDown.png"]];
    [feedbackArrow setTintColor:colour.fmwActiveColor];
    
    float p = layout.padding/2;
    reloadButton = [[UIButton alloc] initWithFrame:CGRectMake((layout.screenWidth - layout.buttonSize)/2, 2 * layout.padding + 4 * layout.buttonSize, layout.buttonSize, layout.buttonSize)];
    [reloadButton setImage:[ImageUtil getImage:@"iconReload.png"] forState:UIControlStateNormal];
    [reloadButton.imageView setTintColor:colour.fmwActiveColor];
    [reloadButton addTarget:self action:@selector(reloadOptions) forControlEvents:UIControlEventTouchUpInside];
    [reloadButton setImageEdgeInsets:UIEdgeInsetsMake(p, p, p, p)];
}

-(void)addUIToView
{
    for(RouteOptionCard *card in routeOptions)
    {
        [scrollView addSubview:card];
    }
    [self XLog:[NSString stringWithFormat:@"Options =  %lu", (unsigned long)[routeOptions count]]];
    
    [self addSubview:headerBackground];
    [self addSubview:cancelButton];
    [self addSubview:resultsLabel];
    
    [self addSubview:locationButton];
    [self addSubview:destinationButton];
    [self addSubview:feedbackLabel];
    [self addSubview:timeButton];
    
    [self addSubview:switchButton];
    [self addSubview:horizontalLine];
    [self addSubview:scrollView];
}

-(void)animateIn
{
    if(isAnimating)
    {
        return;
    }
    isAnimating = YES;
    [super animateIn];
    [parentView.view addSubview:self];
    self.center = centerOffScreen;

    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOnScreen;
         
         [locationButton setTitle:appDelegate.tripQuery.startLocation.name forState:UIControlStateNormal];
         [destinationButton setTitle:appDelegate.tripQuery.endLocation.name forState:UIControlStateNormal];
         [self updateTimeUI];
         //[self createTripCards];
         
     }
                     completion:^(BOOL finished){
//                         [self updateUI];
                         [self getRouteOptions];
                         isAnimating = NO;
                     }];
}

//    DateTimeObject *dateTime = [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime];
-(void)updateTimeUI
{
    NSMutableString *timeString = [[NSMutableString alloc] init];
    [timeString appendString: NSLocalizedString(appDelegate.tripQuery.isDeparting ? @"Departing" : @"Arriving", nil)];
    [timeString appendString:@" "];
    if(appDelegate.tripQuery.dateTime != nil)
    {
        if([DateTimeUtil minutesBetweenCurrentTimeAnd:appDelegate.tripQuery.dateTime] > 24 * 60)
        {
            [timeString appendString:[NSString stringWithFormat:@"%@ %@ %@ %@",
                                  NSLocalizedString(@"at", nil),
                                  [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime].timeString,
                                  NSLocalizedString(@"on", nil),
                                  [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime].dateString]];
        }
        else
        {
            if(appDelegate.tripQuery.isDeparting)
            {
                if([DateTimeUtil minutesBetweenCurrentTimeAnd:appDelegate.tripQuery.dateTime] > 1)
                {
                    [timeString appendString:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"at", nil), [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime].timeString, nil]];
                }
                else
                {
                    [timeString appendString:NSLocalizedString(@"now", nil)];
                }
            }
            else
            {
                [timeString appendString:([DateTimeUtil minutesBetweenCurrentTimeAnd:appDelegate.tripQuery.dateTime] > 1 ?
                                      [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"at ", nil), [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime].timeString, nil]
                                      :
                                      NSLocalizedString(@"now", nil))];
            }
        }
    }
    else
    {
        [timeString appendString:[NSString stringWithFormat:@"%@",
                              //(appDelegate.tripQuery.isDeparting ? NSLocalizedString(@"in", nil) : NSLocalizedString(@"at", nil)),
                              NSLocalizedString(@"now", nil)]];
    }
    
    [timeButton setTitle:timeString forState:UIControlStateNormal];
}

-(void)animateOut
{
    if(isAnimating)
    {
        return;
    }
    isAnimating = YES;
    [super animateOut];
    self.center = centerOnScreen;
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOffScreen;
     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                         isAnimating = NO;
                     }];
}

-(void)back
{
    [feedbackLabel setText:@""];
    [self stopLoader];
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwScreen];
}

// SEARCH

-(void)searchLocation
{
    appDelegate.tripQuery.isEditingStart = YES;
    [self showSearchScreen];
}

-(void)searchDestination
{
    appDelegate.tripQuery.isEditingStart = NO;
    [self showSearchScreen];
}

-(void)showSearchScreen
{
    appDelegate.searchConfig.state = TripsScreen;
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).searchScreen];
}

-(void)showTimePicker
{
    appDelegate.searchConfig.state = TripsScreen;
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).timePicker];
}

-(void)swapRouteEndpoints
{
    [appDelegate.tripQuery swapEndPoints];
    [locationButton setTitle:appDelegate.tripQuery.startLocation.name forState:UIControlStateNormal];
    [destinationButton setTitle:appDelegate.tripQuery.endLocation.name forState:UIControlStateNormal];
    appDelegate.tripQuery.refreshingRouteOptions = YES;
    [self getRouteOptions];
}

// ROUTE OPTIONS

-(void)getRouteOptions
{
    //gettingRouteOptions = NO;
    if(!appDelegate.tripQuery.refreshingRouteOptions)
    {
        return;
    }
    
    appDelegate.tripQuery.refreshingRouteOptions = NO;
    
    if(tripCards != nil)
    {
        for(RouteOptionCard *tripCard in tripCards)
        {
            [tripCard removeFromSuperview];
        }
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.startLocation.latitudeString] forKey:@"startLatitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.startLocation.longitudeString] forKey:@"startLongitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.endLocation.latitudeString] forKey:@"endLatitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.endLocation.longitudeString] forKey:@"endLongitude"];
    
    if(appDelegate.tripQuery.dateTime != nil)
    {
        if(appDelegate.tripQuery.isDeparting)
        {
            [parameters setValue:appDelegate.tripQuery.dateTime forKey:@"startDate"];
        }
        else
        {
            [parameters setValue:appDelegate.tripQuery.dateTime forKey:@"endDate"];
        }
    }
    else
    {
        [parameters setValue:[DateTimeUtil currentDateTimeOffsetByMinutes:@"2"] forKey:@"startDate"];
    }
    
    [parameters setValue:[((AppViewController*)parentView).fmwScreen.modesPicker excludedModes] forKey:@"excludedModes"];
    [parameters setValue:[((AppViewController*)parentView).fmwScreen.modesPicker excludedOperators] forKey:@"excludedOperators"];
    
    [feedbackLabel setText:@"loading route options"];
    [reloadButton removeFromSuperview];
    [self startLoader:CGPointMake(layout.screenWidth/2, layout.buttonSize/2 + 2 * layout.padding + 4 * layout.buttonSize) inView:self];
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:Paths callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
    {
        NSLog(@"API - Paths: %ld", (long)response.statusCode);
        [self handleApiError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode];
        [appDelegate.apiResponse addPaths:[JSONParser convertJSONToDictionary:bodyString]];
        //[self getMapForCompetitionTrips];
        [self createTripCards];
        if(appDelegate.apiResponse.trips.count == 0)
        {
            [self showNoResultError];
        }
    }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
    {
        NSLog(@"API CONNECTION FAILURE - Paths: %@", error);
        [feedbackLabel setText:@""];
        [self stopLoader];
        [self handleError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode error:error];
    }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
    {
         // This is not going to be called in this example but it's included for completeness.
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
    }];
}

-(void)getMapForCompetitionTrips
{
    int i = 0;
    for(MultimodalTrip *trip in appDelegate.apiResponse.trips)
    {
        if(trip.isCompetitionTrip)
        {
            [self getMapCoordinates:trip index:i];
        }
        i++;
    }
}

-(void)getMapCoordinates:(MultimodalTrip*)trip index:(int)index
{
    if(trip.hasMapRoute)
    {
        NSLog(@"Trip[%d] already has coordinates", index);
        return;
    }
    
    NSLog(@"Getting Coordinates for Trip[%d]", index);
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:trip.tripId forKey:@"tripid"];
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:Trips callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
     {
         //[self stopLoader];
         NSLog(@"API - Trips: %ld", (long)response.statusCode);
         [appDelegate.apiResponse addTrip:[JSONParser convertJSONToDictionary:bodyString] toIndex:index];
     }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
     {
         //[self stopLoader];
            NSLog(@"API CONNECTION FAILURE - Trips: %@", error);
     }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
     {
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
     }];
}

-(void)createTripCards
{
    NSLog(@"Creating Trip Cards");
    [feedbackLabel setText:@""];
    [feedbackArrow removeFromSuperview];
    [self stopLoader];
    appDelegate.tripQuery.refreshingRouteOptions = NO;
    
    int offset = 0;//layout.padding;
    //int cardHeight = layout.buttonSize * 4;
    int index = 0;
    
    routeOptions = [[NSMutableArray alloc] init];
    cardHeight = 3 * layout.buttonSize;
    
    if(tripCards != nil)
    {
        for(UIView *view in tripCards)
        {
            [view removeFromSuperview];
        }
    }
    
    tripCards = [[NSMutableArray alloc] init];
    for(MultimodalTrip *trip in appDelegate.apiResponse.trips)
    {
        RouteOptionCard *tripCard = [[RouteOptionCard alloc] initWithFrame:CGRectMake(0, offset + index*(layout.padding + cardHeight), layout.screenWidth, cardHeight)];
        tripCard.center = CGPointMake(tripCard.center.x , tripCard.center.y + layout.screenHeight);
        
        [tripCard initWithIndex:index inController:parentView];
        [tripCard initViewElements:trip];
        tripCard.selectButton.tag = index;
        [tripCard.selectButton addTarget:self action:@selector(selectTrip:) forControlEvents:UIControlEventTouchUpInside];
      //[tripCard.selectButton addTarget:self action:@selector(highlightButton:) forControlEvents:UIControlEventTouchDown];
        
        tripCard.overviewButton.tag = index;
        [tripCard.overviewButton addTarget:self action:@selector(showOverview:) forControlEvents:UIControlEventTouchUpInside];
        
        tripCard.mapImageButton.tag = index;
        [tripCard.mapImageButton addTarget:self action:@selector(showOnMap:) forControlEvents:UIControlEventTouchUpInside];
        
        [tripCards addObject:tripCard];
        [scrollView addSubview:tripCard];
        index++;
    }
    
    for(RouteOptionCard *tripCard in tripCards)
    {
        [UIView animateWithDuration:0.5 animations:^
         {
             tripCard.center = CGPointMake(tripCard.center.x, tripCard.center.y - layout.screenHeight);
         }
                         completion:^(BOOL isFinished){
                             [tripCard setCarbonPoints];
                         }];
    }
    
    NSLog(@"");
    scrollView.contentSize = CGSizeMake(layout.screenWidth, offset + index * (cardHeight + layout.padding));
}

-(IBAction)selectTrip:(id)sender
{
    UIButton *button = (UIButton*)sender;
    [button setBackgroundColor:colour.clear];
    [button setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    
    NSUInteger tag = ((UIButton*)sender).tag;
    appDelegate.tripQuery.selectedTripIndex = tag;
    appDelegate.tripQuery.selectedTrip = (MultimodalTrip*)[appDelegate.apiResponse.trips objectAtIndex:tag];
    [self checkForRoute];
    
    [((AppViewController*)parentView).fmwJourneyScreen initView:@"Journey Mode" inController:parentView];
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwJourneyScreen];
}

-(IBAction)showOnMap:(id)sender
{
    NSUInteger tag = ((UIButton*)sender).tag;
    appDelegate.tripQuery.selectedTripIndex = tag;
    appDelegate.tripQuery.selectedTrip = (MultimodalTrip*)[appDelegate.apiResponse.trips objectAtIndex:tag];
    
    if(appDelegate.tripQuery.selectedTrip.hasMapRoute)
    {
        [((AppViewController*)parentView).fmwMapView setState:JourneyPreview];
        [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwMapView];
    }
}

-(IBAction)showOverview:(id)sender
{
    NSUInteger tag = ((UIButton*)sender).tag;
    appDelegate.tripQuery.selectedTripIndex = tag;
    appDelegate.tripQuery.selectedTrip = (MultimodalTrip*)[appDelegate.apiResponse.trips objectAtIndex:tag];
    
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController.journeyOverviewScreen addContent:appDelegate.tripQuery.selectedTrip];
    [viewController swapInView:viewController.journeyOverviewScreen];
}

-(void)checkForRoute
{
    
}

-(void)showNoResultError
{
    [feedbackLabel setText:@"Couldn't get results.\nTap to retry."];
    scrollView.contentSize = CGSizeMake(layout.screenWidth, layout.screenHeight);
    //[self addSubview:feedbackArrow];
    [self addSubview:reloadButton];
}

-(void)scrollViewDidEndDragging:(UIScrollView *)argScrollView willDecelerate:(BOOL)decelerate
{
    [feedbackLabel setText:@""];
    if (argScrollView.contentOffset.y <= -minRefreshScrollOffset)
    {
        appDelegate.tripQuery.refreshingRouteOptions = YES;
        //[feedbackArrow removeFromSuperview];
        //[feedbackLabel setText:@"loading route options"];
        //[self getRouteOptions];
    }
}

-(void)reloadOptions
{
    appDelegate.tripQuery.refreshingRouteOptions = YES;
    [reloadButton removeFromSuperview];
    [self getRouteOptions];
}

-(void)scrollViewDidScroll:(UIScrollView *)argScrollView
{
    if(arrowAngle <= 180.0)
    {
        arrowAngle = scrollView.contentOffset.y * (180.0 / minRefreshScrollOffset);
    }
    
    if(arrowAngle <= 180.0)
    {
        CALayer *layer = feedbackArrow.layer;
        
        CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
        rotationAndPerspectiveTransform = CATransform3DTranslate(rotationAndPerspectiveTransform, 0, 0, 20);
        rotationAndPerspectiveTransform.m34 = 5.0 / -500;
        rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, arrowAngle * M_PI / 180.0f, 1.0f, 0.0f, 0.0f);
        layer.transform = rotationAndPerspectiveTransform;
    }
    
    if((int)scrollView.contentOffset.y % 5 == 0)
    {
        //NSLog(@"ScrollView Content Offset: %f", scrollView.contentOffset.y);
    }
    
    if(argScrollView.contentOffset.y <= -minRefreshScrollOffset)
    {
        //[feedbackLabel setText:@"release to refresh"];
    }
    
    if(argScrollView.contentOffset.y >= 0)
    {
        [feedbackLabel setText:@""];
    }
}

// WALKING

-(void)getWalkingOption
{
//    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
//    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.startLocation.latitudeString] forKey:@"startLatitude"];
//    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.startLocation.longitudeString] forKey:@"startLongitude"];
//    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.endLocation.latitudeString] forKey:@"endLatitude"];
//    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.endLocation.longitudeString] forKey:@"endLongitude"];
//    [parameters setValue:@"pedestrian" forKey:@"mode"];
//    
//    [self startLoader:CGPointMake((layout.screenWidth)/2, layout.padding * 1.5) inView:self];
//    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:Directions callType:GET parameters:parameters];
//    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
//    [connection executeRequestOnSuccess:
//     ^(NSHTTPURLResponse *response, NSString *bodyString)
//     {
//         NSLog(@"SUCCESS: %ld", (long)response.statusCode);
//         NSDictionary *responseDictionary = [JSONParser convertJSONToDictionary:bodyString];
//         [self handleApiError:responseDictionary statusCode:(int)response.statusCode];
//         [self stopLoader];
//         
//         [selectedStage addDirections:[[MapRoute alloc] initWithDictionary:responseDictionary]];
//         //[self refreshStepTiles];
//     }
//                                failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
//     {
//         [self stopLoader];
//         [self handleError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode error:error];
//     }
//                            didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
//     {
//         // This is not going to be called in this example but it's included for completeness.
//         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
//     }];
}

-(void)addWalkingOption
{
    
}
@end