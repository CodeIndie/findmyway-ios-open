//
//  TimePicker.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/08.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "TimePicker.h"

@implementation TimePicker

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(device.screenWidth*0.5, frame.origin.y + frame.size.height*1.5);
        centerOnScreen = CGPointMake(device.screenWidth*0.5, frame.origin.y + frame.size.height*0.5);
    }
    return self;
}

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    
    onScreenPickerCenter = CGPointMake(self.center.x, self.center.y - appDelegate.layout.buttonSize);
    leftOffScreenCenter = CGPointMake(self.center.x - self.bounds.size.width, self.center.y - appDelegate.layout.buttonSize);
    rightOffScreenCenter = CGPointMake(self.center.x + self.bounds.size.width, self.center.y - appDelegate.layout.buttonSize);
 
    [self initViewElements];
    [self initIntervalPickerView];
    [self initDateTimePickerView];
    
    [self addUIToView];
}

-(void)initViewElements
{
    [self setBackgroundColor:colour.fmwBackgroundColor];//[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    
    // TOP & BOTTOM BAR
    topbarBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.buttonSize)];
    [topbarBackground setBackgroundColor:colour.fmwHeader];
    
    topbarLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, layout.delimiterSize)];
    [topbarLine setBackgroundColor:colour.fmwTextColor];
    
    // BOTTOM BAR
    bottomBarBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.screenHeight - layout.buttonSize - layout.topOfContent, layout.screenWidth, layout.buttonSize)];
    [bottomBarBackground setBackgroundColor:colour.fmwHeader];
    
    bottomBarLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.screenHeight - layout.buttonSize - layout.topOfContent, layout.screenWidth, layout.delimiterSize)];
    [bottomBarLine setBackgroundColor:colour.fmwTextColor];
    
    float y = self.bounds.size.height/2 - 4 * layout.buttonSize;
    pickerBackground = [[UIImageView alloc] initWithFrame:CGRectMake(layout.screenWidth/2 - layout.buttonSize * 3.25 - layout.padding, y, layout.buttonSize * 6.5 + 2 * layout.padding, layout.buttonSize * 8)];
    [pickerBackground setBackgroundColor:colour.fmwBackgroundColor];
    
    float sideButtonSize = layout.buttonSize * 1.5;
    confirmButton =[XUIButton initWithFrame:CGRectMake(self.bounds.size.width - sideButtonSize - layout.padding, 0, sideButtonSize, layout.buttonSize)
                                      title:NSLocalizedString(@"Submit", nil)
                                   fontSize:[UIFont systemFontSize] + 1
                                       bold:YES
                                 titleColor: colour.fmwActiveColor
                            backgroundColor:colour.clear
                        horizontalAlignment:UIControlContentHorizontalAlignmentRight];
    [confirmButton addTarget:self action:@selector(confirm) forControlEvents:UIControlEventTouchUpInside];
    
    cancelButton = [XUIButton initWithFrame:CGRectMake(layout.padding, 0, sideButtonSize, layout.buttonSize)
                                      title:NSLocalizedString(@"Cancel", nil)
                                   fontSize:[UIFont systemFontSize] + 1
                                       bold:YES
                                 titleColor:colour.fmwActiveColor
                            backgroundColor:colour.clear
                        horizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [cancelButton addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
    
    timeToggleButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding + sideButtonSize, 0, layout.screenWidth - 2 * (sideButtonSize + layout.padding), layout.buttonSize)];
    [timeToggleButton setTitle:(appDelegate.tripQuery.isDeparting ? NSLocalizedString(@"Departing", nil) : NSLocalizedString(@"Arriving", nil)) forState:UIControlStateNormal];
    [timeToggleButton.titleLabel setFont:[UIFont boldSystemFontOfSize:layout.systemFontSize + 2]];
    [timeToggleButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [timeToggleButton addTarget:self action:@selector(toggleTimeType) forControlEvents:UIControlEventTouchUpInside];
    
    y = layout.screenHeight - layout.buttonSize - layout.topOfContent;
    dateTimeButton =[[UIButton alloc] initWithFrame:CGRectMake(
                  self.bounds.size.width/2, y,
                  pickerBackground.bounds.size.width/2,
                  layout.buttonSize)];
    [dateTimeButton setTitle:NSLocalizedString(@"Arriving", nil) forState:UIControlStateNormal];
    [dateTimeButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [dateTimeButton addTarget:self action:@selector(showDateTime) forControlEvents:UIControlEventTouchUpInside];
    
    intervalButton = [[UIButton alloc] initWithFrame:CGRectMake(
                     self.bounds.size.width/2 - pickerBackground.bounds.size.width/2,
                     y,
                     pickerBackground.bounds.size.width/2,
                    layout.buttonSize)];
    [intervalButton setTitle:NSLocalizedString(@"Departing", nil) forState:UIControlStateNormal];
    [intervalButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [intervalButton addTarget:self action:@selector(showInterval) forControlEvents:UIControlEventTouchUpInside];
}

-(void)addUIToView
{
    [self addSubview:topbarBackground];
    [self addSubview:topbarLine];
    [self addSubview:bottomBarBackground];
    [self addSubview:bottomBarLine];
    
    [self addSubview:pickerBackground];
    
    //self addSubview:timeToggleButton];
    [self addSubview:confirmButton];
    [self addSubview:cancelButton];
    
    [self addSubview:intervalButton];
    [self addSubview:dateTimeButton];
    
    [self addSubview:intervalPicker];
    [self addSubview:dateTimePicker];
}

-(void)initIntervalPickerView
{
    timeOffsetList = [[NSMutableArray alloc] init];
    [timeOffsetList addObject:@(0)];
    [timeOffsetList addObject:@(15)];
    [timeOffsetList addObject:@(30)];
    [timeOffsetList addObject:@(45)];
    [timeOffsetList addObject:@(60)];
    [timeOffsetList addObject:@(90)];
    for(int i = 2 ; i <= 24 ; i++)
    {
        [timeOffsetList addObject:@(i*60)];
    }
    
    intervalPicker = [[UIPickerView alloc] init];
    CGPoint pickerCenter;
    if(!appDelegate.tripQuery.isDeparting)
    {
        pickerCenter = onScreenPickerCenter;
    }
    else
    {
        pickerCenter = leftOffScreenCenter;
    }
    intervalPicker.center = pickerCenter;
    
    intervalPicker.delegate = self;
    intervalPicker.showsSelectionIndicator = YES;
    [intervalPicker sizeToFit];
    [self addSubview:intervalPicker];
}

-(void)initDateTimePickerView
{
    dateTimePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, appDelegate.layout.screenHeight/4, layout.screenWidth, layout.screenHeight/2)];
    dateTimePicker.datePickerMode = UIDatePickerModeDateAndTime;
    [dateTimePicker setMinimumDate:[NSDate date]];
    [dateTimePicker setDate:[NSDate date]];
    
    CGPoint pickerCenter;
    if(!appDelegate.tripQuery.isDeparting)
    {
        pickerCenter = rightOffScreenCenter;
    }
    else
    {
        pickerCenter = onScreenPickerCenter;
    }
    dateTimePicker.center = pickerCenter;
    [self addSubview:dateTimePicker];
}

-(void)animateIn
{
    [super animateIn];
    [parentView.view addSubview:self];
    self.center = centerOffScreen;
    if(appDelegate.tripQuery.isDeparting)
    {
        dateTimePicker.center = rightOffScreenCenter;
        intervalPicker.center = centerOnScreen;
    }
    else
    {
        dateTimePicker.center = centerOnScreen;
        intervalPicker.center = leftOffScreenCenter;
    }
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOnScreen;
     }
     completion:^(BOOL finished)
     {
         if(appDelegate.tripQuery.isDeparting)
         {
             [dateTimeButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
             [intervalButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
         }
         else
         {
             [dateTimeButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
             [intervalButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
         }
         [timeToggleButton setTitle:(appDelegate.tripQuery.isDeparting ? NSLocalizedString(@"Departing", nil) : NSLocalizedString(@"Arriving", nil)) forState:UIControlStateNormal];
     }];
}

-(void)animateOut
{
    [super animateOut];
    self.center = centerOnScreen;
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOffScreen;
     }
     completion:^(BOOL finished){
         [self removeFromSuperview];
     }];
}

-(void)confirm
{
    if(isUsingDatePicker)
    {
        appDelegate.tripQuery.dateTime = [DateTimeUtil nsDateToUTC:dateTimePicker.date];
    }
    else
    {
        NSString *timeOffset = [NSString stringWithFormat:@"%@", selectedTimeOffset];
        appDelegate.tripQuery.dateTime = [DateTimeUtil currentDateTimeOffsetByMinutes:timeOffset];
    }
    
    if(appDelegate.searchConfig.state == TripsScreen)
    {
        appDelegate.tripQuery.refreshingRouteOptions = YES;
        [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwRouteOptions];
    }
    else
    {
        [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwScreen];
    }
}

-(void)cancel
{
    if(appDelegate.searchConfig.state == TripsScreen)
    {
        [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwRouteOptions];
    }
    else
    {
        [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwScreen];
    }
}

-(void)toggleTimeType
{
    appDelegate.tripQuery.isDeparting = !appDelegate.tripQuery.isDeparting;
    
    if(appDelegate.tripQuery.isDeparting)
    {
        [dateTimeButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
        [intervalButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
        [timeToggleButton setTitle:NSLocalizedString(@"Departing", nil) forState:UIControlStateNormal];
    }
    else
    {
        [dateTimeButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
        [intervalButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
        [timeToggleButton setTitle:NSLocalizedString(@"Arriving", nil) forState:UIControlStateNormal];
    }
}

-(void)showDateTime
{
    if(appDelegate.tripQuery.isDeparting)
    {
        [self toggleTimeType];
    }
    
    [UIView animateWithDuration:0.5
    animations:^(void)
    {
        intervalPicker.center = leftOffScreenCenter;
        dateTimePicker.center = centerOnScreen;
    }
    completion:^(BOOL finished)
    {
        isUsingDatePicker = YES;
    }];
}

-(void)showInterval
{
    if(!appDelegate.tripQuery.isDeparting)
    {
        [self toggleTimeType];
    }
    
    [UIView animateWithDuration:0.5
                     animations:^(void)
     {
         intervalPicker.center = centerOnScreen;
         dateTimePicker.center = rightOffScreenCenter;
     }
                     completion:^(BOOL finished)
     {
         isUsingDatePicker = NO;
     }];
}


// CALLBACKS -  INTERVAL

- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    if(row == 0)
    {
        title = @"Now";
    }
    else
    {
        title = [self formatMinutesToTime:[[timeOffsetList objectAtIndex:row] intValue]];
        //title = [DateTimeUtility timeStringFromMinutes:[NSString stringWithFormat:@"%d",[[timeOffsetList objectAtIndex:row] intValue]]];
    }

    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:title attributes:@{NSForegroundColorAttributeName:colour.fmwTextColor}];
    
    return attString;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component
{
    selectedTimeOffset = [timeOffsetList objectAtIndex:row];
    NSLog(@"Did select row with item: %@", selectedTimeOffset);
    //selectedItemIndex = row;
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSUInteger numRows = [timeOffsetList count];
    
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title;
    if(row == 0)
    {
        title = @"As soon as possible";
    }
    else
    {
        title = [self formatMinutesToTime:[[timeOffsetList objectAtIndex:row] intValue]];
        //title = [DateTimeUtility timeStringFromMinutes:[NSString stringWithFormat:@"%d",[[timeOffsetList objectAtIndex:row] intValue]]];
    }
    
    return title;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    int sectionWidth = 320;
    
    return sectionWidth;
}

-(NSString *)formatMinutesToTime:(int)minutes
{
    NSString *formattedTime;
    if(minutes > 60)
    {
        NSString *hoursString = @" hours";
        if(minutes / 60 < 2)
        {
            hoursString = @"hour";
        }
        NSString *minutesString = @" min";
        if(minutes % 60 < 10)
        {
            formattedTime = [NSString stringWithFormat:@"%d%@", (minutes / 60), hoursString];
        }
        else
        {
            formattedTime = [NSString stringWithFormat:@"%d%@ %d%@", (minutes / 60), hoursString, minutes % 60, minutesString ];
        }
    }
    else
    {
        formattedTime = [NSString stringWithFormat:@"%d min", minutes];
    }
    return formattedTime;
}

@end
