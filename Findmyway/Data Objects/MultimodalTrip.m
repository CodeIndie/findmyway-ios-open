//
//  MultimodalResult.m
//  tct
//
//  Created by Bilo on 9/10/14.
//

#import "MultimodalTrip.h"
#import "DistanceUtil.h"
#import "JCDHTTPConnection.h"

@implementation MultimodalTrip

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self)
    {
        self.tripId = [dictionary objectForKey:@"tripId"];
        self.startTime = [dictionary objectForKey:@"startTime"];
        self.endTime = [dictionary objectForKey:@"endTime"];
        self.estimatedTotalCost = [dictionary objectForKey:@"estimatedTotalCost"];
        self.totalWalkingDistance = [dictionary objectForKey:@"totalWalkingDistance"];
        self.initialWalkingDistance = [dictionary objectForKey:@"initialWalkingDistance"];
        
        self.stages = [[NSMutableArray alloc] init];
        NSMutableArray *routeStages = [dictionary objectForKey:@"stages"];
        for(int i = 0 ; i < [routeStages count] ; i++)
        {
            MultimodalStage *stage = (MultimodalStage*)[[MultimodalStage alloc] initWithDictionary:[routeStages objectAtIndex:i]];
            [self.stages addObject:stage];
         
            if(i == 0)
            {
                self.firstStopName = stage.startLocation.name;
                self.firstStopAddress = stage.startLocation.descriptionText;
                self.firstStopLocation = stage.startLocation.location;
            }
            if( i == routeStages.count - 1)
            {
                self.lastStopName = stage.endLocation.name;
                self.lastStopAddress = stage.endLocation.descriptionText;
                self.lastStopLocation = stage.endLocation.location;
            }
            
            if(![stage.modeType isEqualToString:@"Pedestrian"])
            {
                stage.hasDirections = YES;
            }
        }
        
        for(MultimodalStage *stage in self.stages)
        {
            if([CompetitionUtil isCompetitionOperator:stage.modeName])
            {
                self.isCompetitionTrip = YES;
                //[self getRouteCoordinates];
                break;
            }
        }
        
        self.fareMessages = [[NSMutableArray alloc] init];
        for(NSString *fareMessage in [dictionary objectForKey:@"fareMessages"])
        {
            [self.fareMessages addObject:fareMessage];
        }
    }
    return self;
}

-(void)getRouteCoordinates
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:self.tripId forKey:@"tripid"];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:[[ApiUtil instance] urlForRequest:Trips callType:GET parameters:parameters]];
    
    [connection executeRequestOnSuccess:^(NSHTTPURLResponse *response, NSString *bodyString)
    {
        NSLog(@"API Response: %ld", (long)response.statusCode);
        if(response.statusCode > 200 && response.statusCode < 300)
        {
            [self addMapRoute:[[PublicMapRoute alloc] initWithDictionary:[JSONParser convertJSONToDictionary:bodyString]]];
        }
    }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
    {
        NSLog(@"NETWORK ERROR: %@", error);
    }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
    {
        NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
    }];
}

-(void)addMapRoute:(PublicMapRoute *)mapRoute
{
    self.mapRoute = [[PublicMapRoute alloc] initWithMapRoute:mapRoute];
    self.hasMapRoute = YES;
    
    if(self.isCompetitionTrip)
    {
        [self calculatePoints];
    }
}

-(void)calculatePoints
{
    if(![CompetitionUtil isEarthHourTime] && [CompetitionUtil isCompeting])
    {
        return;
    }

    float meters = 0;
    self.carbonPoints = 0;
    
    int i = 0;
    for(MultimodalStage *stage in self.stages)
    {
        if([CompetitionUtil isCompetitionOperator:stage.modeName])
        {
            if(self.hasMapRoute)
            {
                @try
                {
                    PublicMapRouteSegment *segment = (PublicMapRouteSegment*)[self.mapRoute.segments objectAtIndex:i];
                    
                    for(int x = 0; x < segment.coordinates.count - 1; x++)
                    {
                        Location *pointA = (Location*)[segment.coordinates objectAtIndex:x];
                        Location *pointB = (Location*)[segment.coordinates objectAtIndex:x+1];
                        meters += [DistanceUtil distanceBetween:pointA locationB:pointB];
                    }
                    
                    self.carbonPoints += meters * [CompetitionUtil operatorWeighting:stage.modeName];
                    meters = 0;
                    NSLog(@"_____________________");
                    NSLog(@"Meters: %f", meters);
                    NSLog(@"Weight: %f", [CompetitionUtil operatorWeighting:stage.modeName]);
                    NSLog(@"Points: %d", self.carbonPoints);
                }
                @catch (NSException *e)
                {
                    NSLog(@"ERROR IN POINT CALCULATION: COULD NOT RETRIEVE ROUTE");
                }
            }
        }
        i++;
    }
    
    self.carbonPoints *= 2;
}

-(BOOL)requiresTicketInfo
{
    for(MultimodalStage *stage in self.stages)
    {
        if([stage.modeName isEqualToString:@"MyCiti"])
        {
            return YES;
        }
    }
    return NO;
}
@end