//
//  RouteStep.m
//  tct
//
//  Created by Bilo on 4/20/14.
//

#import "RouteStep.h"

@implementation RouteStep

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self != nil)
    {
        self.order = (int)[[dictionary objectForKey:@"order"] floatValue];
        self.instructions = [dictionary objectForKey:@"instructions"];
        self.distance = [dictionary objectForKey:@"distance"];
        self.duration = [dictionary objectForKey:@"duration"];
        self.point = [[Location alloc] initWithDictionary:[dictionary objectForKey:@"point"]];
    }
    return self;
}

-(id)initWithStep:(RouteStep *)step
{
    self = [super init];
    if(self != nil)
    {
        self.order = step.order;
        self.instructions = step.instructions;
        self.distance = step.distance;
        self.duration = step.duration;
        self.point = step.point;
    }
    return self;
}
@end
