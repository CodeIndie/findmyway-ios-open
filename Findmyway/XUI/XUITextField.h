//
//  XUITextField.h
//  Findmyway
//
//  Created by Bilo on 3/3/15.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface XUITextField : UIView
{
    UITextField *textField;
    UIButton *detectionButton;
    UILabel *placeholder;
}

-(id)initWithFrame:(CGRect)frame placeHolder:(NSString*)placeHolder text:(NSString*)text appDelegate:(AppDelegate*)appDelegate;
@end
