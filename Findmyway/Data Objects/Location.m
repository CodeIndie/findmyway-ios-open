//
//  GPSCoordinate.m
//
//  Created by Bilo on 4/16/14.
//

#import "Location.h"

@implementation Location

-(id)initWithLocation:(Location *)coordinate
{
    self = [super init];
    if(self != nil)
    {
        self.latitude = coordinate.latitude;
        self.longitude = coordinate.longitude;
        self.latitudeString = coordinate.latitudeString;
        self.longitudeString = coordinate.longitudeString;
        self.coordinate = coordinate.coordinate;
        self.name = coordinate.name;
        self.address = coordinate.address;
    }
    return self;
}

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self != nil)
    {
        self.latitudeString = [dictionary objectForKey:@"latitude"];
        self.longitudeString = [dictionary objectForKey:@"longitude"];
        
        self.latitude = [self.latitudeString floatValue];
        self.longitude = [self.longitudeString floatValue];
        
        self.coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    }
    return self;
}

-(id)initWithLatitude:(float)latitude longitude:(float)longitude
{
    self = [super init];
    if(self != nil)
    {
        self.latitude = latitude;
        self.longitude = longitude;
        
        self.latitudeString = [NSString stringWithFormat:@"%f", self.latitude];
        self.longitudeString = [NSString stringWithFormat:@"%f", self.longitude];
        
        self.coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    }
    return self;
}

-(id)initWithLatitudeString:(NSString*)latitude longitudeString:(NSString*)longitude
{
    self = [super init];
    if(self != nil)
    {
        self.latitudeString = latitude;
        self.longitudeString = longitude;
        
        self.latitude = [self.latitudeString floatValue];
        self.longitude = [self.longitudeString floatValue];
        
        self.coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    }
    return self;
}

-(id)initWithCLLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude
{
    self = [super init];
    if(self != nil)
    {
        self.latitudeString = [NSString stringWithFormat:@"%f",latitude];
        self.longitudeString = [NSString stringWithFormat:@"%f",longitude];
        
        self.latitude = [self.latitudeString floatValue];
        self.longitude = [self.longitudeString floatValue];
        
        self.coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    }
    return self;
}

-(NSString*)gpsCoordinates
{
    return [NSString stringWithFormat:@"lat:%@,lon:%@", self.latitudeString, self.longitudeString];
}

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    if (self)
    {
        self.latitudeString = [aDecoder decodeObjectForKey:@"latitudeString"];
        self.longitudeString = [aDecoder decodeObjectForKey:@"longitudeString"];
        self.latitude= [self.latitudeString floatValue];
        self.longitude = [self.longitudeString floatValue];
        self.coordinate = CLLocationCoordinate2DMake(self.latitude, self.longitude);
    }
    return self;

}

-(void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.latitudeString forKey:@"latitudeString"];
    [aCoder encodeObject:self.longitudeString forKey:@"longitudeString"];
}
@end
