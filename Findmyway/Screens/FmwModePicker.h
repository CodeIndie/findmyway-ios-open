//
//  FmwModePicker.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "XUIView.h"
#import "XUIToggleButton.h"
#import "AppViewController.h"

@interface FmwModePicker : XUIView
{
    UIImageView *horizontalLine;
    BOOL showingOperatorsMenu;
    BOOL showingMainMenu;
}
@property XUIToggleButton *pedestrian;
@property XUIToggleButton *bus;
@property XUIToggleButton *rail;
@property XUIToggleButton *taxi;
@property XUIToggleButton *boat;

@property UIButton *filter;
@property UIButton *menuButton;

-(void)toggleMainMenu;
-(void)toggleOperatorsMenu;
-(NSMutableArray*)excludedModes;
-(NSMutableArray*)excludedOperators;

-(void)setShowOperatorsMenuTo:(BOOL)val;
-(void)setShowMainMenuTo:(BOOL)val;

@end
