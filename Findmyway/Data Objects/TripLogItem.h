//
//  TripLogItem.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/11.
//

#import <Foundation/Foundation.h>

@interface TripLogItem : NSObject <NSCoding>

@property NSString *startLocation;
@property NSString *endLocation;
@property int points;
@property NSDate *savedDateTime;
@property NSString *dateTime;

-(id)initWithStart:(NSString*)startLocation end:(NSString*)endLocation points:(int)points dateTime:(NSString*)dateTime;
-(BOOL)isEqualTo:(TripLogItem*)item;
-(void)encodeWithCoder:(NSCoder *)aCoder;
-(id)initWithCoder:(NSCoder *)aDecoder;


@end
