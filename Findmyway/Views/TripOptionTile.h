//
//  TripOptionTile.h
//  Findmyway
//
//  Created by Bilo on 12/9/14.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface TripOptionTile : UIView

@property UIButton *button;
@property UIButton *announcements;
@property UIImageView *background;
@property UIImageView *letter;

-(id)initWithDelegate:(AppDelegate*)appDelegate index:(int)index;

@end
