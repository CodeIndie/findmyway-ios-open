//
//  MultimodalPoint.m
//  tct
//
//  Created by Bilo on 9/10/14.
//

#import "MultimodalPoint.h"

@implementation MultimodalPoint

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self)
    {
        self.name = [dictionary objectForKey:@"name"];
        self.descriptionText = [dictionary objectForKey:@"description"];
        self.time = [dictionary objectForKey:@"time"];
        self.type = [dictionary objectForKey:@"type"];
        self.platformNumber = [dictionary objectForKey:@"platformNumber"];
        self.location = [[Location alloc] initWithDictionary:[dictionary objectForKey:@"point"]];
    }
    return self;
}
@end
