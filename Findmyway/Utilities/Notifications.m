//
//  Notifications.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/07.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "Notifications.h"

@implementation Notifications

static Notifications *sharedInstance = nil;

+(Notifications*)instance
{
    if(sharedInstance == nil)
    {
        sharedInstance = [[Notifications alloc] init];
    }
    return sharedInstance;
}

-(id)init
{
    self = [super init];
    if(self)
    {
        self.GETAddressSuccess = @"GETAddressSuccess";
        self.GETAddressFail = @"GETAddressFail";
        
        self.GETTripsSuccess = @"GETTripsSuccess";
        self.GETTripsFail = @"GETTripsFail";

        self.GETPathsSuccess = @"GETPathsSuccess";
        self.GETPathsFail = @"GETPathsFail";
        
        self.GETSearchItemsSuccess = @"GETSearchItemsSuccess";
        self.GETSearchItemsFail = @"GETSearchItemsFail";
        
        self.GETOperatorsSuccess = @"GETOperatorsSuccess";
        self.GETOperatorsFail = @"GETOperatorsFail";
        
        self.MapReloadRoute = @"MapReloadRoute";
    }
    return self;
}
@end
