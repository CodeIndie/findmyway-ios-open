//
//  XUIRect.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "XUIRect.h"

@implementation XUIRect

-(id)initWithRect:(CGRect)rect centerOffset:(CGPoint)offset
{
    self = [super init];
    if(self)
    {
        self.rect = rect;
        self.center = CGPointMake(self.rect.origin.x + self.rect.size.width, self.rect.origin.y + self.rect.size.height);
        
        self.rectOffset = CGRectMake(self.rect.origin.x + offset.x, self.rect.origin.y + offset.y, self.rect.size.width, self.rect.size.height);
        self.centerOffset = CGPointMake(self.center.x + offset.x, self.center.y + offset.y);
    }
    return self;
}

@end
