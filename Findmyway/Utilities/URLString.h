//
//  URLString.h
//  FMW
//
//  Created by Bilo Lwabona on 11/21/13.
//

#import <Foundation/Foundation.h>
#import "DictionaryUtil.h"
@interface URLString : NSObject

@property CFStringRef encoded;
@property CFStringRef decoded;
@property NSString *encodedString;
@property NSString *decodedString;

-(id)initWithString:(NSString *)urlString;
+(NSString*)encodeString:(NSString*)string;
+(NSString*)urlParameterString:(NSDictionary*)dictionary;
@end
