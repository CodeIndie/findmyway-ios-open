//
//  TripStepTile.h
//  Findmyway
//
//  Created by Bilo on 3/5/15.
//

#import <UIKit/UIKit.h>
#import "MultimodalTrip.h"
#import "AppDelegate.h"
#import "XUIShape.h"
#import "DateTimeUtil.h"

typedef enum
{
    WalkingStart,
    WalkingMiddle,
    WalkingEnd,
    WalkingExpansionButton,
    
    TransportStart,
    TransportMiddle,
    TransportEnd,
    TransportExpansionButton
    
}TripStepType;

@interface TripStepTile : UIView
{
    AppDelegate *appDelegate;
    UIView *line;
    UIView *circle;
    UIImageView *icon;
    
    UILabel *mainText;
    UILabel *subText;
    UILabel *time;
    UILabel *stopCount;
    LayoutConfig *layout;
    BOOL isShowingDetail;
}

@property UIButton *button;

-(id)initWithFrame:(CGRect)frame stepType:(TripStepType)stepType step:(MultimodalPoint*)step stage:(MultimodalStage*)stage trip:(MultimodalTrip*)operatorName instructions:(NSString*)instructions directionIndex:(int)directionIndex;

@end
