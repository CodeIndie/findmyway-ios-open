//
//  MapConfig.h
//  FindMyWay
//
//  Created by Bilo on 1/22/15.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    PointSelection = 0,
    PointPreview = 1,
    PointPreviewFromHome = 2,
    JourneyPreview = 3,
    JourneyOverView = 4,
    JourneyMode = 5
}
MapState;

@interface MapConfig : NSObject

@property MapState state;

@end
