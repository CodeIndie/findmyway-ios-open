//
//  MapRoute.m
//  tct
//
//  Created by Bilo on 4/16/14.
//

#import "MapRoute.h"

@implementation MapRoute

-(id)initWithDictionary:(NSDictionary *)dict
{
    self = [super init];
    
    if(self != nil)
    {
        
        //dictionary = [dictionary objectForKey:@"routes"];
        NSArray *routesArray = [dict objectForKey:@"routes"];
        NSDictionary *dictionary = [routesArray objectAtIndex:0];
        
        
        self.totalDistance = [dictionary objectForKey:@"totalDistance"];
        self.estimatedTotalTime = [dictionary objectForKey:@"estimatedTotalTime"];
        
        NSDictionary *boundingBoxTopLeft = [dictionary objectForKey:@"boundingBoxTopLeft"];
        self.boundingBoxTopLeft = [[Location alloc] initWithDictionary:boundingBoxTopLeft];

        NSDictionary *boundingBoxBottomRight = [dictionary objectForKey:@"boundingBoxBottomRight"];
        self.boundingBoxBottomRight = [[Location alloc] initWithDictionary:boundingBoxBottomRight];
        
        NSArray *stepsArray = [dictionary objectForKey:@"steps"];
        self.steps = [[NSMutableArray alloc] init];
        for(int i = 0 ; i < [stepsArray count] ; i++)
        {
            RouteStep *step = [[RouteStep alloc] initWithDictionary:[stepsArray objectAtIndex:i]];
            
            [self.steps addObject:step];
        }
        NSLog(@"Steps: %lu", (unsigned long)[self.steps count]);
        
        NSArray *coordinatesArray = [dictionary objectForKey:@"points"];
        self.coordinates = [[NSMutableArray alloc] init];
        for(int i = 0 ; i < [coordinatesArray count] ; i++)
        {
            NSDictionary *coordinateDictionary = [coordinatesArray objectAtIndex:i];
            Location *coordinate = [[Location alloc] initWithDictionary:coordinateDictionary];
            [self.coordinates addObject:coordinate];
        }
        NSLog(@"Coordinates: %lu", (unsigned long)[self.coordinates count]);
    }
    
    return self;
}

-(id)initWithMapRoute:(MapRoute *)mapRoute
{
    self = [super init];
    if(self)
    {
        self.totalDistance = mapRoute.totalDistance;
        self.estimatedTotalTime = mapRoute.estimatedTotalTime;
        
        self.boundingBoxTopLeft = [[Location alloc] initWithLocation:mapRoute.boundingBoxTopLeft];
        self.boundingBoxBottomRight = [[Location alloc] initWithLocation:mapRoute.boundingBoxBottomRight];
        
        self.steps = [[NSMutableArray alloc] init];

        for(int i = 0 ; i < [mapRoute.steps count] ; i++)
        {
            RouteStep *mapRouteStep = (RouteStep*)[mapRoute.steps objectAtIndex:i];
            RouteStep *step = [[RouteStep alloc] initWithStep:mapRouteStep];
            
            [self.steps addObject:step];
        }
        NSLog(@"Steps: %lu", (unsigned long)[self.steps count]);
        
        self.coordinates = [[NSMutableArray alloc] init];
        for(int i = 0 ; i < [mapRoute.coordinates count] ; i++)
        {
            Location *mapRouteCoordinate = (Location*)[mapRoute.coordinates objectAtIndex:i];
            Location *coordinate = [[Location alloc] initWithLocation:mapRouteCoordinate];
            
            [self.coordinates addObject:coordinate];
        }
    }
    return self;
}
@end
