//
//  XUILoader.h
//  Findmyway
//
//  Created by Bilo on 5/7/15.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ImageUtil.h"
#import "LayoutConfig.h"
#import "ColourConfig.h"

@interface XUILoader : UIView
{
    UIImageView *loaderImage;
    NSTimer *animationTimer;
    float animationCounter;
    float animationSpeed;
}

-(id)initWithFrame:(CGRect)frame layout:(LayoutConfig*)layout colour:(ColourConfig*)colour inView:(UIView*)view;

-(void)startAnimation;
-(void)stopAnimation;
-(void)updateAnimation;
@end
