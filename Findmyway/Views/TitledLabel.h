//
//  TitledLabel.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/12.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface TitledLabel : UIView
{
    UILabel *titleLabel;
    UILabel *contentLabel;
}

-(id)initWithDelegate:(AppDelegate*)appDelegate frame:(CGRect)frame title:(NSString*)title contentLabel:(NSString*)contentLabel alignment:(NSTextAlignment)alignment color:(UIColor*)color titleColor:(UIColor*)titleColor;
@end
