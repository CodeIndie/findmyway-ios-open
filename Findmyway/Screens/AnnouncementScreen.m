//
//  AnnouncementScreen.m
//  Findmyway
//
//  Created by Bilo on 4/13/15.
//

#import "AnnouncementScreen.h"

@implementation AnnouncementScreen

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(device.screenWidth * 1.5, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth * 0.5, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
    [self setBackgroundColor:colour.fmwHeader];
}

-(void)initViewElements
{
    backButton = [XUIButton backButtonWithTitle:NSLocalizedString(@"Back", nil) color:colour.fmwActiveColor layout:layout];
    [backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    screenTitle = [XUILabel initWithFrame:CGRectMake(layout.buttonSize * 2, 0, self.frame.size.width - 4 * layout.buttonSize, layout.buttonSize)
                                     text:NSLocalizedString(@"Announcements", nil)
                                 fontSize:[UIFont systemFontSize] + 1
                                     bold:YES textColor:colour.fmwTextColor
                          backgroundColor:colour.clear
                            textAlignment:NSTextAlignmentCenter];
    
    delimiter = [[UIView alloc] initWithFrame:CGRectMake(0, layout.buttonSize - layout.delimiterSize, layout.screenWidth, layout.delimiterSize)];
    [delimiter setBackgroundColor:colour.fmwTextColorLight];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, self.frame.size.height - layout.buttonSize)];
}

-(void)addUIToView
{
    [self addSubview:backButton];
    [self addSubview:screenTitle];
    [self addSubview:delimiter];
    
    [self addSubview:scrollView];
}

-(void)getAnnouncements
{
    //AppViewController *viewController = (AppViewController*)parentView;
    [self startLoader:CGPointMake(self.frame.size.width/2, layout.buttonSize * 2) inView:self];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    //[parameters setValue:[viewController.fmwScreen.modesPicker excludedModes] forKey:@"excludedModes"];
    //[parameters setValue:[viewController.fmwScreen.modesPicker excludedOperators] forKey:@"excludedOperators"];
    
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:Announcements callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
     {
        NSLog(@"API - Announcements: %ld", (long)response.statusCode);
        [self stopLoader];
        [self handleApiError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode];
        [appDelegate.apiResponse addAnnouncements:[JSONParser convertJSONToDictionary:bodyString]];
        [self addAnnouncements];
     }
     failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
     {
        NSLog(@"API CONNECTION FAILURE - Announcements: %@", error);
        [self stopLoader];
        [self handleError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode error:error];
     }
     didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
     {
         // This is not going to be called in this example but it's included for completeness.
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
     }];
}

-(void)addAnnouncements
{
    [self clearScrollView];
    
    float yPos = 0;
    int i = 0;
    for(Announcement *announcement in appDelegate.apiResponse.announcements)
    {
        AnnouncementListItem *item = [[AnnouncementListItem alloc] initWithAnnouncement:announcement frame:CGRectMake(0, yPos, layout.screenWidth, layout.buttonSize)];
        item.shareButton.tag = i;
        [item.shareButton addTarget:self action:@selector(shareAnnouncement:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:item];
        
        yPos += [item height] + layout.margin;
        i++;
    }
    NSLog(@"yPos: %f", yPos);
    scrollView.contentSize = CGSizeMake(layout.screenWidth, yPos);
}

-(IBAction)shareAnnouncement:(id)sender
{
    NSInteger tag = ((UIButton*)sender).tag;
    
    NSString *announcementString = ((Announcement*)[appDelegate.apiResponse.announcements objectAtIndexedSubscript:tag]).descriptionText;
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[announcementString] applicationActivities:nil];
    [parentView presentViewController:activityViewController animated:YES completion:nil];
}


-(void)clearScrollView
{
    for(UIView *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
}

-(void)animateIn
{
    [super animateIn];
    [self getAnnouncements];
    [parentView.view addSubview:self];
    self.center = centerOffScreen;
    self.alpha = 0;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOnScreen;
         self.alpha = 1;
     }
     completion:^(BOOL finished)
     {
         //[searchField becomeFirstResponder];
         //[self initRecentPlaces];
     }];
}

-(void)animateOut
{
    [super animateOut];
    [self removeNotificationListeners];
    
    self.center = centerOnScreen;
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOffScreen;
         self.alpha = 0;
     }
     completion:^(BOOL finished){
         //[searchField resignFirstResponder];
         [self removeFromSuperview];
     }];
}

-(void)backButtonAction
{
    NSLog(@"Back to main menu");
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController hideView:viewController.announcementScreen];
}

@end
