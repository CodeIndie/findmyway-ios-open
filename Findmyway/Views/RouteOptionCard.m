//
//  RouteOptionCard.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/06.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "RouteOptionCard.h"

@implementation RouteOptionCard

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(-device.screenWidth/2, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth/2, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initWithIndex:(int)i inController:(UIViewController*)parentViewController
{
    [super initView:@"" inController:parentViewController];
    index = i;
}

-(void)initViewElements:(MultimodalTrip*)trip
{
    [self setBackgroundColor:[UIColor clearColor]];

    horizontalLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height, layout.screenWidth, layout.delimiterSize)];
    [horizontalLine setBackgroundColor:colour.fmwTextColorLight];
    
    multimodalTrip = trip;
    tripId = trip.tripId;
    
    [self createScrollViewPager];
    
    [self createSummaryView:trip];
    [self createMapView:trip];
    //[self createTicketView];
    
    [self addUIToView];
}

-(void)addNotificationListeners
{
    [super addNotificationListeners];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addMapCard) name:[[Notifications instance] GETTripsSuccess] object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showError) name:[[Notifications instance] GETTripsFail] object:nil];
}

-(void)removeNotificationListeners
{
    [super removeNotificationListeners];
}

-(void)createScrollViewPager
{
    pageWidth = layout.screenWidth;
    pageHeight = self.bounds.size.height;// - layout.buttonSize;
    //pageCount = 2;
    pageCount = 2;
    
    CGRect bounds = self.bounds;
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, bounds.size.width, bounds.size.height)];
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.contentSize = CGSizeMake(pageCount * pageWidth, pageHeight);
    
    pageControl = [[UIPageControl alloc] init];
    pageControl.center = CGPointMake(layout.screenWidth/2, self.bounds.size.height - layout.padding);
    [pageControl setNumberOfPages:pageCount];
    pageControl.currentPageIndicatorTintColor = colour.fmwActiveColor;
    pageControl.pageIndicatorTintColor = colour.fmwActiveColorLight;
}

-(void)createSummaryView:(MultimodalTrip*)trip
{
    for(UIImageView *imageView in [self addModeIcons:trip offset:CGPointMake(layout.padding,0)])
    {
        [scrollView addSubview:imageView];
    }
    
    float y;
    y = layout.buttonSize;
    routeCost = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding, layout.buttonSize, layout.paddedWidth, layout.buttonSize)];
    [routeCost setText:trip.estimatedTotalCost];
    [routeCost setTextColor:colour.fmwTextColor];
    [routeCost setTextAlignment:NSTextAlignmentLeft];
    
    routeCostLabel = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding, y + layout.margin, layout.buttonSize * 3, layout.margin)];
    [routeCostLabel setText:@"approx. cost"];
    [routeCostLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 2]];
    [routeCostLabel setTextColor:colour.fmwTextColorLight];
    [routeCostLabel setTextAlignment:NSTextAlignmentLeft];
    
    arrivalTime = [[UILabel alloc] initWithFrame:CGRectMake(layout.screenWidth/2 - layout.buttonSize, layout.buttonSize, layout.buttonSize * 2, layout.buttonSize)];
    [arrivalTime setText:[DateTimeUtil parseDateTime:trip.endTime].timeString];
    [arrivalTime setTextColor:colour.fmwTextColor];
    [arrivalTime setTextAlignment:NSTextAlignmentCenter];
    
    arrivalTimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(layout.screenWidth/2 - layout.buttonSize, y + layout.margin, layout.buttonSize * 2, layout.margin)];
    [arrivalTimeLabel setText:@"arriving"];
    [arrivalTimeLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 2]];
    [arrivalTimeLabel setTextColor:colour.fmwTextColorLight];
    [arrivalTimeLabel setTextAlignment:NSTextAlignmentCenter];
    
    walking = [[UILabel alloc] initWithFrame:CGRectMake(layout.screenWidth - 2 * layout.buttonSize - layout.padding, layout.buttonSize, layout.buttonSize * 2, layout.buttonSize)];
    [walking setText:[DistanceUtil distanceString:[trip.totalWalkingDistance intValue] metricSystem:YES]];
    [walking setTextColor:colour.fmwTextColor];
    [walking setTextAlignment:NSTextAlignmentRight];
    
    walkingLabel = [[UILabel alloc] initWithFrame:CGRectMake(layout.screenWidth - 2 * layout.buttonSize - layout.padding, y + layout.margin, layout.buttonSize * 2, layout.margin)];
    [walkingLabel setText:@"walking"];
    [walkingLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 2]];
    [walkingLabel setTextColor:colour.fmwTextColorLight];
    [walkingLabel setTextAlignment:NSTextAlignmentRight];
    
    departureTime = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding, layout.padding, layout.paddedWidth, layout.buttonSize/2)];
    [departureTime setText:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"Leaves in ",nil), [DateTimeUtil timeBetweenCurrentTimeAnd:trip.startTime]]];
    [departureTime setTextColor:colour.fmwTextColor];
    //self.overviewButton = [[XUIButton alloc] initWithFrame:CGRectMake(0, pageHeight - layout.buttonSize, layout.paddedWidth, layout.buttonSize)];
    self.overviewButton = [[XUIButton alloc] initWithFrame:CGRectMake(layout.padding, pageHeight - layout.buttonSize - layout.delimiterSize, layout.paddedWidth/2, layout.buttonSize)];
    [self.overviewButton setColorsOnFront:colour.fmwLight
                                   onBack:colour.fmwActiveColor
                                 offFront:colour.fmwActiveColor
                                  offBack:colour.clear];
    
    [self.overviewButton setTitle:@"Overview" alignment:UIControlContentHorizontalAlignmentLeft];
    [self.overviewButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
//    [self.overviewButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
    float padding = layout.padding;
    UIImageView *arrow = [[UIImageView alloc] initWithFrame:CGRectMake(layout.paddedWidth - layout.buttonSize/2 - padding, padding, layout.buttonSize - 2 * padding, layout.buttonSize - 2 * padding)];
    [arrow setImage:[UIImage imageNamed:@"iconNext.png"]];
    //[self.overviewButton addSubview:arrow];
    
    self.selectButton = [[XUIButton alloc] initWithFrame:CGRectMake(layout.screenWidth/2, pageHeight - layout.delimiterSize, layout.paddedWidth/2, layout.buttonSize)];
    [self.selectButton setColorsOnFront:colour.fmwLight
                                 onBack:colour.fmwActiveColor
                               offFront:colour.fmwActiveColor
                                offBack:colour.clear];
    
    [self.selectButton setTitle:@"Begin Journey" forState:UIControlStateNormal];
    [self.selectButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    
    self.logButton = [[XUIButton alloc] initWithFrame:CGRectMake(layout.screenWidth/2, pageHeight - layout.delimiterSize - layout.buttonSize, layout.paddedWidth/2, layout.buttonSize)];
    [self.logButton setColorsOnFront:colour.fmwLight
                                 onBack:colour.fmwActiveColor
                               offFront:colour.fmwActiveColor
                                offBack:colour.clear];
    [self.logButton setTitle:@"Log Journey" alignment:UIControlContentHorizontalAlignmentRight];
    [self.logButton addTarget:self action:@selector(logJourney) forControlEvents:UIControlEventTouchUpInside];
    [self.logButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
}

-(NSMutableArray*)addModeIcons:(MultimodalTrip*)trip offset:(CGPoint)offset
{
    NSMutableArray *tripModes = [[NSMutableArray alloc] init];
    NSMutableArray *modeColor = [[NSMutableArray alloc] init];
    for(MultimodalStage* stage in trip.stages)
    {
        if(![stage.modeType isEqualToString:@"Pedestrian"])
        {
            [tripModes addObject:stage.modeType];
            [modeColor addObject:stage.colour];
        }
    }
    
    NSMutableArray *iconArray = [[NSMutableArray alloc] init];
    
    for(long i = tripModes.count - 1; i >= 0 ; i--)
    {
        NSString *mode = (NSString*)[tripModes objectAtIndex:i];
        float iconSize = layout.buttonSize/2;
        
        UIImageView *modeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(offset.x + layout.paddedWidth - (tripModes.count - i)*(iconSize), offset.y + layout.padding, iconSize, iconSize)];
        [modeIcon setImage:[[appDelegate getImageForMode:mode] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        [modeIcon setTintColor:(UIColor*)[modeColor objectAtIndex:i]];
        [iconArray addObject:modeIcon];
    }
    return iconArray;
}

-(void)createMapView:(MultimodalTrip*)trip
{
    float x = pageWidth;
    mapLoadingMessage = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, pageWidth, pageHeight)];
    [mapLoadingMessage setText:@"... loading map ..."];
    [mapLoadingMessage setTextAlignment:NSTextAlignmentCenter];
    [mapLoadingMessage setTextColor:colour.fmwTextColor];
    
    self.mapImageButton = [[UIButton alloc] initWithFrame:CGRectMake(x, 0, pageWidth, pageHeight)];
    [self.mapImageButton setBackgroundColor:[UIColor clearColor]];
    
    float gradientHeight = layout.buttonSize * 0.5;
    gradientTop = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, pageWidth, gradientHeight)];
    [gradientTop setImage:[ImageUtil getImage:@"gradientTop.png"]];
    [gradientTop setTintColor:colour.fmwBackgroundColor];
    
    gradientBottom = [[UIImageView alloc] initWithFrame:CGRectMake(x, pageHeight - gradientHeight, pageWidth, gradientHeight)];
    [gradientBottom setImage:[ImageUtil getImage:@"gradientBottom.png"]];
    [gradientBottom  setTintColor:colour.fmwBackgroundColor];
    
    mapViewModeIcons = [[NSMutableArray alloc] init];
    [mapViewModeIcons setArray:[self addModeIcons:trip offset:CGPointMake(pageWidth + layout.padding, 0)]];
}

-(void)createTicketView
{
    float x = 2 * pageWidth;
    ticketInfo = [[UILabel alloc] initWithFrame:CGRectMake(x, 0, pageWidth, pageHeight)];
    [ticketInfo setText:@"Ticket Info"];
    [ticketInfo setTextColor:[UIColor blackColor]];
    [ticketInfo setTextAlignment:NSTextAlignmentCenter];
    [ticketInfo setBackgroundColor:[UIColor whiteColor]];
}

-(BOOL)showTicketInfo
{
    for(MultimodalStage *stage in multimodalTrip.stages)
    {
        if([stage.modeName isEqualToString:@"MyCiTi"] || [stage.modeName isEqualToString:@"Jammie Shuttle"])
        {
            return YES;
        }
    }
    return NO;
}

-(void)addUIToView
{
    // OVERVIEW CARD
    [scrollView addSubview:departureTime];
    [scrollView addSubview:routeCost];
    [scrollView addSubview:routeCostLabel];
    [scrollView addSubview:arrivalTime];
    [scrollView addSubview:arrivalTimeLabel];
    [scrollView addSubview:walking];
    [scrollView addSubview:walkingLabel];
    
    for(UIImageView *imageView in modeIcons)
    {
        [scrollView addSubview:imageView];
    }
    [scrollView addSubview:self.overviewButton];
    //[scrollView addSubview:self.selectButton];
    if([CompetitionUtil isEarthHourTime] && multimodalTrip.isCompetitionTrip && [CompetitionUtil isCompeting])
    {
        [scrollView addSubview:self.logButton];
        [self setCarbonPoints];
    }
    [scrollView addSubview:startTime];
    [scrollView addSubview:endTime];
    [scrollView addSubview:barBackground];
    
    // MAP CARD
    [scrollView addSubview:mapLoadingMessage];
    [scrollView addSubview:self.mapImageButton];
    [scrollView addSubview:gradientTop];
    [scrollView addSubview:gradientBottom];
    for(UIImageView *imageView in mapViewModeIcons)
    {
        [scrollView addSubview:imageView];
    }
    
    // TICKET INFO
    [scrollView addSubview:ticketInfo];
    
    [self addSubview:scrollView];
    [self addSubview:pageControl];
    [self addSubview:horizontalLine];
}

- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    // Switch the indicator when more than 50% of the previous/next page is visible
    if(sender == scrollView)
    {
        CGFloat pageSizeWidth = pageWidth;//scrollView.frame.size.width;
        activePage = floor((scrollView.contentOffset.x - pageSizeWidth / 2) / pageSizeWidth) + 1;
        pageControl.currentPage = activePage;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self getMapForRoute];
}

-(void)getMapForRoute
{
    if(pageControl.currentPage == 1 || activePage == 1)
    {
        [self getMapCoordinates];
    }
}

-(void)setCarbonPoints
{
    if(multimodalTrip.isCompetitionTrip) 
    {
        if(multimodalTrip.hasMapRoute)
        {
            [walking setText:[NSString stringWithFormat:@"%d", multimodalTrip.carbonPoints]];
            [walkingLabel setText:@"carbon points"];
        }
        else
        {
            [self getRouteCoordinates:multimodalTrip.tripId];
        }
    }
}

-(void)logJourney
{
    BOOL tripSaved = [CompetitionUtil logJourney:multimodalTrip.firstStopAddress end:multimodalTrip.lastStopAddress dateTime:multimodalTrip.startTime points:multimodalTrip.carbonPoints];
    
    if(tripSaved)
    {
        [self showAlertView:@"Trip Logged" message:@"Your trip has been logged.\n\nView it on the Home screen." button:@"Ok"];
    }
    else
    {
         [self showAlertView:@"Denied" message:@"This trip has already been logged." button:@"Ok"];
    }
}

-(void)getMapCoordinates
{
    MultimodalTrip *trip = (MultimodalTrip*)[appDelegate.apiResponse.trips objectAtIndex:index];
    if(trip.hasMapRoute)
    {
        [self addMapImageButton:trip];
        return;
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:tripId forKey:@"tripid"];
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:Trips callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
     {
         //[self stopLoader];
         NSLog(@"API - Trips: %ld", (long)response.statusCode);
         [appDelegate.apiResponse addTrip:[JSONParser convertJSONToDictionary:bodyString] toIndex:index];
         [self addMapImageButton:trip];
     }
                                failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
     {
         //[self stopLoader];
         NSLog(@"API CONNECTION FAILURE - Trips: %@", error);
     }
                            didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
     {
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
     }];
}

-(void)getRouteCoordinates:(NSString*)tripIdGuid
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:tripIdGuid forKey:@"tripid"];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:[appDelegate.apiUtil urlForRequest:Trips callType:GET parameters:parameters]];
    
    [connection executeRequestOnSuccess:^(NSHTTPURLResponse *response, NSString *bodyString)
    {
        NSLog(@"API Response: %ld", (long)response.statusCode);
        [appDelegate.apiResponse addTrip:[JSONParser convertJSONToDictionary:bodyString] toIndex:index];
        [walking setText:[NSString stringWithFormat:@"%d", multimodalTrip.carbonPoints]];
        [walkingLabel setText:@"carbon points"];
        [scrollView addSubview:self.logButton];
    }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error){
        NSLog(@"NETWORK ERROR: %@", error);
        
    }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
    {
        NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
    }];
}

-(void)addMapImageButton:(MultimodalTrip*)trip
{
    MKMapSnapshotOptions *options = [[MKMapSnapshotOptions alloc] init];
    CLLocationCoordinate2D delta = CLLocationCoordinate2DMake(
                                fabsf(trip.mapRoute.boundingBoxTopLeft.latitude - trip.mapRoute.boundingBoxBottomRight.latitude),
                                fabsf(trip.mapRoute.boundingBoxTopLeft.longitude - trip.mapRoute.boundingBoxBottomRight.longitude));
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(trip.mapRoute.boundingBoxTopLeft.latitude + delta.latitude/2,
                                                               trip.mapRoute.boundingBoxTopLeft.longitude + delta.longitude/2);
    options.region = MKCoordinateRegionMake(center, MKCoordinateSpanMake(delta.latitude * 1.4, delta.longitude * 1.4));
    options.size = self.mapImageButton.frame.size;
    options.scale = [[UIScreen mainScreen] scale];
    
    MKMapSnapshotter *snapShotter= [[MKMapSnapshotter alloc] initWithOptions:options];
    [snapShotter startWithCompletionHandler:^(MKMapSnapshot *snapshot, NSError *error)
    {
        if(error)
        {
            [self XLog:[NSString stringWithFormat:@"[MKMapSnapshot Error] : %@", error]];
            return;
        }
        
//        polylines = [[NSMutableArray alloc] init];
//        for(PublicMapRouteSegment *routeSegment in trip.mapRoute.routes)
//        {
//            [self addRoute:routeSegment.coordinates color:routeSegment.color lineWidth:8];
//        }
        
        [self addRoute:trip.mapRoute.coordinates color:colour.fmwActiveColor lineWidth:4];
        UIImage *image = [self drawRoute:customPolyline onSnapshot:snapshot withColor:colour.fmwActiveColor];
//        for(CustomMapLine *line in polylines)
//        {
//            [self drawRoute:line.polyLine onSnapshot:snapshot withColor:line.color];
//        }
        //[self drawRoute:customPolyline onSnapshot:snapshot withColor:colour.fmwActiveColor];
        //UIImage *image = snapshot.image;
        
        [self.mapImageButton setImage:image forState:UIControlStateNormal];
    }];
}

-(void)addRoute:(NSMutableArray*)coordinates color:(UIColor*)color lineWidth:(float)lineWidth
{
    int numberOfPoints = (int)coordinates.count;
    CLLocationCoordinate2D *points = malloc(numberOfPoints * sizeof(CLLocationCoordinate2D));
    
    for(int i = 0 ; i <  numberOfPoints; i++)
    {
        CLLocationDegrees lat = ((Location*)[coordinates objectAtIndex:i]).latitude;
        CLLocationDegrees lon = ((Location*)[coordinates objectAtIndex:i]).longitude;
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, lon);
        
        points[i] = coordinate;
    }
    
    @try
    {
        customPolyline = [MKPolyline polylineWithCoordinates:points count:[coordinates count] - 1];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Invalid Argument Exception ...");
    }
    @finally
    {
        //NSLog(@"Continuing program execution...");
    }
}

-(void)addRouteSegment:(NSMutableArray*)coordinates color:(UIColor*)color lineWidth:(float)lineWidth
{
    if([coordinates count] == 0)
    {
        //[self customAlert:@"No route to display." withMessage:appDelegate.apiMessage andDuration:2];
        [self XLog:@"NO COORDINATES"];
    }
    else
    {
        @try
        {
            //[polylines addObject:[[CustomMapLine alloc] initWithCoordinates:coordinates color:color lineWidth:lineWidth]];
        }
        @catch (NSException *exception)
        {
            NSLog(@"Invalid Argument Exception ...");
        }
        @finally
        {
            //NSLog(@"Continuing program execution...");
        }
        
        //[_myMapView addOverlay: ((CustomMapLine*)[self.polyLines objectAtIndex:[self.polyLines count] - 1]).polyEdges];
        //[_myMapView addOverlay: ((CustomMapLine*)[self.polyLines objectAtIndex:[self.polyLines count] - 1]).polyLine];
    }
}

-(UIImage*)drawRoute:(MKPolyline*)polyline onSnapshot:(MKMapSnapshot*)snapShot withColor:(UIColor*)lineColor
{
    UIGraphicsBeginImageContext(snapShot.image.size);
    CGRect rectForImage = CGRectMake(0, 0, snapShot.image.size.width, snapShot.image.size.height);
    
    // Draw map
    [snapShot.image drawInRect:rectForImage];
    
    // Get points in the snapshot from the snapshot
    int lastPointIndex = 0;
    int firstPointIndex = 0;
    BOOL isfirstPoint = NO;
    NSMutableArray *pointsToDraw = [NSMutableArray array];
    for (int i = 0; i < polyline.pointCount; i++)
    {
        MKMapPoint point = polyline.points[i];
        CLLocationCoordinate2D pointCoord = MKCoordinateForMapPoint(point);
        CGPoint pointInSnapshot = [snapShot pointForCoordinate:pointCoord];
        if (CGRectContainsPoint(rectForImage, pointInSnapshot))
        {
            [pointsToDraw addObject:[NSValue valueWithCGPoint:pointInSnapshot]];
            lastPointIndex = i;
            if (i == 0)
                firstPointIndex = YES;
            if (!isfirstPoint)
            {
                isfirstPoint = YES;
                firstPointIndex = i;
            }
        }
    }
    
    /*
    // Adding the first point on the outside too so we have a nice path
    if (lastPointIndex+1 <= polyline.pointCount)
    {
        MKMapPoint point = polyline.points[lastPointIndex+1];
        CLLocationCoordinate2D pointCoord = MKCoordinateForMapPoint(point);
        CGPoint pointInSnapshot = [snapShot pointForCoordinate:pointCoord];
        [pointsToDraw addObject:[NSValue valueWithCGPoint:pointInSnapshot]];
    }
    // Adding the point before the first point in the map as well (if needed) to have nice path
    
    if (firstPointIndex != 0)
    {
        MKMapPoint point = polyline.points[firstPointIndex-1];
        CLLocationCoordinate2D pointCoord = MKCoordinateForMapPoint(point);
        CGPoint pointInSnapshot = [snapShot pointForCoordinate:pointCoord];
        [pointsToDraw insertObject:[NSValue valueWithCGPoint:pointInSnapshot] atIndex:0];
    }
    */
    
    // Draw that points
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 4.0);
    
    for (NSValue *point in pointsToDraw)
    {
        CGPoint pointToDraw = [point CGPointValue];
        if ([pointsToDraw indexOfObject:point] == 0)
        {
            CGContextMoveToPoint(context, pointToDraw.x, pointToDraw.y);
        }
        else
        {
            CGContextAddLineToPoint(context, pointToDraw.x, pointToDraw.y);
        }
    }
    CGContextSetStrokeColorWithColor(context, [lineColor CGColor]);
    CGContextStrokePath(context);
    
    UIImage *resultingImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return resultingImage;
}

-(void)drawRouteInSnapshot:(MKMapSnapshot*)snapshot
{
    UIImage *image = snapshot.image;
    UIGraphicsBeginImageContextWithOptions(image.size, YES, image.scale);
    {
        [image drawAtPoint:CGPointMake(0.0f, 0.0f)];
        
        CGContextRef c = UIGraphicsGetCurrentContext();
        MKPolylineRenderer *polylineRenderer = [[MKPolylineRenderer alloc] initWithPolyline:customPolyline];
        if (polylineRenderer.path)
        {
            [polylineRenderer applyStrokePropertiesToContext:c atZoomScale:1.0f];
            CGContextAddPath(c, polylineRenderer.path);
            CGContextStrokePath(c);
        }
        
//        UIImage *stepImage = UIGraphicsGetImageFromCurrentImageContext();
//        [mutableStepImages addObject:stepImage];
    }
    UIGraphicsEndImageContext();

}


@end
