//
//  XUINotification.m
//  Findmyway
//
//  Created by Bilo on 4/9/15.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUINotification.h"

@implementation XUINotification

-(id)initWithMessage:(NSString*)message inView:(UIView*)view
{
    self = [super init];
    if(self)
    {
        appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        layout = appDelegate.layout;
        colour = appDelegate.colour;
        
        parentView = view;
        displayDuration = 2;
        animDuration = 0.3;
        
        float notificationHeight = layout.buttonSize * 1;
        float notificationWidth = layout.screenWidth - 2 * layout.margin;
        
        background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.screenHeight)];
        [background setUserInteractionEnabled:YES];
        UIColor *backgroundColour = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        [background setBackgroundColor:backgroundColour];
        background.alpha = 0;

        messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(layout.margin, layout.screenHeight - layout.margin - notificationHeight, notificationWidth, notificationHeight)];
        messageCenter = CGPointMake(messageLabel.frame.origin.x + messageLabel.frame.size.width/2, messageLabel.frame.origin.y + messageLabel.frame.size.height/2);
        messageLabel.center = CGPointMake(messageCenter.x, messageCenter.y + layout.screenHeight/2);
        //[messageLabel setBackgroundColor:colour.fmwActiveColor];
        [messageLabel setText:message];
        [messageLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
        [messageLabel setTextColor:colour.fmwBackgroundColor];
        [messageLabel setTextAlignment:NSTextAlignmentCenter];
        CALayer *layer = [messageLabel layer];
        [layer setCornerRadius:layout.padding];
        [layer setBackgroundColor:colour.fmwActiveColor.CGColor];
        
        [self initDisplaySequence];
    }
    return self;
}

-(void)initDisplaySequence
{
    [self animateIn];
    [NSTimer scheduledTimerWithTimeInterval:animDuration + displayDuration target:self selector:@selector(animateOut) userInfo:nil repeats:NO];
}

-(void)animateIn
{
    NSLog(@"Adding Notification: %@", messageLabel.text);
    [self addToView];

    [UIView animateWithDuration:animDuration delay:0 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
        messageLabel.center = messageCenter;
        background.alpha = 1;
    }completion:nil];
}

-(void)animateOut
{
//    messageLabel.center = CGPointMake(messageCenter.x, messageCenter.y + layout.screenHeight/2);
//    background.alpha = 0;
    
    [UIView animateWithDuration:animDuration delay:0 options:UIViewAnimationOptionCurveEaseInOut
     animations:^{
        messageLabel.center = CGPointMake(messageCenter.x, messageCenter.y + layout.screenHeight/2);
        background.alpha = 0;
    }completion:^(BOOL finished){
        [self removeFromSuperview];
    }];
}

-(void)addToView
{
    [parentView addSubview:background];
    [parentView addSubview:messageLabel];
}

-(void)removeFromSuperview
{
    NSLog(@"Removing Notification From View");
    [background removeFromSuperview];
    [messageLabel removeFromSuperview];
}
@end
