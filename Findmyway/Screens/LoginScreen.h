//
//  LoginScreen.h
//  Findmyway
//
//  Created by Bilo on 2/9/15.
//

#import "XUIView.h"
#import "LoginButton.h"

@interface LoginScreen : XUIView
{
    UIButton *cancelButton;
    UILabel *signInLabel;
    UIImageView *topLine;
    
    UILabel *explanationLabel;
    
    LoginButton *facebookLogin;
    LoginButton *twitterLogin;
    LoginButton *googleLogin;
    LoginButton *emailLogin;
    
    float y;
}
@end
