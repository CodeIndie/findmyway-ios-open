//
//  AnnouncementScreen.h
//  Findmyway
//
//  Created by Bilo on 4/13/15.
//

#import "XUIView.h"
#import "AnnouncementListItem.h"
#import "AppViewController.h"

@interface AnnouncementScreen : XUIView
{
    UIScrollView *scrollView;
    UIButton *backButton;
    UILabel *screenTitle;
    UIView *delimiter;
}
@end
