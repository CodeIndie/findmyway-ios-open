//
//  GPSCoordinate.h
//  tct
//
//  Created by Bilo on 4/16/14.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Location : NSObject<NSCoding>

@property NSString *name;
@property NSString *address;
@property float latitude;
@property float longitude;
@property NSString *latitudeString;
@property NSString *longitudeString;
@property CLLocationCoordinate2D coordinate;

-(NSString*)gpsCoordinates;

-(id)initWithDictionary:(NSDictionary *)dictionary;
-(id)initWithLatitude:(float)latitude longitude:(float)longitude;
-(id)initWithLatitudeString:(NSString*)latitude longitudeString:(NSString*)longitude;
-(id)initWithCLLatitude:(CLLocationDegrees)latitude longitude:(CLLocationDegrees)longitude;
-(id)initWithLocation:(Location*)coordinate;

@end
