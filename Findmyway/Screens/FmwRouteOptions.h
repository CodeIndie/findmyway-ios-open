//
//  FmwRouteOptions.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/06.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIView.h"
#import "RouteOptionCard.h"
#import "AppViewController.h"

@interface FmwRouteOptions : XUIView<UIScrollViewDelegate>
{
    float y;
    UIScrollView *scrollView;
    NSMutableArray *routeOptions;
    NSMutableArray *tripCards;
    UIButton *reloadButton;
    
    float cardHeight;
    UIButton *cancelButton;
    UILabel *resultsLabel;
    UIView *headerBackground;
    UIButton *locationButton;
    UILabel *locationLabel;
    
    UIButton *destinationButton;
    UILabel *destinationLabel;
    
    UIButton *timeButton;
    UILabel *timeLabel;
    
    UIButton *switchButton;
    UIView *horizontalLine;
    
    UILabel *feedbackLabel;
    UIImageView *feedbackArrow;
    CGFloat minRefreshScrollOffset;
    CGFloat arrowAngle;
}

-(void)createTripCards;
-(IBAction)showOnMap:(id)sender;
-(IBAction)selectTrip:(id)sender;
-(IBAction)showOverview:(id)sender;
@end
