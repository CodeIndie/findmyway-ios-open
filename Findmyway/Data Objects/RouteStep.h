//
//  RouteStep.h
//  tct
//
//  Created by Bilo on 4/20/14.
//

#import <Foundation/Foundation.h>
#import "Location.h"

@interface RouteStep : NSObject

@property int order;
@property NSString *instructions;
@property NSString *distance;
@property NSString *duration;
@property Location *point;

-(id)initWithDictionary:(NSDictionary*)dictionary;
-(id)initWithStep:(RouteStep*)step;

@end
