//
//  CompetitionTripsScreen.h
//  Findmyway
//
//  Created by Bilo on 3/16/15.
//

#import "XUIView.h"
#import "AppViewController.h"

@interface CompetitionTripsScreen : XUIView<UIAlertViewDelegate>
{
    UIImageView *topbarBackground;
    UIImageView *topbarLine;
    UIImageView *bottomBarBackground;
    UIImageView *bottomBarLine;
    UIButton *teamButton;
    
    UIButton *backArrow;
    UIButton *backButton;
    UIScrollView *scrollView;
    
    UILabel *totalPoints;
    
    NSMutableArray *loggedTrips;
    UIAlertView *teamAlertView;
}
@end
