//
//  MainMenuButton.m
//  Findmyway
//
//  Created by Bilo on 12/4/14.
//

#import "MainMenuButton.h"

@implementation MainMenuButton


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
    }
    return self;
}

-(void)setDelegate:(AppDelegate*)appDelegate title:(NSString *)title iconName:(NSString *)iconName index:(int)index
{
    LayoutConfig *layout = appDelegate.layout;
    
    float pad = layout.padding/2;
    
    self.button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.buttonSize)];
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(layout.buttonSize + 2 * layout.padding, 0, layout.screenWidth, layout.buttonSize)];
    self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(pad, pad, layout.buttonSize - 2*pad, layout.buttonSize - 2*pad)];
    
    [self.icon setImage:[ImageUtil getImage:iconName]];
    [self.icon setTintColor:appDelegate.colour.fmwTextColorLight];
    
    [self.label setTextColor:[UIColor whiteColor]];
    [self.label setText:title];
    [self.label setTextColor:appDelegate.colour.fmwTextColor];
    
    [self addSubview:self.button];
    [self addSubview:self.label];
    [self addSubview:self.icon];
}
@end
