//
//  TripLogListItem.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/11.
//

#import "TripLogListItem.h"

@implementation TripLogListItem

-(id)initWithFrame:(CGRect)frame item:(TripLogItem *)item
{
    self = [super initWithFrame:frame];
    if(self)
    {
        appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        LayoutConfig *layout = appDelegate.layout;
        
        circle = [XUIShape circle:CGPointMake(layout.padding * 1.25, layout.padding * 0.25) size:layout.buttonSize - layout.padding/2 borderWidth:0 fillColor:appDelegate.colour.fmwActiveColor strokeColor:appDelegate.colour.fmwActiveColor];
        [self addSubview:circle];
        
        points = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding, 0, layout.buttonSize, layout.buttonSize)];
        [points setTextColor:appDelegate.colour.white];
        [points setText:[NSString stringWithFormat:@"%d",item.points]];
        [points setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 2]];
        [points setTextAlignment:NSTextAlignmentCenter];
        
//        CALayer *layer = [points layer];
//        [layer setCornerRadius:layout.buttonSize/2];
        [self addSubview:points];
        
        locationName = [[UILabel alloc] initWithFrame:CGRectMake(layout.margin + layout.buttonSize, 0, self.frame.size.width - layout.buttonSize * 2 - layout.margin,  self.frame.size.height)];
        [locationName setNumberOfLines:0];
        [locationName setText:[NSString stringWithFormat:@"%@\n%@", item.startLocation, item.endLocation]];
        [locationName setTextColor:appDelegate.colour.fmwTextColor];
        [locationName setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 1]];
        [self addSubview:locationName];
        
        savedDateTime = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - layout.buttonSize, 0, layout.buttonSize, layout.buttonSize)];
        [savedDateTime setNumberOfLines:0];
        [savedDateTime setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
        [savedDateTime setText:[DateTimeUtil parseDateTime:item.dateTime].timeString];
        [savedDateTime setTextColor:appDelegate.colour.fmwTextColorLight];
        [self addSubview:savedDateTime];
    }
    return self;
}
@end
