//
//  CompetitionView.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/09.
//

#import "XUIView.h"
#import "DateTimeUtil.h"
#import "TripLogListItem.h"
#import "AppViewController.h"

@interface CompetitionView : XUIView<UIAlertViewDelegate>
{
    UIView *welcomeView;
    UIView *countdownView;
    UIView *teamView;
    
    UIImageView *logo;
    UIImageView *bus;
    UIImageView *line;
    
    UILabel *title;
    UIButton *closeButton;
    UIButton *closeCountdown;
    UIButton *acceptButton;
    UIButton *infoButton;
    UIButton *wideInfoButton;
    UILabel *text;
    
    UILabel *remainingDays;
    UILabel *remainingHours;
    UILabel *remainingMinutes;
    UILabel *remainingSeconds;
    UILabel *countdownText;
    NSTimer *countdownTimer;
    
    UIButton *teamLabel;
    
    UIScrollView *logsScrollView;
    UIAlertView *teamAlertView;
    UIAlertView *closeCompetitionAlertView;
    NSMutableArray *loggedTrips;
    
    UIButton *allTripsButton;
}

-(void)updateScrollViewItems;

@end
