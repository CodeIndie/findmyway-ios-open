//
//  MainMenu.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "MainMenu.h"

@implementation MainMenu

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(-device.screenWidth * 0.5, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth * 0.5, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    [super initView:name inController:parentViewController];
    [self setBackgroundColor:colour.clear];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    float menuWidth = layout.screenWidth;
    float menuHeight = layout.screenHeight - layout.topOfContent - layout.buttonSize;
    background = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, menuWidth, menuHeight)];
    [background setBackgroundColor:colour.fmwBackgroundColor];
    
    scrollView = [[UIScrollView alloc] initWithFrame:background.frame];
    [self initButtons];
    
    UISwipeGestureRecognizer *swipeLeftGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeftFrom:)];
    swipeLeftGestureRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [self addGestureRecognizer:swipeLeftGestureRecognizer];
}

-(void)handleSwipeLeftFrom:(UIGestureRecognizer*)recognizer
{
    AppViewController *viewController = (AppViewController*)parentView;
    [self XLog:@"Hiding Main Menu"];
    [viewController.fmwScreen.modesPicker toggleMainMenu];//hideView:viewController.mainMenu];
//    [viewController.fmwModePicker setShowMainMenuTo:NO];
//    //[viewController.fmwModePicker.menuButton setImage:[ImageUtil getImage:@"iconBullets.png"] forState:UIControlStateNormal];
//    [viewController.fmwModePicker.menuButton.imageView setTintColor:colour.fmwTextColor];
}

-(void)initButtons
{
    float yPos = layout.padding;
    
//    directions = [[MainMenuButton alloc] initWithFrame:CGRectMake(layout.padding, yPos, scrollView.frame.size.width - layout.padding * 2, layout.buttonSize)];
//    [directions setDelegate:appDelegate title:NSLocalizedString(@"Directions", nil) iconName:@"" index:0];
//    [scrollView addSubview:directions];
//    yPos += layout.buttonSize;
//    
//    favourites = [[MainMenuButton alloc] initWithFrame:CGRectMake(layout.padding, yPos, scrollView.frame.size.width - layout.padding * 2, layout.buttonSize)];
//    [favourites setDelegate:appDelegate title:NSLocalizedString(@"Favourites", nil) iconName:@"" index:1];
//    [scrollView addSubview:favourites];
//    yPos += layout.buttonSize;
//
    
    transitMaps = [[MainMenuButton alloc] initWithFrame:CGRectMake(layout.padding, yPos, scrollView.frame.size.width - layout.padding * 2, layout.buttonSize)];
    [transitMaps setDelegate:appDelegate title:NSLocalizedString(@"Transit Maps", nil) iconName:@"iconMap.png" index:3];
    [transitMaps.button addTarget:self action:@selector(openTransitMaps) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:transitMaps];
    yPos += layout.buttonSize;
    
    announcements = [[MainMenuButton alloc] initWithFrame:CGRectMake(layout.padding, yPos, scrollView.frame.size.width - layout.padding * 2, layout.buttonSize)];
    [announcements setDelegate:appDelegate title:NSLocalizedString(@"Announcements", nil) iconName:@"iconAnnouncement.png" index:2];
    [announcements.button addTarget:self action:@selector(openAnnouncements) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:announcements];
    yPos += layout.buttonSize;
    
    contactUs = [[MainMenuButton alloc] initWithFrame:CGRectMake(layout.padding, yPos, scrollView.frame.size.width - layout.padding * 2, layout.buttonSize)];
    [contactUs setDelegate:appDelegate title:NSLocalizedString(@"Contact us", nil) iconName:@"iconMail.png" index:7];
    [contactUs.button addTarget:self action:@selector(openMailView) forControlEvents:UIControlEventTouchUpInside];
    [scrollView addSubview:contactUs];
    yPos += layout.buttonSize;
    
//    places = [[MainMenuButton alloc] initWithFrame:CGRectMake(layout.padding, yPos, scrollView.frame.size.width - layout.padding * 2, layout.buttonSize)];
//    [places setDelegate:appDelegate title:NSLocalizedString(@"Places", nil) iconName:@"" index:4];
//    [scrollView addSubview:places];
//    yPos += layout.buttonSize;
//
//    commuterKarma = [[MainMenuButton alloc] initWithFrame:CGRectMake(layout.padding, yPos, scrollView.frame.size.width - layout.padding * 2, layout.buttonSize)];
//    [commuterKarma setDelegate:appDelegate title:NSLocalizedString(@"Commuter Karma", nil) iconName:@"" index:5];
//    [scrollView addSubview:commuterKarma];
//    yPos += layout.buttonSize;
//
//    settings = [[MainMenuButton alloc] initWithFrame:CGRectMake(layout.padding, yPos, scrollView.frame.size.width - layout.padding * 2, layout.buttonSize)];
//    [settings setDelegate:appDelegate title:NSLocalizedString(@"Settings", nil) iconName:@"" index:6];
//    [scrollView addSubview:settings];
//    yPos += layout.buttonSize;

//    about = [[MainMenuButton alloc] initWithFrame:CGRectMake(layout.padding, yPos, scrollView.frame.size.width - layout.padding * 2, layout.buttonSize)];
//    [about setDelegate:appDelegate title:NSLocalizedString(@"About", nil) iconName:@"iconAbout.png" index:9];
//    [scrollView addSubview:about];
//    yPos += layout.buttonSize;
    
    yPos += layout.padding;
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, yPos);
}

-(void)addUIToView
{
    [self addSubview:background];
    [self addSubview:scrollView];
}

-(void)animateIn
{
    [super animateIn];
    [parentView.view addSubview:self];
    self.center = centerOffScreen;
    self.alpha = 0;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOnScreen;
         self.alpha = 1;
     }
                     completion:nil];
}

-(void)animateOut
{
    [super animateOut];
    self.center = centerOnScreen;
    self.alpha = 1;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOffScreen;
         self.alpha = 0;
     }
     completion:^(BOOL finished){
         self.isActive = NO;
         [self removeFromSuperview];
     }];
}

// ANNOUNCEMENTS

-(void)openAnnouncements
{
    [self XLog:@"opening announcements"];
    [self animateOut];
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController.fmwScreen.modesPicker toggleMainMenu];
    [viewController showView:viewController.announcementScreen];
}


// TRANSIT MAPS

-(void)openTransitMaps
{
    [self XLog:@"opening transit maps"];
    [self animateOut];
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController.fmwScreen.modesPicker toggleMainMenu];
    [viewController showView:viewController.transitMapScreen];
}

// CONTACT US

-(void)openMailView
{
    [self XLog:@"opening messaging interface"];
    NSString *emailSubject = @"Findmyway V2 iOS Feedback";
    NSString *emailMessage = @"";
    NSArray *emailRecipients = [NSArray arrayWithObject:@"code.indie@gmail.com" ];
    
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailSubject];
    [mc setMessageBody:emailMessage isHTML:NO];
    [mc setToRecipients:emailRecipients];
    
    [parentView presentViewController:mc animated:YES completion:NULL];
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    // Close the Mail Interface
    [parentView dismissViewControllerAnimated:YES completion:NULL];
}


@end
