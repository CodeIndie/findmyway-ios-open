//
//  FavouritesView.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/10.
//

#import "XUIView.h"
#import "LocationListItem.h"

@interface FavouritesView : XUIView
{
    UILabel *title;
    NSMutableArray *locations;
    UIButton *showAll;
}
@end
