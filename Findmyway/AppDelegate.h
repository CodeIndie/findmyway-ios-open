//
//  AppDelegate.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "LayoutConfig.h"
#import "DeviceConfig.h"
#import "ColourConfig.h"
#import "TripQuery.h"
#import "MapConfig.h"
#import "SearchConfig.h"
#import "JSONParser.h"
#import "LocationUtil.h"
#import "Notifications.h"
#import "DefaultsUtil.h"

#import "ApiUtil.h"
#import "ApiError.h"
#import "ApiResponse.h"
#import "ImageUtil.h"

@class LocationUtil, LayoutConfig, DeviceConfig, ColourConfig, ApiUtil, ApiResponse, TripQuery, CompetitionUtil;

@interface AppDelegate : UIResponder <UIApplicationDelegate, NSURLConnectionDelegate>
{
    BOOL gettingNewToken;
}
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

// FMW VARIABLES
@property NSString *storyboardName;

// UTILITIES
@property LayoutConfig *layout;
@property DeviceConfig *device;
@property ColourConfig *colour;
@property TripQuery *tripQuery;
@property MapConfig *mapConfig;
@property SearchConfig *searchConfig;
@property LocationUtil *locationUtil;

@property ApiUtil *apiUtil;
@property ApiError *apiError;
@property ApiResponse *apiResponse;

@property NSDate *earthHourStartDate;
@property NSDate *earthHourEndDate;

// ASYNCHRONOUS NSURL VARS
@property NSHTTPURLResponse *asyncResponse;
@property NSMutableData *asyncResponseData;
@property int statusCode;

-(void)configureApp;
-(void)getNewToken;

-(void)GETRequest:(NSString*)urlString
   isAsynchronous:(BOOL)isAsynchronous
    cacheResponse:(BOOL)cachingEnabled;

-(UIImage*)getImageForMode:(NSString*)mode;
-(void)openWebsite:(NSString*)urlString;

@end

