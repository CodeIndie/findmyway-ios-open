//
//  SearchItem.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/08.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"

@interface SearchItem : NSObject<NSCoding>

@property Location *location;
@property NSMutableArray *categories;
@property NSString *name;
@property NSString *address;
@property int distance;
@property NSString *resourceId;
@property NSString *descriptionText;
@property BOOL isFavourite;

-(id)initWithDictionary:(NSDictionary*)dictionary;
-(id)initWithSearchItem:(SearchItem*)searchItem;
-(id)initWithLocation:(Location*)location;
-(BOOL)isEqualTo:(SearchItem*)searchItem;
@end
