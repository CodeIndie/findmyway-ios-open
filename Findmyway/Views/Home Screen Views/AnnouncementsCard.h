//
//  AnnouncementsView.h
//  Findmyway
//
//  Created by Bilo on 4/20/15.
//

#import "XUIView.h"
#import "AppViewController.h"

@class AppViewController;

@interface AnnouncementsCard : XUIView
{
    UILabel *title;
    UIButton *showAllButton;
    UIScrollView *scrollView;
}

@end
