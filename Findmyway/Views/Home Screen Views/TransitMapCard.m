//
//  TransitMapCard.m
//  Findmyway
//
//  Created by Bilo on 4/20/15.
//

#import "TransitMapCard.h"

@implementation TransitMapCard

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self addNotificationListeners];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    title = [XUILabel initWithFrame:CGRectMake(layout.padding, 0, layout.paddedWidth, layout.buttonSize)
                               text:NSLocalizedString(@"Transit Maps", nil)
                          textColor:colour.fmwTextColor
                    backgroundColor:colour.clear
                      textAlignment:NSTextAlignmentCenter];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, [self width], [self height])];
    [self initMapButtons];
    
    showAllButton = [XUIButton initWithFrame:CGRectMake(0, layout.buttonSize * 4, self.frame.size.width, layout.buttonSize)
                                       title:NSLocalizedString(@"SHOW ALL", nil)
                                  titleColor:colour.fmwActiveColor
                             backgroundColor:colour.clear
                         horizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [showAllButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [showAllButton addTarget:self action:@selector(showAllMaps) forControlEvents:UIControlEventTouchUpInside];
    CALayer *buttonLayer = [showAllButton layer];
    [buttonLayer setBorderColor:colour.fmwBorderColor.CGColor];
    [buttonLayer setBorderWidth:layout.delimiterSize];
}

-(void)addUIToView
{
    [self addSubview:title];
    [self addSubview:scrollView];
    [self addSubview:showAllButton];
}

-(void)showAllMaps
{
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController showView:viewController.transitMapScreen];
}

-(void)initMapButtons
{
    int i = 0;
    for(Operator *operator in appDelegate.apiResponse.operators)
    {
        if(i>=3) break;
        
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, i * layout.buttonSize, layout.screenWidth, layout.buttonSize)];
        [button setTitle:operator.displayName forState:UIControlStateNormal];
        [button setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, layout.margin, 0, 0)];
        [button setTag:i];
        [button addTarget:self action:@selector(openSystemMap:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:button];
        i++;
    }
    scrollView.contentSize = CGSizeMake(layout.screenWidth, layout.buttonSize * i);
}

-(IBAction)openSystemMap:(id)sender
{
    NSInteger index = ((UIButton*)sender).tag;
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController.transitMapScreen openWithMap:index];
}

-(void)addNotificationListeners
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(initMapButtons) name:[[Notifications instance] GETOperatorsSuccess] object:nil];
}
@end
