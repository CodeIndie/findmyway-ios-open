//
//  TransitMapScreen.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/30.
//

#import <Foundation/Foundation.h>
#import "XUIView.h"
#import "AppViewController.h"
typedef enum
{
    MapList,
    LoadingImage,
    ShowingImage
}TransitMapState;

@interface TransitMapScreen : XUIView<UIScrollViewDelegate>
{
    UIScrollView *scrollView;
    UIImageView *mapImage;
    UIScrollView *imageScrollView;
    TransitMapState screenState;
    UIButton *backButton;
    UIView *topBar;
    UIView *delimiter;
    UILabel *mapTitle;
    NSString *selectedOperatorName;
    NSInteger mapIndex;
}

-(void)openWithMap:(NSInteger)index;

@end
