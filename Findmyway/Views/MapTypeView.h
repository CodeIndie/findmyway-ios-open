//
//  MapTypeView.h
//  Findmyway
//
//  Created by Bilo on 4/15/15.
//

#import "XUIView.h"
#import "XUIButton.h"

typedef enum
{
    StandardMapView = 0,
    SatelliteMapView = 1,
    HybridMapView = 2,
}MapViewType;

@interface MapTypeView : XUIView
{

}

@property UIButton *backgroundButton;
@property UIView *backgroundPanel;

@property UIButton *standardMapButton;
@property UIButton *satelliteMapButton;
@property UIButton *hybridMapButton;

@end
