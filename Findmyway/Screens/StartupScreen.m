//
//  StartupScreen.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/02/11.
//

#import "StartupScreen.h"

@implementation StartupScreen



-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
    [self startupRoutine];
}

-(void)initViewElements
{
    CGRect logoRect = CGRectMake(layout.screenWidth/2 - layout.buttonSize, layout.screenHeight/2 - layout.buttonSize, layout.buttonSize * 2, layout.buttonSize * 2);
    
    // LOGO BACKGROUND (BLUE SHAPE)
    logoBackground = [[UIImageView alloc] initWithFrame:logoRect];
    [logoBackground setImage:[UIImage imageNamed:@"FMWSymbolOutline.png"]];
    
    // LOGO IMAGE (A >>> B)
    animatedImage = [[UIImageView alloc] initWithFrame:logoRect];
    [animatedImage setImage:[UIImage imageNamed:@"FMWSymbolAB.png"]];
    
    // LABEL INDICATING WHAT IS HAPPENING
    loadingLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, layout.screenHeight/2 + layout.buttonSize * 2, layout.screenWidth, layout.buttonSize)];
    [loadingLabel setTextColor:appDelegate.colour.fmwActiveColor];
    [loadingLabel setTextAlignment:NSTextAlignmentCenter];
    
    // POWERED BY
    UIImage *poweredByImage = [UIImage imageNamed:@"poweredByWhiteBGCentered.png"];
    float aspectRatio = poweredByImage.size.width / poweredByImage.size.height;
    poweredByButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, aspectRatio * layout.buttonSize, layout.buttonSize)];
    [poweredByButton setImage:poweredByImage forState:UIControlStateNormal];
    poweredByButton.center = CGPointMake(layout.screenWidth/2, layout.screenHeight - layout.buttonSize);
    [poweredByButton addTarget:self action:@selector(openWebsite) forControlEvents:UIControlEventTouchUpInside];
    
    //    LoginScreen *loginScreen = [[LoginScreen alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.screenHeight)];
    //    [loginScreen initView:@"Log In" inController:self];
    //    [loginScreen animateIn];
}

-(void)addUIToView
{
    [self addSubview:logoBackground];
    [self addSubview:animatedImage];
    [self addSubview:loadingLabel];
    [self addSubview:poweredByButton];
}

-(void)animateIn
{
    [super animateIn];
    [self addNotificationListeners];
    
    [parentView.view addSubview:self];
    [parentView.navigationItem setTitle:self.viewName];
    //self.center = centerOffScreen;
    self.alpha = 0;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         //self.center = centerOnScreen;
         self.alpha = 1;
     }
                     completion:^(BOOL finished){
                         //[self updateUI];
                     }];
}

-(void)animateOut
{
    [super animateOut];
    [self removeNotificationListeners];
    
    //self.center = centerOnScreen;
    self.alpha = 1;
    [UIView animateWithDuration:animationInterval delay:animationInterval * 0.6 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         //self.center = centerOffScreen;
         self.alpha = 0;
     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                     }];
}

-(void)openWebsite
{
    [appDelegate openWebsite:@"http://www.google.com"];
}

-(void)rotateLogo
{
    if(loadingData)
    {
        logoAngle += increment;
        if(logoAngle >= 540)
        {
            logoAngle = 0;
            [self loadDataSet];
            if(loadingIndex == 6)
            {
                [self animateFmwScreenIn];
            }
        }
        if(logoAngle <= 360)
        {
            CALayer *layer = animatedImage.layer;
            
            CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
            rotationAndPerspectiveTransform = CATransform3DTranslate(rotationAndPerspectiveTransform, 0, 0, 20);
            rotationAndPerspectiveTransform.m34 = 5.0 / -500;
            rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, logoAngle * M_PI / 180.0f, 0.0f, 1.0f, 0.0f);
            layer.transform = rotationAndPerspectiveTransform;
        }
        else
        {
            //logoAngle = 0;
        }
    }
}

-(void)initRotationTimer
{
    logoAngle = 0;
    increment = 4;
    loadingData = YES;
    loadingIndex = 0;
    if(animationTimer != (id)[NSNull null])
    {
        [animationTimer invalidate];
        animationTimer = nil;
    }
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(rotateLogo) userInfo:nil repeats:YES];
}

-(void)startupRoutine
{
    //[appDelegate.apiUtil GET:Operators parameters:nil];
    [self initRotationTimer];
    return;
}

-(void)showFmwScreen
{
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwScreen];
}

-(void)loadDataSet
{
    switch(loadingIndex)
    {
        case 0:
        {
            [loadingLabel setText:@""];
            increaseLoadingIndex = YES;
        }
            break;
            
        case 1:// CREATE USER
        {
            if(increaseLoadingIndex)
            {
                if(appDelegate.apiUtil.fmwToken != nil)
                {
                    loadingIndex++;
                    return;
                }
                
                [loadingLabel setText:@"initialising Findmyway"];
                JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:[appDelegate.apiUtil urlForRequest:Users callType:POST parameters:nil]];
                increaseLoadingIndex = NO;
                [connection executeRequestOnSuccess:^(NSHTTPURLResponse *response, NSString *bodyString){
                    
                    increaseLoadingIndex = YES;
                    NSLog(@"API - Users: %ld", (long)response.statusCode);
                    [appDelegate.apiResponse addToken:[JSONParser convertJSONToDictionary:bodyString]];
                    loadingIndex++;
                }
                failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
                {
                     NSLog(@"API CONNECTION FAILURE - Users: %@", error);
                    
                    //[self addressFailedLoading];
                    //increaseLoadingIndex = YES;
                    
                    [self showAlertView:@"Connection Error" message:@"There was a connection error.\n\nPlease retry loading data." button:@"Retry"];
                }
                didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite){
                NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
                }];
            }
        }
        break;
            
        case 2:// GET OPERATORS
        {
            if(increaseLoadingIndex)
            {
                loadingIndex++;
                return;
                [loadingLabel setText:@"Getting Operators"];
                JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:[appDelegate.apiUtil urlForRequest:Operators callType:GET parameters:nil]];
                increaseLoadingIndex = NO;
                [connection executeRequestOnSuccess:^(NSHTTPURLResponse *response, NSString *bodyString){
                    
                    increaseLoadingIndex = YES;
                    NSLog(@"API - Operators: %ld", (long)response.statusCode);
                    [appDelegate.apiResponse addOperators:[JSONParser convertJSONToDictionary:bodyString]];
                    loadingIndex++;
                }
                failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error){
                     NSLog(@"API CONNECTION FAILURE - Operators: %@", error);
                    
                    //[self addressFailedLoading];
                    increaseLoadingIndex = YES;
                }
                didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite){
                    NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
                }];
            }
        }
            
        default:
        {
            [loadingLabel setText:@""];
            loadingData = NO;
            increaseLoadingIndex = YES;
            [self animateFmwScreenIn];
            
        }break;
    }
    
    if(increaseLoadingIndex)
    {
        loadingIndex++;
    }
}

-(void)animateFmwScreenIn
{
    [UIView animateWithDuration:1 animations:^
     {
         float p = layout.padding * 0.75;
         CGRect rect = CGRectMake(layout.padding + p, p, layout.buttonSize - 2*p, layout.buttonSize - 2*p);
         
         logoBackground.frame = rect;
         animatedImage.frame = rect;
         
         loadingLabel.alpha = 0;
     }
                     completion:^(BOOL finished)
     {
         [self showFmwScreen];
     }];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView isEqual:alertViewPopup])
    {
        if(buttonIndex == 0)
        {
            [self loadDataSet];
        }
    }
}

@end
