//
//  AnnouncementListItem.h
//  Findmyway
//
//  Created by Bilo on 4/13/15.
//

#import <UIKit/UIKit.h>
#import "Announcement.h"
#import "AppDelegate.h"

@interface AnnouncementListItem : UIView
{
    AppDelegate *appDelegate;
    LayoutConfig *layout;
    ColourConfig *colour;
    
    Announcement *announcement;
    
    UIImageView *icon;
    UILabel *text;
}

@property UIButton *shareButton;

-(id)initWithFrame:(CGRect)frame announcement:(Announcement*)announcement;
-(id)initWithAnnouncement:(Announcement*)announcement frame:(CGRect)frame;
-(float)height;

@end
