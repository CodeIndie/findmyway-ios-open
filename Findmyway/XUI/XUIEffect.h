//
//  XUIEffect.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/02/16.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
typedef enum
{
    Pulse,
    RectFlash
}XUIEffectType;

@interface XUIEffect : UIView
{
    XUIEffectType effectType;
    NSMutableArray *graphics;
    NSTimer *animationTimer;
    UIImageView *effectImage;
    
    float pauseDuration;
    float effectDuration;
    float animationCounter;
    
    float startSize;
    float endSize;
    float currentSize;
}

-(id)initPulseWithDuration:(float)duration pause:(float)pause mainGraphic:(UIImageView*)mainGraphic effectGraphic:(UIImageView*)effectGraphic;

-(void)pulseEffect;
@end
