//
//  TransitMapScreen.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/30.
//

#import "TransitMapScreen.h"

@implementation TransitMapScreen


-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(device.screenWidth * 1.5, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth * 0.5, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
    [self setBackgroundColor:colour.fmwBackgroundColor];
}

-(void)initViewElements
{
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    layout = appDelegate.layout;
    colour = appDelegate.colour;

    backButton = [XUIButton backButtonWithTitle:NSLocalizedString(@"Back", nil) color:colour.fmwActiveColor layout:layout];
    [backButton addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    
    mapTitle = [[UILabel alloc] initWithFrame:CGRectMake(layout.buttonSize * 2, 0, self.frame.size.width - 2 * layout.buttonSize - layout.margin, layout.buttonSize)];
    [mapTitle setTextAlignment:NSTextAlignmentRight];
    //[mapTitle setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [mapTitle setTextColor:colour.fmwTextColor];
    [mapTitle setText:NSLocalizedString(@"Select an operator", nil)];
    
    topBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.buttonSize)];
    [topBar setBackgroundColor:colour.fmwHeader];
    
    delimiter = [[UIView alloc] initWithFrame:CGRectMake(0, layout.buttonSize - layout.delimiterSize, layout.screenWidth, layout.delimiterSize)];
    [delimiter setBackgroundColor:colour.fmwBorderColor];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, self.frame.size.height - layout.buttonSize)];
    
    int i = 0;
    for(Operator *operator in appDelegate.apiResponse.operators)
    {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, i * layout.buttonSize, layout.screenWidth, layout.buttonSize)];
        [button setTitle:operator.displayName forState:UIControlStateNormal];
        [button setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
        button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        [button setTitleEdgeInsets:UIEdgeInsetsMake(0, layout.margin, 0, 0)];
        [button setTag:i];
        [button addTarget:self action:@selector(openSystemMap:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:button];
        i++;
    }
    scrollView.contentSize = CGSizeMake(layout.screenWidth, layout.buttonSize * i);
}

-(void)clearView
{
    for(UIView *view in self.subviews)
    {
        [view removeFromSuperview];
    }
    [self addSubview:topBar];
    [self addSubview:backButton];
    [self addSubview:mapTitle];
    [self addSubview:delimiter];
}

-(void)addUIToView
{
    [self addSubview:topBar];
    [self addSubview:backButton];
    [self addSubview:delimiter];
    [self addSubview:mapTitle];
    [self addSubview:scrollView];
}

-(void)animateIn
{
    [super animateIn];
    [parentView.view addSubview:self];
    
    [self removeSubviews];
    [self initViewElements];
    [self addUIToView];
    
    self.center = centerOffScreen;
    self.alpha = 0;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOnScreen;
         self.alpha = 1;
     }
     completion:^(BOOL finished){
         if(mapIndex >= 0)
         {
             [self openImageForIndex:mapIndex];
             mapIndex = -1;
         }
     }];
}

-(void)animateOut
{
    [super animateOut];
    self.center = centerOnScreen;
    self.alpha = 1;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOffScreen;
         self.alpha = 0;
     }
     completion:^(BOOL finished){
         [self removeFromSuperview];
     }];
}

-(void)openWithMap:(NSInteger)index
{
    mapIndex = index;
    [self animateIn];
}

-(IBAction)openSystemMap:(id)sender
{
    NSInteger index = ((UIButton*)sender).tag;
    [self openImageForIndex:index];
}

-(void)openImageForIndex:(NSInteger)index
{
    Operator *selectedOperator = (Operator*)[appDelegate.apiResponse.operators objectAtIndex:index];
    selectedOperatorName = selectedOperator.displayName;
    
    [self adjustState:LoadingImage];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                   ^{
                       NSURL *imageURL = [NSURL URLWithString:selectedOperator.routeMapUrl];
                       __block NSData *imageData;
                       
                       
                       dispatch_sync(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),
                                     ^{
                                         imageData = [NSData dataWithContentsOfURL:imageURL];
                                         
                                         
                                         dispatch_sync(dispatch_get_main_queue(), ^{
                                             UIImage *image = [UIImage imageWithData:imageData];
                                             [self addImageToView:image];
                                             [self adjustState:ShowingImage];
                                         });
                                     });
                   });
}

-(void)addImageToView:(UIImage*)image
{
    if(image)
    {
        float width, height;
        if(image.size.width > image.size.height)
        {
            width = layout.screenWidth;
            //height = (layout.screenHeight/layout.screenWidth) * width;
            height = (image.size.height/image.size.width) * width;
        }
        else
        {
            height = layout.screenHeight;
            //width = (layout.screenWidth/layout.screenHeight) * height;
            width = (image.size.width/image.size.height) * height;
        }
        mapImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
        [mapImage setImage:image];
        [mapImage setContentMode:UIViewContentModeScaleAspectFit];
        
        NSLog(@"Adding image to uiview");
        imageScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, self.frame.size.height - layout.buttonSize - layout.delimiterSize)];
        imageScrollView.contentSize = mapImage.bounds.size;
        imageScrollView.maximumZoomScale = 4;
        imageScrollView.minimumZoomScale = 1;
        imageScrollView.delegate = self;
        
        [imageScrollView addSubview:mapImage];
    }
}

-(void)adjustState:(TransitMapState)state
{
    screenState = state;
    switch(screenState)
    {
        case MapList:
        {
            [self clearView];
            [self addSubview:scrollView];
            [mapTitle setText:NSLocalizedString(@"Select an operator", nil)];
        }break;
            
        case LoadingImage:
        {
            [self clearView];
            [self startLoader:CGPointMake(layout.screenWidth - layout.buttonSize/2 - layout.padding, layout.buttonSize * 0.5) inView:self];
            [mapTitle setText:@""];
        }break;
            
        case ShowingImage:
        {
            [self clearView];
            [self addSubview:imageScrollView];
            [mapTitle setText:selectedOperatorName];
        }break;
    }
}

-(void)removeSubviews
{
    for(UIView *view in self.subviews)
    {
        [view removeFromSuperview];
    }
}

-(void)backButtonAction
{
    switch(screenState)
    {
        case ShowingImage:
        {
            NSLog(@"Back to maplist");
            [self adjustState:MapList];
        }break;
        
        case LoadingImage:
        {
            NSLog(@"Back to maplist");
            [self adjustState:MapList];
        }
        
        case MapList:
        {
            NSLog(@"Back to main menu");
            AppViewController *viewController = (AppViewController*)parentView;
            [viewController hideView:viewController.transitMapScreen];
        }
    }
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return mapImage;
}
@end
