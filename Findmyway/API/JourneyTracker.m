//
//  JourneyTracker.m
//  Findmyway
//
//  Created by Bilo on 2/25/15.

#import "JourneyTracker.h"

@implementation JourneyTracker

#pragma mark JOURNEY HUB PROTOCOL
-(void)gainInterestInJourney:(NSString *)journeyGUID
{
    
}

-(void)loseInterestInJourney:(NSString *)journeyGUID
{
    
}

-(void)startJourney:(NSString *)journeyGUID
{
    
}

-(void)endJourney
{
    
}

-(void)message:(NSString *)message
{
    
}

#pragma mark JOURNEY LISTENER PROTOCOL
-(void)journeyUpdate:(int)leg journeyProgress:(float)journeyProgress
{
    
}

-(void)receiveMessage:(NSString *)message
{
    
}

-(void)pingInterval:(long)millisecondsBetweenCalls
{
    
}

@end