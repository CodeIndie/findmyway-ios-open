//
//  XUIButton.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/01/30.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIButton.h"

@implementation XUIButton

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        //[self addTarget:self action:@selector(setPressed) forControlEvents:UIControlEventTouchDown];
        //[self addTarget:self action:@selector(setReleased) forControlEvents:UIControlEventTouchUpInside];
        //[self addTarget:self action:@selector(setReleased) forControlEvents:UIControlEventTouchUpOutside];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame setColorsOnFront:(UIColor *)onFront onBack:(UIColor *)onBack offFront:(UIColor *)offFront offBack:(UIColor *)offBack
{
    self = [self initWithFrame:frame];
    if(self)
    {
        [self setColorsOnFront:onFront onBack:onBack offFront:offFront offBack:offBack];
    }
    return self;
}

-(void)setColorsOnFront:(UIColor *)onFront onBack:(UIColor *)onBack offFront:(UIColor *)offFront offBack:(UIColor *)offBack
{
    onFrontCol = onFront;
    onBackCol = onBack;
    offFrontCol = offFront;
    offBackCol = offBack;
    
    [self setTitleColor:offFrontCol forState:UIControlStateNormal];
    [self setBackgroundColor:offBackCol];
}

-(void)setPressed
{
    [self setBackgroundColor:onBackCol];
    [self setTintColor:onFrontCol];
    [self setTitleColor:onFrontCol forState:UIControlStateNormal];
}

-(void)setReleased
{
    [self setBackgroundColor:offBackCol];
    [self setTintColor:offFrontCol];
    [self setTitleColor:offFrontCol forState:UIControlStateNormal];
}

-(void)setTitle:(NSString*)title alignment:(UIControlContentHorizontalAlignment)alignment
{
    [self setTitle:title forState:UIControlStateNormal];
    self.contentHorizontalAlignment = alignment;
}

+(XUIButton*)initWithFrame:(CGRect)frame title:(NSString *)title titleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor horizontalAlignment:(UIControlContentHorizontalAlignment)horizontalAlignment
{
    XUIButton *button = [[XUIButton alloc] initWithFrame:frame];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    [button setBackgroundColor:backgroundColor];
    button.contentHorizontalAlignment = horizontalAlignment;
    return button;
}

+(XUIButton*)initWithFrame:(CGRect)frame title:(NSString *)title fontSize:(float)fontSize bold:(BOOL)bold titleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor horizontalAlignment:(UIControlContentHorizontalAlignment)horizontalAlignment
{
    XUIButton *button = [[XUIButton alloc] initWithFrame:frame];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    [button setBackgroundColor:backgroundColor];
    [button.titleLabel setFont:(bold ? [UIFont boldSystemFontOfSize:fontSize] : [UIFont systemFontOfSize:fontSize])];
    button.contentHorizontalAlignment = horizontalAlignment;
    return button;
}

+(XUIButton*)backButtonWithTitle:(NSString*)title color:(UIColor*)color layout:(LayoutConfig*)layout
{
    XUIButton *button = [XUIButton initWithFrame:CGRectMake(0, 0, layout.buttonSize * 2, layout.buttonSize)
                                          title:title
                                       fontSize:[UIFont systemFontSize] + 1
                                           bold:YES titleColor:color
                                backgroundColor:[UIColor clearColor]
                            horizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0, layout.padding * 3, 0, 0)];
    
    UIImageView *backArrow = [[UIImageView alloc] initWithFrame:CGRectMake(layout.padding/2, layout.padding/2, layout.buttonSize - layout.padding, layout.buttonSize - layout.padding)];
    [backArrow setImage:[ImageUtil getImage:@"iconBack.png"]];
    [backArrow setTintColor:color];
    [button addSubview:backArrow];
    return button;
}
@end
