//
//  SearchItem.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/08.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "SearchListItem.h"

@implementation SearchListItem

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        
    }
    return self;
}

-(void)initView:(AppDelegate*)appDelegate searchItem:(SearchItem*)searchItem height:(float)height
{
    layout = appDelegate.layout;
    colour = appDelegate.colour;
    
    self.searchItem = [[SearchItem alloc] initWithSearchItem:searchItem];
    
    [self setBackgroundColor:appDelegate.colour.clear];
    
    float y = layout.padding/2;
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding, y, layout.paddedWidth - layout.buttonSize * 1.5, layout.buttonSize)];
    [self.label setBackgroundColor:appDelegate.colour.clear];
    [self.label setText:[NSString stringWithFormat:@"%@ - %@", searchItem.location.name, searchItem.descriptionText]];
    [self.label setTextColor:appDelegate.colour.fmwTextColorLight];
    [self.label setTextAlignment:NSTextAlignmentLeft & NSTextAlignmentNatural];
    [self.label setNumberOfLines:2];
    [self.label setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 1]];
    [self addSubview:self.label];
    
    self.distanceLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width - layout.buttonSize * 2, y, layout.buttonSize * 2 - layout.padding, layout.buttonSize * 0.3)];
    [self.distanceLabel setBackgroundColor:appDelegate.colour.clear];
    if([appDelegate.locationUtil isLocationAvailable])
    {
        [self.distanceLabel setText:[DistanceUtil metersStringBetween:searchItem.location locationB:appDelegate.locationUtil.userLocation]];
    }
    [self.distanceLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize] - 2]];
    [self.distanceLabel setTextColor:appDelegate.colour.fmwActiveColor];
    [self.distanceLabel setTextAlignment:NSTextAlignmentRight];
    [self addSubview:self.distanceLabel];
    self.button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, height)];
    [self addSubview:self.button];
    
    self.mapButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - layout.buttonSize, layout.padding, layout.buttonSize, layout.buttonSize)];
    [self.mapButton setImage:[ImageUtil getImage:@"iconPin.png"] forState:UIControlStateNormal];
    float p = layout.padding;
    [self.mapButton setImageEdgeInsets:UIEdgeInsetsMake(p, p, p, p)];
    [self.mapButton.imageView setTintColor:appDelegate.colour.fmwActiveColor];
    [self addSubview:self.mapButton];
    
    self.favouritesButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth - layout.buttonSize, (height - layout.buttonSize)/2, layout.buttonSize, layout.buttonSize)];
    self.horizontalLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, height - layout.delimiterSize, layout.screenWidth, layout.delimiterSize)];
    [self.horizontalLine setBackgroundColor:appDelegate.colour.fmwBorderColor];
    [self addSubview:self.horizontalLine];
}

-(void)setItemTag:(int)index
{
    self.mapButton.tag = index;
    self.button.tag = index;
}

-(void)highlightItem
{
    [self.button setBackgroundColor:colour.fmwActiveColorLight];
    [self.icon setTintColor:colour.white];
    [self.label setTextColor:colour.white];
    [self.mapButton.imageView setTintColor:colour.white];
    [self.distanceLabel setTextColor:colour.white];
}

-(void)unhighlightItem
{
    [self.button setBackgroundColor:colour.clear];
    [self.icon setTintColor:colour.fmwTextColorLight];
    [self.label setTextColor:colour.fmwTextColorLight];
    [self.mapButton.imageView setTintColor:colour.fmwActiveColor];
    [self.distanceLabel setTextColor:colour.fmwActiveColor];
}
@end
