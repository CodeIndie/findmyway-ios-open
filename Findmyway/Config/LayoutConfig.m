//
//  LayoutConfig.m
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//

#import "LayoutConfig.h"

@implementation LayoutConfig

-(id)init:(NSDictionary *)configDictionary
{
    self = [super init];
    if(self)
    {
        NSDictionary *dictionary = [configDictionary objectForKey:@"Layout"];
        
        self.buttonSize = [[dictionary objectForKey:@"buttonSize"] floatValue];
        self.margin = [[dictionary objectForKey:@"margin"] floatValue];
        self.padding = [[dictionary objectForKey:@"padding"] floatValue];
        
        self.systemFontSize = [UIFont systemFontSize];
        
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        self.screenWidth = appDelegate.device.screenWidth;
        self.screenHeight = appDelegate.device.screenHeight;
        self.paddedWidth = self.screenWidth - self.padding*2;
        self.marginWidth = self.screenWidth - self.margin*2;
        
        self.delimiterSize = 1;
        
        [self print];
    }
    return self;
}

-(void)print
{
    NSLog(@"Layout: buttonSize: %f", self.buttonSize);
}
@end
