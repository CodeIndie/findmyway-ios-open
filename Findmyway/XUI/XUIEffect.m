//
//  XUIEffect.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/02/16.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIEffect.h"

@implementation XUIEffect

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initPulseWithDuration:(float)duration pause:(float)pause mainGraphic:(UIImageView *)mainGraphic effectGraphic:(UIImageView *)effectGraphic
{
    self = [super init];
    if(self)
    {
        effectDuration = duration;
        pauseDuration = pause;
        
        effectType = Pulse;
        graphics = [[NSMutableArray alloc] init];
        [graphics addObject:mainGraphic];
        [graphics addObject:effectGraphic];
        
//        [self addSubview:(UIImageView*)[graphics objectAtIndex:0]];
//        [self addSubview:(UIImageView*)[graphics objectAtIndex:1]];
        effectImage = mainGraphic;
        [self addSubview:effectImage];
        [self initAnimationEffect];
    }
    return self;
}

-(void)initAnimationEffect
{
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:(effectDuration + pauseDuration) target:self selector:@selector(pulseEffect) userInfo:nil repeats:YES];
}

-(void)pulseEffect
{
    float fxSize = 100;
    [UIView animateWithDuration:effectDuration delay:pauseDuration options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         //self.center = centerOffScreen;
         effectImage.alpha = 0;
         effectImage.frame =
         CGRectMake(effectImage.frame.origin.x - fxSize,
                    effectImage.frame.origin.y - fxSize,
                   fxSize, fxSize);
     }
                     completion:^(BOOL finished){
//                         [self removeFromSuperview];
                         effectImage.alpha = 1;
                         effectImage.frame = ((UIImageView*)[graphics objectAtIndex:0]).frame;
                     }];
}
@end
