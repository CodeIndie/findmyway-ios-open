//
//  PlacesUtil.h
//  Findmyway
//
//  Created by Bilo on 3/18/15.
//

#import <Foundation/Foundation.h>
#import "SearchItem.h"
#import "DefaultsUtil.h"

@interface PlacesUtil : NSObject

+(void)storeLocation:(SearchItem*)location;
+(NSArray*)getLocations;
+(void)setLocations:(NSArray*)locations;

@end
