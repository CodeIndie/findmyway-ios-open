//
//  OperatorList.m
//  Findmyway
//
//  Created by Bilo on 2/13/15.
//

#import "OperatorList.h"

@implementation OperatorList

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(device.screenWidth * 1.5, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth * 0.5, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    [super initView:name inController:parentViewController];
    [self setBackgroundColor:colour.clear];
    [self initViewElements];
    [self addUIToView];
    
    UISwipeGestureRecognizer *swipeRightGestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRightFrom:)];
    swipeRightGestureRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [self addGestureRecognizer:swipeRightGestureRecognizer];
}

-(void)handleSwipeRightFrom:(UIGestureRecognizer*)recognizer
{
    AppViewController *viewController = (AppViewController*)parentView;
    [self XLog:@"Hiding Operators"];
    [viewController.fmwScreen.modesPicker toggleOperatorsMenu];
}

-(void)initViewElements
{
    float menuWidth = layout.screenWidth;
    float menuHeight = layout.screenHeight - layout.topOfContent - layout.buttonSize;

    background = [[UIImageView alloc] initWithFrame:CGRectMake(layout.screenWidth - menuWidth, 0, menuWidth, menuHeight)];
    [background setBackgroundColor:colour.fmwBackgroundColor];
    
    scrollView = [[UIScrollView alloc] initWithFrame:background.frame];
    [self addOperators];
}

-(void)getOperators
{
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:[appDelegate.apiUtil urlForRequest:Operators callType:GET parameters:nil]];
    [self startLoader:CGPointMake(self.frame.size.width/2, self.frame.size.height/2) inView:self];
    [connection executeRequestOnSuccess:^(NSHTTPURLResponse *response, NSString *bodyString)
    {
        NSLog(@"API - Operators: %ld", (long)response.statusCode);
        [appDelegate.apiResponse addOperators:[JSONParser convertJSONToDictionary:bodyString]];
        [self addOperators];
        [self stopLoader];
    }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
    {
        NSLog(@"API CONNECTION FAILURE - Operators: %@", error);
    }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
    {
        NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
    }];
}

-(void)addOperators
{
    [self separatePrivateAndPublicOperators];
    
    int i = 0;
    float labelOffset = 0;
    self.operatorListItems = [[NSMutableArray alloc] init];
    for(Operator *operatorItem in operators)
    {
        if(!operatorItem.isPublic && privateOperatorLabel == nil)
        {
            NSMutableAttributedString *privateOperatorString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"Private Operators", nil)];
            [privateOperatorString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:(NSRange){0, [privateOperatorString length]}];

            privateOperatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding, labelOffset + i * layout.buttonSize, layout.paddedWidth, layout.buttonSize)];
            [privateOperatorLabel setAttributedText:privateOperatorString];
            [privateOperatorLabel setTextColor:colour.fmwTextColor];
            [scrollView addSubview:privateOperatorLabel];
            labelOffset += layout.buttonSize;
        }
        
        if(operatorItem.isPublic && publicOperatorLabel == nil)
        {
            NSMutableAttributedString *publicOperatorString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"Public Operators", nil)];
            [publicOperatorString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1] range:(NSRange){0, [publicOperatorString length]}];
            
            publicOperatorLabel = [[UILabel alloc] initWithFrame:CGRectMake(layout.padding, labelOffset + i * layout.buttonSize, layout.paddedWidth, layout.buttonSize)];
            [publicOperatorLabel setAttributedText:publicOperatorString];
            [publicOperatorLabel setTextColor:colour.fmwTextColor];
            [scrollView addSubview:publicOperatorLabel];
            labelOffset += layout.buttonSize;
        }
        
        OperatorListItem *listItem = [[OperatorListItem alloc] initWithFrame:CGRectMake(0, labelOffset + i * layout.buttonSize, background.frame.size.width, background.frame.size.height) operator:operatorItem appDelegate:appDelegate];
        [self.operatorListItems addObject:listItem];
        
        [scrollView addSubview:listItem];
        i++;
    }
    scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, labelOffset + i * layout.buttonSize);
}

-(void)separatePrivateAndPublicOperators
{
    // PRIVATE OPERATORS
    operators = [[NSMutableArray alloc] init];
    for(Operator *operatorItem in appDelegate.apiResponse.operators)
    {
        if(!operatorItem.isPublic)
        {
            [operators addObject:operatorItem];
        }
    }
    
    // PUBLIC OPERATORS
    for(Operator *operatorItem in appDelegate.apiResponse.operators)
    {
        if(operatorItem.isPublic)
        {
            [operators addObject:operatorItem];
        }
    }
}

-(void)checkModesAgainsOperators
{
    
}

-(NSMutableArray*)excludedOperators
{
    NSMutableArray *excludedOperators = [[NSMutableArray alloc] init];
    for(OperatorListItem *item in self.operatorListItems)
    {
        if(!item.uiSwitch.isOn)
        {
            [excludedOperators addObject:item.apiName];
        }
    }
    return excludedOperators;
}

-(void)addUIToView
{
//    [self addSubview:blackBackButton];
    [self addSubview:background];
    [self addSubview:scrollView];
}

-(void)animateIn
{
    [super animateIn];
    [parentView.view addSubview:self];
    self.center = centerOffScreen;
    self.alpha = 0;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOnScreen;
         self.alpha = 1;
     }
                     completion:nil];
}

-(void)updateUI
{
    [self XLog:@"Updating UI from Child Class"];
    if(appDelegate.apiResponse.operators.count == 0)
    {
        [self getOperators];
    }
}

-(void)animateOut
{
    [super animateOut];
    self.center = centerOnScreen;
    self.alpha = 1;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.center = centerOffScreen;
         self.alpha = 0;
     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                     }];
}

@end
