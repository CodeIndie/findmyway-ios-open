//
//  JourneyListener.h
//  Findmyway
//
//  Created by Bilo on 2/24/15.
//

// StreetLegal.h
#import <Foundation/Foundation.h>

@protocol JourneyListener <NSObject>

- (void)receiveMessage:(NSString*)message;
- (void)journeyUpdate:(int)leg journeyProgress:(float)journeyProgress;
- (void)pingInterval:(long)millisecondsBetweenCalls;

@end