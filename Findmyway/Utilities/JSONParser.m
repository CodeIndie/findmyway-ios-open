//
//  JSONParser.m
//  FmwV3
//
//  Created by Bilo Lwabona on 1/14/14.
//

#import "JSONParser.h"

@implementation JSONParser

+(BOOL)responseIsArray:(NSData*)data
{
    if([data isKindOfClass:[NSArray class]])
    {
        //NSLog(@"JSON is an ARRAY");
    }
    else
    {
        //NSLog(@"JSON is a DICTIONARY");
    }
    return [data isKindOfClass:[NSArray class]];
}

+(NSDictionary *)convertResponseToDictionary:(NSData *)responseData withError:(NSError **)error
{
    NSError *localError;
    NSDictionary *responseDictionary;
    responseDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&localError];
    
    if(localError != nil)
    {
        *error = localError;
        return nil;
    }
    return responseDictionary;
}

+(NSArray*)convertResponseToArray:(NSData*)responseData withError:(NSError **)error
{
    NSError *localError;
    NSArray *responseArray;
    responseArray = [NSJSONSerialization JSONObjectWithData:responseData options:NSJSONReadingMutableContainers error:&localError];
    
    if(localError != nil)
    {
        *error = localError;
        return nil;
    }
    
    if(!responseArray)
    {
        NSLog(@"Error Parsing JSON - Array empty");//: %@", error);
        return nil;
    }
    return responseArray;
}

+(NSString*)convertDictionaryToJSON:(NSDictionary*)dictionary
{
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"convertedJSON: %@", jsonString);
    return jsonString;
}

+(NSDictionary*)convertJSONToDictionary:(NSString *)jsonString
{
    NSError *error;
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];
    return jsonDictionary;
}
@end
