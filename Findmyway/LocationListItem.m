//
//  LocationItem.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/10.
//

#import "LocationListItem.h"

@implementation LocationListItem

-(id)initWithFrame:(CGRect)frame item:(LocationItem*)item
{
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    self = [super initWithFrame:frame];
    if(self)
    {
        float radius = appDelegate.layout.buttonSize * 0.7;
        circle = [[UILabel alloc] initWithFrame:CGRectMake(appDelegate.layout.margin + appDelegate.layout.buttonSize/2 - radius, appDelegate.layout.buttonSize/2 - radius, appDelegate.layout.buttonSize, appDelegate.layout.buttonSize)];
        [circle setBackgroundColor:appDelegate.colour.fmwActiveColor];
        CALayer *circleLayer = [circle layer];
        [circleLayer setCornerRadius:radius];
    }
    return self;
}
@end
