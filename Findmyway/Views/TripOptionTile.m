//
//  TripOptionTile.m
//  Findmyway
//
//  Created by Bilo on 12/9/14.
//

#import "TripOptionTile.h"

@implementation TripOptionTile

-(id)initWithDelegate:(AppDelegate*)appDelegate index:(int)index
{
    self = [super init];
    if(self)
    {
        
        LayoutConfig *layout = appDelegate.layout;
        ColourConfig *colour = appDelegate.colour;
        
        float tileHeight = layout.buttonSize * 2;
        self.button = [[UIButton alloc] initWithFrame:CGRectMake(0, index * (tileHeight + layout.padding), layout.screenWidth - 2*layout.padding, tileHeight)];
        [self.button setBackgroundColor:colour.fmwActiveColor];
        
        self.background = [[UIImageView alloc] initWithFrame:
           CGRectMake(layout.buttonSize*1.5,
                      2*layout.delimiterSize + index*(tileHeight + layout.padding),
                      layout.screenWidth - 2*layout.padding - 1.5*layout.buttonSize - 2*layout.delimiterSize,
                      tileHeight - 4 * layout.delimiterSize)];
        
        [self.background setBackgroundColor:colour.fmwActiveColor];
        
        self.letter = [[UIImageView alloc] initWithFrame:CGRectMake(layout.padding, index * (tileHeight + layout.padding) + layout.padding, layout.buttonSize - 2 * layout.padding, layout.buttonSize - 2 * layout.padding)];
        [self.letter setImage:[self imageForIndex:index]];
        
        [self addSubviews];
    }
    return self;
}

-(void)addSubviews
{
    [self addSubview:self.button];
    [self addSubview:self.background];
    [self addSubview:self.letter];
}

-(UIImage*)imageForIndex:(int)index
{
    NSString *imageName;
    switch(index)
    {
        case 0:
        {
            imageName = @"FMWSymbol-GreyA.png";
        }break;
            
        case 1:
        {
            imageName = @"FMWSymbol-GreyB.png";
        }break;
            
        case 2:
        {
            imageName = @"FMWSymbol-GreyC.png";
        }break;

        case 3:
        {
            imageName = @"FMWSymbol-GreyD.png";
        }break;
            
        case 4:
        {
            imageName = @"FMWSymbol-GreyE.png";
        }break;
            
        default:
        {
            imageName = @"FMWSymbol-Grey.png";
        }break;
    }
    return [UIImage imageNamed:imageName];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
