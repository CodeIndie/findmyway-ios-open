//
//  LocationItem.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/10.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "XUIToggleButton.h"
#import "LocationItem.h"
#import "AppDelegate.h"

@interface LocationListItem : UIView
{
    AppDelegate *appDelegate;
    UILabel *circle;
    UILabel *locationName;
    XUIToggleButton *toggleButton;
}

-(id)initWithFrame:(CGRect)frame item:(LocationItem*)item;
@end
