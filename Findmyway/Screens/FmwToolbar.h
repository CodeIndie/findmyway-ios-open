//
//  FmwToolbar.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "XUIView.h"
#import "XUIToolbarButton.h"
#import "AppViewController.h"

@interface FmwToolbar : XUIView
{
    XUIToolbarButton *tripsButton;
    XUIToolbarButton *myTripsButton;
    XUIToolbarButton *alertsButton;
    XUIToolbarButton *searchButton;
    XUIToolbarButton *moreButton;
}
-(void)initViewElements;

-(void)showFindMyWay;
-(void)showPlaces;
-(void)showAlerts;
-(void)showMyTrips;
-(void)showMainMenu;
@end
