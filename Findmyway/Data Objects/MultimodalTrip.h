//
//  MultimodalResult.h
//  tct
//
//  Created by Bilo on 9/10/14.
//

#import <Foundation/Foundation.h>
#import "MultimodalStage.h"
#import "PublicMapRoute.h"
#import "DistanceUtil.h"
#import "CompetitionUtil.h"

@interface MultimodalTrip : NSObject

@property NSString *tripId;
@property NSString *startTime;
@property NSString *endTime;
@property NSString *estimatedTotalCost;
@property NSMutableArray *stages;

@property NSString *firstStopName;
@property NSString *firstStopAddress;
@property Location *firstStopLocation;

@property NSString *lastStopName;
@property NSString *lastStopAddress;
@property Location *lastStopLocation;

@property NSMutableArray *fareMessages;
@property NSString *totalWalkingDistance;
@property NSString *initialWalkingDistance;

@property PublicMapRoute *mapRoute;
@property BOOL hasMapRoute;

@property BOOL isCompetitionTrip;
@property int carbonPoints;

-(id)initWithDictionary:(NSDictionary*)dictionary;
-(void)addMapRoute:(PublicMapRoute*)mapRoute;
-(BOOL)requiresTicketInfo;

@end
