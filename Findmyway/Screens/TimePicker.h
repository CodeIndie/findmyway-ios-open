//
//  TimePicker.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/08.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIView.h"
#import "AppViewController.h"
#import "DateTimeUtil.h"

@interface TimePicker : XUIView<UIPickerViewDelegate>
{
    UIImageView *pickerBackground;
    
    UIImageView *topbarBackground;
    UIImageView *topbarLine;
    UIImageView *bottomBarBackground;
    UIImageView *bottomBarLine;
    
    UIButton *dateTimeButton;
    UIButton *intervalButton;
    UIButton *confirmButton;
    UIButton *cancelButton;
    UIButton *timeToggleButton;
    
    UIPickerView *intervalPicker;
    UIDatePicker *dateTimePicker;
    
    CGPoint leftOffScreenCenter;
    CGPoint rightOffScreenCenter;
    CGPoint onScreenPickerCenter;
    
    NSMutableArray *timeOffsetList;
    NSInteger *selectedItemIndex;
    NSString *selectedTimeOffset;
    
    BOOL isUsingDatePicker;
}

-(void)initIntervalPickerView;
-(void)initDateTimePickerView;

-(void)toggleTimeType;

-(void)showInterval;
-(void)showDateTime;

-(void)confirm;
-(void)cancel;
@end
