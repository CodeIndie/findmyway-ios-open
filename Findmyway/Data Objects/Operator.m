//
//  Operator.m
//  Findmyway
//
//  Created by Bilo on 2/18/15.
//

#import "Operator.h"

@implementation Operator

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if(self)
    {
        self.name = [dictionary objectForKey:@"name"];
        self.displayName = [dictionary objectForKey:@"displayName"];
        self.category = [dictionary objectForKey:@"category"];
        
        self.twitterHandle = [dictionary objectForKey:@"twitterHandle"];
        self.websiteAddress = [dictionary objectForKey:@"websiteAddress"];
        
        self.routeMapUrl = [dictionary objectForKey:@"routeMapUrl"];
        self.contactEmail = [dictionary objectForKey:@"contactEmail"];
        self.contactNumber = [dictionary objectForKey:@"contactNumber"];
        self.isPublic = [[dictionary objectForKey:@"isPublic"] boolValue];
        
        self.modes = [[NSMutableArray alloc] init];
        NSArray *modesArray = [dictionary objectForKey:@"modes"];
        for(NSString *modeName in modesArray)
        {
            [self.modes addObject:modeName];
        }
    }
    
    return self;
}

-(id)initWithOperator:(Operator *)operator
{
    self = [super init];
    
    if(self)
    {
        self.name = operator.name;
        self.displayName = operator.displayName;
        self.category = operator.category;
        
        self.twitterHandle = operator.twitterHandle;
        self.websiteAddress = operator.websiteAddress;
        
        self.routeMapUrl = operator.routeMapUrl;
        self.contactEmail = operator.contactEmail;
        self.contactNumber = operator.contactEmail;
        self.isPublic = operator.isPublic;
        
        self.modes = [[NSMutableArray alloc] init];
        for(NSString *modeName in operator.modes)
        {
            [self.modes addObject:modeName];
        }
    }
    
    return self;
}

@end
