//
//  CompetitionView.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/09.
//

#import "CompetitionView.h"

@implementation CompetitionView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
    }
    return self;
}

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    float logoSize = layout.buttonSize * 2;
    float busSize = layout.buttonSize * 0.5;
    
    CALayer *viewLayer = [self layer];
    [viewLayer setBorderWidth:layout.delimiterSize];
    [viewLayer setBorderColor:colour.fmwTextColorLight.CGColor];
    
    logo = [[UIImageView alloc] initWithFrame:CGRectMake(layout.margin, self.frame.size.height - layout.buttonSize * 3, logoSize, logoSize)];
    bus = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width - busSize - layout.margin, self.frame.size.height - layout.buttonSize - busSize, busSize, busSize)];
    line = [[UIImageView alloc] initWithFrame:CGRectMake(0, self.frame.size.height - layout.delimiterSize, self.frame.size.width, layout.padding/2)];
    
    [logo setImage:[ImageUtil getImage:@"AreyengLogo.png"]];
    [logo setTintColor:colour.fmwActiveColorLight];
    [bus setImage:[ImageUtil getImage:@"AreyengBus.png"]];
    [bus setTintColor:colour.fmwActiveColorLight];
    
    scrollableView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    scrollableView.pagingEnabled = YES;
    
    [self initWelcomeView];
    
    [self initCountdownView];
    
    [self initTeamView];
}

-(void)initWelcomeView
{
    welcomeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, self.frame.size.height - layout.buttonSize)];
    [welcomeView setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.5]];
    
    closeButton = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - layout.buttonSize, 0, layout.buttonSize, layout.buttonSize)];
    [closeButton addTarget:self action:@selector(promptClosingWelcomeView) forControlEvents:UIControlEventTouchUpInside];
    
    title = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, layout.buttonSize * 1.5)];
    text = [[UILabel alloc] initWithFrame:CGRectMake(layout.margin, title.frame.size.height - layout.padding * 1.5, layout.marginWidth, self.frame.size.height - title.frame.size.height - layout.buttonSize - layout.margin)];
    acceptButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.frame.size.height - layout.buttonSize, layout.screenWidth/2 + layout.delimiterSize, layout.buttonSize)];
    [acceptButton addTarget:self action:@selector(showTeamAlertView) forControlEvents:UIControlEventTouchUpInside];
    infoButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth/2, self.frame.size.height - layout.buttonSize, layout.screenWidth/2, layout.buttonSize)];
    [infoButton addTarget:self action:@selector(showInfo) forControlEvents:UIControlEventTouchUpInside];
    
    wideInfoButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.frame.size.height - layout.buttonSize, layout.screenWidth, layout.buttonSize)];
    [wideInfoButton addTarget:self action:@selector(showInfo) forControlEvents:UIControlEventTouchUpInside];
    
    [title setText:NSLocalizedString(@"Earth Hour\nchallenge", nil)];
    [title setTextColor:colour.fmwActiveColor];
    [title setFont:[UIFont systemFontOfSize:48]];
    [title setTextAlignment:NSTextAlignmentCenter];
    [text setText:NSLocalizedString(@"AreyengCompetitionText", nil)];
    [text setTextColor:colour.fmwTextColorLight];
    [text setNumberOfLines:0];
    [text setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 2]];
    [text setTextAlignment:NSTextAlignmentCenter];
    
    [acceptButton setTitle:NSLocalizedString(@"Accept", nil) forState:UIControlStateNormal];
    [acceptButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [infoButton setTitle:NSLocalizedString(@"More Info", nil) forState:UIControlStateNormal];
    [infoButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [wideInfoButton setTitle:NSLocalizedString(@"More Info", nil) forState:UIControlStateNormal];
    [wideInfoButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    
    CALayer *acceptLayer = [acceptButton layer];
    [acceptLayer setBorderColor:colour.fmwBorderColor.CGColor];
    [acceptLayer setBorderWidth:layout.delimiterSize];
    CALayer *infoLayer = [infoButton layer];
    [infoLayer setBorderColor:colour.fmwBorderColor.CGColor];
    [infoLayer setBorderWidth:layout.delimiterSize];
    CALayer *wideInfoLayer = [wideInfoButton layer];
    [wideInfoLayer setBorderColor:colour.fmwBorderColor.CGColor];
    [wideInfoLayer setBorderWidth:layout.delimiterSize];
    
    float p = layout.padding;
    [closeButton setImage:[ImageUtil getImage:@"iconCross.png"] forState:UIControlStateNormal];
    [closeButton.imageView setTintColor:colour.fmwTextColorLight];
    [closeButton setImageEdgeInsets:UIEdgeInsetsMake(p, p, p, p)];
}

-(void)initCountdownView
{
    countdownView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.frame.size.width, self.frame.size.height)];
    float y = layout.buttonSize*0.1;
    
    float p = layout.padding;
    closeCountdown = [[UIButton alloc] initWithFrame:CGRectMake(self.frame.size.width - layout.buttonSize, 0, layout.buttonSize, layout.buttonSize)];
    [closeCountdown addTarget:self action:@selector(closeCountdownView) forControlEvents:UIControlEventTouchUpInside];
    [closeCountdown setImage:[ImageUtil getImage:@"iconCross.png"] forState:UIControlStateNormal];
    [closeCountdown.imageView setTintColor:colour.fmwTextColorLight];
    [closeCountdown setImageEdgeInsets:UIEdgeInsetsMake(p, p, p, p)];
    
    remainingDays = [[UILabel alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 2)];
    [remainingDays setTextAlignment:NSTextAlignmentCenter];
    [remainingDays setTextColor:colour.fmwActiveColor];
    [remainingDays setFont:[UIFont systemFontOfSize:64]];
    [countdownView addSubview:remainingDays];
    
    //[countdownView addSubview:closeCountdown];
    
    y+= layout.buttonSize * 1.66;
    remainingHours = [[UILabel alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 1.5)];
    [remainingHours setTextAlignment:NSTextAlignmentCenter];
    [remainingHours setTextColor:colour.fmwActiveColor];
    [remainingHours  setFont:[UIFont systemFontOfSize:48]];
    [countdownView addSubview:remainingHours];

    y+= layout.buttonSize * 1.3;
    remainingMinutes = [[UILabel alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 1)];
    [remainingMinutes setTextAlignment:NSTextAlignmentCenter];
    [remainingMinutes setTextColor:colour.fmwActiveColor];
    [remainingMinutes  setFont:[UIFont systemFontOfSize:32]];
    [countdownView addSubview:remainingMinutes];
    
    y += layout.buttonSize;
    remainingSeconds = [[UILabel alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 0.5)];
    [remainingSeconds setTextAlignment:NSTextAlignmentCenter];
    [remainingSeconds setTextColor:colour.fmwActiveColor];
    [remainingSeconds  setFont:[UIFont systemFontOfSize:24]];
    [countdownView addSubview:remainingSeconds];
    
    y += layout.buttonSize;
    countdownText = [[UILabel alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 0.5)];
    [countdownText setTextAlignment:NSTextAlignmentCenter];
    [countdownText setText:NSLocalizedString(@"until start of earth hour city challenge", nil)];
    [countdownText setTextColor:colour.fmwActiveColor];
    [countdownText setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [countdownView addSubview:countdownText];
    
    [self startCountdownTimer];
}

-(void)initTeamView
{
    teamView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.frame.size.width, self.frame.size.height)];
    
    float y = layout.buttonSize*0.1;
    //[[DefaultsUtil instance] SetString:@"Bilo" ForKey:@"TeamMember1"];
    //[[DefaultsUtil instance] SetString:@"Henk" ForKey:@"TeamMember2"];
    
    teamLabel = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding, 0, layout.paddedWidth, layout.buttonSize)];
    [teamLabel setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
    [teamLabel setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [teamLabel addTarget:self action:@selector(showTeamAlertView) forControlEvents:UIControlEventTouchUpInside];
    
    NSString *teamMember1 = [[DefaultsUtil instance] GetObjectForKey:@"TeamMember1"];
    NSString *teamMember2 = [[DefaultsUtil instance] GetObjectForKey:@"TeamMember2"];
    if(teamMember1.length == 0 && teamMember2.length == 0)
    {
        [teamLabel setTitle:@"Enter 2 team members" forState:UIControlStateNormal];
    }
    else
    {
        [teamLabel setTitle:[NSString stringWithFormat:@"%@: %@ & %@", NSLocalizedString(@"Team", nil), teamMember1, teamMember2]
                                 forState:UIControlStateNormal];
    }
    [teamView addSubview:teamLabel];
    
    y+= layout.buttonSize;
    
    loggedTrips = [[NSMutableArray alloc] init];
    logsScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, layout.buttonSize * 4)];
    
    allTripsButton = [[UIButton alloc] initWithFrame:CGRectMake(0, self.frame.size.height - layout.buttonSize, layout.screenWidth, layout.buttonSize)];
    [allTripsButton addTarget:self action:@selector(showAllLoggedTrips) forControlEvents:UIControlEventTouchUpInside];
    [allTripsButton setTitle:NSLocalizedString(@"Show All Trips", nil) forState:UIControlStateNormal];
    [allTripsButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    CALayer *allTripsLayer = [allTripsButton layer];
    [allTripsLayer setBorderColor:colour.fmwBorderColor.CGColor];
    [allTripsLayer setBorderWidth:layout.delimiterSize];

    [self updateScrollViewItems];
    
    [teamView addSubview:logsScrollView];
    [teamView addSubview:allTripsButton];
}

-(void)updateScrollViewItems
{
    for(TripLogListItem *view in logsScrollView.subviews)
    {
        [view removeFromSuperview];
    }
    loggedTrips = nil;
    
    
    NSArray *loggedTripItems = (NSArray*)[[DefaultsUtil instance] GetArrayForKey:@"LoggedTrips"];
    if(loggedTripItems.count > 0)
    {
        [self XLog:[NSString stringWithFormat:@"Logged Trips: %lu", (unsigned long)loggedTripItems.count]];
        loggedTrips = [[NSMutableArray alloc] init];
        
        for(int i = 0; i < loggedTripItems.count && i < 3; i ++)
        {
            TripLogListItem *listItem = [[TripLogListItem alloc] initWithFrame:CGRectMake(0, i * (layout.padding + layout.buttonSize * 1.2), self.frame.size.width, layout.buttonSize * 1.2) item:(TripLogItem*)[loggedTripItems objectAtIndex:i]];
            [loggedTrips addObject:listItem];
            [logsScrollView addSubview:listItem];
        }
    }
    
    logsScrollView.contentSize = CGSizeMake(layout.screenWidth, 3 * layout.buttonSize);
}

-(void)addUIToView
{
    [self addSubview:logo];
    [self addSubview:bus];
    
    [welcomeView addSubview:title];
    [self addSubview:acceptButton];
    [self addSubview:infoButton];
    [welcomeView addSubview:text];
    [welcomeView addSubview:closeButton];
    
    if([CompetitionUtil isCompeting])
    {
        if(![CompetitionUtil isEarthHourTime])
        {
            [acceptButton removeFromSuperview];
            [infoButton removeFromSuperview];
            [self addSubview:countdownView];
            [self addSubview:wideInfoButton];
        }
        else
        {
            [acceptButton removeFromSuperview];
            [infoButton removeFromSuperview];
            [self addSubview:teamView];
        }
    }
    else if(![CompetitionUtil hasExitedCompetition])
    {
        [self addSubview:welcomeView];
    }
    //[scrollableView addSubview:countdownView];
    //[scrollableView addSubview:teamView];

    
    //scrollableView.contentSize = CGSizeMake(layout.screenWidth * 3, self.frame.size.height);
    
    [self addSubview:line];
    //[self addSubview:scrollableView];
}

-(void)startCountdownTimer
{
    if(countdownTimer != (id)[NSNull null])
    {
        [countdownTimer invalidate];
        countdownTimer = nil;
    }
    countdownTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateCountdownTimer) userInfo:nil repeats:YES];
}

-(void)updateCountdownTimer
{
    if([DateTimeUtil totalSecondsUntilDate:appDelegate.earthHourStartDate] > 0)
    {
        NSString *days = [NSString stringWithFormat:@"%d %@", [DateTimeUtil daysUntilDate:appDelegate.earthHourStartDate], NSLocalizedString(@"days", nil)];
        NSString *hours = [NSString stringWithFormat:@"%d %@", [DateTimeUtil hoursUntilDate:appDelegate.earthHourStartDate], NSLocalizedString(@"hours", nil)];
        NSString *minutes = [NSString stringWithFormat:@"%d %@", [DateTimeUtil minutesUntilDate:appDelegate.earthHourStartDate], NSLocalizedString(@"min", nil)];
        NSString *seconds = [NSString stringWithFormat:@"%d", [DateTimeUtil secondsUntilDate:appDelegate.earthHourStartDate]];
        
        [remainingDays setText:days];
        [remainingHours setText:hours];
        [remainingMinutes setText:minutes];
        [remainingSeconds setText:seconds];
    }
    else
    {
        // IF ENTERED COMPETIITION: Show Team View
        [self showTeamView];
        // ELSE
        
    }
}

-(void)showInfo
{
    NSString *urlString = @"http://wwf.panda.org/what_we_do/footprint/cities/earth_hour_city_challenge/";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

-(void)showTeamAlertView
{
    NSLog(@"Showing Team Alert View");
    teamAlertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Enter Challenge", nil) message:NSLocalizedString(@"Enter your names below to enter the Earth Hour City Challenge in Tshwane", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:NSLocalizedString(@"Submit", nil), nil];
    [teamAlertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];

    [teamAlertView textFieldAtIndex:1].secureTextEntry = NO;
    [teamAlertView textFieldAtIndex:0].placeholder = NSLocalizedString(@"Team Member 1", nil);
    [teamAlertView textFieldAtIndex:1].placeholder = NSLocalizedString(@"Team Member 2", nil);
    
    NSString *teamMember1 = [[DefaultsUtil instance] GetStringForKey:@"TeamMember1"];
    NSString *teamMember2 = [[DefaultsUtil instance] GetStringForKey:@"TeamMember2"];
    
    if(teamMember1 != nil && ![teamMember1 isEqual:(id)[NSNull null]] && [teamMember1 length] > 0)
    {
        [[teamAlertView textFieldAtIndex:0] setText:teamMember1];
    }
    if(teamMember2 != nil && ![teamMember2 isEqual:(id)[NSNull null]] && [teamMember2 length] > 0)
    {
        [[teamAlertView textFieldAtIndex:1] setText:teamMember2];
    }
    
    [teamAlertView show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([alertView isEqual:teamAlertView])
    {
        if(buttonIndex == 0)
        {
            [teamAlertView dismissWithClickedButtonIndex:0 animated:YES];
        }
        else
        {
            [self submitTeamDetails];
            [self showCountdownView];
        }
    }
    
    if([alertView isEqual:closeCompetitionAlertView])
    {
        if(buttonIndex == 0)
        {
            [CompetitionUtil setExitedCompetition:YES];
            [closeCompetitionAlertView dismissWithClickedButtonIndex:0 animated:YES];
            [((AppViewController*)parentView).fmwScreen resetScrollView];
            [self closeWelcomeView];
        }
        else
        {
            [closeCompetitionAlertView dismissWithClickedButtonIndex:1 animated:YES];
            [self showTeamAlertView];
        }
    }
}

-(void)submitTeamDetails
{
    [CompetitionUtil setCompetitionMember1:[teamAlertView textFieldAtIndex:0].text member2:[teamAlertView textFieldAtIndex:1].text];
    [teamLabel setTitle:[NSString stringWithFormat:@"%@: %@ & %@",
                        NSLocalizedString(@"Team", nil),
                        [[DefaultsUtil instance] GetObjectForKey:@"TeamMember1"],
                        [[DefaultsUtil instance] GetObjectForKey:@"TeamMember2"]] forState:UIControlStateNormal];
    [teamLabel.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
}

-(void)showCountdownView
{
    for(UIView *view in self.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self addSubview:logo];
    [self addSubview:bus];

    [self addSubview:countdownView];
    [self addSubview:wideInfoButton];
}

-(void)showTeamView
{
    for(UIView *view in self.subviews)
    {
        [view removeFromSuperview];
    }
    [self addSubview:logo];
    [self addSubview:bus];
    
    [self addSubview:teamView];
}

-(void)promptClosingWelcomeView
{
    closeCompetitionAlertView = [[UIAlertView alloc] initWithTitle:@"Skip Competition" message:@"Are you sure you don't want to be in the competition?" delegate:self cancelButtonTitle:@"Skip" otherButtonTitles:@"Accept", nil];
    [closeCompetitionAlertView show];
}

-(void)closeWelcomeView
{
    [self removeFromSuperview];
}

-(void)closeCountdownView
{
    
}

-(void)showAllLoggedTrips
{
    AppViewController *viewController = ((AppViewController*)parentView);
    [viewController swapInView:viewController.competitionTripsView];
}
@end