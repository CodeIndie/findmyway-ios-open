//
//  MulticolorMapLine.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/01/30.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MulticolorMapLine : MKPolyline

@property UIColor *color;
@property float width;

@end
