//
//  XUIViewController.h
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XUIView.h"

@class XUIView;

@interface XUIViewController : UIViewController
{
    AppDelegate *appDelegate;
    LayoutConfig *layout;
    ColourConfig *colour;
    DeviceConfig *device;
    
    XUIView *oldView;
    XUIView *newView;
    XUIView *peekingView;
    
    NSMutableArray *screenStack;
}

-(id)init;
-(void)initialiseProperties;
-(void)initWithView:(XUIView*)xuiView;
-(void)showView:(XUIView*)xuiView;
-(void)hideView:(XUIView*)xuiView;
-(void)swapInView:(XUIView*)xuiView;
-(void)pushView:(XUIView*)xuiView;
-(void)popToView:(XUIView*)xuiView;
-(void)popView;

-(void)XLog:(NSString*)logString;

@end
