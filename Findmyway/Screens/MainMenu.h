//
//  MainMenu.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import <MessageUI/MessageUI.h>
#import "XUIView.h"
#import "MainMenuButton.h"
#import "AppViewController.h"

@interface MainMenu : XUIView<MFMailComposeViewControllerDelegate>
{
    //UIButton *blackBackButton;
    UIImageView *background;
    
    UIScrollView *scrollView;
    
    MainMenuButton *directions;
    
    MainMenuButton *favourites;
    MainMenuButton *announcements;
    MainMenuButton *transitMaps;
    MainMenuButton *places;
    MainMenuButton *commuterKarma;
    
    MainMenuButton *settings;
    MainMenuButton *contactUs;
    MainMenuButton *about;
}



@end
