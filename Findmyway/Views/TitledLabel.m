//
//  TitledLabel.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/12.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "TitledLabel.h"

@implementation TitledLabel

-(id)initWithDelegate:(AppDelegate*)appDelegate frame:(CGRect)frame title:(NSString*)title contentLabel:(NSString*)label alignment:(NSTextAlignment)alignment color:(UIColor *)color titleColor:(UIColor*)titleColor
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self setDelegate:appDelegate Title:title contentLabel:label alignment:alignment color:color titleColor:titleColor];
    }
    return self;
}

-(void)setDelegate:(AppDelegate*)appDelegate Title:(NSString*)title contentLabel:(NSString*)label alignment:(NSTextAlignment)alignment color:(UIColor*)color titleColor:(UIColor*)titleColor
{
    LayoutConfig *layout = appDelegate.layout;
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, layout.margin, self.bounds.size.width, layout.margin)];
    [titleLabel setText:title];
    [titleLabel setTextAlignment:alignment];
    [titleLabel setTextColor:titleColor];
    [titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] - 2]];
    [self addSubview:titleLabel];
    
    contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, layout.margin)];
    [contentLabel setText:label];
    [contentLabel setTextAlignment:alignment];
    [contentLabel setTextColor:color];
    [self addSubview:contentLabel];
}
@end
