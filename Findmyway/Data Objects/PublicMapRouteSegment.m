//
//  PublicMapRouteSegment.m
//  tct
//
//  Created by Bilo on 7/21/14.
//

#import "PublicMapRouteSegment.h"

@implementation PublicMapRouteSegment

-(id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    if(self)
    {
        self.hexColor = [dictionary objectForKey:@"colour"];
        self.color = [self colorWithHextString:self.hexColor];
        self.coordinates = [[NSMutableArray alloc] init];
        NSArray *coordinatesArray = [dictionary objectForKey:@"points"];
        for(int i = 0 ; i < [coordinatesArray count] ; i++)
        {
            Location *coordinate = [[Location alloc] initWithDictionary:[coordinatesArray objectAtIndex:i]];
            [self.coordinates addObject:coordinate];
        }
    }
    return self;
}

-(id)initWithMapRouteSegment:(PublicMapRouteSegment *)segment
{
    self = [super init];
    if(self)
    {
        self.hexColor = segment.hexColor;
        self.color = [self colorWithHextString:self.hexColor];
        self.coordinates = [[NSMutableArray alloc] init];

        for(int i = 0 ; i < segment.coordinates.count ; i++)
        {
            Location *coordinate = [[Location alloc] initWithLocation:(Location*)[segment.coordinates objectAtIndex:i]];
            [self.coordinates addObject:coordinate];
        }
    }
    return self;
}

-(UIColor*)colorWithHextString:(NSString*)hexString
{
    unsigned int hexInt = [self intFromHexString:hexString];

    UIColor *color =
    [UIColor colorWithRed:((CGFloat) ((hexInt & 0xFF0000) >> 16))/255
                    green:((CGFloat) ((hexInt & 0xFF00) >> 8))/255
                     blue:((CGFloat) (hexInt & 0xFF))/255
                    alpha:1];
    return color;
}

-(unsigned int)intFromHexString:(NSString *)hexString
{
    unsigned int hexInt = 0;
    
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    
    // Tell scanner to skip the # character
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"#"]];
    
    // Scan hex value
    [scanner scanHexInt:&hexInt];
    
    return hexInt;
}
@end
