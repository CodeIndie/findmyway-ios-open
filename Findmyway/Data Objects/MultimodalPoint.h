//
//  MultimodalPoint.h
//  tct
//
//  Created by Bilo on 9/10/14.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Location.h"

@interface MultimodalPoint : NSObject

@property NSString *name;
@property NSString *descriptionText;
@property NSString *time;
@property NSString *type;
@property NSString *platformNumber;
@property UIColor *colour;
@property Location *location;

-(id)initWithDictionary:(NSDictionary*)dictionary;

@end
