//
//  RecentsView.m
//  Findmyway
//
//  Created by Bilo on 4/20/15.
//

#import "RecentsCard.h"

@implementation RecentsCard

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self initRecentPlaces];
    [self addUIToView];
}

-(void)initViewElements
{
    [self setBackgroundColor:colour.clear];
    CALayer *layer = [self layer];
    [layer setBorderColor:colour.fmwBorderColor.CGColor];
    [layer setBorderWidth:layout.delimiterSize];
    
    itemHeight = layout.buttonSize;
    
    title = [XUILabel initWithFrame:CGRectMake(layout.padding, 0, layout.paddedWidth, layout.buttonSize)
                               text:NSLocalizedString(@"Recent Places", nil)
                          textColor:colour.fmwTextColor
                    backgroundColor:colour.clear
                      textAlignment:NSTextAlignmentCenter];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, [self width], [self height])];
    [self addRecentsUI];
    
    showAllButton = [XUIButton initWithFrame:CGRectMake(0, layout.buttonSize * 4, self.frame.size.width, layout.buttonSize)
                                       title:NSLocalizedString(@"SHOW ALL", nil)
                                  titleColor:colour.fmwActiveColor
                             backgroundColor:colour.clear
                         horizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [showAllButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [showAllButton addTarget:self action:@selector(showAllPlaces) forControlEvents:UIControlEventTouchUpInside];
    CALayer *buttonLayer = [showAllButton layer];
    [buttonLayer setBorderColor:colour.fmwBorderColor.CGColor];
    [buttonLayer setBorderWidth:layout.delimiterSize];
    
}

-(void)addUIToView
{
    [self addSubview:title];
    [self addSubview:scrollView];
    [self addSubview:showAllButton];
}

-(void)updateUI
{
    [super updateUI];
    [self initRecentPlaces];
}

-(void)initRecentPlaces
{
    [self XLog:@"InitRecentPlaces"];
    NSArray *recents = [PlacesUtil getLocations];// reverseObjectEnumerator] allObjects];
    recentPlaces = [[NSMutableArray alloc] init];
    
    if(recentPlaces.count > 0)
    {
        recentPlaces = nil;
        recentPlaces = [[NSMutableArray alloc] init];
    }
    
    for(SearchItem *item in recents)
    {
        if(recentPlaces.count >= 3) break;
        [recentPlaces addObject:item];
    }
    
    if(recents.count == 0)
    {
        if(explanationLabel != nil)
        {
            [explanationLabel removeFromSuperview];
        }
        
        explanationLabel = [XUILabel initWithFrame:CGRectMake(layout.padding, layout.buttonSize, layout.paddedWidth, layout.buttonSize * 2)
                                              text:NSLocalizedString(@"There are no recent places.\nAs you select places they will appear here.", nil)
                                          fontSize:[UIFont systemFontSize]
                                              bold:NO
                                         textColor:colour.fmwTextColorLight
                                   backgroundColor:colour.clear
                                     textAlignment:NSTextAlignmentCenter];
        [explanationLabel setNumberOfLines:0];
        //[self addSubview:explanationLabel];
    }

    [self addRecentsUI];
}

-(void)addRecentsUI
{
    for(UIView *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    int i = 0;
    for(SearchItem *item in recentPlaces)
    {
        SearchListItem *listItem = [[SearchListItem alloc] initWithFrame:CGRectMake(0, i * itemHeight, layout.screenWidth, itemHeight)];
        [listItem initView:appDelegate searchItem:item height:itemHeight];
        [listItem setItemTag:i];
        [listItem.button addTarget:self action:@selector(selectRecentItem:) forControlEvents:UIControlEventTouchUpInside];
        [listItem.horizontalLine removeFromSuperview];
        [listItem.distanceLabel removeFromSuperview];
        [listItem.mapButton addTarget:self action:@selector(showRecentOnMap:) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:listItem];
        i++;
    }
    scrollView.contentSize = CGSizeMake(layout.screenWidth, layout.buttonSize * recentPlaces.count);
}

-(IBAction)selectRecentItem:(id)sender
{
    NSInteger tag = ((UIButton*)sender).tag;
    SearchItem *item = ((SearchItem*)[recentPlaces objectAtIndex:tag]);
    [appDelegate.tripQuery setPoint:item.location];
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController.fmwScreen updateUI];
}

-(void)showAllPlaces
{
    appDelegate.tripQuery.isEditingStart = NO;
    AppViewController *viewController = (AppViewController*)parentView;
    appDelegate.searchConfig.state = HomeScreen;
    [viewController swapInView:viewController.searchScreen];
}

-(void)showRecentOnMap:(id)sender
{
    NSInteger tag = ((UIButton*)sender).tag;
    SearchItem *searchItem = (SearchItem*)[recentPlaces objectAtIndex:tag];
    AppViewController *viewController = ((AppViewController*)parentView);
    appDelegate.searchConfig.state = HomeScreen;
    [viewController.fmwMapView setState:PointPreviewFromHome];
    [viewController.fmwMapView setSearchItemForPreview:searchItem];
    [viewController swapInView:viewController.fmwMapView];
    [self XLog:[NSString stringWithFormat:@"Showing on Map [%ld]: %@", (long)tag, searchItem.location.address]];
}

@end
