//
//  RecentsView.h
//  Findmyway
//
//  Created by Bilo on 4/20/15.
//

#import "XUIView.h"
#import "PlacesUtil.h"
#import "SearchListItem.h"
#import "AppViewController.h"

@class AppViewController;

@interface RecentsCard : XUIView
{
    UILabel *title;
    UILabel *explanationLabel;
    UIButton *showAllButton;
    UIScrollView *scrollView;
    NSMutableArray *recentPlaces;
    float itemHeight;
}
@end
