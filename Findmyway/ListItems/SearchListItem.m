//
//  SearchItem.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/08.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "SearchListItem.h"

@implementation SearchListItem

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        
    }
    return self;
}

-(void)initView:(AppDelegate*)appDelegate searchItem:(SearchItem*)searchItem
{
    LayoutConfig *layout = appDelegate.layout;
    self.searchItem = [[SearchItem alloc] initWithSearchItem:searchItem];
    
    [self setBackgroundColor:appDelegate.colour.clear];
    
    self.button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, layout.paddedWidth - layout.buttonSize, layout.buttonSize)];
    [self.button setBackgroundColor:appDelegate.colour.clear];
    [self.button setTitle:searchItem.location.address forState:UIControlStateNormal];
    [self.button setTitleColor:appDelegate.colour.fmwTextColorLight forState:UIControlStateNormal];
    [self.button setTitleEdgeInsets:UIEdgeInsetsMake(0, layout.buttonSize + layout.padding, 0, 0)];
    self.button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.icon = [[UIImageView alloc] initWithFrame:CGRectMake(layout.padding, layout.padding, layout.buttonSize - 2*layout.padding, layout.buttonSize - 2*layout.padding)];
    [self.icon setImage:[ImageUtil getImage:@"iconPin.png"]];
    [self.icon setTintColor:appDelegate.colour.fmwTextColorLight];
    [self.button addSubview:self.icon];
    
    [self addSubview:self.button];
    
    self.mapButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.paddedWidth - layout.buttonSize, 0, layout.buttonSize, layout.buttonSize)];
    [self.mapButton setImage:[UIImage imageNamed:@"iconMapOutline.png"] forState:UIControlStateNormal];
    [self addSubview:self.mapButton];
    
    self.horizontalLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.buttonSize - layout.delimiterSize, layout.paddedWidth, layout.delimiterSize)];
    [self.horizontalLine setBackgroundColor:appDelegate.colour.fmwTextColorLight];
    [self addSubview:self.horizontalLine];
}

-(void)setItemTag:(int)index
{
    self.mapButton.tag = index;
    self.button.tag = index;
}

@end
