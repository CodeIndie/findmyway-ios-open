//
//  DefaultsUtil.m
//  tct
//
//  Created by Bilo on 5/27/14.
//

#import "DefaultsUtil.h"

@implementation DefaultsUtil

static DefaultsUtil *instance = nil;

-(id)init
{
    self = [super init];
    
    defaults = [NSUserDefaults standardUserDefaults];
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"UserData.sqlite"]];
    
    return self;
}

+(DefaultsUtil*)instance
{
    if(instance == nil)
    {
        instance = [[DefaultsUtil alloc] init];
    }
    return instance;
}

-(id)GetObjectForKey:(NSString*)key
{
    return ([defaults objectForKey:key] != (id)[NSNull null] ? [defaults objectForKey:key] : nil);
}

-(void)SetObject:(NSString*)value ForKey:(NSString*)key
{
    [defaults setObject:value forKey:key];
    [defaults synchronize];
}

-(void)SetData:(NSData*)data ForKey:(NSString*)key
{
    [defaults setObject:data forKey:key];
    [defaults synchronize];
}

-(id)GetDataForKey:(NSString*)key
{
    NSData *objectData = [defaults objectForKey:key];
    id object = [NSKeyedUnarchiver unarchiveObjectWithData:objectData];
    return object;
}

-(void)SetString:(NSString*)string ForKey:(NSString*)key
{
    //NSData *defaultsData = [NSKeyedArchiver archivedDataWithRootObject:string];
    
    [defaults setObject:string forKey:key];
    [defaults synchronize];
}

-(NSString*)GetStringForKey:(NSString*)key
{
    return [defaults objectForKey:key];
}

-(void)ResetToRegistrationDomain
{
    [defaults setPersistentDomain:[NSDictionary dictionary] forName:[[NSBundle mainBundle] bundleIdentifier]];
}

-(void)RemoveObjectForKey:(NSString*)key
{
    [defaults removeObjectForKey:key];
    [defaults synchronize];
}


-(void)SetArray:(NSArray *)array ForKey:(NSString *)key
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:array];
    [defaults setObject:data forKey:key];
    [defaults synchronize];
}

-(NSArray*)GetArrayForKey:(NSString *)key
{
    NSData *arrayData = [defaults objectForKey:key];
    NSArray *array = [NSKeyedUnarchiver unarchiveObjectWithData:arrayData];
    [defaults synchronize];
    return array;
}
@end
