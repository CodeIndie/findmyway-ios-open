//
//  StagePage.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/09.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MultimodalStage.h"
#import "Base64Image.h"
#import "TitledLabel.h"
#import "DateTimeUtil.h"
#import "XUIShape.h"

@interface StagePage : UIView
{
    NSUInteger index;
    NSUInteger count;
    MultimodalStage *stage;
    
    UIImageView *modeIcon;
    UILabel *operatorName;
    UILabel *busName;
    UILabel *routeName;
    
    TitledLabel *stageStart;
    TitledLabel *stageEnd;
    
    TitledLabel *nextStop;
    TitledLabel *eta;
    TitledLabel *stopsTillDestination;

}

-(void)setIndex:(NSUInteger)stageIndex stageCount:(NSUInteger)stageCount stage:(MultimodalStage*)multimodalStage appDelegate:(AppDelegate*)appDelegate;
@end
