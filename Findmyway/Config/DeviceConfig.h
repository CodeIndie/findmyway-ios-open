//
//  DeviceConfig.h
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@class AppDelegate;

@interface DeviceConfig : NSObject
{
    UIDevice *device;
    NSString *deviceModelName;
    NSString *deviceType;
}

@property NSString *deviceModel;
@property NSString *userAgent;
@property NSString *appDisplayName;
@property NSString *appVersionNumber;
@property NSString *majorVersion;
@property float screenScale;
@property float screenWidth;
@property float screenHeight;
@property float screenWidthPixels;
@property float screenHeightPixels;
@property float screenAspectRatio;
@property CGRect screenRect;

-(id)initWithDelegate:(AppDelegate*)appDelegate;
-(id)init:(AppDelegate*)appDelegate;

//-(id)initWithDelegate:(AppDelegate*)appDelegate;
//-(id)initWithDebugOutput:(AppDelegate*)appDelegate;
//-(id)initWithDebugOutput:(BOOL *)printDebugInfo
//                delegate:(AppDelegate *)appDelegate
//enableProximityMonitoring:(BOOL*)proximityMonitoringEnabled
// enableBatteryMonitoring:(BOOL *)batteryMonitoringEnabled;

-(NSString *)getDeviceModel:(NSString *)platform;

@end

