		//
//  XUIViewController.m
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIViewController.h"

@interface XUIViewController ()

@end

@implementation XUIViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initialiseProperties];
    // Do any additional setup after loading the view.
}

-(id)init;
{
    self = [super init];
    if(self)
    {
        appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        screenStack = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)initialiseProperties
{
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    layout = appDelegate.layout;
    colour = appDelegate.colour;
    device = appDelegate.device;
}

// TO BE USED FOR INITIAL CUSTOM VIEW
-(void)initWithView:(XUIView*)xuiView
{
    newView = xuiView;
    [newView animateIn];
    screenStack = [[NSMutableArray alloc] init];
    [screenStack addObject:xuiView];
    [self.view addSubview:xuiView];
}

// TO BE USED WHEN ADDING A VIEW, WITHOUT REMOVING THE CURRENT VIEWS
-(void)logScreenStack
{
    NSLog(@"ScreenStack Size: %lu", (unsigned long)screenStack.count);
    for(XUIView *view in screenStack)
    {
        NSLog(@"%@", view.viewName);
    }
}

-(void)showView:(XUIView*)xuiView
{
    [self XLog:[NSString stringWithFormat:@"showing  %@", [xuiView class]]];
    [xuiView animateIn];
}

-(void)hideView:(XUIView *)xuiView
{
    [self XLog:[NSString stringWithFormat:@"hiding %@", [xuiView class]]];
    [xuiView animateOut];
}

-(void)pushView:(XUIView*)xuiView
{
    //[self logScreenStack];
    [self swapInView:xuiView];
    if(![screenStack containsObject:oldView])
    {
        [screenStack addObject:oldView];
        [self XLog:[NSString stringWithFormat:@"Stack already contains: %@", [oldView class]]];
        [self logScreenStack];
        return;
    }
    [self XLog:[NSString stringWithFormat:@"Adding to stacks: %@", [oldView class]]];
    [self logScreenStack];
}

-(void)popView
{
    if([screenStack count] <= 0)
    {
        [self XLog:[NSString stringWithFormat:@"Popping uiview: %@", [screenStack lastObject]]];
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    else
    {
        oldView = newView;
        newView = screenStack.lastObject;
        
        [oldView animateOut];
        [newView animateIn];
        [screenStack removeLastObject];
    }
    [self logScreenStack];
}

-(void)popToView:(XUIView*)xuiView
{
    while([xuiView class] != [newView class])
    {
        [self popView];
        [self XLog:[NSString stringWithFormat:@"Popping View: %@", [newView class]]];
    }
}
// TO BE USED WHEN REPLACING A SCREEN
-(void)swapInView:(XUIView*)xuiView
{
    //[screenStack addObject:oldView];
    
    oldView = newView;
    newView = xuiView;
    
    [oldView animateOut];
    [newView animateIn];
}

-(void)XLog:(NSString *)logString
{
    NSString *className = [NSString stringWithFormat:@"%@",self.class];
    NSLog(@"[Class %@]: %@", className, logString);
}
@end
