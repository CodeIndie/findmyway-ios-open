//
//  SearchConfig.h
//  Findmyway
//
//  Created by Bilo on 5/4/15.
//

#import <Foundation/Foundation.h>
typedef enum
{
    HomeScreen = 0,
    TripsScreen = 1
}SearchState;

@interface SearchConfig : NSObject

@property SearchState state;

@end
