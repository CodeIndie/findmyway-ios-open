//
//  FmwScreen.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "FmwScreen.h"
#import "UIView+RoundedCorners.h"

@implementation FmwScreen

// UI
-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(-device.screenWidth/2, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth/2, frame.origin.y + frame.size.height/2);
        self.usingCurrentLocation = NO;
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    appDelegate.tripQuery.isDeparting = YES;
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
    
    // TESTING XUIEFFECT
    [self addUIEffect];
}

-(void)addUIEffect
{
    XUIEffect *pulseEffect;
    UIImageView *circleImage = [[UIImageView alloc] initWithFrame:CGRectMake(layout.margin + layout.padding, layout.buttonSize * 2 + layout.margin, layout.buttonSize - layout.margin, layout.buttonSize - layout.margin)];
    [circleImage setBackgroundColor:colour.fmwActiveColor];
    CALayer *circleLayer = [circleImage layer];
    [circleLayer setBorderWidth:(layout.buttonSize - layout.margin)/2];
    
    pulseEffect = [[XUIEffect alloc] initPulseWithDuration:0.8 pause:0.8 mainGraphic:circleImage effectGraphic:circleImage];
    pulseEffect.frame = CGRectMake(layout.screenWidth/2 - layout.buttonSize/2, layout.screenHeight/2 - layout.buttonSize/2, layout.buttonSize, layout.buttonSize);
    [pulseEffect setBackgroundColor:colour.fmwTextColorLight];
    
    //[scrollView addSubview:pulseEffect];
}

-(void)initViewElements
{
    float margin = layout.padding;
    float paddedWidth = layout.paddedWidth;
    y = layout.padding;
    //UIEdgeInsets insets = UIEdgeInsetsMake(layout.padding, layout.padding, layout.padding, layout.padding);

    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, layout.screenHeight - layout.buttonSize * 1.5)];

    self.modesPicker = [[FmwModePicker alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.buttonSize)];
    [self.modesPicker initView:@"" inController:parentView];
    
    float labelSize = layout.buttonSize * 1.2;
    float buttonSize = layout.buttonSize * 0.9;
    locationButton = [[UIButton alloc] initWithFrame:CGRectMake(margin + layout.buttonSize, y, layout.marginWidth - layout.buttonSize/2, buttonSize)];
    [locationButton setTitle:NSLocalizedString(@"My Location", nil) forState:UIControlStateNormal];
    [locationButton setTitleEdgeInsets:UIEdgeInsetsMake(0, labelSize + layout.padding, 0, 0)];
    [locationButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
    [locationButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    locationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [locationButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [locationButton addTarget:self action:@selector(searchLocation) forControlEvents:UIControlEventTouchUpInside];
    CALayer *location = [locationButton layer];
    [location setMasksToBounds:YES];
    [location setBorderWidth:layout.delimiterSize];
    [location setBorderColor:[colour.fmwTextColorLight CGColor]];
    [location setCornerRadius:layout.padding];
    [locationButton setBackgroundColor:[UIColor clearColor]];
    locationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, labelSize, buttonSize)];
    [locationLabel setTextColor:colour.fmwTextColorLight];
    [locationLabel setText:NSLocalizedString(@"Start:", nil)];
    [locationLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [locationLabel setTextAlignment:NSTextAlignmentRight];
    [locationButton addSubview:locationLabel];
    
    y += buttonSize + layout.padding;
    destinationButton = [[UIButton alloc] initWithFrame:CGRectMake(margin + layout.buttonSize, y, layout.marginWidth - layout.buttonSize/2, buttonSize)];
    [destinationButton setTitle:NSLocalizedString(@"Choose Destination", nil) forState:UIControlStateNormal];
    [destinationButton setTitleEdgeInsets:UIEdgeInsetsMake(0, labelSize + layout.padding, 0, 0)];
    [destinationButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize]]];
    [destinationButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
    destinationButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [destinationButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [destinationButton addTarget:self action:@selector(searchDestination) forControlEvents:UIControlEventTouchUpInside];
    CALayer *destination = [destinationButton layer];
    [destination setMasksToBounds:YES];
    [destination setBorderWidth:layout.delimiterSize];
    [destination setBorderColor:[colour.fmwTextColorLight CGColor]];
    [destination setCornerRadius:layout.padding];
    [destinationButton setBackgroundColor:[UIColor clearColor]];
    destinationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, labelSize, buttonSize)];
    [destinationLabel setTextColor:colour.fmwTextColorLight];
    [destinationLabel setText:NSLocalizedString(@"End:", nil)];
    [destinationLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [destinationLabel setTextAlignment:NSTextAlignmentRight];
    [destinationButton addSubview:destinationLabel];
    
    switchButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding, y - buttonSize * 0.5 - layout.padding / 2, layout.buttonSize, layout.buttonSize)];
    [switchButton setImage:[ImageUtil getImage:@"iconSwitch.png"] forState:UIControlStateNormal];
    [switchButton.imageView setTintColor:colour.fmwActiveColor];
    [switchButton addTarget:self action:@selector(swapRouteEndpoints) forControlEvents:UIControlEventTouchUpInside];
    //[switchButton setImageEdgeInsets:insets];
    
    y += buttonSize + layout.padding;
    NSArray *itemsArray = [NSArray arrayWithObjects:NSLocalizedString(@"Departing", nil), NSLocalizedString(@"Arriving", nil), nil];
    timeSegmentedControl = [[UISegmentedControl alloc] initWithItems:itemsArray];
    timeSegmentedControl.frame = CGRectMake(layout.padding, y, layout.paddedWidth, layout.buttonSize);
    timeSegmentedControl.selectedSegmentIndex = 0;
    [timeSegmentedControl addTarget:self action:@selector(segmentedControlAction:) forControlEvents:UIControlEventValueChanged];
    CALayer *timeSegmentLayer = [timeSegmentedControl layer];
    [timeSegmentLayer setCornerRadius:layout.padding];
    
    departArriveBackground = [[UIView alloc] initWithFrame:CGRectMake(margin + layout.buttonSize, y, layout.paddedWidth - layout.buttonSize * 2, buttonSize)];
    [departArriveBackground setBackgroundColor:colour.clear];
    CALayer *departArriveLayer = [departArriveBackground layer];
    [departArriveLayer setBorderColor:[colour.fmwActiveColor CGColor]];
    [departArriveLayer setBorderWidth:layout.delimiterSize];
    [departArriveLayer setBackgroundColor:colour.clear.CGColor];
    [departArriveLayer setCornerRadius:layout.padding];
    //[departArriveLayer insertSublayer:[XUIView gradientInView:departArriveBackground gradientStart:colour.gradientStart gradientEnd:colour.gradientEnd] above:departArriveLayer];
    
    timeDeparting = [[UIButton alloc] initWithFrame:CGRectMake(margin + layout.buttonSize, y,paddedWidth/2 - layout.buttonSize, buttonSize)];
    //[timeDeparting setBackgroundColor:colour.fmwActiveColor];
    [timeDeparting setTitleColor:colour.fmwLight forState:UIControlStateNormal];
    [timeDeparting setTitle:NSLocalizedString(@"Departing",nil) forState:UIControlStateNormal];
    [timeDeparting.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [timeDeparting addTarget:self action:@selector(setTimeForDeparting) forControlEvents:UIControlEventTouchUpInside];
    CALayer *departingLayer = [timeDeparting layer];
    [departingLayer setMasksToBounds:YES];
    [departingLayer setBorderWidth:layout.delimiterSize];
    [departingLayer setBorderColor:[colour.fmwActiveColor CGColor]];
    [timeDeparting setRoundedCorners:UIRectCornerTopLeft|UIRectCornerBottomLeft radius:layout.padding];
    
    timeArriving = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth/2, y,paddedWidth/2 - layout.buttonSize, buttonSize)];
    [timeArriving setBackgroundColor:colour.clear];
    [timeArriving setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [timeArriving setTitle:NSLocalizedString(@"Arriving", nil) forState:UIControlStateNormal];
    [timeArriving.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [timeArriving addTarget:self action:@selector(setTimeForArriving) forControlEvents:UIControlEventTouchUpInside];
    CALayer *arrivingLayer = [timeArriving layer];
    [arrivingLayer setMasksToBounds:YES];
    [arrivingLayer setBorderWidth:layout.delimiterSize];
    [arrivingLayer setBorderColor:[colour.fmwActiveColor CGColor]];
    [timeArriving setRoundedCorners:UIRectCornerTopRight|UIRectCornerBottomRight radius:layout.padding];
    
    [self setTimeForDeparting];
    
    y += buttonSize + layout.padding;
    clockButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth/2 - layout.buttonSize * 0.75, y, layout.buttonSize * 1.5, layout.buttonSize *1.5)];
    [clockButton setBackgroundColor:colour.clear];
    [clockButton setImage:[ImageUtil getImage:@"iconClockFull.png"] forState:UIControlStateNormal];
    [clockButton setImageEdgeInsets:UIEdgeInsetsMake(layout.padding, layout.padding, layout.padding, layout.padding)];
    [clockButton.imageView setTintColor:colour.fmwActiveColor];
    [clockButton addTarget:self action:@selector(showTimePicker) forControlEvents:UIControlEventTouchUpInside];
    
    cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth/2 + 2 *layout.buttonSize + layout.padding, y + layout.padding, layout.buttonSize, layout.buttonSize)];
    [cancelButton setImage:[ImageUtil getImage:@"iconCross.png"] forState:UIControlStateNormal];
    [cancelButton.imageView setTintColor:colour.fmwActiveColor];
    CALayer *cancelButtonLayer = [cancelButton layer];
    [cancelButtonLayer setCornerRadius:layout.buttonSize/2];
    [cancelButtonLayer setBorderColor:colour.fmwActiveColor.CGColor];
    [cancelButtonLayer setBorderWidth:layout.delimiterSize];
    [cancelButton setImageEdgeInsets:UIEdgeInsetsMake(layout.padding, layout.padding, layout.padding, layout.padding)];
    [cancelButton addTarget:self action:@selector(stopLoadingOptions) forControlEvents:UIControlEventTouchUpInside];
    
    y += buttonSize * 1.5;
    timeButton = [[UIButton alloc] initWithFrame:CGRectMake(margin, y, paddedWidth, layout.buttonSize)];
    [timeButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [timeButton setTitle:NSLocalizedString(@"Now", nil) forState:UIControlStateNormal];
    [timeButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    [timeButton addTarget:self action:@selector(showTimePicker) forControlEvents:UIControlEventTouchUpInside];
    
    y += layout.buttonSize;
    fmwButtonPosY = y;
    fmwButton = [[UIButton alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize)];
    [fmwButton setBackgroundColor:colour.fmwActiveColor];
    CALayer *fmwLayer = [fmwButton layer];
    [fmwLayer setMasksToBounds:YES];
    [fmwButton.layer insertSublayer:[XUIView gradientInView:fmwButton gradientStart:colour.gradientStart gradientEnd:colour.gradientEnd] atIndex:0];
    
    fmwButtonCover = [[UIImageView alloc] initWithFrame:CGRectMake(0, fmwButtonPosY, layout.screenWidth, layout.buttonSize)];
    [fmwButtonCover setBackgroundColor:colour.fmwBackgroundColor];
    [fmwButtonCover setUserInteractionEnabled:YES];
    
    //fmwButton.bounds = CGRectMake(0, y, layout.screenWidth, 0);
    [fmwButton setTitle:NSLocalizedString(@"findmyway", nil) forState:UIControlStateNormal];
    //[fmwButton.titleLabel setFont:[UIFont systemFontOfSize:[UIFont systemFontSize] + 1]];
    [fmwButton setTitleColor:colour.white forState:UIControlStateNormal];
    [fmwButton addTarget:self action:@selector(findMyWayAction) forControlEvents:UIControlEventTouchUpInside];
    //y += layout.buttonSize + layout.padding;
    
    y += layout.buttonSize;
    [self initCards];
//    if(![CompetitionUtil hasExitedCompetition])
//    {
//        self.competitionView = [[CompetitionView alloc] initWithFrame:CGRectMake(0, y, layout.screenWidth, layout.buttonSize * 8)];
//        [self.competitionView initView:@"CompetitionView" inController:parentView];
//    }
}

-(void)initCards
{
    recents = [[RecentsCard alloc] initWithFrame:CGRectMake(0, y, [self width], layout.buttonSize * 5)];
    [recents initView:@"Recents Card" inController:parentView];
    y += [recents height];
    
    transitMaps = [[TransitMapCard alloc] initWithFrame:CGRectMake(0, y, [self width], layout.buttonSize * 5)];
    [transitMaps initView:@"Transit Map Card" inController:parentView];
    y += [transitMaps height];
    
    announcements = [[AnnouncementsCard alloc] initWithFrame:CGRectMake(0, y, [self width], layout.buttonSize * 5)];
    [announcements initView:@"Announcements Card" inController:parentView];
    y += [announcements height];
}

-(void)addUIToView
{
    [self addSubview:self.modesPicker];
    //[scrollView addSubview:self.competitionView];
    [scrollView addSubview:background];
    [scrollView addSubview:locationButton];
    [scrollView addSubview:destinationButton];
    [scrollView addSubview:switchButton];
    [scrollView addSubview:departArriveBackground];
    [scrollView addSubview:timeDeparting];
    [scrollView addSubview:timeArriving];
    [scrollView addSubview:clockButton];
    [scrollView addSubview:timeButton];
    [scrollView addSubview:fmwButton];
    [scrollView addSubview:fmwButtonCover];
    
    [scrollView addSubview:recents];
    [scrollView addSubview:announcements];
    [scrollView addSubview:transitMaps];
    
    [self addSubview:scrollView];
    
//    if([CompetitionUtil isAfterEarthHour])
//    {
//        return;
//    }
    //scrollView.contentSize = CGSizeMake(layout.screenWidth, y + layout.buttonSize + self.competitionView.frame.size.height);
    [self resetScrollView];
}

-(void)animateIn
{
    [super animateIn];
    [self addNotificationListeners];
    if(![CompetitionUtil hasExitedCompetition] && [CompetitionUtil isEarthHourTime])
    {
        [self.competitionView updateScrollViewItems];
    }
    
    [parentView.view addSubview:self];
    [parentView.navigationItem setTitle:self.viewName];

    self.alpha = 0;
    
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         self.alpha = 1;
     }
                     completion:^(BOOL finished){
                         //[self updateUI];
                     }];
    
    [recents updateUI];
    [announcements updateUI];
}

-(void)animateOut
{
    [super animateOut];
    [self removeNotificationListeners];
    
    //self.center = centerOnScreen;
    self.alpha = 1;
    [UIView animateWithDuration:animationInterval delay:animationInterval/2 options:UIViewAnimationOptionCurveEaseInOut
                     animations:^
     {
         //self.center = centerOffScreen;
         self.alpha = 0;
     }
     completion:^(BOOL finished){
         //fmwButton.frame = CGRectMake(0, fmwButtonPosY, layout.screenWidth, 0);
         [self removeFromSuperview];
     }];
}

-(void)updateUI
{
    [super updateUI];
    
    if(self.usingCurrentLocation)
    {
        self.usingCurrentLocation = NO;
        [self getUserLocation];
    }
    
    [self resetScrollView];
    
    validEndPoints = NO;
    BOOL startIsValid = NO;
    BOOL endIsValid = NO;
    
    if(appDelegate.tripQuery.startLocation != nil && ![appDelegate.tripQuery.startLocation isEqual:(id)[NSNull null]] && appDelegate.tripQuery.startLocation.name.length > 0)
    {
        startIsValid = YES;
        NSString *startName = [NSString stringWithFormat:@"%@, %@", appDelegate.tripQuery.startLocation.name, appDelegate.tripQuery.startLocation.address];
        [locationButton setTitle:startName forState:UIControlStateNormal];
    }
    
    if(appDelegate.tripQuery.endLocation != nil && ![appDelegate.tripQuery.endLocation isEqual:(id)[NSNull null]] && appDelegate.tripQuery.endLocation.name.length > 0)
    {
        endIsValid = YES;
        NSString *endName = [NSString stringWithFormat:@"%@, %@", appDelegate.tripQuery.endLocation.name, appDelegate.tripQuery.endLocation.address];
        [destinationButton setTitle:endName forState:UIControlStateNormal];
    }
    
    if(appDelegate.tripQuery.startLocation != nil  &&  ![appDelegate.tripQuery.startLocation isEqual:(id)[NSNull null]]
       && appDelegate.tripQuery.endLocation != nil && ![appDelegate.tripQuery.endLocation isEqual:(id)[NSNull null]])
    {
        validEndPoints = YES;
    }
    else if([locationButton.titleLabel.text isEqualToString:NSLocalizedString(@"My Location", nil)])
    {
        if(endIsValid)
        {
            validEndPoints = YES;
        }
    }
    else if([destinationButton.titleLabel.text isEqualToString:NSLocalizedString(@"My Location", nil)])
    {
        if(startIsValid)
        {
            validEndPoints = YES;
        }
    }
    
    if(appDelegate.tripQuery.isDeparting)
    {
        [self setTimeForDeparting];
    }
    else
    {
        [self setTimeForArriving];
    }
    
    [self updateTimeDisplay];
    
    [self updateFmwButton];
}

-(void)updateTimeDisplay
{
    //    DateTimeObject *dateTime = [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime];
    if(appDelegate.tripQuery.dateTime != nil)
    {
        if([DateTimeUtil minutesBetweenCurrentTimeAnd:appDelegate.tripQuery.dateTime] > 24 * 60)
        {
            [timeButton setTitle:[NSString stringWithFormat:@"%@ %@ %@ %@",
                                  NSLocalizedString(@"at", nil),
                                  [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime].timeString,
                                  NSLocalizedString(@"on", nil),
                                  [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime].dateString] forState:UIControlStateNormal];
        }
        else
        {
            if(appDelegate.tripQuery.isDeparting)
            {
                if([DateTimeUtil minutesBetweenCurrentTimeAnd:appDelegate.tripQuery.dateTime] > 1)
                {
                    [timeButton setTitle:[NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"at", nil), [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime].timeString, nil] forState:UIControlStateNormal];
                }
                else
                {
                    [timeButton setTitle:NSLocalizedString(@"now", nil) forState:UIControlStateNormal];
                }
            }
            else
            {
                [timeButton setTitle:([DateTimeUtil minutesBetweenCurrentTimeAnd:appDelegate.tripQuery.dateTime] > 1 ?
                                      [NSString stringWithFormat:@"%@ %@", NSLocalizedString(@"at ", nil), [DateTimeUtil parseDateTime:appDelegate.tripQuery.dateTime].timeString, nil]
                                      :
                                      NSLocalizedString(@"now", nil)) forState:UIControlStateNormal];
            }
        }
    }
    else
    {
        [timeButton setTitle:[NSString stringWithFormat:@"%@",
                              //(appDelegate.tripQuery.isDeparting ? NSLocalizedString(@"in", nil) : NSLocalizedString(@"at", nil)),
                              NSLocalizedString(@"now", nil)] forState:UIControlStateNormal];
    }
}

-(void)updateFmwButton
{
    
    //CALayer *layer = [fmwButton layer];
    //[layer setMasksToBounds:YES];
    
    if(validEndPoints)
    {
        //[self addSubview:fmwButtonCover];
        [UIView animateWithDuration:0.5 animations:^
         {
             fmwButtonCover.frame = CGRectMake(0, fmwButtonPosY + layout.buttonSize, layout.screenWidth, 0);
         }
                         completion:^(BOOL finished)
         {
             //[fmwButtonCover removeFromSuperview];
             //[scrollView addSubview:fmwButton];
         }];
    }
    else
    {
        [UIView animateWithDuration:0.5 animations:^
         {
             fmwButtonCover.frame = CGRectMake(0, fmwButtonPosY, layout.screenWidth, layout.buttonSize);
         }
                         completion:^(BOOL finished)
         {
             //[fmwButton removeFromSuperview];
             
         }];
    }
}

-(void)swapRouteEndpoints
{
    if(appDelegate.tripQuery.startLocation == nil || appDelegate.tripQuery.endLocation == nil)
    {
        return;
    }
    [appDelegate.tripQuery swapEndPoints];
    [self updateUI];
}

-(void)addNotificationListeners
{
    [super addNotificationListeners];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showRouteOptions) name:[[Notifications instance] GETPathsSuccess] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoadingOptions) name:[[Notifications instance] GETPathsFail] object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setUserLocation) name:[[Notifications instance] GETAddressSuccess] object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoadingOptions) name:[[Notifications instance] GETAddressFail] object:nil];
}

-(void)removeNotificationListeners
{
    [super removeNotificationListeners];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[[Notifications instance] GETPathsSuccess] object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[[Notifications instance] GETPathsFail] object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[[Notifications instance] GETAddressSuccess] object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[[Notifications instance] GETAddressFail] object:nil];
}

// TRIP QUERY CONSTRUCTION

-(void)getUserLocation
{
    self.usingCurrentLocation = NO;
    if(![appDelegate.locationUtil isLocationAvailable])
    {
        [self showAlertView:@"Location Services" message:@"To use your location, enable location services for Findmyway in the device settings.\n\nOtherwise search for a location." button:@"Ok"];
        return;
    }
    
    // IF ADDRESS IS AVAILABLE, LOOK IT UP
    if(![appDelegate.locationUtil.userLocation.address isEqual:(id)[NSNull null]] &&
        appDelegate.locationUtil.userLocation.address != nil)
    {
        appDelegate.apiResponse.location = appDelegate.locationUtil.userLocation;
        [self setUserLocation];
        return;
    }
        
    // OTHERWISE GET ADDRESS FROM API
    [locationButton setTitle:@"Getting Location ..." forState:UIControlStateNormal];
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%@", appDelegate.locationUtil.userLocation.latitudeString] forKey:@"latitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@", appDelegate.locationUtil.userLocation.longitudeString] forKey:@"longitude"];
    
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:ReverseGeocode callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
     {
         NSLog(@"API - ReverseGeocode: %ld", (long)response.statusCode);
         [self handleApiError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode];
         [appDelegate.apiResponse addAddress:[JSONParser convertJSONToDictionary:bodyString]];
         [self setUserLocation];
         if(gettingRouteOptions)
         {
             [self showRouteOptions];
         }
     }
     failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
     {
         NSLog(@"API CONNECTION FAILURE - ReverseGeocode: %@", error);
         [self stopLoader];
         [self stopLoadingOptions];
         [self handleError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode error:error];
     }
     didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
     {
         // This is not going to be called in this example but it's included for completeness.
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
     }];
}

-(void)setUserLocation
{
    [locationButton setTitle:@"Current Location" forState:UIControlStateNormal];
    appDelegate.tripQuery.startLocation = appDelegate.apiResponse.location;
    [self updateUI];
}

-(void)cancelReverseGeocode
{
    [locationButton setTitle:@"Choose Starting Location" forState:UIControlStateNormal];
}

-(void)useCurrentLocation
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.locationUtil.userLocation.latitudeString] forKey:@"latitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.locationUtil.userLocation.longitudeString] forKey:@"longitude"];
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:ReverseGeocode callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
     {
        NSLog(@"API - ReverseGeocode: %ld", (long)response.statusCode);
        [self handleApiError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode];
        [appDelegate.apiResponse addAddress:[JSONParser convertJSONToDictionary:bodyString]];
        [self setUserLocation];
     }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
     {
         NSLog(@"API CONNECTION FAILURE - ReverseGeocode: %@", error);
         [self stopLoader];
         [self stopLoadingOptions];
         [self handleError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode error:error];
     }
                            didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
     {
         // This is not going to be called in this example but it's included for completeness.
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
     }];
}

-(void)searchLocation
{
    appDelegate.tripQuery.isEditingStart = YES;
    [self showSearchScreen];
}

-(void)searchDestination
{
    appDelegate.tripQuery.isEditingStart = NO;
    [self showSearchScreen];
}

-(void)showSearchScreen
{
    appDelegate.searchConfig.state = HomeScreen;
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).searchScreen];
}

-(void)segmentedControlAction:(UISegmentedControl*)segment
{
    if(segment.selectedSegmentIndex == 0)
    {
        [self setTimeForDeparting];
    }
    else
    {
        [self setTimeForArriving];
    }
}

-(void)setTimeForDeparting
{
    appDelegate.tripQuery.isDeparting = YES;
    [UIView animateWithDuration:0.5
    animations:^
    {
        [timeDeparting setBackgroundColor:colour.fmwActiveColor];
        [timeDeparting setTitleColor:colour.fmwLight forState:UIControlStateNormal];
//        [timeDeparting.layer insertSublayer:[XUIView gradientInView:timeDeparting gradientStart:colour.fmwGradientStart gradientEnd:colour.fmwGradientEnd] atIndex:1];
        
        [timeArriving setBackgroundColor:colour.clear];
        [timeArriving setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
//        if(timeArriving.layer.sublayers.count > 0)
//        {
//            [[timeArriving.layer.sublayers objectAtIndex:1] removeFromSuperlayer];
//        }
        
    } completion:nil];
}

-(void)setTimeForArriving
{
    appDelegate.tripQuery.isDeparting = NO;
    [UIView animateWithDuration:0.5
    animations:^
    {
        [timeArriving setBackgroundColor:colour.fmwActiveColor];
        [timeArriving setTitleColor:colour.fmwLight forState:UIControlStateNormal];
//        [timeArriving.layer insertSublayer:[XUIView gradientInView:timeArriving gradientStart:colour.fmwGradientStart gradientEnd:colour.fmwGradientEnd] atIndex:1];
        
        [timeDeparting setBackgroundColor:colour.clear];
        [timeDeparting setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
//        if(timeDeparting.layer.sublayers.count > 0)
//        {
//            [[timeDeparting.layer.sublayers objectAtIndex:0] removeFromSuperlayer];
//        }
    }
    completion:nil];
}

-(void)setTime
{
    
}

-(void)showTimePicker
{
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).timePicker];
}

// SIGNAL R

-(void)initSignalR
{
//    // HUBS
//    
//    // Connect to the service
//    SRHubConnection *hubConnection = [SRHubConnection connectionWithURL:@"http://routethink.cloudapp.net"];
//    NSLog(@"%@", hubConnection.url);
//    
//    // Create a proxy to the chat service
//    chat = [hubConnection createHubProxy:@"MobileTestHub"];
//    
//    [chat on:@"ReceiveMessage" perform:self selector:@selector(receiveMessage:)];
//    [chat on:@"JourneyUpdate" perform:self selector:@selector(journeyUpdate:progressOfLeg:)];
//    [chat on:@"PingInterval" perform:self selector:@selector(pingInterval:)];
//    
//    // Start the connection
//    hubConnection.started = ^{
//        NSLog(@"iOS has Started connection to SignalR");
//        [hubConnection send:@"Hello from iOS"];
//    };
//    
//    [hubConnection start];
//    signalRTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(callSignalRTestMessage) userInfo:nil repeats:NO];
}

-(void)callSignalRTestMessage
{
    NSLog(@"Calling Message Function");
    NSArray *argArray = [NSArray arrayWithObjects:@"First iOS to SignalR Test Message", nil];
    [chat invoke:@"message" withArgs:argArray completionHandler:^(id response){
        NSLog(@"HubResult %@", (NSString*)response);
    }];
}

- (void)receiveMessage:(NSString *)message
{
    // Print the message when it comes in
    NSLog(@"%@", message);
}

-(void)journeyUpdate:(int)leg progressOfLeg:(float)progressOfLeg
{
    NSLog(@"[Leg %d] Progress of Leg: %f", leg, progressOfLeg);
}

-(void)pingInterval:(long)milliSecondsBetweenPolls
{
    NSLog(@"%ld", milliSecondsBetweenPolls);
}

// RESULTS QUERY

-(void)stopLoadingOptions
{
    [self stopLoader];
    [cancelButton removeFromSuperview];
}

-(void)findMyWayAction
{
    //[self initSignalR];
    
    gettingRouteOptions = YES;
    if(appDelegate.tripQuery.startLocation == nil)
    {
        [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(reactivateFmwButton) userInfo:nil repeats:NO];
        [self getUserLocation];
    }
    else if(appDelegate.tripQuery.endLocation != nil)
    {
        [self showRouteOptions];
    }
    else
    {
        NSLog(@"Start or End not set for journey");
    }
}

-(void)pauseFmwButton
{
}

-(void)reactivateFmwButton
{
    
}

-(void)getRouteOptions
{
    gettingRouteOptions = NO;
    if(tripCards != nil)
    {
        for(RouteOptionCard *tripCard in tripCards)
        {
            [tripCard removeFromSuperview];
        }
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.startLocation.latitudeString] forKey:@"startLatitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.startLocation.longitudeString] forKey:@"startLongitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.endLocation.latitudeString] forKey:@"endLatitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",appDelegate.tripQuery.endLocation.longitudeString] forKey:@"endLongitude"];
    
    if(appDelegate.tripQuery.dateTime != nil)
    {
        if(appDelegate.tripQuery.isDeparting)
        {
            [parameters setValue:appDelegate.tripQuery.dateTime forKey:@"startDate"];
        }
        else
        {
            [parameters setValue:appDelegate.tripQuery.dateTime forKey:@"endDate"];
        }
    }
    else
    {
        [parameters setValue:[DateTimeUtil currentDateTimeOffsetByMinutes:@"2"] forKey:@"startDate"];
    }
    
    [parameters setValue:[self.modesPicker excludedModes] forKey:@"excludedModes"];
    [parameters setValue:[self.modesPicker excludedOperators] forKey:@"excludedOperators"];
    
    //[scrollView addSubview:cancelButton];
    //[self startLoader:CGPointMake(layout.screenWidth/2 - 3 * layout.buttonSize - layout.buttonSize/2, layout.buttonSize * 4 + 2 * layout.padding) inView:scrollView];
    
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:Paths callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
     {
         NSLog(@"API - Paths: %ld", (long)response.statusCode);
         [self handleApiError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode];
         [appDelegate.apiResponse addPaths:[JSONParser convertJSONToDictionary:bodyString]];
          [self showRouteOptions];
     }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
    {
        NSLog(@"API CONNECTION FAILURE - Paths: %@", error);
        [self stopLoader];
        [self stopLoadingOptions];
        [self handleError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode error:error];
    }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
    {
        // This is not going to be called in this example but it's included for completeness.
        NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
    }];
}

-(IBAction)highlightButton:(id)sender
{
    UIButton *button = (UIButton*)sender;
    [button setBackgroundColor:colour.fmwActiveColor];
    [button setTitleColor:colour.white forState:UIControlStateNormal];
}

-(void)showRouteOptions
{
    //[self stopLoader];
    //[cancelButton removeFromSuperview];
    
    appDelegate.tripQuery.refreshingRouteOptions = YES;
    //[((AppViewController*)parentView).fmwRouteOptions initView:@"Trip Options" inController:parentView];
    [((AppViewController*)parentView) swapInView:((AppViewController*)parentView).fmwRouteOptions];
    
    //[self createTripCards];
}

-(void)resetScrollView
{
    scrollView.contentSize = CGSizeMake(layout.screenWidth, fmwButtonPosY + layout.buttonSize +
                                        [recents height] +
                                        [announcements height] +
                                        [transitMaps height]
                                        );
}
@end
