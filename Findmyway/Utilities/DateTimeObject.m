//
//  DateTimeObject.m
//  tct
//
//  Created by Bilo on 4/22/14.
//

#import "DateTimeObject.h"

@implementation DateTimeObject

-(id)init
{
    self = [super init];
    if(self != nil)
    {
        
    }
    return self;
}

-(id)initWithDateTime:(NSString*)dateTime
{
    self = [super init];
    if(self != nil)
    {
        self.time = [self substring:dateTime from:@"T" length:5];
        self.hours = [self substring:self.time upTo:@":"];
        self.minutes = [self substring:self.time from:@":" length:2];
        
        self.date = [self substring:dateTime upTo:@"T"];
        self.dateYear = [self substring:self.date upTo:@"-"];
        NSString *tempString = [self substring:self.date from:@"-"length:5];
        self.dateMonth = [self substring:tempString upTo:@"-"];
        
        tempString = [self substring:tempString from:@"-" length:2];
        self.dateDay = tempString;
        
        self.dateTimeString = [self formattedString];
    }
    return self;
}

-(id)initWithTime:(NSString*)time
{
    self = [super init];
    if(self != nil)
    {
        self.time = time;
    }
    return self;
}

-(NSString*)substring:(NSString*)sampleString from:(NSString*)start length:(NSUInteger)length
{
    NSRange rangeStart = [sampleString rangeOfString:start];
    NSString *subString = nil;
    
    if(rangeStart.length == 1)
    {
        NSRange subRange;
        subRange.location = rangeStart.location + 1;
        if(length > 0)
        {
            subRange.length = length;
            subString = [sampleString substringWithRange:subRange];
        }
    }
    return subString;
}

-(NSString*)substring:(NSString*)sampleString upTo:(NSString*)end
{
    NSRange rangeEnd = [sampleString rangeOfString:end];
    NSString *subString = nil;
    
    if(rangeEnd.length == 1)
    {
        NSRange subRange;
        subRange.location = 0;
        subRange.length = rangeEnd.location;
        subString = [sampleString substringWithRange:subRange];
    }
    return subString;
}

-(NSString*)formattedString
{
    NSMutableString *returnString = [[NSMutableString alloc] initWithString:@""];
    
    self.timeString = [NSString stringWithFormat:@"%@:%@", self.hours, self.minutes];
    [returnString appendString:self.timeString];
    [returnString appendString:@" "];

    self.dateString = [NSString stringWithFormat:@"%@/%@/%@", self.dateDay, self.dateMonth, self.dateYear];
    [returnString appendString:self.dateString];
    
    return returnString;
}

-(void)print
{
    NSLog(@"%@", [self toString]);
}

-(NSString*)toString
{
    return [NSString stringWithFormat:@"Time: %@, Date: %@, FullString: %@", self.timeString, self.dateString, self.formattedString];
}
@end
