//
//  ApiResponse.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/07.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "SearchItem.h"
#import "Operator.h"
#import "MultimodalTrip.h"
#import "Announcement.h"

@class AppDelegate;

@interface ApiResponse : NSObject
{
    AppDelegate *appDelegate;
}

@property NSMutableArray *announcements;
@property NSMutableArray *operators;
@property NSMutableArray *trips;
@property NSMutableArray *searchItems;
@property Location *location;
@property NSString *fmwToken;

-(void)addTrip:(NSDictionary*)dictionary toIndex:(int)index;
-(void)addPaths:(NSDictionary*)dictionary;
-(void)addAddress:(NSDictionary*)dictionary;
-(void)addToken:(NSDictionary*)dictionary;
-(void)addOperators:(NSDictionary*)dictionary;
-(void)addSearchItems:(NSDictionary*)dictionary;
-(void)addAnnouncements:(NSDictionary*)dictionary;

@end
