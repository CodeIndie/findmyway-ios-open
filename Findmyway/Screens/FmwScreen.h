//
//  FmwScreen.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "XUIView.h"
#import "AppViewController.h"
#import "FmwModePicker.h"
#import "CompetitionView.h"
#import "DateTimeUtil.h"
#import "RouteOptionCard.h"
#import "SignalR.h"
#import "AnnouncementsCard.h"
#import "RecentsCard.h"
#import "TransitMapCard.h"

@class FmwModePicker, CompetitionView, AnnouncementsCard, TransitMapCard, RecentsCard;

@interface FmwScreen : XUIView
{
    UIScrollView *scrollView;
    
    // FMW Card
    UIImageView *background;
    UISegmentedControl *timeToggleSG;
    
    UIButton *switchButton;
    UIButton *locationButton;
    UIButton *destinationButton;
    UILabel *locationLabel;
    UILabel *destinationLabel;

    UIView *departArriveBackground;
    UIButton *timeDeparting;
    UIButton *timeArriving;

    UIButton *timeButton;
    UIButton *clockButton;
    UIButton *fmwButton;
    UIImageView *fmwButtonCover;
    UIButton *cancelButton;
    
    UISegmentedControl *timeSegmentedControl;
    BOOL validEndPoints;
    BOOL gettingRouteOptions;
    float y;
    float fmwButtonPosY;
    NSMutableArray *tripCards;
    
    RecentsCard *recents;
    AnnouncementsCard *announcements;
    TransitMapCard *transitMaps;
    
    NSTimer *signalRTimer;
    SRHubProxy *chat;
}

@property FmwModePicker *modesPicker;
@property CompetitionView *competitionView;
@property BOOL usingCurrentLocation;

-(void)showTimePicker;
-(void)setTimeForDeparting;
-(void)setTimeForArriving;
-(void)setTime;

-(void)updateUI;
-(void)setUserLocation;
-(void)cancelReverseGeocode;

-(void)searchLocation;
-(void)searchDestination;
-(void)showSearchScreen;
-(void)swapRouteEndpoints;

-(IBAction)highlightButton:(id)sender;
-(void)findMyWayAction;
-(void)getRouteOptions;
-(void)showRouteOptions;
-(void)stopLoadingOptions;
-(void)resetScrollView;

@end
