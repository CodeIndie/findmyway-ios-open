//
//  ValidationUtil.h
//  Findmyway
//
//  Created by Bilo on 3/20/15.
//

#import <Foundation/Foundation.h>

@interface ValidationUtil : NSObject

+(BOOL)isValidName:(NSString*)name;

@end
