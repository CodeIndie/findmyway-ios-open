//
//  PublicMapRoute.h
//  tct
//
//  Created by Bilo on 7/21/14.
//

#import <Foundation/Foundation.h>
#import "Location.h"
#import "PublicMapRouteSegment.h"

@interface PublicMapRoute : NSObject

@property Location *boundingBoxTopLeft;
@property Location *boundingBoxBottomRight;
@property NSMutableArray *segments;
@property NSMutableArray *coordinates;

-(id)initWithDictionary:(NSDictionary*)dictionary;
-(id)initWithMapRoute:(PublicMapRoute*)mapRoute;
-(void)consolidateCoordinates;
@end
