//
//  XUIButton.h
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/01/30.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LayoutConfig.h"
@interface XUIButton : UIButton
{
    UIColor *onFrontCol;
    UIColor *offFrontCol;
    UIColor *onBackCol;
    UIColor *offBackCol;
}

-(id)initWithFrame:(CGRect)frame setColorsOnFront:(UIColor*)onFront onBack:(UIColor*)onBack
          offFront:(UIColor*)offFront offBack:(UIColor*)offBack;

-(void)setColorsOnFront:(UIColor*)onFront onBack:(UIColor*)onBack
            offFront:(UIColor*)offFront offBack:(UIColor*)offBack;

-(void)setPressed;
-(void)setReleased;
-(void)setTitle:(NSString*)title alignment:(UIControlContentHorizontalAlignment)alignment;

+(XUIButton*)initWithFrame:(CGRect)frame title:(NSString*)title titleColor:(UIColor*)titleColor backgroundColor:(UIColor*)backgroundColor horizontalAlignment:(UIControlContentHorizontalAlignment)horizontalAlignment;
+(XUIButton*)initWithFrame:(CGRect)frame title:(NSString *)title fontSize:(float)fontSize bold:(BOOL)bold titleColor:(UIColor *)titleColor backgroundColor:(UIColor *)backgroundColor horizontalAlignment:(UIControlContentHorizontalAlignment)horizontalAlignment;
+(XUIButton*)backButtonWithTitle:(NSString*)title color:(UIColor*)color layout:(LayoutConfig*)layout;
@end
