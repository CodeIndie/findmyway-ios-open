//
//  DateTimeUtilitiy.h
//  tct
//
//  Created by Bilo on 4/22/14.
//

#import <Foundation/Foundation.h>
#import "DateTimeObject.h"

@interface DateTimeUtil : NSObject

+(DateTimeObject*)parseDateTime:(NSString*)dateTimeString;
+(int)minutesBetweenCurrentTimeAnd:(NSString*)dateTime;
+(NSString*)timeBetweenCurrentTimeAnd:(NSString*)dateTime;
+(NSString*)currentTimeOffsetByMinutes:(NSString*)minutes;
+(NSString*)timeStringFromMinutes:(NSString*)minutesString;
//+(NSString*)timeStringFromUTCDate:(NSString*)utcDate;

+(NSString*)currentDateTimeInUtcFormat;
+(NSString*)dateInUtcFormat:(NSDate*)givenDate;

+(NSString*)timeSince:(NSString*)pastDate;
+(int)timeIntUntilDate:(NSDate*)givenDate;
+(NSString*)timeUntilDate:(NSDate*)givenDate;

+(NSString*)substring:(NSString*)sampleString from:(NSString*)start to:(NSString*)end;
+(NSString*)substring:(NSString*)sampleString from:(NSString*)start length:(NSUInteger)length;
+(NSString*)substring:(NSString*)sampleString upTo:(NSString*)end;
+(NSString*)nsDateToUTC:(NSDate*)nsDate;
+(int)minutesIntBetweenTimeA:(NSString*)dateTimeA andTimeB:(NSString*)dateTimeB;
+(NSString*)timeBetweenTimeA:(NSString*)dateTimeA andTimeB:(NSString*)dateTimeB;
+(NSString*)localDateTimeFromUTC:(NSString*)utcDateTimeString;
+(NSString*)currentDateTimeOffsetByMinutes:(NSString*)minutes;

+(int)secondsUntilDate:(NSDate*)givenDate;
+(int)daysUntilDate:(NSDate*)givenDate;
+(int)hoursUntilDate:(NSDate*)givenDate;
+(int)minutesUntilDate:(NSDate*)givenDate;
+(int)totalSecondsUntilDate:(NSDate*)givenDate;
@end
