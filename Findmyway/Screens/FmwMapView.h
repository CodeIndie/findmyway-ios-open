//
//  FmwMapView.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/12.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIView.h"
#import <MapKit/MapKit.h>
#import <AudioToolbox/AudioToolbox.h>
#import "Location.h"
#import "AppViewController.h"
#import "MulticolorMapLine.h"
#import "MapTypeView.h"
#import "SearchItem.h"

@interface FmwMapView : XUIView<MKMapViewDelegate>
{
    UIImageView *topbarBackground;
    UIImageView *topbarLine;
    UIImageView *toolbarBackground;
    UIImageView *toolbarLine;
    
    MapTypeView *mapTypeView;
    
    CGRect topLeftRect;
    CGRect topRightRect;
    
    CGRect bottomLeftRect;
    CGRect bottomRightRect;
    CGRect bottomMiddleRect;
    
    UILabel *topbarLabel;
    UILabel *toolbarLabel;
    UILabel *bottomBarLabel;
    
    UIButton *backToSearchButton;
    UIButton *backToTripsButton;
    UIButton *backToOverviewButton;
    UIButton *backToHomeButton;
    UIButton *backButton;
    UIButton *cancelButton;
    UIButton *submitButton;
    
    UIButton *showLocationButton;
    UIButton *infoButton;
    UIButton *favouriteAddButton;
    UIButton *beginJourneyButton;
    
    NSString *StartEndPinIdentifier;
    MKPointAnnotation *selectedStartLocation;
    MKPointAnnotation *selectedEndLocation;
    BOOL locationSelected;
    
    MKPolyline *route;
    MKPolylineView *lineView;
    NSMutableArray *routeStops;
    NSMutableArray *polylines;
    
    BOOL showingMapFirstTime;
}

@property MKMapView *mapView;

-(void)showUserLocation;
-(void)backToSearch;
-(void)submitSelection;
-(void)pointSelected;
-(void)setState:(MapState)state;
-(void)setSearchItemForPreview:(SearchItem*)item;
-(void)setAddress;
-(void)addressFailedLoading;

-(void)drawRoute;
-(void)backToTrips;

@end
