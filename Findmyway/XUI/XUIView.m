//
//  XUIView.m
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUIView.h"

@implementation XUIView

-(float)width
{
    return self.frame.size.width;
}

-(float)height
{
    return self.frame.size.height;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self setConfigRefs];
        [self setXuiProperties];
        scrollableView = [[UIScrollView alloc] initWithFrame:frame];
    }
    return self;
}

-(void)setConfigRefs
{
    appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    layout = appDelegate.layout;
    colour = appDelegate.colour;
    device = appDelegate.device;
}

+(CAGradientLayer*)gradientInView:(UIView*)view gradientStart:(UIColor*)start gradientEnd:(UIColor*)end
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = view.bounds;
    gradient.startPoint = CGPointMake(0.0, 0.5);
    gradient.endPoint = CGPointMake(1.0, 0.5);
    gradient.colors = [NSArray arrayWithObjects:(id)start.CGColor, (id)end.CGColor, nil];
    return gradient;
}

-(void)setXuiProperties
{
    transitionDuration = 0.5;
    animationInterval = 0.4;
    delay = 0.08;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    if(!initialised)
    {
        [self setBackgroundColor:colour.fmwBackgroundColor];
        self.viewName = name;
        parentView = parentViewController;
    }
}

-(void)addNotificationListeners
{
    //[self XLog:@"Adding Notification Listeners"];
}

-(void)removeNotificationListeners
{
    //[self XLog:@"Removing Notification Listeners"];
}

-(void)animateIn
{
    self.isActive = YES;
    [self XLog:@"Animating IN" ];
    [self bringSubviewToFront:self];
    [UIView animateWithDuration:animationInterval animations:^
    {
    }
    completion:^(BOOL finished)
    {
        [self updateUI];
    }];
}

-(void)updateUI
{
    [self XLog:@"updating UI"];
}

-(void)animateOut
{
    self.isActive = NO;
    [self XLog:@"Animating OUT"];
}

-(float)getDelay:(int)layerIndex
{
    return transitionDuration + delay * layerIndex;
}

-(void)XLog:(NSString *)logString
{
    NSString *className = [NSString stringWithFormat:@"%@",self.class];
    NSLog(@"[Class %@]: %@", className, logString);
}

-(void)startLoader:(CGPoint)point inView:(UIView*)view
{
//    if(activityIndicator != nil)
//    {
//        [activityIndicator removeFromSuperview];
//    }
//
//    float xWidth = layout.buttonSize * 0.4;
//    float yHeight = layout.buttonSize * 0.4;
//    float x = point.x - xWidth/2;
//    float y = point.y - yHeight/2;
//    activityIndicator = [[HZActivityIndicatorView alloc] initWithFrame:CGRectMake(x,y,xWidth, yHeight)];
//    activityIndicator.backgroundColor = [UIColor clearColor];
//    activityIndicator.opaque = YES;
//    activityIndicator.steps = 12;
//    activityIndicator.finSize = CGSizeMake(2,7);
//    activityIndicator.indicatorRadius = 8;
//    activityIndicator.stepDuration = 0.03;
//    activityIndicator.color = colour.fmwActiveColor;
//    activityIndicator.cornerRadii = CGSizeMake(2, 2);
//    [activityIndicator startAnimating];
//    
//    [view addSubview:activityIndicator];
    float loaderSize = layout.buttonSize - layout.margin * 0.75;
    xuiLoader = [[XUILoader alloc] initWithFrame:CGRectMake(0, 0, loaderSize, loaderSize)
                                          layout:layout
                                          colour:colour
                                          inView:view];
    xuiLoader.center = point;
    [xuiLoader startAnimation];
}

-(void)stopLoader
{
    [xuiLoader stopAnimation];
//    [activityIndicator removeFromSuperview];
}

-(void)showNotification:(NSString*)message inView:(UIView*)view
{
    notification = [[XUINotification alloc] initWithMessage:message inView:view];
}

// NATIVE POPUP
-(void)showAlertView:(NSString *)title message:(NSString *)message button1:(NSString*)button1 button2:(NSString*)button2
{
    
    alertViewPopup = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:button1 otherButtonTitles:(button2 != nil ? button2 : nil), nil];
    [alertViewPopup show];
}

-(void)showAlertView:(NSString *)title message:(NSString *)message button:(NSString *)button
{
    alertViewPopup = [[UIAlertView alloc] initWithTitle:title message:message delegate:nil cancelButtonTitle:button otherButtonTitles:nil];
    [alertViewPopup show];
}

-(void)showAlertView:(NSString *)title message:(NSString *)message buttons:(NSArray *)buttons
{
    
}

-(void)hideAlertView
{
    [alertViewPopup dismissWithClickedButtonIndex:0 animated:YES];
}

// NOTIFICATION POPUP
-(void)showNotification:(NSString *)title message:(NSString *)message buttons:(NSArray *)buttons
{
    [self XLog:@"NO CUSTOM NOTIFICATION CREATED YET"];
}

-(void)hideNotification
{
    [self XLog:@"HIDING CUSTOM NOTIFICATION"];
}

// ERROR HANDLING

-(void)handleError:(NSDictionary *)errorDictionary statusCode:(int)statusCode error:(NSError *)error
{
    NSLog(@"FAILURE: %d", statusCode);
    NSLog(@"STILL NEED TO DO GENERIC ERROR HANDLING");
    
    [self handleIOSError:error];
}

-(void)handleApiError:(NSDictionary*)errorDictionary statusCode:(int)statusCode
{
    if(statusCode >= 300)
    {
        apiError = [[ApiError alloc] initWithDictionary:errorDictionary];
        
        switch(apiError.errorCode)
        {
            case 1001:// INPUT MODEL VARIABLES INCORRECT
            {
                NSLog(@"Could not process input details.");
            }break;

            case 1010:// INPUT MODEL VARIABLES INCORRECT
            {
                NSLog(@"Invalid App Key");
            }break;
                
            case 1020:// INVALID TOKEN
            {
                NSLog(@"Invalid Token ... Please Reregister the app.");
                [appDelegate getNewToken];
                //[self showAlertView:@"Invalid Token" message:@"Your token is no longer valid. The app needs to be reverfied" button:@"Verify"];
            }break;
                
            case 1022:// INPUT MODEL VARIABLES INCORRECT
            {
                NSLog(@"Account no longer active");
            }break;
                
            default:
            {
                NSLog(@"An UNKOWN ERROR OCCURED: %d", apiError.errorCode);
            }break;
        }
    }
}

-(void)handleIOSError:(NSError*)error
{
    switch(error.code)
    {
        case -1009:
        {
            NSLog(@"No Network Connection");
        }break;
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end