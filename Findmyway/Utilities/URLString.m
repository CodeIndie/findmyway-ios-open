//
//  URLString.m
//  FMW
//
//  Created by Bilo Lwabona on 11/21/13.
//

#import "URLString.h"

@implementation URLString

-(id)initWithString:(NSString *)urlString
{
    _encoded = (__bridge CFStringRef)urlString;
    _decoded = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, self.encoded,NULL,CFSTR(":/?#[]@!$&'()*+;="), kCFStringEncodingUTF8);
    
    _encodedString = [NSString stringWithFormat:@"%@", _encoded];
    _decodedString = [NSString stringWithFormat:@"%@", _decoded];
    
    return self;
}

+(NSString*)encodeString:(NSString*)string
{
    CFStringRef encodedVersion = (__bridge CFStringRef)string;
    CFStringRef decodedVersion = CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, encodedVersion,NULL,CFSTR(":/?#[]@!$&'()*+;="), kCFStringEncodingUTF8);
    
    //NSString *encodedReturnString = [NSString stringWithFormat:@"%@", encodedVersion];
    NSString *decodedReturnString = [NSString stringWithFormat:@"%@", decodedVersion];
    
    return decodedReturnString;
}

+(NSString*)urlParameterString:(NSDictionary*)dictionary
{
    NSMutableString *parameterString = [[NSMutableString alloc] init];
    [parameterString appendString:@""];
    if(dictionary == nil)
    {
        return parameterString;
    }

    for(NSString *key in [dictionary allKeys])
    {
        NSObject *dictionaryObject = [dictionary objectForKey:key];
        if([dictionaryObject isKindOfClass:[NSString class]])
        {
            [parameterString appendString:[NSString stringWithFormat:@"%@=%@", key, [URLString encodeString:[dictionary objectForKey:key]]]];
            if(![key isEqual:dictionary.allKeys.lastObject])
            {
                [parameterString appendString:@"&"];
            }
        }
        else if([dictionaryObject isKindOfClass:[NSNumber class]])
        {
            [parameterString appendString:[NSString stringWithFormat:@"%@=%@", key, [URLString encodeString:[dictionary objectForKey:key]]]];
            if(![key isEqual:dictionary.allKeys.lastObject])
            {
                [parameterString appendString:@"&"];
            }
        }
        else if([dictionaryObject isKindOfClass:[NSArray class]])
        {
            NSArray *dictionaryArray = (NSArray*)dictionaryObject;
            if(dictionaryArray.count == 0)
            {
                continue;
            }
            
            for(NSString *keyValue in dictionaryArray)
            {
                [parameterString appendString:[NSString stringWithFormat:@"%@=%@", key, [URLString encodeString:keyValue]]];
                if(![keyValue isEqual:dictionaryArray.lastObject])
                {
                    [parameterString appendString:@"&"];
                }
            }
            
            if(![key isEqual:dictionary.allKeys.lastObject])
            {
                [parameterString appendString:@"&"];
            }
        }
    }
    return parameterString;
}

@end
