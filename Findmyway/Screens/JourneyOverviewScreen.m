//
//  JourneyOverviewScreen.m
//  Findmyway
//
//  Created by Bilo on 2/25/15.
//

#import "JourneyOverviewScreen.h"

@implementation JourneyOverviewScreen

-(void)animateIn
{
    [super animateIn];
    [parentView.view addSubview:self];
    self.alpha = 0;
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
    animations:^
    {
        self.alpha = 1;
    }
    completion:^(BOOL finished)
    {
        
    }];
}

-(void)animateOut
{
    [super animateOut];
    [super animateIn];
    self.alpha = 1;
    [UIView animateWithDuration:animationInterval delay:animationInterval options:UIViewAnimationOptionCurveEaseInOut
    animations:^
    {
        self.alpha = 0;
    }
    completion:^(BOOL finished)
    {
     
    }];
}

-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    // TOP & BOTTOM BAR
    topbarBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.buttonSize)];
    [topbarBackground setBackgroundColor:colour.fmwHeader];
    
    topbarLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.buttonSize, layout.screenWidth, layout.delimiterSize)];
    [topbarLine setBackgroundColor:colour.fmwTextColor];
    
    // BOTTOM BAR
    bottomBarBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.screenHeight - layout.buttonSize - layout.topOfContent, layout.screenWidth, layout.buttonSize)];
    [bottomBarBackground setBackgroundColor:colour.fmwBackgroundColor];
    
    bottomBarLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.screenHeight - layout.buttonSize - layout.topOfContent, layout.screenWidth, layout.delimiterSize)];
    [bottomBarLine setBackgroundColor:colour.fmwTextColor];
    
    backButton = [XUIButton backButtonWithTitle:NSLocalizedString(@"Back", nil) color:colour.fmwActiveColor layout:layout];
    [backButton addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    mapButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth - layout.buttonSize - layout.padding * 2, 0, layout.buttonSize * 1.5, layout.buttonSize)];
    [mapButton setTitle:NSLocalizedString(@"Map", nil) forState:UIControlStateNormal];
    [mapButton.titleLabel setFont:[UIFont boldSystemFontOfSize:[UIFont systemFontSize] + 1]];
    [mapButton setTitleColor:colour.fmwActiveColor forState:UIControlStateNormal];
    [mapButton addTarget:self action:@selector(showOnMap) forControlEvents:UIControlEventTouchUpInside];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, layout.buttonSize + layout.delimiterSize, layout.screenWidth, layout.screenHeight - (layout.buttonSize + layout.padding))];
}

-(void)addUIToView
{
    [self addSubview:topbarBackground];
    [self addSubview:topbarLine];
    [self addSubview:backButton];
    [self addSubview:scrollView];
    [self addSubview:mapButton];
}

-(void)addContent:(MultimodalTrip*)trip
{
    [self createInteractiveView:trip];
}

-(void)createNaturalFlow:(MultimodalTrip*)trip
{
    for(UIView *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    int i = 0;
    for(MultimodalStage *stage in trip.stages)
    {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(layout.buttonSize + layout.margin, i * layout.buttonSize, layout.screenWidth - layout.buttonSize - layout.margin, layout.buttonSize)];
        [label setText:stage.modeName];
        [label setTextColor:colour.fmwTextColorLight];
        
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(layout.padding, i * layout.buttonSize, layout.buttonSize, layout.buttonSize)];
        [image setImage:[[appDelegate getImageForMode:stage.modeType] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate]];
        [image setTintColor:colour.black];
        
        [scrollView addSubview:label];
        [scrollView addSubview:image];
        i++;
    }
    scrollView.contentSize = CGSizeMake(layout.screenWidth, i*layout.buttonSize + layout.margin);
}

-(void)createInteractiveView:(MultimodalTrip*)trip
{
    if(lines != nil)
    {
        lines = nil;
    }
    lines = [[NSMutableArray alloc] init];
    
    for(UIView *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    int i = 0;
    int stageIndex = 0;
    for(MultimodalStage * stage in trip.stages)
    {
        for(__strong MultimodalPoint *point in stage.points)
        {
            TripStepTile *tile;
            CGRect frame = CGRectMake(0, i * layout.buttonSize, layout.screenWidth, layout.buttonSize);
            
            // WALKING
            if([stage.modeType isEqualToString:@"Pedestrian"])
            {
                if(stage.isShowingDirections)
                {
                    // START
                    if(point == stage.points.firstObject)
                    {
                        tile = [[TripStepTile alloc] initWithFrame:frame stepType:WalkingStart step:point stage:stage trip:trip instructions:@"" directionIndex:0];
                        tile.button.tag = stageIndex;
                        [tile.button addTarget:self action:@selector(toggleStageDetail:) forControlEvents:UIControlEventTouchUpInside];
                        [scrollView addSubview:tile];
                        i++;
                    }
                    
                    // DIRECTIONS
                    if(point == stage.points.firstObject)// only draw once
                    {
                        for(int directionStepIndex = 0 ; directionStepIndex < stage.directions.steps.count ; directionStepIndex++, i++)
                        {
                            frame = CGRectMake(0, i * layout.buttonSize, layout.screenWidth, layout.buttonSize);
                            
                            RouteStep *step = (RouteStep*)[stage.directions.steps objectAtIndex:directionStepIndex];
                            tile = [[TripStepTile alloc] initWithFrame:frame stepType:WalkingMiddle step:point stage:stage trip:trip instructions:[NSString stringWithFormat:@"%d. %@", step.order + 1, step.instructions] directionIndex:directionStepIndex];
                            tile.button.tag = stageIndex;
                            [tile.button addTarget:self action:@selector(toggleStageDetail:) forControlEvents:UIControlEventTouchUpInside];
                            [scrollView addSubview:tile];
                        }
                    }
                    
                    point = stage.points.lastObject;
                    // END
                    if(point == stage.points.lastObject)
                    {
                        frame = CGRectMake(0, i * layout.buttonSize, layout.screenWidth, layout.buttonSize);
                        tile = [[TripStepTile alloc] initWithFrame:frame stepType:WalkingEnd step:point stage:stage trip:trip instructions:@"" directionIndex:0];
                        tile.button.tag = stageIndex;
                        [tile.button addTarget:self action:@selector(toggleStageDetail:) forControlEvents:UIControlEventTouchUpInside];
                        [scrollView addSubview:tile];
                        i++;
                        break;
                    }
                }
                else
                {
                    if(point == stage.points.firstObject)
                    {
                        tile = [[TripStepTile alloc] initWithFrame:frame stepType:WalkingStart step:point stage:stage trip:trip instructions:@"" directionIndex:0];
                        tile.button.tag = stageIndex;
                        [tile.button addTarget:self action:@selector(toggleStageDetail:) forControlEvents:UIControlEventTouchUpInside];
                        [scrollView addSubview:tile];
                        
                        i++;
                        frame = CGRectMake(0, i * layout.buttonSize, layout.screenWidth, layout.buttonSize);
                        tile = [[TripStepTile alloc] initWithFrame:frame stepType:WalkingExpansionButton step:point stage:stage trip:trip instructions:@"" directionIndex:0];
                        tile.button.tag = stageIndex;
                        [tile.button addTarget:self action:@selector(toggleStageDetail:) forControlEvents:UIControlEventTouchUpInside];
                    }
                    else if(point == stage.points.lastObject)
                    {
                        tile = [[TripStepTile alloc] initWithFrame:frame stepType:WalkingEnd step:point stage:stage trip:trip instructions:@"" directionIndex:0];
                        [scrollView addSubview:tile];
                    }
                }
            }
            // PUBLIC TRANSPORT
            else
            {
                if(point == stage.points.firstObject)
                {
                    tile = [[TripStepTile alloc] initWithFrame:frame stepType:TransportStart step:point stage:stage trip:trip instructions:@"" directionIndex:0];
                    tile.button.tag = stageIndex;
                    [tile.button addTarget:self action:@selector(toggleStageDetail:) forControlEvents:UIControlEventTouchUpInside];
                    
                    if(!stage.isShowingDirections)
                    {
                        [scrollView addSubview:tile];
                        i++;
                        frame = CGRectMake(0, i * layout.buttonSize, layout.screenWidth, layout.buttonSize);
                        tile = [[TripStepTile alloc] initWithFrame:frame stepType:TransportExpansionButton step:point stage:stage trip:trip instructions:@"" directionIndex:0];
                        tile.button.tag = stageIndex;
                        [tile.button addTarget:self action:@selector(toggleStageDetail:) forControlEvents:UIControlEventTouchUpInside];
                    }
                }
                else if(point == stage.points.lastObject)
                {
                    tile = [[TripStepTile alloc] initWithFrame:frame stepType:TransportEnd step:point stage:stage trip:trip instructions:@"" directionIndex:0];
                    tile.button.tag = stageIndex;
                    [tile.button addTarget:self action:@selector(toggleStageDetail:) forControlEvents:UIControlEventTouchUpInside];
                }
                else if(stage.isShowingDirections)
                {
                    tile = [[TripStepTile alloc] initWithFrame:frame stepType:TransportMiddle step:point stage:stage trip:trip instructions:@"" directionIndex:0];
                    tile.button.tag = stageIndex;
                    [tile.button addTarget:self action:@selector(toggleStageDetail:) forControlEvents:UIControlEventTouchUpInside];
                }
                else
                {
                    continue;
                }
            }
            [scrollView addSubview:tile];
            i++;
        }
        stageIndex++;
    }
    scrollView.contentSize = CGSizeMake(layout.screenWidth, layout.buttonSize * i);
}

-(void)refreshStepTiles
{
    for(UIView *view in scrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    [self addContent:appDelegate.tripQuery.selectedTrip];
}

-(IBAction)toggleStageDetail:(id)sender
{
    NSInteger tag = (int)((UIButton*)sender).tag;
    selectedStage = (MultimodalStage*)[appDelegate.tripQuery.selectedTrip.stages objectAtIndex:tag];
    
    if(![selectedStage.modeName isEqualToString:@"Pedestrian"])
    {
        selectedStage.isShowingDirections = !selectedStage.isShowingDirections;
        [self refreshStepTiles];
    }
    else if(selectedStage.hasDirections)
    {
        selectedStage.isShowingDirections = !selectedStage.isShowingDirections;
        [self refreshStepTiles];
    }
    else
    {
        [self getWalkingDirections];
    }
}

-(void)getWalkingDirections
{
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%@",selectedStage.startLocation.location.latitudeString] forKey:@"startLatitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",selectedStage.startLocation.location.longitudeString] forKey:@"startLongitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",selectedStage.endLocation.location.latitudeString] forKey:@"endLatitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",selectedStage.endLocation.location.longitudeString] forKey:@"endLongitude"];
    [parameters setValue:@"pedestrian" forKey:@"mode"];
    
    [self startLoader:CGPointMake((layout.screenWidth)/2, layout.padding * 1.5) inView:self];
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:Directions callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
     {
         NSLog(@"API - Directions: %ld", (long)response.statusCode);
         NSDictionary *responseDictionary = [JSONParser convertJSONToDictionary:bodyString];
         [self handleApiError:responseDictionary statusCode:(int)response.statusCode];
         [self stopLoader];
         
         [selectedStage addDirections:[[MapRoute alloc] initWithDictionary:responseDictionary]];
         [self refreshStepTiles];
     }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
    {
        NSLog(@"API CONNECTION FAILURE - Directions: %@", error);
        [self stopLoader];
        [self handleError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode error:error];
    }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
    {
        // This is not going to be called in this example but it's included for completeness.
        NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
    }];
}

-(void)back
{
    AppViewController *viewController = (AppViewController*)parentView;
    [viewController swapInView:viewController.fmwRouteOptions];
}

-(void)showOnMap
{
    if(!appDelegate.tripQuery.selectedTrip.hasMapRoute)
    {
        [self getRouteForTrip];
    }
    else
    {
        [self navigateToMap];
    }
}

-(void)getRouteForTrip
{
    MultimodalTrip *trip = appDelegate.tripQuery.selectedTrip;
    if(trip.hasMapRoute)
    {
        return;
    }
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:trip.tripId forKey:@"tripid"];
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:Trips callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:^(NSHTTPURLResponse *response, NSString *bodyString)
     {
         //[self stopLoader];
         NSLog(@"API - Trips: %ld", (long)response.statusCode);
         [appDelegate.apiResponse addTrip:[JSONParser convertJSONToDictionary:bodyString] toIndex:appDelegate.tripQuery.selectedTripIndex];
         [self navigateToMap];
     }
    failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
     {
         //[self stopLoader];
        NSLog(@"API CONNECTION FAILURE - Trips: %@", error);
     }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
     {
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
     }];
}

-(void)navigateToMap
{
    AppViewController *viewController = (AppViewController*)parentView;
    appDelegate.mapConfig.state = JourneyOverView;
    [viewController.fmwMapView setState:JourneyOverView];
    [viewController swapInView:viewController.fmwMapView];
}
@end
