//
//  LocationUtility.m
//  tct
//
//  Created by Bilo on 5/28/14.
//

#import "LocationUtil.h"

@implementation LocationUtil

static LocationUtil *instance = nil;

+(LocationUtil*)instance
{
    if(instance == nil)
    {
        instance = [[LocationUtil alloc] init];
    }
    return instance;
    
}

+(LocationUtil*)sharedInstance
{
    static LocationUtil *sharedInstance = nil;
    static dispatch_once_t predicate;
    dispatch_once(&predicate, ^{
        sharedInstance = [[self alloc] init];
    });
    
     return sharedInstance;
}

-(BOOL)isLocationEnabled
{
    BOOL locationEnabled = [CLLocationManager locationServicesEnabled];
    BOOL locationAppEnabled = [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied;

    if(locationEnabled)
    {
        return locationAppEnabled;
    }
    return locationEnabled;
}

-(id)init
{
    self = [super init];
    
    if(self)
    {
        self.logLocationUpdates = YES;
        locationManager = [[CLLocationManager alloc] init];
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        
        self.capeTown = [[Location alloc] initWithLatitude:-33.9253 longitude:18.4239];
        self.tshwane = [[Location alloc] initWithLatitude:-25.6667 longitude:28.3333];
        self.southAfrica = [[Location alloc] initWithLatitude:-30.0000 longitude:25.0000];
        
        [self iOS8LocationSetup];
        [locationManager startUpdatingLocation];
        
    }
    return self;
}

-(void)iOS8LocationSetup
{
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
    {
        if([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
        {
            [locationManager requestAlwaysAuthorization];
        }
    }
}


// FUNCTIONS
-(void)setLocation:(CLLocationCoordinate2D)location
{
    self.userLocation = [[Location alloc] initWithCLLatitude: location.latitude longitude:location.longitude];
}

-(Location*)getLocation
{
    return self.userLocation;
}

-(void)startUpdatingUserLocation
{
    [self iOS8LocationSetup];
    [locationManager startUpdatingLocation];
}

-(void)stopUpdatingUserLocation
{
    [locationManager stopUpdatingLocation];
}


// DELEGATE CALLBACK METHODS
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *location = (CLLocation*)[locations objectAtIndex:0];
    if(self.logLocationUpdates)
    {
        //NSLog(@"Updated Location: %f,%f", location.coordinate.latitude, location.coordinate.longitude);
    }
    self.userLocation = [[Location alloc] initWithCLLatitude:location.coordinate.latitude longitude:location.coordinate.longitude];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation*)oldLocation
{
    Location *usersCurrentLocation = [[Location alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    
    if(usersCurrentLocation != nil)
    {
        self.userLocation = usersCurrentLocation;
        [self.delegate locationUpdate:newLocation];
    }
    
    self.userLocation = [[Location alloc]
                         initWithCLLatitude:newLocation.coordinate.latitude
                         longitude:newLocation.coordinate.longitude];
    [self getAddressFromAPI];
    
    if(self.logLocationUpdates)
    {
        NSLog(@"User Location: %@", [self.userLocation gpsCoordinates]);
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
//    NSLog(@"didFailWithError: %@", error);
//    //[self stopProgressHUD];
//    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Location Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    //[self stopProgressHUD];
//    [errorAlert show];
}

-(void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager
{
    
}

-(void)locationManagerDidResumeLocationUpdates:(CLLocationManager *)manager
{
    
}

-(void)promptGPSActivation
{
    if(![CLLocationManager locationServicesEnabled])
    {
        NSLog(@"Location disabled");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Location disabled!", nil)
                                                        message:NSLocalizedString(@"Location Services Instructions.", nil) delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];//]@"Yes", nil];
        [alert show];
    }
    else
    {
        NSLog(@"Location enabled");
        [self getLocation];
    }
}

-(BOOL)isLocationAvailable
{
    if([self isLocationEnabled])
    {
        if(self.userLocation.latitudeString != nil && self.userLocation.longitudeString != nil)
        {
            return YES;
        }
        return NO;
    }
    else
    {
        return NO;
    }
}

-(void)getAddressFromAPI
{
    if(!self.hasAddressForCoords)
    {
        [self getUserAddress];
    }
}
-(void)getUserAddress
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
    [parameters setValue:[NSString stringWithFormat:@"%@",self.userLocation.latitudeString] forKey:@"latitude"];
    [parameters setValue:[NSString stringWithFormat:@"%@",self.userLocation.longitudeString] forKey:@"longitude"];
    
    NSMutableURLRequest *request = [appDelegate.apiUtil urlForRequest:ReverseGeocode callType:GET parameters:parameters];
    JCDHTTPConnection *connection = [[JCDHTTPConnection alloc] initWithRequest:request];
    [connection executeRequestOnSuccess:
     ^(NSHTTPURLResponse *response, NSString *bodyString)
     {
         NSLog(@"API - ReverseGeocode: %ld", (long)response.statusCode);
         //[self handleApiError:[JSONParser convertJSONToDictionary:bodyString] statusCode:(int)response.statusCode];
         [appDelegate.apiResponse addAddress:[JSONParser convertJSONToDictionary:bodyString]];
         [appDelegate.tripQuery setPoint:appDelegate.apiResponse.location];
         if(response.statusCode < 300)
         {
             self.userLocation.address = appDelegate.apiResponse.location.address;
             self.hasAddressForCoords = YES;
         }
     }
     failure:^(NSHTTPURLResponse *response, NSString *bodyString, NSError *error)
     {
         NSLog(@"PI CONNECTION FAILURE LocationUtil: (%ld) FAILED TO GET ADDRESS", (long)response.statusCode);
     }
    didSendData:^(NSInteger bytesWritten, NSInteger totalBytesWritten, NSInteger totalBytesExpectedToWrite)
     {
         // This is not going to be called in this example but it's included for completeness.
         NSLog(@"DID SEND DATA: %ld", (long)bytesWritten);
     }];
}
@end
