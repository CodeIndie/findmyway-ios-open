//
//  JourneyOverviewScreen.h
//  Findmyway
//
//  Created by Bilo on 2/25/15.
//

#import "XUIView.h"
#import "AppViewController.h"
#import "TripStepTile.h"

@interface JourneyOverviewScreen : XUIView
{
    UIImageView *topbarBackground;
    UIImageView *topbarLine;
    UIImageView *bottomBarBackground;
    UIImageView *bottomBarLine;
    
    UIButton *backButton;
    UIButton *mapButton;
    
    UIScrollView *scrollView;
    NSMutableArray *lines;
    BOOL directionsIndex;
    MultimodalStage *selectedStage;
}

-(void)back;
-(void)addContent:(MultimodalTrip*)trip;
@end
