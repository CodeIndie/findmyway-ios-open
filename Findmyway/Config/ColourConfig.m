//
//  ColourConfig.m
//  Findmyway
//
//  Created by Bilo on 11/22/14.
//

#import "ColourConfig.h"

@implementation ColourConfig

-(id)init:(NSDictionary*)dictionary
{
    self = [super init];
    if(self)
    {
        self.transparent = [UIColor clearColor];
        NSDictionary *colourDictionary = [DictionaryUtil getChildDictionary:dictionary withKey:@"Colours"];
        
        
        // AREYENG
        self.areyengPurple = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"AreyengPurple"]];
        self.areyengGreen = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"AreyengGreen"]];
        self.areyengGradientStart = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"AreyengGradientStart"]];
        self.areyengGradientEnd = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"AreyengGradientEnd"]];
        
        // FINDMYWAY
        self.fmwGradientStart = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwGradientStart"]];
        self.fmwGradientEnd = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwGradientEnd"]];
        
        self.fmwRed = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwRed"]];
        self.fmwDark = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwDark"]];
        self.fmwLight = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwLight"]];
        self.fmwHeader = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwHeader"]];
        
        self.fmwBlue1 = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwBlue1"]];
        self.fmwLightBorder = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwLightBorder"]];
        self.fmwDarkBorder = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwDarkBorder"]];
        self.fmwHeaderLight = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwHeaderLight"]];
        self.fmwHeaderDark = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"fmwHeaderDark"]];

        // SOCIAL MEDIA
        self.facebook = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"colorFacebook"]];
        self.twitter = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"colorTwitter"]];
        self.google = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"colorGoogle"]];
        
        // IOS 8 COLOURS: http://iosdesign.ivomynttinen.com/
        self.iosCyan = [Base64Image colorWithHexString:@"54c7fc"];
        self.iosYellow = [Base64Image colorWithHexString:@"ffcd00"];
        self.iosOrange = [Base64Image colorWithHexString:@"ff9600"];
        self.iosPink = [Base64Image colorWithHexString:@"ff2851"];
        self.iosBlue = [Base64Image colorWithHexString:@"0076ff"];
        self.iosGreen = [Base64Image colorWithHexString:@"44db5e"];
        self.iosRed = [Base64Image colorWithHexString:@"ff3824"];
        self.iosGrey = [Base64Image colorWithHexString:@"8e8e93"];
        
        
        self.lightGray = [Base64Image colorWithHexString:[colourDictionary objectForKey:@"iOSLightGray"]];
        self.white = [UIColor whiteColor];
        self.black = [UIColor blackColor];
        self.clear = [UIColor clearColor];
        
        //[self setDarkTheme:NO];
        [self initTimeSensitiveThemes];
        [self determineTheme];
        
    }
    return self;
}

-(void)initTimeSensitiveThemes
{
    // AREYENG - EARTH HOUR
    NSDateComponents *startDate = [[NSDateComponents alloc] init];
    NSDateComponents *endDate = [[NSDateComponents alloc] init];
    
    [startDate setDay:27];
    [startDate setMonth:3];
    [startDate setYear:2015];
    [startDate setHour:0];
    [startDate setMinute:0];
    [startDate setSecond:0];
    self.areyengStartDate = [[NSCalendar currentCalendar] dateFromComponents:startDate];

    [endDate setDay:30];
    [endDate setMonth:3];
    [endDate setYear:2015];
    [endDate setHour:0];
    [endDate setMinute:0];
    [endDate setSecond:0];
    self.areyengEndDate = [[NSCalendar currentCalendar] dateFromComponents:endDate];
    
    // TEST
//    [startDate setDay:11];
//    [startDate setMonth:3];
//    [startDate setYear:2015];
//    [startDate setHour:16];
//    [startDate setMinute:0];
//    [startDate setSecond:0];
//    self.areyengStartDate = [[NSCalendar currentCalendar] dateFromComponents:startDate];
//    
//    [endDate setDay:30];
//    [endDate setMonth:3];
//    [endDate setYear:2015];
//    [endDate setHour:0];
//    [endDate setMinute:0];
//    [endDate setSecond:0];
//    self.areyengEndDate = [[NSCalendar currentCalendar] dateFromComponents:endDate];
    
    //NSTimer *themeTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(determineTheme) userInfo:nil repeats:YES];
}

-(void)determineTheme
{
    //NSLog(@"Minutes until startDate: %d", [DateTimeUtil minutesUntilDate:self.areyengStartDate]);
    
    if(//[DateTimeUtil minutesUntilDate:self.areyengStartDate] < 0 &&
       //[DateTimeUtil minutesUntilDate:self.areyengEndDate] > 0)
       false)
    {
        [self setTheme:Areyeng];
    }
    else
    {
        [self setTheme:Findmyway];
    }
}

-(void)setTheme:(FmwAppTheme)theme
{
    switch(theme)
    {
        case Findmyway:
        {
            self.fmwBorderColor = self.fmwLightBorder;
            self.fmwTextColor = self.fmwDark;
            self.fmwBackgroundColor = self.fmwLight;
            
            self.fmwActiveColor = self.fmwBlue1;
            self.gradientStart = self.fmwGradientStart;
            self.gradientEnd = self.fmwGradientEnd;
            self.fmwHeader = self.fmwHeaderLight;
        }break;
            
        case Areyeng:
        {
            self.fmwBorderColor = self.fmwLightBorder;
            self.fmwTextColor = self.fmwDark;
            self.fmwBackgroundColor = self.fmwLight;
            
            self.fmwActiveColor = self.areyengPurple;
            self.gradientStart = self.areyengGradientStart;
            self.gradientEnd = self.areyengGradientEnd;
            self.fmwHeader = self.fmwHeaderLight;
        }break;
    }
    
    CGFloat red, green, blue, alpha;
    
    [self.fmwTextColor getRed:&red green:&green blue:&blue alpha:&alpha];
    self.fmwTextColorLight = [UIColor colorWithRed:red green:green blue:blue alpha:0.6];
    
    [self.fmwActiveColor getRed:&red green:&green blue:&blue alpha:&alpha];
    self.fmwActiveColorLight = [UIColor colorWithRed:red green:green blue:blue alpha:0.6];
}

-(void)setDarkTheme:(BOOL)isDarkTheme
{
    if(isDarkTheme)
    {
        self.fmwBorderColor = self.fmwDarkBorder;
        self.fmwTextColor = self.fmwLight;
        self.fmwBackgroundColor = self.fmwDark;
        
        //self.fmwActiveColor = self.fmwBlue1;
        self.fmwActiveColor = self.areyengPurple;
        self.gradientStart = self.areyengGradientStart;
        self.gradientEnd = self.areyengGradientEnd;
        
        self.gradientStart = self.fmwGradientStart;
        self.gradientEnd = self.fmwGradientEnd;
    }
    else
    {
        self.fmwBorderColor = self.fmwLightBorder;
        self.fmwTextColor = self.fmwDark;
        self.fmwBackgroundColor = self.fmwLight;
        
        //self.fmwActiveColor = self.fmwBlue1;
        self.fmwActiveColor = self.areyengPurple;
        self.gradientStart = self.areyengGradientStart;
        self.gradientEnd = self.areyengGradientEnd;
        
        self.gradientStart = self.fmwGradientStart;
        self.gradientEnd = self.fmwGradientEnd;
    }
}
@end

