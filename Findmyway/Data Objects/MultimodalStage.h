//
//  MultimodalStage.h
//  tct
//
//  Created by Bilo on 9/10/14.
//

#import <Foundation/Foundation.h>
#import "MultimodalPoint.h"
#import "MapRoute.h"

@interface MultimodalStage : NSObject

@property NSString *name;
@property NSString *modeName;
@property NSString *modeType;
@property NSString *routeDescription;
@property NSString *colourHex;
@property NSString *vehicleNumber;
@property NSString *duration;
@property NSString *cost;

@property UIColor *colour;

@property NSMutableArray *points;
@property NSMutableArray *announcements;

@property MultimodalPoint *startLocation;
@property MultimodalPoint *endLocation;

@property MapRoute* directions;
@property BOOL hasDirections;
@property BOOL isShowingDirections;

-(id)initWithDictionary:(NSDictionary*)dictionary;
-(void)addDirections:(MapRoute*)mapRoute;
@end
