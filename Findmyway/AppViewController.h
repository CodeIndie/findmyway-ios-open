//
//  AppViewController.h
//  Findmyway
//
//  Created by Bilo on 11/24/14.
//

#import "XUIViewController.h"
#import "FmwModePicker.h"

#import "StartupScreen.h"
#import "FmwScreen.h"
#import "SearchScreen.h"
#import "FmwMapView.h"
#import "FmwJourneyScreen.h"

#import "FmwRouteOptions.h"
#import "MainMenu.h"
#import "TimePicker.h"
#import "OperatorList.h"
#import "JourneyOverviewScreen.h"
#import "LoginScreen.h"
#import "CompetitionTripsScreen.h"
#import "TransitMapScreen.h"
#import "AnnouncementScreen.h"

@class

StartupScreen,

FmwScreen, FmwRouteOptions, MainMenu, FmwModePicker, OperatorList, JourneyOverviewScreen, CompetitionTripsScreen,

SearchScreen, FmwJourneyScreen, FmwMapView,  TimePicker, TransitMapScreen, AnnouncementScreen;

@interface AppViewController : XUIViewController
{
    UIImageView *backgroundImage;
}

@property MainMenu *mainMenu;
@property StartupScreen *startupScreen;
@property FmwModePicker *fmwModePicker;
@property FmwScreen *fmwScreen;
@property FmwMapView *fmwMapView;
@property FmwJourneyScreen *fmwJourneyScreen;
@property FmwRouteOptions *fmwRouteOptions;
@property SearchScreen *searchScreen;
@property TimePicker *timePicker;
@property OperatorList *operatorList;
@property JourneyOverviewScreen *journeyOverviewScreen;
@property LoginScreen *loginScreen;
@property CompetitionTripsScreen *competitionTripsView;
@property TransitMapScreen *transitMapScreen;
@property AnnouncementScreen *announcementScreen;

-(void)initBackButton;
-(void)customBackAction;

@end