//
//  XUIRect.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface XUIRect : NSObject

@property CGRect rect;
@property CGRect rectOffset;
@property CGPoint center;
@property CGPoint centerOffset;

-(id)initWithRect:(CGRect)rect centerOffset:(CGPoint)offset;

@end
