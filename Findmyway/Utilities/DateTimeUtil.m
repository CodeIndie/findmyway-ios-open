//
//  DateTimeUtilitiy.m
//  tct
//
//  Created by Bilo on 4/22/14.
//

#import "DateTimeUtil.h"

@implementation DateTimeUtil

+(DateTimeObject*)parseDateTime:(NSString *)dateTimeString
{
    
    DateTimeObject *dateTimeObject = [[DateTimeObject alloc] initWithDateTime:[DateTimeUtil localDateTimeFromUTC:dateTimeString]];
    return dateTimeObject;
}

//+(DateTimeObject*)parseNSDate:(NSDate*)nsDate
//{
//    DateTimeObject *dateTimeObject = [[DateTimeObject alloc] initWithSAST:[DateTimeUtility localDateTimeFromUTC:nsDate]];
//}

+(NSString*)currentDateTimeInUtcFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    
    NSDate *now = [NSDate date];    
    NSString *currentDate = [dateFormatter stringFromDate:now];
    
    return currentDate;
}

+(int)daysUntilDate:(NSDate*)givenDate
{
    NSTimeInterval interval = [givenDate timeIntervalSinceDate:[NSDate date]];
    int days = interval / (60 * 60 * 24);
    return days;
}

+(int)hoursUntilDate:(NSDate*)givenDate
{
    NSTimeInterval interval = [givenDate timeIntervalSinceDate:[NSDate date]];
    int hours = interval / (60 * 60);
    return (hours / 60) % 24;
}

+(int)minutesUntilDate:(NSDate *)givenDate
{
    NSTimeInterval interval = [givenDate timeIntervalSinceDate:[NSDate date]];
    int minutes = ((int)interval) / 60;
    return minutes % 60;
}

+(int)secondsUntilDate:(NSDate*)givenDate
{
    NSTimeInterval interval = [givenDate timeIntervalSinceDate:[NSDate date]];
    int seconds = (int)interval;
    return seconds % 60;
}

+(int)totalSecondsUntilDate:(NSDate*)givenDate
{
    NSTimeInterval interval = [givenDate timeIntervalSinceDate:[NSDate date]];
    int seconds = (int)interval;
    return seconds;
}

+(int)timeIntUntilDate:(NSDate*)givenDate
{
    NSString *dateUtc = [DateTimeUtil dateInUtcFormat:givenDate];
    int minutes = [DateTimeUtil minutesBetweenCurrentTimeAnd:dateUtc];
    return minutes;
}

+(NSString*)timeUntilDate:(NSDate *)givenDate
{
    NSString *dateUtc = [DateTimeUtil dateInUtcFormat:givenDate];
    return [DateTimeUtil timeBetweenCurrentTimeAnd:dateUtc];
}

+(NSString*)dateInUtcFormat:(NSDate*)givenDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *givenDateUtc = [dateFormatter stringFromDate:givenDate];
    return givenDateUtc;
}

+(NSString*)nsDateToUTC:(NSDate*)nsDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];
    
    NSString *givenDateUtc = [dateFormatter stringFromDate:nsDate];
    return givenDateUtc;
}

+(int)minutesIntBetweenTimeA:(NSString*)dateTimeA andTimeB:(NSString*)dateTimeB
{
    int timeUntilA = [DateTimeUtil minutesBetweenCurrentTimeAnd:dateTimeA];
    int timeUntilB = [DateTimeUtil minutesBetweenCurrentTimeAnd:dateTimeB];
    return timeUntilB - timeUntilA;
}

+(NSString*)timeBetweenTimeA:(NSString*)dateTimeA andTimeB:(NSString*)dateTimeB
{
    int minutes = [DateTimeUtil minutesIntBetweenTimeA:dateTimeA andTimeB:dateTimeB];
    
    if(minutes < 0)
    {
        minutes *= -1;
    }
    
    NSMutableString *timeString = [[NSMutableString alloc] initWithString:@""];
    if(minutes > 60 * 24)
    {
        if((minutes / (60 * 24)) > 1)
        {
            [timeString appendString:[NSString stringWithFormat:@"%d days", minutes / (60 * 24)]];
        }
        else
        {
            [timeString appendString:[NSString stringWithFormat:@"%d day", minutes / (60 * 24)]];
        }
        return timeString;
    }
    if(minutes > 59)
    {
        [timeString appendString:[NSString stringWithFormat:@"%dh ", minutes / 60]];
    }
    if(minutes % 60 > 10)
    {
        [timeString appendString:[NSString stringWithFormat:@"%dmin", minutes % 60]];
    }
    else if(minutes >= 60)
    {
        [timeString appendString:[NSString stringWithFormat:@"0%dmin", minutes % 60]];
    }
    else
    {
        [timeString appendString:[NSString stringWithFormat:@"%dmin", minutes % 60]];
    }
    return timeString;
}

+(int)minutesBetweenCurrentTimeAnd:(NSString*)dateTime
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    NSTimeZone *timeZone = [NSTimeZone timeZoneWithName:@"UTC"];
    [dateFormatter setTimeZone:timeZone];

    NSDate *now = [NSDate date];
    
    NSString *currentDate = [dateFormatter stringFromDate:now];
    NSTimeInterval time = [[dateFormatter dateFromString:dateTime] timeIntervalSinceDate:[dateFormatter dateFromString:currentDate]];
    
    int minutes = time / 60;
    return minutes;
}

+(NSString*)localDateTimeFromUTC:(NSString*)utcDateTimeString
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    
    NSTimeZone *destinationTimeZone = [NSTimeZone timeZoneForSecondsFromGMT:2 * 60 * 60];
    [dateFormatter setTimeZone:destinationTimeZone];
    
    NSDate *oldTime = [dateFormatter dateFromString:utcDateTimeString];
    NSString *estDateString = [dateFormatter stringFromDate:[oldTime dateByAddingTimeInterval:2 * 60 * 60]];
    
    return estDateString;
}

+(NSString*)timeBetweenCurrentTimeAnd:(NSString*)dateTime
{
    int minutes = [DateTimeUtil minutesBetweenCurrentTimeAnd:dateTime];
    
    if(minutes < 0)
    {
        minutes *= -1;
    }
    
    NSMutableString *timeString = [[NSMutableString alloc] initWithString:@""];
    if(minutes > 60 * 24)
    {
        if((minutes / (60 * 24)) > 1)
        {
            [timeString appendString:[NSString stringWithFormat:@"%d days", minutes / (60 * 24)]];
        }
        else
        {
            [timeString appendString:[NSString stringWithFormat:@"%d day", minutes / (60 * 24)]];
        }
        return timeString;
    }
    if(minutes > 59)
    {
        [timeString appendString:[NSString stringWithFormat:@"%dh ", minutes / 60]];
    }
    if(minutes % 60 > 10)
    {
        [timeString appendString:[NSString stringWithFormat:@"%dmin", minutes % 60]];
    }
    else if(minutes >= 60)
    {
        [timeString appendString:[NSString stringWithFormat:@"0%dmin", minutes % 60]];
    }
    else
    {
        [timeString appendString:[NSString stringWithFormat:@"%dmin", minutes % 60]];
    }
    return timeString;
}

+(NSString*)currentTimeOffsetByMinutes:(NSString*)minutes
{
    NSDateComponents *offset = [[NSDateComponents alloc] init];
    [offset setMinute:[minutes intValue]];
    NSDate *newDate = [[NSCalendar currentCalendar] dateByAddingComponents:offset toDate:[NSDate date] options:0];
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:newDate];
    
    NSString *extraZero;
    if([components minute] < 10)
    {
        extraZero = @"0";
    }
    else
    {
        extraZero = @"";
    }
    
    NSString *newTime = [NSString stringWithFormat:@"%ld:%@%ld", (long)[components hour], extraZero, (long)[components minute]];
    NSLog(@"New Time: %@", newTime);
    return newTime;
}

+(NSString*)currentDateTimeOffsetByMinutes:(NSString*)minutes
{
    NSTimeInterval timeOffset = (NSTimeInterval)[minutes intValue] * 60;
    NSDate *newDate = [[NSDate date] dateByAddingTimeInterval:timeOffset];
    return [DateTimeUtil dateInUtcFormat:newDate];
}

+(NSString*)timeStringFromMinutes:(NSString*)minutesString
{
    int minutes = [minutesString intValue];
    NSMutableString *timeString = [[NSMutableString alloc] initWithString:@""];
    
    if(minutes > 60 * 24)
    {
        if((minutes / (60 * 24)) > 1)
        {
            [timeString appendString:[NSString stringWithFormat:@"%d days", minutes / (60 * 24)]];
        }
        else
        {
            [timeString appendString:[NSString stringWithFormat:@"%d day", minutes / (60 * 24)]];
        }
        return timeString;
    }
    if(minutes > 59)
    {
        [timeString appendString:[NSString stringWithFormat:@"%dh ", minutes / 60]];
    }
    if(minutes % 60 > 10)
    {
        [timeString appendString:[NSString stringWithFormat:@"%dmin", minutes % 60]];
    }
    else if(minutes >= 60)
    {
        [timeString appendString:[NSString stringWithFormat:@"0%dmin", minutes % 60]];
    }
    else
    {
        [timeString appendString:[NSString stringWithFormat:@"%dmin", minutes % 60]];
    }
    
    if(minutes == 0)
    {
        [timeString setString:@"< 1min"];
    }
    
    return timeString;
}

+(NSString*)timeSince:(NSString *)pastDate
{
    NSMutableString *timeSincePastDate = [[NSMutableString alloc] initWithString:@""];
    //DateTimeObject *newDateTime = [DateTimeUtility parseDateTime:pastDate];
    int minutesInt = 0;
    minutesInt = [DateTimeUtil minutesBetweenCurrentTimeAnd:pastDate];
    if(minutesInt < 0)
    {
        minutesInt *= -1;
    }
    //NSLog(@"Minutes: %d", minutesInt);
    
    if(minutesInt < 1)
    {
        [timeSincePastDate appendString:@"just now"];
        return timeSincePastDate;
    }
    if(minutesInt >= 60 * 24)
    {
        [timeSincePastDate appendString:[NSString stringWithFormat:@"%d days ago", minutesInt / (60 * 24)]];
        return timeSincePastDate;
    }
    else
    {
        if(minutesInt > 59)
        {
            [timeSincePastDate appendString:[NSString stringWithFormat:@"%dh ", minutesInt / 60]];
            if(minutesInt % 60 == 0)
            {
                [timeSincePastDate appendString:[NSString stringWithFormat:@"ago"]];
                return timeSincePastDate;
            }
        }
        if(minutesInt % 60 > 10)
        {
            [timeSincePastDate appendString:[NSString stringWithFormat:@"%dmin ago", minutesInt % 60]];
        }
        else
        {
            [timeSincePastDate appendString:[NSString stringWithFormat:@"0%dmin ago", minutesInt % 60]];
        }
        if(minutesInt < 60)
        {
            timeSincePastDate = (NSMutableString*)[NSString stringWithFormat:@"%dmin ago", minutesInt];
        }
    }
    return timeSincePastDate;
}

+(NSString*)substring:(NSString*)sampleString from:(NSString*)start to:(NSString*)end
{
    NSRange rangeStart = [sampleString rangeOfString:start];
    NSRange rangeEnd = [sampleString rangeOfString:end];
    
    NSString *subString = nil;
    if((rangeStart.length == 1) && (rangeEnd.length == 1) && (rangeEnd.location > rangeStart.location))
    {
        NSRange subRange;
        subRange.location = rangeStart.location + 1;
        subRange.length = (rangeEnd.location - rangeStart.location) - 1;
        subString = [sampleString substringWithRange:subRange];
    }
    return subString;
}

+(NSString*)substring:(NSString*)sampleString from:(NSString*)start length:(NSUInteger)length
{
    NSRange rangeStart = [sampleString rangeOfString:start];
    NSString *subString = nil;
    
    if(rangeStart.length == 1)
    {
        NSRange subRange;
        subRange.location = rangeStart.location + 1;
        if(length > 0)
        {
            subRange.length = length;
            subString = [sampleString substringWithRange:subRange];
        }
    }
    return subString;
}

+(NSString*)substring:(NSString*)sampleString upTo:(NSString*)end
{
    NSRange rangeEnd = [sampleString rangeOfString:end];
    NSString *subString = nil;
    
    if(rangeEnd.length == 1)
    {
        NSRange subRange;
        subRange.location = 0;
        subRange.length = rangeEnd.location;
        subString = [sampleString substringWithRange:subRange];
    }
    return subString;
}

@end
