//
//  RouteOptionCard.h
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/06.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XUIView.h"
#import "XUIButton.h"
#import "MultimodalTrip.h"
#import "DateTimeUtil.h"
#import "DistanceUtil.h"
#import "ImageUtil.h"
#import "NSURLConnection+Blocks.h"
#import "JCDHTTPConnection.h"
//#import "CustomMapLine.h"
#import "MulticolorMapLine.h"
#import "CompetitionUtil.h"
#import <MapKit/MapKit.h>

@interface RouteOptionCard : XUIView<UIScrollViewDelegate>
{
    UIScrollView *scrollView;
    UIPageControl *pageControl;
    float pageWidth;
    float pageHeight;
    int pageCount;
    int activePage;
    MultimodalTrip *multimodalTrip;
    MKPolyline *customPolyline;
    NSMutableArray *polylines;
    
    NSString *tripId;
    UIImageView *horizontalLine;
    UILabel *departureTime;
    
    UILabel *routeCost;
    UILabel *routeCostLabel;
    
    UILabel *arrivalTime;
    UILabel *arrivalTimeLabel;
    
    UILabel *walking;
    UILabel *walkingLabel;
    
    UILabel *startTime;
    UILabel *endTime;
    UILabel *walkingDistance;
    UIImageView *barBackground;
    
    NSMutableArray *modes;
    NSMutableArray *modeIcons;
    NSMutableArray *mapViewModeIcons;

    // MAP CARD
    UIImageView *gradientTop;
    UIImageView *gradientBottom;
    UILabel *mapLoadingMessage;
    UILabel *ticketInfo;
    int index;
}

@property XUIButton *overviewButton;
@property XUIButton *selectButton;
@property XUIButton *logButton;
@property UIButton *mapImageButton;

-(void)initWithIndex:(int)i inController:(UIViewController*)parentViewController;
-(void)initViewElements:(MultimodalTrip*)trip;
-(void)getMapForRoute;
-(void)setCarbonPoints;
@end
