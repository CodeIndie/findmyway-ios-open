//
//  CompetitionUtil.m
//  Findmyway
//
//  Created by Bilo Lwabona on 2015/03/12.
//

#import "CompetitionUtil.h"
#import "TripLogItem.h"

@implementation CompetitionUtil

static CompetitionUtil *instance = nil;

-(id)init
{
    self = [super init];
    if(self)
    {
        instance = self;
    }
    return self;
}

+(CompetitionUtil*)instance
{
    if(instance == nil)
    {
        instance = [[CompetitionUtil alloc] init];
    }
    return instance;
}

+(BOOL)isEarthHourTime
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    BOOL duringEarthHour = [DateTimeUtil secondsUntilDate:appDelegate.earthHourStartDate] < 0 && [DateTimeUtil secondsUntilDate:appDelegate.earthHourEndDate] > 0;
    return duringEarthHour;
}

+(BOOL)isBeforeEarthHour
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    BOOL beforeEarthHour = [DateTimeUtil secondsUntilDate:appDelegate.earthHourStartDate] > 0;
    return beforeEarthHour;
}

+(BOOL)isAfterEarthHour
{
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    BOOL afterEarthHour = [DateTimeUtil secondsUntilDate:appDelegate.earthHourEndDate] < 0;
    return afterEarthHour;
}


+(BOOL)isCompetitionOperator:(NSString *)operatorName
{
    if(![CompetitionUtil isEarthHourTime])
    {
        return false;
    }
    
//    if([operatorName caseInsensitiveCompare:@"MyCiTi"] == NSOrderedSame)
//    {
//        return YES;
//    }
    
    if([operatorName caseInsensitiveCompare:@"Tshwane Bus Services"] == NSOrderedSame)
    {
        return YES;
    }
    
    if([operatorName caseInsensitiveCompare:@"Gautrain"] == NSOrderedSame)
    {
        return YES;
    }
    
    if([operatorName caseInsensitiveCompare:@"Gaubus"] == NSOrderedSame)
    {
        return YES;
    }
    
    if([operatorName caseInsensitiveCompare:@"Metrorail Gauteng"] == NSOrderedSame)
    {
        return YES;
    }
    
    if([operatorName caseInsensitiveCompare:@"A Re Yeng"] == NSOrderedSame)
    {
        return YES;
    }
    
    return false;
}

+(float)operatorWeighting:(NSString *)operatorName
{
//    if([operatorName caseInsensitiveCompare:@"MyCiTi"] == NSOrderedSame)
//    {
//        return 0.036;
//    }
    
    if([operatorName caseInsensitiveCompare:@"Tshwane Bus Services"] == NSOrderedSame)
    {
        return 0.036;
    }
    
    if([operatorName caseInsensitiveCompare:@"Gautrain"] == NSOrderedSame)
    {
        return 0.048;
    }
    
    if([operatorName caseInsensitiveCompare:@"Gaubus"] == NSOrderedSame)
    {
        return 0.036;
    }
    
    if([operatorName caseInsensitiveCompare:@"Metrorail Gauteng"] == NSOrderedSame)
    {
        return 0.048;
    }
    
    if([operatorName caseInsensitiveCompare:@"A Re Yeng"] == NSOrderedSame)
    {
        return 0.036;
    }
    return 0;
}

+(BOOL)logJourney:(NSString*)startLocation end:(NSString*)endLocation dateTime:(NSString*)dateTime points:(int)points
{
    TripLogItem *newTrip = [[TripLogItem alloc] initWithStart:startLocation end:endLocation points:points dateTime:dateTime];
    NSMutableArray *loggedTrips = (NSMutableArray*)[[DefaultsUtil instance] GetArrayForKey:@"LoggedTrips"];
    
    if(loggedTrips.count > 0)
    {
        for(TripLogItem *existingTrip in loggedTrips)
        {
            if([newTrip isEqualTo:existingTrip])
            {
                NSLog(@"Trip already exists");
                return NO;
            }
        }
        [loggedTrips addObject:newTrip];
        NSLog(@"Trip Added");
        [[DefaultsUtil instance] SetArray:loggedTrips ForKey:@"LoggedTrips"];
    }
    else
    {
        loggedTrips = [[NSMutableArray alloc] init];
        NSLog(@"Trip Log Created");
        [loggedTrips addObject:newTrip];
        NSLog(@"Trip Added");
        [[DefaultsUtil instance] SetArray:loggedTrips ForKey:@"LoggedTrips"];
    }
    return YES;
}

+(void)setCompetitionMember1:(NSString*)member1 member2:(NSString*)member2
{
    [[DefaultsUtil instance] SetString:member1 ForKey:@"TeamMember1"];
    [[DefaultsUtil instance] SetString:member2 ForKey:@"TeamMember2"];
    [[DefaultsUtil instance] SetString:@"YES"  ForKey:@"isCompeting"];
}

+(BOOL)isCompeting
{
    NSString *isCompetingString = [[DefaultsUtil instance] GetStringForKey:@"isCompeting"];
    return ([isCompetingString isEqualToString:@"YES"]);
}

+(BOOL)hasExitedCompetition
{
    NSString *exitCompetition = [[DefaultsUtil instance] GetStringForKey:@"exitEarthHourCompetition"];
    if([exitCompetition isEqualToString:@"YES"])
    {
        return YES;
    }
    return NO;
}

+(void)setExitedCompetition:(BOOL)exitCompetition
{
    if(exitCompetition)
    {
        [[DefaultsUtil instance] SetString:@"YES" ForKey:@"exitEarthHourCompetition"];
    }
    else
    {
        [[DefaultsUtil instance] SetString:@"YES" ForKey:@"exitEarthHourCompetition"];
    }
}

@end
