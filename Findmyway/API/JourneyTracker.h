//
//  JourneyTracker.h
//  Findmyway
//
//  Created by Bilo on 2/25/15.
//  Copyright (c) 2015 WhereIsMyTrans/Users/bilolwabona/Downloads/JourneyTracker/CrowdAvlReportPayload.javaport. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JourneyListener.h"
#import "JourneyHub.h"

@interface JourneyTracker : NSObject<JourneyListener,JourneyHub>

@end
