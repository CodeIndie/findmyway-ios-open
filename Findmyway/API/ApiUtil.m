//
//  ApiUtil.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2015/01/07.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "ApiUtil.h"

@implementation ApiUtil

static ApiUtil *instance = nil;

-(id)init
{
    self = [super init];
    if(self)
    {
        instance = self;
    }
    return self;
}

+(ApiUtil*)instance
{
    if(instance == nil)
    {
        instance = [[ApiUtil alloc] init:[NSDictionary dictionaryWithContentsOfFile: [[NSBundle mainBundle] pathForResource:@"FmwAppConfig" ofType:@"plist"]]];// [[ApiUtil alloc] init];
    }
    return instance;
}

-(id)init:(NSDictionary*)configDictionary
{
    self = [super init];
    if(self)
    {
        instance = self;
        NSDictionary *dictionary = [configDictionary objectForKey:@"APIs"];
        
        self.apiUrl = [[URLString alloc] initWithString:[dictionary objectForKey:@"API"]];
        self.apiUberUrl = [[URLString alloc] initWithString:[dictionary objectForKey:@"UBER"]];
        self.apiFmwUrl = [[URLString alloc] initWithString:[dictionary objectForKey:@"FMW"]];
        
        self.appKey = [[URLString alloc] initWithString:[dictionary objectForKey:@"AppKey"]];
        if([[DefaultsUtil instance] GetObjectForKey:@"token"] != nil )//&& [[[DefaultsUtil instance] GetObjectForKey:@"token"] stringValue].length > 1)
        {
            self.fmwToken = [[DefaultsUtil instance] GetStringForKey:@"token"];
        }
        else
        {
            self.fmwToken = nil;
        }
        // self.fmwToken = [dictionary objectForKey:@"TestToken"];
        //[[URLString alloc] initWithString:[dictionary objectForKey:@"TestToken"]];
        
        appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;

    }
    return self;
}

-(NSMutableURLRequest*)urlForRequest:(ApiRequest)requestType callType:(RestCallType)callType parameters:(NSDictionary *)parameters
{
    // CREATE REQUEST WITH OPTIONAL URL PARAMETERS
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[self apiRequest:requestType parameters:parameters]]];
    [request setHTTPMethod:[self requestMethod:callType]];
    
    // ADD LOCATION (if available)
    if(appDelegate.locationUtil.isLocationEnabled)
    {
        [parameters setValue:appDelegate.locationUtil.userLocation.latitudeString forKey:@"latitude"];
        [parameters setValue:appDelegate.locationUtil.userLocation.longitudeString forKey:@"longitude"];
    }

    // ADD TOKEN (if available)
    if(self.fmwToken != nil && self.fmwToken != (id)[NSNull null])
    {
        [request setValue:self.fmwToken forHTTPHeaderField:@"token"];
    }

    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:self.appKey.encodedString forHTTPHeaderField:@"AppKey"];
    [request setValue:appDelegate.device.userAgent forHTTPHeaderField:@"User-Agent"];
    
    NSLog(@"REQUEST: (%@) %@", request.HTTPMethod, request.URL.absoluteString);
    NSLog(@"U-AGENT: %@",[request valueForHTTPHeaderField:@"User-Agent"]);
    
    return request;
}

-(NSString*)requestMethod:(RestCallType)callType
{
    NSString *requestType;
    switch(callType)
    {
        case GET:
        {
            requestType = @"GET";
        }break;
            
        case POST:
        {
            requestType = @"POST";
        }break;
            
        case PUT:
        {
            requestType = @"PUT";
        }break;
            
        case PATCH:
        {
            requestType = @"PATCH";
        }break;
            
        case UPDATE:
        {
            requestType = @"UPDATE";
        }break;
            
        case DELETE:
        {
            requestType = @"DELETE";
        }break;
    }
    return requestType;
}

-(NSMutableString*)apiRequest:(ApiRequest)requestType parameters:(NSDictionary*)parameters
{
    NSMutableString* urlString = [[NSMutableString alloc] init];
    switch(requestType)
    {
        // API CALLS
        // ---------
            
        case About:
        {
//            [urlString appendString:self.apiUrl.encodedString];
//            [urlString appendString:@"about?"];
        }break;
            
        case Announcements:
        {
            [urlString appendString:self.apiUrl.encodedString];
            [urlString appendString:@"announcements?"];
        }break;
            
        case Operators:
        {
            [urlString appendString:self.apiUrl.encodedString];
            [urlString appendString:@"operators?"];
        }break;
            
        case SearchItems:
        {
            [urlString appendString:self.apiUrl.encodedString];
            [urlString appendString:@"searchitems?"];
        }break;
            
        case Trips:
        {
            [urlString appendString:self.apiUrl.encodedString];
            [urlString appendString:@"trips?"];
        }break;
            
        case ReverseGeocode:
        {
            [urlString appendString:self.apiUrl.encodedString];
            [urlString appendString:@"locations?"];
        }break;
        
        case Paths:
        {
            [urlString appendString:self.apiUrl.encodedString];
            [urlString appendString:@"paths?"];
        }break;
            
        case Directions:
        {
            [urlString appendString:self.apiUrl.encodedString];
            [urlString appendString:@"routes?"];
        }break;
            
        // FMW API CALLS
        // -------------
            
        case Users:
        {
            // GET, POST, PATCH
            [urlString appendString:self.apiFmwUrl.encodedString];
            [urlString appendString:@"users"];
        }break;
            
        case AboutFmw:
        {
            // GET
            [urlString appendString:self.apiFmwUrl.encodedString];
            [urlString appendString:@"about?"];
        }break;
            
        case Messages:
        {
            // POST
            [urlString appendString:self.apiFmwUrl.encodedString];
            [urlString appendString:@"users/messages?"];
        }break;
            
        case AuthPins:
        {
            // POST
            [urlString appendString:self.apiFmwUrl.encodedString];
            [urlString appendString:@"users/"];
            [urlString appendString:@"token"];
            [urlString appendString:@"authpins"];
        }break;
    }
    
    [urlString appendString:[URLString urlParameterString:parameters]];
    return urlString;
}

-(NSMutableString*)uberRequest:(UberRequest)requestType parameters:(NSDictionary *)parameters
{
    NSMutableString* requestString;
    return requestString;
}
@end
