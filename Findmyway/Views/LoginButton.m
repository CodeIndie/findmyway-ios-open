//
//  LoginButton.m
//  Findmyway
//
//  Created by Bilo on 2/9/15.
//

#import "LoginButton.h"

@implementation LoginButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(id)initWithFrame:(CGRect)frame name:(NSString *)name image:(NSString *)image colour:(UIColor *)color layout:(LayoutConfig*)layout
{
    self = [super initWithFrame:frame];
    if(self)
    {
        float buttonHeight = layout.buttonSize;
        
        // LOGO
        self.logo = [[UIImageView alloc] initWithFrame:CGRectMake(layout.margin, 0, buttonHeight, buttonHeight)];
        [self.logo setImage:[UIImage imageNamed:image]];//[ImageUtil getImage:image]];
        //[self.logo setTintColor:[UIColor whiteColor]];
        
        // LINE
        self.bottomLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, buttonHeight - 2 * layout.delimiterSize, layout.screenWidth, layout.delimiterSize * 2)];
        [self.bottomLine setBackgroundColor:color];
        
        // TITLE
        self.buttonName = [[UILabel alloc] initWithFrame:CGRectMake(layout.screenWidth - 3 * layout.buttonSize - layout.padding, buttonHeight - layout.buttonSize * 0.75, 3 * layout.buttonSize, layout.buttonSize * 0.75)];
        [self.buttonName setText:name];
        [self.buttonName setTextColor:color];
        [self.buttonName setTextAlignment:NSTextAlignmentRight];
        
        // BUTTON (Functionality)
        self.button = [[UIButton alloc] initWithFrame:frame];
        
        
        [self addSubview:self.logo];
        [self addSubview:self.bottomLine];
        [self addSubview:self.buttonName];
        [self addSubview:self.button];
    }
    return self;
}
@end
