//
//  MapTypeView.m
//  Findmyway
//
//  Created by Bilo on 4/15/15.
//

#import "MapTypeView.h"

@implementation MapTypeView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(layout.screenWidth/2, layout.screenHeight * 1.5);
        centerOnScreen = CGPointMake(layout.screenWidth/2, layout.screenHeight * 0.5);
    }
    return self;
}
-(void)initView:(NSString *)name inController:(UIViewController *)parentViewController
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
}

-(void)initViewElements
{
    [self setBackgroundColor:colour.clear];
    self.backgroundButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.screenHeight)];
    [self.backgroundButton setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.5]];
    
    float panelHeight = layout.buttonSize * 3 + layout.padding * 3;
    self.backgroundPanel = [[UIView alloc] initWithFrame:CGRectMake(0, layout.screenHeight, layout.screenWidth, panelHeight)];
    [self.backgroundPanel setBackgroundColor:colour.lightGray];
    
    self.standardMapButton = [XUIButton initWithFrame:CGRectMake(0, 0, layout.screenWidth, layout.buttonSize)
                                                title:NSLocalizedString(@"Standard Map", nil)
                                           titleColor:colour.fmwActiveColor
                                      backgroundColor:colour.fmwBackgroundColor
                                  horizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    
    self.satelliteMapButton = [XUIButton initWithFrame:CGRectMake(0, layout.buttonSize + layout.padding, layout.screenWidth, layout.buttonSize)
                               title:NSLocalizedString(@"Satellite Map", nil)
                                            titleColor:colour.fmwActiveColor
                                       backgroundColor:colour.fmwBackgroundColor
                                   horizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    
    self.hybridMapButton = [XUIButton initWithFrame:CGRectMake(0, 2 * (layout.buttonSize + layout.padding), layout.screenWidth, layout.buttonSize)
                                              title:NSLocalizedString(@"Hybrid Map", nil)
                                         titleColor:colour.fmwActiveColor
                                    backgroundColor:colour.fmwBackgroundColor
                                horizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    
    
    
}

-(void)addUIToView
{
    [self addSubview:self.backgroundButton];
    [self addSubview:self.backgroundPanel];
    
    [self.backgroundPanel addSubview:self.standardMapButton];
    [self.backgroundPanel addSubview:self.satelliteMapButton];
    [self.backgroundPanel addSubview:self.hybridMapButton]; 
}

-(void)animateIn
{
    [super animateIn];
    [parentView.view addSubview:self];
    
    [UIView animateWithDuration:animationInterval delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.backgroundButton.alpha = 1;
        self.backgroundPanel.center = CGPointMake(layout.screenWidth/2, layout.screenHeight - self.backgroundPanel.bounds.size.height/2);
    }completion:nil];
}

-(void)animateOut
{
    [super animateOut];
    
    [UIView animateWithDuration:animationInterval delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.backgroundButton.alpha = 0;
        self.backgroundPanel.center = CGPointMake(layout.screenWidth/2, layout.screenHeight + self.backgroundPanel.bounds.size.height/2);
    }completion:^(BOOL finished){
        [self removeFromSuperview];
    }];
}
@end
