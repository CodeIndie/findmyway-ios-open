//
//  JSONParser.h
//  FmwV3
//
//  Created by Bilo Lwabona on 1/14/14.
//

#import <Foundation/Foundation.h>
#import "DictionaryUtil.h"

@interface JSONParser : NSObject

+(NSDictionary *)convertResponseToDictionary:(NSData *)responseData withError:(NSError **)error;
+(NSArray*)convertResponseToArray:(NSData*)responseData withError:(NSError **)error;
+(BOOL)responseIsArray:(NSData*)data;
+(NSString*)convertDictionaryToJSON:(NSDictionary*)dictionary;
+(NSDictionary*)convertJSONToDictionary:(NSString*)jsonString;
@end
