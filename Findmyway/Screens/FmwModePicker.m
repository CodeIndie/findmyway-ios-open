//
//  FmwModePicker.m
//  FindMyWay
//
//  Created by Bilo Lwabona on 2014/12/31.
//  Copyright (c) 2014 Bilo Lwabona. All rights reserved.
//

#import "FmwModePicker.h"

@implementation FmwModePicker

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        centerOffScreen = CGPointMake(-device.screenWidth/2, frame.origin.y + frame.size.height/2);
        centerOnScreen = CGPointMake(device.screenWidth/2, frame.origin.y + frame.size.height/2);
    }
    return self;
}

-(void)initView:(NSString*)name inController:(UIViewController*)parentViewController;
{
    [super initView:name inController:parentViewController];
    [self initViewElements];
    [self addUIToView];
    [self setBackgroundColor:colour.fmwHeader];
}

-(void)initViewElements
{
    float offset = layout.padding * 2 + layout.buttonSize;
    float buttonWidth = (layout.screenWidth - layout.padding * 4 - layout.buttonSize * 2) / 3;
    float p = layout.padding*0.75;
    UIEdgeInsets edgeInsets = UIEdgeInsetsMake(p,p,p,p);
    
    horizontalLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, layout.buttonSize - layout.delimiterSize, layout.screenWidth, layout.delimiterSize)];
    [horizontalLine setBackgroundColor:colour.fmwTextColorLight];

    self.menuButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.padding, 0, layout.buttonSize, layout.buttonSize)];
    [self.menuButton setImage:[UIImage imageNamed:@"FMWSymbol.png"] forState:UIControlStateNormal];
    [self.menuButton.imageView setTintColor:colour.white];
    [self.menuButton setImageEdgeInsets:edgeInsets];
    [self.menuButton addTarget:self action:@selector(toggleMainMenu) forControlEvents:UIControlEventTouchUpInside];
    
    self.filter = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth - offset, 0, layout.buttonSize, layout.buttonSize)];
    [self.filter setImage:[ImageUtil getImage:@"iconBullets.png"] forState:UIControlStateNormal];
    [self.filter.imageView setTintColor:colour.fmwTextColorLight];
    [self.filter setImageEdgeInsets:edgeInsets];
    [self.filter addTarget:self action:@selector(toggleOperatorsMenu) forControlEvents:UIControlEventTouchUpInside];

    //self.pedestrian = [[XUIToggleButton alloc] initWithName:@"Pedestrian" activeImage:@"iconModeWalking.png" inactiveImage:@"iconModeWalking.png" activeColor:colour.fmwActiveColor inactiveColor:colour.fmwTextColorLight xPosition:offset yPosition:0 size:layout.buttonSize target:self];
    self.bus = [[XUIToggleButton alloc] initWithName:@"Bus" activeImage:@"iconModeBusWhite.png" inactiveImage:@"iconModeBusOff.png" activeColor:colour.fmwActiveColor inactiveColor:colour.fmwTextColorLight xPosition:offset + buttonWidth * 0.5 - layout.buttonSize / 2  yPosition:0 size:layout.buttonSize target:self];
    [self.bus.button addTarget:self action:@selector(checkModeValidity) forControlEvents:UIControlEventTouchUpInside];
    
    self.rail = [[XUIToggleButton alloc] initWithName:@"Rail" activeImage:@"iconModeRailWhite.png" inactiveImage:@"iconModeRailOff.png" activeColor:colour.fmwActiveColor inactiveColor:colour.fmwTextColorLight xPosition:offset+buttonWidth * 1.5 - layout.buttonSize / 2 yPosition:0 size:layout.buttonSize target:self];
    [self.rail.button addTarget:self action:@selector(checkModeValidity) forControlEvents:UIControlEventTouchUpInside];
    //self.taxi = [[XUIToggleButton alloc] initWithName:@"Taxi" activeImage:@"iconModeTaxiWhite.png" inactiveImage:@"iconModeTaxiOff.png" activeColor:colour.fmwActiveColor inactiveColor:colour.fmwTextColorLight xPosition:offset+2*buttonWidth yPosition:0 size:layout.buttonSize target:self];
    
    self.boat = [[XUIToggleButton alloc] initWithName:@"Boat" activeImage:@"iconModeBoatWhite.png" inactiveImage:@"iconModeBoatOff.png" activeColor:colour.fmwActiveColor inactiveColor:colour.fmwTextColorLight xPosition:offset+2.5*buttonWidth - layout.buttonSize / 2 yPosition:0 size:layout.buttonSize target:self];
    [self.boat.button addTarget:self action:@selector(checkModeValidity) forControlEvents:UIControlEventTouchUpInside];
}

-(void)addUIToView
{
    [self addSubview:self.pedestrian.button];
    [self addSubview:self.bus.button];
    [self addSubview:self.rail.button];
    [self addSubview:self.taxi.button];
    [self addSubview:self.boat.button];
 
    [self addSubview:self.menuButton];
    [self addSubview:self.filter];
    [self addSubview:horizontalLine];
}

-(void)checkModeValidity
{
    if(!self.bus.isActive && !self.rail.isActive && !self.boat.isActive)
    {
        [self showNotification:@"Select at least 1 mode" inView:parentView.view];
    }
}

-(void)toggleMainMenu
{
    showingMainMenu = !showingMainMenu;
    AppViewController *viewController = (AppViewController*)parentView;
    
    if(showingMainMenu)
    {
        if(viewController.operatorList.isActive)
        {
            [viewController.operatorList animateOut];
            [self.filter setImage:[ImageUtil getImage:@"iconBullets.png"] forState:UIControlStateNormal];
            [self.filter.imageView setTintColor:colour.fmwTextColor];
        }
        [viewController showView:viewController.mainMenu];
    }
    else
    {
        [self XLog:@"Hiding Main Menu"];
        [viewController hideView:viewController.mainMenu];
    }
}

-(void)toggleOperatorsMenu
{
    showingOperatorsMenu = !showingOperatorsMenu;
    AppViewController *viewController = (AppViewController*)parentView;
    
    if(showingOperatorsMenu)
    {
        if(viewController.mainMenu.isActive)
        {
            [viewController.mainMenu animateOut];
        }
        
        if(viewController.operatorList.operatorListItems.count == 0)
        {
            [viewController.operatorList addOperators];
        }
        [viewController showView:viewController.operatorList];
        [self.filter setImage:[ImageUtil getImage:@"iconBulletsFull.png"] forState:UIControlStateNormal];
        [self.filter.imageView setTintColor:colour.fmwActiveColor];
    }
    else
    {
        [self XLog:@"Hiding Operators"];
        [viewController hideView:viewController.operatorList];
        [self.filter setImage:[ImageUtil getImage:@"iconBullets.png"] forState:UIControlStateNormal];
        [self.filter.imageView setTintColor:colour.fmwTextColor];
    }
}

-(void)setShowOperatorsMenuTo:(BOOL)val
{
    showingOperatorsMenu = val;
}

-(void)setShowMainMenuTo:(BOOL)val
{
    showingMainMenu = val;
}

-(NSMutableArray*)excludedModes
{
    NSMutableArray *modes = [[NSMutableArray alloc] init];
 
    if(self.bus)
    {
        if(!self.bus.isActive)
        {
            [modes addObject:self.bus.name];
        }
    }
    
    if(self.rail)
    {
        if(!self.rail.isActive)
        {
            [modes addObject:self.rail.name];
        }
    }
    
    if(self.taxi)
    {
        if(!self.taxi.isActive)
        {
            [modes addObject:self.taxi.name];
        }
    }
    
    if(self.boat)
    {
        if(!self.boat.isActive)
        {
            [modes addObject:self.boat.name];
        }
    }
    
    return modes;
}

-(NSMutableArray*)excludedOperators
{
    return [((AppViewController*)parentView).operatorList excludedOperators];
}
@end