//
//  AnnouncementListItem.m
//  Findmyway
//
//  Created by Bilo on 4/13/15.
//

#import "AnnouncementListItem.h"

@implementation AnnouncementListItem

-(id)initWithFrame:(CGRect)frame announcement:(Announcement*)announcementArg
{
    self = [super initWithFrame:frame];
    if(self)
    {
        appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        layout = appDelegate.layout;
        colour = appDelegate.colour;
        
        announcement = announcementArg;
        
        [self setBackgroundColor:colour.fmwBackgroundColor];
        
        float iconSize = layout.buttonSize - layout.padding * 1.5;
        float padding = layout.padding * 0.75;
        
        icon = [[UIImageView alloc] initWithFrame:CGRectMake(padding, padding, iconSize, iconSize)];
        [icon setImage:[appDelegate getImageForMode:[announcement.modes objectAtIndex:0]]];
        [icon setTintColor:colour.fmwTextColorLight];
        [self addSubview:icon];
        
        text = [[UILabel alloc] initWithFrame:CGRectMake(iconSize + layout.padding * 2, 0, layout.screenWidth - layout.buttonSize - layout.padding - iconSize, layout.buttonSize)];
        [text setText:announcement.descriptionText];
        [text setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
        [text setTextColor:colour.fmwTextColorLight];
        [text setNumberOfLines:0];
        [text sizeToFit];
        [self addSubview:text];
        self.frame = CGRectMake(0, self.frame.origin.y, self.frame.size.width, [self height]);
        
        self.shareButton = [[UIButton alloc] initWithFrame:CGRectMake(layout.screenWidth - layout.buttonSize, 0, layout.buttonSize, layout.buttonSize)];
        [self.shareButton setImage:[ImageUtil getImage:@"iconShare.png"] forState:UIControlStateNormal];
        [self.shareButton.imageView setTintColor:colour.fmwActiveColor];
        [self.shareButton setImageEdgeInsets:UIEdgeInsetsMake(padding, padding, padding, padding)];
        [self addSubview:self.shareButton];
    }
    return self;
}

-(id)initWithAnnouncement:(Announcement *)announcementArg frame:(CGRect)frame
{
    return [self initWithFrame:frame announcement:announcementArg];
}

-(float)height
{
    return text.frame.size.height;
}

@end
