//
//  OperatorListItem.h
//  Findmyway
//
//  Created by Bilo on 2/18/15.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface OperatorListItem : UIView
{
    AppDelegate *appDelegate;
}
@property UISwitch *uiSwitch;
@property UILabel *label;
@property NSString *name;
@property NSString *apiName;

-(id)initWithFrame:(CGRect)frame operator:(Operator*)operatorItem appDelegate:(AppDelegate*)appDel;
-(void)toggleOperator;
@end
