//
//  TripQuery.h
//  Findmyway
//
//  Created by Bilo on 12/9/14.
//

#import <Foundation/Foundation.h>
#import "Location.h"
#import "MultimodalTrip.h"

@class MultimodalTrip;

@interface TripQuery : NSObject

@property Location *startLocation;
@property Location *endLocation;

@property NSMutableArray *excludedModes;
@property NSMutableArray *excludedOperators;

@property BOOL isEditingStart;
@property BOOL usingDatePicker;
@property BOOL isDeparting;
@property BOOL refreshingRouteOptions;

@property NSString *placesSearch;
@property NSString *dateTime;
@property NSString *interval;

@property NSUInteger selectedTripIndex;
@property MultimodalTrip *selectedTrip;

-(void)swapEndPoints;
-(void)clearLocation;
-(void)clearDestination;
-(void)clearLocationAndDestination;
-(void)setPoint:(Location*)location;

@end
