//
//  TripQuery.m
//  Findmyway
//
//  Created by Bilo on 12/9/14.
//

#import "TripQuery.h"

@implementation TripQuery

-(id)init
{
    self = [super init];
    if(self)
    {
        self.placesSearch = @"";
        self.usingDatePicker = YES;
    }
    return self;
}

-(void)swapEndPoints
{
    Location *tempLocation = [[Location alloc] initWithLocation:self.startLocation];
    self.startLocation = [[Location alloc] initWithLocation:self.endLocation];
    self.endLocation = [[Location alloc] initWithLocation:tempLocation];
}

-(void)clearLocation
{
    
}

-(void)clearDestination
{
    
}

-(void)clearLocationAndDestination
{
    
}

-(void)setPoint:(Location*)location
{
    if(self.isEditingStart)
    {
        self.startLocation = [[Location alloc] initWithLocation:location];
    }
    else
    {
        self.endLocation = [[Location alloc] initWithLocation:location];
    }
}
@end
