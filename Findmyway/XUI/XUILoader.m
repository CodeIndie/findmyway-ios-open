//
//  XUILoader.m
//  Findmyway
//
//  Created by Bilo on 5/7/15.
//  Copyright (c) 2015 Bilo Lwabona. All rights reserved.
//

#import "XUILoader.h"

@implementation XUILoader

-(id)initWithFrame:(CGRect)frame layout:(LayoutConfig*)layout colour:(ColourConfig*)colour inView:(UIView*)view
{
    self = [super initWithFrame:frame];
    if(self)
    {
        loaderImage = [[UIImageView alloc] initWithFrame:frame];
        [loaderImage setImage:[ImageUtil getImage:@"loader2.png"]];
        [loaderImage setTintColor:colour.fmwActiveColor];
        [self addSubview:loaderImage];
        [view addSubview:self];
        animationSpeed = 10;
    }
    return self;
}

-(void)startAnimation
{
    if(animationTimer != (id)[NSNull null])
    {
        [animationTimer invalidate];
        animationTimer = nil;
    }
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(updateAnimation) userInfo:nil repeats:YES];
}

-(void)stopAnimation
{
    [self removeFromSuperview];
}

-(void)updateAnimation
{
    animationCounter += animationSpeed;
    if(animationCounter >= 360)
    {
        animationCounter = 0;
    }
    
    CALayer *layer = loaderImage.layer;
    
    CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
    rotationAndPerspectiveTransform = CATransform3DTranslate(rotationAndPerspectiveTransform, 0, 0, 20);
    rotationAndPerspectiveTransform.m34 = 5.0 / -500;
    rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, animationCounter * M_PI / 180.0f, 0.0f, 0.0f, 1.0f);
    layer.transform = rotationAndPerspectiveTransform;
}
@end
